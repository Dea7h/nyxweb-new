$(function () {
    loader(location.pathname);
    $(window).on('popstate', function () {
        loader(location.pathname, false);
    });

    $(document).on('click', '[ajax]', function () {
        loader($(this).attr('ajax'));
    });

    $(document).on('click', '.news-content img', function () {
        $('body').append("<div class='image-preview-container' onclick='$(this).remove();'><img src='" + $(this).attr('src') + "' /></div>");
    });

    $(document).on('contextmenu', '.storage-items', function (event) {
        event.preventDefault();
    });

    $(document).on('contextmenu', '.draggable-item', function (event) {
        event.preventDefault();
        $(this).click();
        //$('#storageItemMenu').remove();
        //$(this).append("<div id='storageItemMenu'>menu</div>");
    });

    // $(document).on('click', '.main-middle', function() {
    //     $('#storageItemMenu').remove();
    // });

    $(document).on('submit', 'form', function (event) {
        event.preventDefault();
        $('.ajaxLoader').show();

        $.ajax({
            url: '/index.php',
            type: 'POST',
            data: 'request=' + $(this).attr('action') + '&' + $(this).serialize()
        }).done(function (data) {
            ajaxRequest(data);
        });
    });

    $(document).ajaxComplete(function () {
        $('time.timeago').each(function () {
            $(this).text(moment(new Date($(this).attr('datetime'))).fromNow());
        });

        $("[title][title!='']").tooltipster({
            theme: 'tooltipster-borderless',
            contentAsHTML: true,
            position: 'bottom',
            animationDuration: 0,
            delay: 0
        });

        App.init();
    });

    $('[quickLoad]').each(function () {
        var target = $(this);
        quickLoad(target.attr('quickLoad'));
        if (typeof target.attr('autoRefresh') !== typeof undefined) {
            setInterval(function () {
                quickLoad(target.attr('quickLoad'));
            }, target.attr('autoRefresh') + '000');
        }
    });

    subMenu('#submenu-' + location.pathname.split('/')[1]);
    $('.menu [ajax]').mouseenter(function () {
        subMenu('#submenu-' + $(this).attr('ajax').replace('/', ''));
    });

    //Draggable warehouse
    class App {
        static init() {
            const dragables = $('.draggable-item');
            for (const dragable of dragables) {
                dragable.addEventListener("dragstart", App.dragstart);
            }

            const droppables = $('.droppable-item');
            for (const droppable of droppables) {
                droppable.addEventListener('drop', App.drop);
                droppable.addEventListener('dragover', App.dragover);
                droppable.addEventListener('dragenter', App.dragenter);
            }
        }

        static dragstart(event) {
            $('.tooltipstered').tooltipster('hide');

            App.draggableItemID = $(this);
        }

        static dragenter(event) {
            $('.droppable-item').css('background-color', 'transparent');

            var allSlots = $('#warehouseSlots').val();

            var dragToSlot = parseInt($(this).attr('id'));

            if (allSlots.substr(dragToSlot - 1, 1) == 0) {
                var rowEnds = (Math.ceil(dragToSlot / 8) * 8);

                var item = App.draggableItemID.attr('data-item').split('.');
                var itemX = parseInt(item[0]);
                var itemY = parseInt(item[1]);
                var slotFree = false;
                var slotsToColor = [dragToSlot];

                //checking if X-horizontally is empty
                if (dragToSlot + parseInt(itemX - 1) <= rowEnds && allSlots.substr(dragToSlot - 1, itemX) == '0'.repeat(itemX)) {
                    slotFree = true;
                }

                var visibleSlots = ((dragToSlot - 1) + itemX) - rowEnds > 0 ? itemX - (((dragToSlot - 1) + itemX) - rowEnds) : itemX;

                if (itemX > 1) {
                    for (let i = 1; i < itemX; i++) {
                        if(i < visibleSlots) {
                            slotsToColor.push(dragToSlot + i);
                        }
                    }
                }

                if (itemY > 1) {
                    for (let y = 1; y < itemY; y++) {
                        for (let i = 0; i < itemX; i++) {
                            if(i < visibleSlots) {
                                slotsToColor.push(dragToSlot + (8 * y) + i);
                            }
                        }
                    }
                }

                //checking if Y-vertically is empty
                if (itemY > 1 && slotFree === true) {
                    for (let i = 1; i < itemY; i++) {
                        if (allSlots.substr((dragToSlot + (8 * i)) - 1, itemX) == '0'.repeat(itemX)) {
                            slotFree = true;
                        } else {
                            slotFree = false;
                        }
                    }
                }

                slotsToColor.forEach(function (id) {
                    let color = slotFree ? 'rgba(123, 239, 178, 0.4)' : 'rgba(255,0,0,0.4)';
                    $('#' + id).css('background-color', color);
                });
            }
        }

        static dragover(event) {
            event.preventDefault();
        }

        static drop(event) {
            $('.droppable-item').css('background-color', 'transparent');
            var dropOn = $(this).attr('id');
            request(App.draggableItemID.parent().attr('target'), {item: App.draggableItemID.attr('id'), toSlot: dropOn});
        }
    }

});

var timeOut;

function ajaxRequest(data, timer = 10000) {
    var pos = $('.content-middle').offset().top;
    if (pos < $(document).scrollTop()) {
        $('html').animate({scrollTop: pos}, 500);
    }

    $('#ajaxRequests').empty().hide().fadeIn().append(data);
    $('.ajaxLoader').hide();

    clearTimeout(timeOut);
    timeOut = setTimeout(function () {
        $('#ajaxRequests').fadeOut();
    }, timer);
}

function setCookie(name, value = '') {
    if (value !== '')
        document.cookie = name + '=' + value + '; expires=Thu, 01 Jan 2030 00:00:01 GMT; path=/;';
    else
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
}

function quickLoad(elementAttr) {
    var element = $('[quickLoad=' + elementAttr + ']');
    $.ajax({
        url: '/index.php',
        type: 'POST',
        data: 'quickload=' + element.attr('quickLoad')
    }).done(function (data) {
        element.empty().append(data);
    });
}

function loader(page, pushSt = true, target = 'ajaxContent') {
    $('.ajaxLoader').show();
    if (pushSt)
        window.history.pushState(null, null, page);

    $.ajax({
        url: '/index.php',
        type: 'POST',
        data: 'page=' + page
    }).done(function (data) {
        $('#' + target).empty().append(data);
    }).always(function () {
        $('.ajaxLoader').hide();
    });
}

function request(request, data = {}) {
    $('.ajaxLoader').show();
    $.ajax({
        url: '/index.php',
        type: 'POST',
        data: 'request=' + request + '&data=' + JSON.stringify(data)
    }).done(function (data) {
        ajaxRequest(data);
    });
}

function showHide(show, hide, effect = false) {
    $(hide).hide(effect);
    $(show).show(effect);
}

function subMenu(element) {
    if ($(element).length) {
        if ($(element).css('display') !== 'block') {
            showHide(element, '.submenuBlock');
        }
    } else {
        if ($('#submenu-default').css('display') !== 'block') {
            showHide('#submenu-default', '.submenuBlock');
        }
    }
}

function addZero(int) {
    if (int <= 9) {
        int = '0' + int;
    }

    return int;
}

function getServerTime() {
    var timeZone = $('#momentTimezone').val();
    return moment().tz(timeZone).toObject();
}