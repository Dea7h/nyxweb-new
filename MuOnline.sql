USE [master]
GO
/****** Object:  Database [MuOnline]    Script Date: 07-Jun-19 19:18:03 ******/
CREATE DATABASE [MuOnline]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'MuOnline_Data', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\MuOnline.mdf' , SIZE = 6784KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
 LOG ON 
( NAME = N'MuOnline_Log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\MuOnline_0.ldf' , SIZE = 9027840KB , MAXSIZE = UNLIMITED, FILEGROWTH = 10%)
GO
ALTER DATABASE [MuOnline] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MuOnline].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [MuOnline] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MuOnline] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MuOnline] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MuOnline] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MuOnline] SET ARITHABORT OFF 
GO
ALTER DATABASE [MuOnline] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MuOnline] SET AUTO_SHRINK ON 
GO
ALTER DATABASE [MuOnline] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MuOnline] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MuOnline] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MuOnline] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MuOnline] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MuOnline] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MuOnline] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MuOnline] SET  DISABLE_BROKER 
GO
ALTER DATABASE [MuOnline] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MuOnline] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MuOnline] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MuOnline] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MuOnline] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MuOnline] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MuOnline] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MuOnline] SET RECOVERY FULL 
GO
ALTER DATABASE [MuOnline] SET  MULTI_USER 
GO
ALTER DATABASE [MuOnline] SET PAGE_VERIFY TORN_PAGE_DETECTION  
GO
ALTER DATABASE [MuOnline] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MuOnline] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MuOnline] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [MuOnline] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MuOnline] SET QUERY_STORE = OFF
GO
USE [MuOnline]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_md5]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE FUNCTION [dbo].[fn_md5] (@data VARCHAR(10), @data2 VARCHAR(10))
RETURNS BINARY(16) AS
BEGIN
DECLARE @hash BINARY(16)
EXEC master.dbo.XP_MD5_EncodeKeyVal @data, @data2, @hash OUT
RETURN @hash
END
GO
/****** Object:  UserDefinedFunction [dbo].[UFN_MD5_CHECKVALUE]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ЗФјцён : UFN_MD5_ENCODEVALUE()
-- і»їл : ЖЇБ¤№®АЪї­°ъ АОµ¦ЅєїН MD5 °ЄА» єс±іЗПї© ёВґВБц ГјЕ©
CREATE FUNCTION [dbo].[UFN_MD5_CHECKVALUE]
(
	@btInStr		VARCHAR(10),
	@btInStrIndex		VARCHAR(10),
	@btInVal		BINARY(16)
)
RETURNS TINYINT 
AS  
BEGIN 
	DECLARE	@iOutResult	TINYINT

	EXEC master..XP_MD5_CheckValue @btInStr, @btInVal, @btInStrIndex, @iOutResult OUT

	RETURN 	@iOutResult
END

GO
/****** Object:  UserDefinedFunction [dbo].[UFN_MD5_ENCODEVALUE]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ЗФјцён : UFN_MD5_ENCODEVALUE()
-- і»їл : ЖЇБ¤№®АЪї­°ъ АОµ¦Ѕєё¦ АМїлЗПї© MD5 °ЄА» »эјє
CREATE FUNCTION [dbo].[UFN_MD5_ENCODEVALUE]
(
	@btInStr		VARCHAR(10),
	@btInStrIndex		VARCHAR(10)
)
RETURNS BINARY(16)
AS  
BEGIN 
	DECLARE	@btOutVal	BINARY(16)

	EXEC master..XP_MD5_EncodeKeyVal @btInStr, @btInStrIndex, @btOutVal OUT

	RETURN 	@btOutVal
END

GO
/****** Object:  Table [dbo].[Character]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Character](
	[AccountID] [varchar](10) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[cLevel] [int] NULL,
	[LevelUpPoint] [int] NULL,
	[Class] [tinyint] NULL,
	[Experience] [int] NULL,
	[Strength] [smallint] NULL,
	[Dexterity] [smallint] NULL,
	[Vitality] [smallint] NULL,
	[Energy] [smallint] NULL,
	[Inventory] [varbinary](1080) NULL,
	[MagicList] [varbinary](180) NULL,
	[Money] [int] NULL,
	[Life] [real] NULL,
	[MaxLife] [real] NULL,
	[Mana] [real] NULL,
	[MaxMana] [real] NULL,
	[MapNumber] [smallint] NULL,
	[MapPosX] [smallint] NULL,
	[MapPosY] [smallint] NULL,
	[MapDir] [tinyint] NULL,
	[PkCount] [int] NULL,
	[PkLevel] [int] NULL,
	[PkTime] [int] NULL,
	[MDate] [smalldatetime] NULL,
	[LDate] [smalldatetime] NULL,
	[CtlCode] [tinyint] NULL,
	[DbVersion] [tinyint] NULL,
	[Quest] [varbinary](50) NULL,
	[Leadership] [smallint] NULL,
	[ChatLimitTime] [smallint] NULL,
	[Resets] [int] NOT NULL,
	[BanPost] [int] NOT NULL,
	[IsMarried] [int] NOT NULL,
	[MarryName] [varchar](11) NULL,
	[GrandResets] [int] NOT NULL,
	[QuestInProgress] [int] NOT NULL,
	[QuestNumber] [int] NOT NULL,
	[QuestMonsters] [int] NOT NULL,
	[SkyEventWins] [int] NOT NULL,
	[IsVip] [int] NOT NULL,
	[VipExpirationTime] [int] NOT NULL,
	[nyxPVP1] [int] NULL,
	[nyxPVP2] [int] NULL,
	[nyxPVP3] [int] NULL,
	[nyxEventWins] [int] NULL,
	[nyxSTYLISH] [varchar](10) NULL,
	[profileViews] [int] NULL,
	[profileMessage] [text] NULL,
 CONSTRAINT [PK_Character] PRIMARY KEY NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  View [dbo].[vCharacterPreview]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[vCharacterPreview] 
AS SELECT Name, cLevel, Class, Inventory, CtlCode, DbVersion FROM Character

GO
/****** Object:  Table [dbo].[AccountCharacter]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountCharacter](
	[Number] [int] IDENTITY(1,1) NOT NULL,
	[Id] [varchar](10) NOT NULL,
	[GameID1] [varchar](10) NULL,
	[GameID2] [varchar](10) NULL,
	[GameID3] [varchar](10) NULL,
	[GameID4] [varchar](10) NULL,
	[GameID5] [varchar](10) NULL,
	[GameIDC] [varchar](10) NULL,
	[MoveCnt] [tinyint] NULL,
 CONSTRAINT [PK_AccountCharacter] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[Id] [varchar](10) NOT NULL,
	[Pass] [varchar](10) NOT NULL,
	[AdminLevel] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BLOCKING]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BLOCKING](
	[block_guid] [int] IDENTITY(1,1) NOT NULL,
	[memb_guid] [int] NOT NULL,
	[serv_guid] [int] NOT NULL,
	[char_name] [varchar](20) NULL,
	[take_code] [char](1) NOT NULL,
	[take_cont] [varchar](1000) NOT NULL,
	[memb_cont] [varchar](2000) NULL,
	[appl_days] [char](8) NOT NULL,
	[rels_days] [char](8) NOT NULL,
	[ctl1_code] [char](1) NOT NULL,
 CONSTRAINT [PK_BLOCKING] PRIMARY KEY CLUSTERED 
(
	[block_guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BLOCKING_LOG]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BLOCKING_LOG](
	[appl_days] [datetime] NOT NULL,
	[admin_guid] [int] NOT NULL,
	[block_guid] [int] NOT NULL,
	[dist_code] [char](2) NOT NULL,
	[admin_name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_BLOCKING_LOG] PRIMARY KEY CLUSTERED 
(
	[appl_days] DESC,
	[admin_guid] DESC,
	[dist_code] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ChangeCharacter]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChangeCharacter](
	[Col001] [int] NULL,
	[Col002] [varchar](10) NULL,
	[Col003] [varchar](10) NULL,
	[Col004] [varchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CharPreview]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CharPreview](
	[Name] [varchar](10) NOT NULL,
	[cLevel] [int] NULL,
	[Class] [tinyint] NOT NULL,
	[Inventory] [varbinary](84) NULL,
 CONSTRAINT [PK_CharPreview] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Day_Statistics]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Day_Statistics](
	[ConnectTime] [datetime] NOT NULL,
	[TotalCount] [int] NOT NULL,
 CONSTRAINT [PK_Day_Statistics] PRIMARY KEY CLUSTERED 
(
	[ConnectTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DefaultClassType]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DefaultClassType](
	[Class] [tinyint] NOT NULL,
	[Strength] [smallint] NULL,
	[Dexterity] [smallint] NULL,
	[Vitality] [smallint] NULL,
	[Energy] [smallint] NULL,
	[Inventory] [varbinary](1080) NULL,
	[MagicList] [varbinary](180) NULL,
	[Life] [real] NULL,
	[MaxLife] [real] NULL,
	[Mana] [real] NULL,
	[MaxMana] [real] NULL,
	[MapNumber] [smallint] NULL,
	[MapPosX] [smallint] NULL,
	[MapPosY] [smallint] NULL,
	[Quest] [varbinary](50) NULL,
	[DbVersion] [tinyint] NULL,
	[Leadership] [smallint] NULL,
	[Level] [smallint] NULL,
	[LevelUpPoint] [smallint] NULL,
 CONSTRAINT [PK_DefaultClassType] PRIMARY KEY CLUSTERED 
(
	[Class] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DT_Day_Statistics]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_Day_Statistics](
	[ConnectTime] [datetime] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[TotalCount] [int] NOT NULL,
 CONSTRAINT [PK_DT_Day_Statistics] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[ConnectTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DT_Week_Statistics]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_Week_Statistics](
	[Year] [smallint] NOT NULL,
	[Week] [tinyint] NOT NULL,
	[WeekDay] [tinyint] NOT NULL,
	[HalfDay] [tinyint] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_DT_Week_Statistics] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[Year] ASC,
	[Week] ASC,
	[WeekDay] ASC,
	[HalfDay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DT_WeekDay_Statistics]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_WeekDay_Statistics](
	[Year] [smallint] NOT NULL,
	[Week] [tinyint] NOT NULL,
	[WeekDay] [tinyint] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[Hour] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_DT_WeekDay_Statistics] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[Week] ASC,
	[WeekDay] ASC,
	[Hour] ASC,
	[Year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DT_Year_Statistics]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DT_Year_Statistics](
	[Year] [smallint] NOT NULL,
	[Month] [tinyint] NOT NULL,
	[Day] [tinyint] NOT NULL,
	[Type] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_DT_Year_Statistics] PRIMARY KEY CLUSTERED 
(
	[Type] ASC,
	[Year] ASC,
	[Month] ASC,
	[Day] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EVENT_INFO]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_INFO](
	[Server] [smallint] NOT NULL,
	[Square] [tinyint] NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Point] [int] NOT NULL,
 CONSTRAINT [PK_EVENT_INFO] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[Square] ASC,
	[AccountID] ASC,
	[CharacterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EVENT_INFO_BC]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_INFO_BC](
	[Server] [smallint] NOT NULL,
	[Bridge] [tinyint] NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Point] [int] NOT NULL,
	[PlayCount] [int] NOT NULL,
 CONSTRAINT [PK_EVENT_INFO_BC] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[Bridge] ASC,
	[AccountID] ASC,
	[CharacterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EVENT_INFO_BC_3RD]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_INFO_BC_3RD](
	[Server] [tinyint] NOT NULL,
	[Bridge] [tinyint] NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Point] [int] NOT NULL,
	[PlayCount] [int] NOT NULL,
	[SumLeftTime] [int] NOT NULL,
 CONSTRAINT [PK_EVENT_INFO_BC_3RD] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[Bridge] ASC,
	[AccountID] ASC,
	[CharacterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EVENT_INFO_BC_4TH]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_INFO_BC_4TH](
	[Server] [tinyint] NOT NULL,
	[Bridge] [tinyint] NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Point] [int] NOT NULL,
	[PlayCount] [int] NOT NULL,
	[SumLeftTime] [int] NOT NULL,
	[MinLeftTime] [int] NOT NULL,
 CONSTRAINT [PK_EVENT_INFO_BC_4TH] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[Bridge] ASC,
	[AccountID] ASC,
	[CharacterName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EVENT_INFO_BC_5TH]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EVENT_INFO_BC_5TH](
	[Server] [tinyint] NOT NULL,
	[Bridge] [tinyint] NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[CharacterName] [varchar](10) NOT NULL,
	[Class] [tinyint] NOT NULL,
	[Point] [int] NOT NULL,
	[PlayCount] [int] NOT NULL,
	[SumLeftTime] [int] NOT NULL,
	[MinLeftTime] [int] NOT NULL,
	[RegDate] [smalldatetime] NOT NULL,
	[AlivePartyCount] [int] NOT NULL,
	[MaxPointLeftTime] [int] NOT NULL,
 CONSTRAINT [PK_EVENT_INFO_BC_5TH] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[Bridge] ASC,
	[AccountID] ASC,
	[CharacterName] ASC,
	[RegDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GameServerInfo]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GameServerInfo](
	[Number] [int] NOT NULL,
	[ItemCount] [int] NOT NULL,
	[ZenCount] [int] NULL,
 CONSTRAINT [PK_GameServerInfo] PRIMARY KEY NONCLUSTERED 
(
	[Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Guild]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Guild](
	[G_Name] [varchar](8) NOT NULL,
	[G_Mark] [varbinary](32) NULL,
	[G_Score] [int] NULL,
	[G_Master] [varchar](10) NULL,
	[G_Count] [int] NULL,
	[G_Notice] [varchar](60) NULL,
	[Number] [int] IDENTITY(1,1) NOT NULL,
	[G_Type] [int] NOT NULL,
	[G_Rival] [int] NOT NULL,
	[G_Union] [int] NOT NULL,
 CONSTRAINT [PK_Guild] PRIMARY KEY CLUSTERED 
(
	[G_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GuildMember]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GuildMember](
	[Name] [varchar](10) NOT NULL,
	[G_Name] [varchar](8) NOT NULL,
	[G_Level] [tinyint] NULL,
	[G_Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_GuildMember] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MANG_INFO]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MANG_INFO](
	[mang_guid] [int] IDENTITY(1,1) NOT NULL,
	[mang___id] [varchar](15) NOT NULL,
	[mang__pwd] [varchar](34) NOT NULL,
	[mang_name] [varchar](20) NOT NULL,
	[auth_code] [int] NOT NULL,
	[appl_day] [smalldatetime] NULL,
	[bloc_code] [int] NOT NULL,
	[perm] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MEMB_CREDITS]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEMB_CREDITS](
	[memb___id] [varchar](10) NOT NULL,
	[credits] [int] NOT NULL,
 CONSTRAINT [PK_MEMB_CREDITS] PRIMARY KEY CLUSTERED 
(
	[memb___id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MEMB_DETA]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEMB_DETA](
	[memb_guid] [int] NOT NULL,
	[desc_text] [varchar](5000) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MEMB_INFO]    Script Date: 07-Jun-19 19:18:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEMB_INFO](
	[memb_guid] [int] IDENTITY(1,1) NOT NULL,
	[memb___id] [varchar](10) NOT NULL,
	[memb__pwd] [varchar](10) NOT NULL,
	[memb_name] [varchar](10) NOT NULL,
	[sno__numb] [varchar](10) NOT NULL,
	[post_code] [varchar](10) NULL,
	[addr_info] [varchar](50) NULL,
	[addr_deta] [varchar](50) NULL,
	[tel__numb] [varchar](20) NULL,
	[phon_numb] [nvarchar](20) NULL,
	[mail_addr] [varchar](50) NULL,
	[fpas_ques] [varchar](50) NULL,
	[fpas_answ] [varchar](50) NULL,
	[job__code] [char](2) NULL,
	[appl_days] [datetime] NULL,
	[modi_days] [datetime] NULL,
	[out__days] [datetime] NULL,
	[true_days] [datetime] NULL,
	[mail_chek] [char](1) NULL,
	[bloc_code] [char](1) NOT NULL,
	[ctl1_code] [char](1) NOT NULL,
	[memb__pwd2] [varchar](11) NULL,
	[IsVip] [int] NOT NULL,
	[VipExpirationTime] [int] NOT NULL,
	[mainCharacter] [nvarchar](50) NULL,
	[requestInAction] [int] NOT NULL,
 CONSTRAINT [PK_MEMB_INFO_1] PRIMARY KEY NONCLUSTERED 
(
	[memb_guid] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_MEMB_INFO_1]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_MEMB_INFO_1] ON [dbo].[MEMB_INFO]
(
	[memb___id] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 70) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MEMB_STAT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MEMB_STAT](
	[memb___id] [varchar](10) NOT NULL,
	[ConnectStat] [tinyint] NULL,
	[ServerName] [varchar](10) NULL,
	[IP] [varchar](15) NULL,
	[ConnectTM] [smalldatetime] NULL,
	[DisConnectTM] [smalldatetime] NULL,
 CONSTRAINT [PK_MEMB_STAT] PRIMARY KEY CLUSTERED 
(
	[memb___id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_DATA]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_DATA](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[SIEGE_START_DATE] [datetime] NOT NULL,
	[SIEGE_END_DATE] [datetime] NOT NULL,
	[SIEGE_GUILDLIST_SETTED] [bit] NOT NULL,
	[SIEGE_ENDED] [bit] NOT NULL,
	[CASTLE_OCCUPY] [bit] NOT NULL,
	[OWNER_GUILD] [varchar](8) NOT NULL,
	[MONEY] [money] NOT NULL,
	[TAX_RATE_CHAOS] [int] NOT NULL,
	[TAX_RATE_STORE] [int] NOT NULL,
	[TAX_HUNT_ZONE] [int] NOT NULL,
 CONSTRAINT [PK_MuCastle_DATA] PRIMARY KEY CLUSTERED 
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_MONEY_STATISTICS]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_MONEY_STATISTICS](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[LOG_DATE] [datetime] NOT NULL,
	[MONEY_CHANGE] [money] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Index [IX_MuCastle_MONEY_STATISTICS]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_MuCastle_MONEY_STATISTICS] ON [dbo].[MuCastle_MONEY_STATISTICS]
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_NPC]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_NPC](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[NPC_NUMBER] [int] NOT NULL,
	[NPC_INDEX] [int] NOT NULL,
	[NPC_DF_LEVEL] [int] NOT NULL,
	[NPC_RG_LEVEL] [int] NOT NULL,
	[NPC_MAXHP] [int] NOT NULL,
	[NPC_HP] [int] NOT NULL,
	[NPC_X] [tinyint] NOT NULL,
	[NPC_Y] [tinyint] NOT NULL,
	[NPC_DIR] [tinyint] NOT NULL,
	[NPC_CREATEDATE] [datetime] NOT NULL,
 CONSTRAINT [IX_NPC_SUBKEY] UNIQUE NONCLUSTERED 
(
	[MAP_SVR_GROUP] ASC,
	[NPC_NUMBER] ASC,
	[NPC_INDEX] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_NPC_PK]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_NPC_PK] ON [dbo].[MuCastle_NPC]
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_REG_SIEGE]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_REG_SIEGE](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[REG_SIEGE_GUILD] [varchar](8) NOT NULL,
	[REG_MARKS] [int] NOT NULL,
	[IS_GIVEUP] [tinyint] NOT NULL,
	[SEQ_NUM] [int] NOT NULL,
 CONSTRAINT [IX_ATTACK_GUILD_SUBKEY] UNIQUE NONCLUSTERED 
(
	[MAP_SVR_GROUP] ASC,
	[REG_SIEGE_GUILD] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_ATTACK_GUILD_KEY]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_ATTACK_GUILD_KEY] ON [dbo].[MuCastle_REG_SIEGE]
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuCastle_SIEGE_GUILDLIST]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuCastle_SIEGE_GUILDLIST](
	[MAP_SVR_GROUP] [int] NOT NULL,
	[GUILD_NAME] [varchar](10) NOT NULL,
	[GUILD_ID] [int] NOT NULL,
	[GUILD_INVOLVED] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Index [IX_MuCastle_SIEGE_GUILDLIST]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_MuCastle_SIEGE_GUILDLIST] ON [dbo].[MuCastle_SIEGE_GUILDLIST]
(
	[MAP_SVR_GROUP] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MuMakerDupeFinder]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuMakerDupeFinder](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[AC] [varchar](10) NOT NULL,
	[PJ] [varchar](10) NOT NULL,
	[Item] [varchar](32) NOT NULL,
	[ItemID] [varchar](2) NOT NULL,
	[ItemTP] [varchar](2) NOT NULL,
	[Serial] [varchar](8) NOT NULL,
	[Posicion] [smallint] NOT NULL,
	[Vault] [smallint] NOT NULL,
	[Invent] [smallint] NOT NULL,
	[ExtraW] [smallint] NOT NULL,
	[NumExt] [smallint] NOT NULL,
	[Items] [varchar](4000) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_BLACKLIST]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_BLACKLIST](
	[account] [nvarchar](10) NOT NULL,
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[blocked] [nvarchar](10) NOT NULL,
	[time] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_LOGS]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_LOGS](
	[account] [nvarchar](10) NOT NULL,
	[id] [int] IDENTITY(1,1) NOT NULL,
	[module] [nvarchar](20) NOT NULL,
	[message] [nvarchar](100) NOT NULL,
	[time] [bigint] NOT NULL,
	[ip] [nvarchar](15) NOT NULL,
	[hidden_info] [nvarchar](max) NULL,
	[type] [nvarchar](10) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_MARKET]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_MARKET](
	[item_id] [int] IDENTITY(1,1) NOT NULL,
	[account] [nvarchar](10) NOT NULL,
	[account_id] [int] NOT NULL,
	[time] [bigint] NOT NULL,
	[hex] [nvarchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[type] [int] NOT NULL,
	[id] [int] NOT NULL,
	[durability] [int] NOT NULL,
	[level] [int] NOT NULL,
	[skill] [int] NOT NULL,
	[luck] [int] NOT NULL,
	[options] [int] NOT NULL,
	[exo] [int] NOT NULL,
	[ancient] [int] NOT NULL,
	[price_name] [nvarchar](10) NULL,
	[price_amount] [bigint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_NEWS]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_NEWS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](10) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[message] [text] NOT NULL,
	[author] [nvarchar](10) NOT NULL,
	[time] [bigint] NOT NULL,
	[title_Bulgarian] [nvarchar](50) NULL,
	[message_Bulgarian] [text] NULL,
	[comments] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_NEWS_COMMENTS]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_NEWS_COMMENTS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[article_id] [int] NULL,
	[rate] [int] NOT NULL,
	[message] [text] NOT NULL,
	[author] [nvarchar](10) NOT NULL,
	[time] [bigint] NOT NULL,
	[reply] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_NEWS_COMMENTS_VOTES]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_NEWS_COMMENTS_VOTES](
	[id] [int] NOT NULL,
	[account] [nvarchar](10) NOT NULL,
	[vote] [nvarchar](10) NOT NULL,
	[time] [bigint] NOT NULL,
	[ip] [nvarchar](30) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_REQUESTS]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_REQUESTS](
	[ip] [nvarchar](50) NULL,
	[time] [bigint] NOT NULL,
	[type] [nvarchar](10) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NYX_RESOURCES]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NYX_RESOURCES](
	[account] [nvarchar](10) NOT NULL,
	[bless] [int] NULL,
	[soul] [int] NULL,
	[chaos] [int] NULL,
	[life] [int] NULL,
	[creation] [int] NULL,
	[apple] [int] NULL,
	[box1] [int] NULL,
	[box2] [int] NULL,
	[box3] [int] NULL,
	[box4] [int] NULL,
	[box5] [int] NULL,
	[stone] [int] NULL,
	[rena] [int] NULL,
	[credits] [int] NULL,
	[zen] [bigint] NULL,
	[boh] [int] NULL,
	[bol] [int] NULL,
	[hol] [int] NULL,
	[heart] [int] NULL,
	[items] [text] NULL,
	[craftCounter] [int] NOT NULL,
	[craftItems] [text] NULL,
	[QuestNumber] [int] NOT NULL,
	[QuestItems] [text] NULL,
	[recycleCounter] [int] NULL,
	[recycleItems] [text] NULL,
	[messageItems] [text] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OptionData]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OptionData](
	[Name] [varchar](10) NOT NULL,
	[SkillKey] [binary](10) NULL,
	[GameOption] [tinyint] NULL,
	[Qkey] [tinyint] NULL,
	[Wkey] [tinyint] NULL,
	[Ekey] [tinyint] NULL,
	[ChatWindow] [tinyint] NULL,
 CONSTRAINT [PK_OptionData] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RM_AUTHDATA]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_AUTHDATA](
	[UID] [varchar](10) NOT NULL,
	[PASS] [varchar](10) NOT NULL,
	[UNAME] [nvarchar](16) NULL,
	[AuthCode] [int] NOT NULL,
	[CountryCode] [tinyint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SameTimeConnectLog]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SameTimeConnectLog](
	[ConnectTime] [datetime] NOT NULL,
	[Server] [tinyint] NOT NULL,
	[SubServer] [tinyint] NOT NULL,
	[ConnectCount] [int] NOT NULL,
 CONSTRAINT [PK_ConnectLog] PRIMARY KEY CLUSTERED 
(
	[ConnectTime] ASC,
	[SubServer] ASC,
	[Server] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_BC_PCROOM_PLAYCOUNT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_BC_PCROOM_PLAYCOUNT](
	[PCROOM_GUID] [int] NOT NULL,
	[AccountID] [varchar](50) NOT NULL,
	[PlayCount] [int] NOT NULL,
	[Point] [int] NOT NULL,
 CONSTRAINT [PK_T_BC_PCROOM_PLAYCOUNT] PRIMARY KEY CLUSTERED 
(
	[PCROOM_GUID] ASC,
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_BLOOD_CASTLE]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_BLOOD_CASTLE](
	[AccountID] [varchar](10) NOT NULL,
	[StoneCount] [int] NOT NULL,
	[Check_Code] [int] NOT NULL,
 CONSTRAINT [PK_T_BLOOD_CASTLE] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_BLOOD_PLAYCOUNT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_BLOOD_PLAYCOUNT](
	[AccountId] [varchar](50) NOT NULL,
	[PlayCount] [int] NOT NULL,
	[RecoverCheck] [bit] NOT NULL,
 CONSTRAINT [PK_T_BLOOD_PLAYCOUNT] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_CC_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_CC_OFFLINE_GIFT](
	[Guid] [int] IDENTITY(1,1) NOT NULL,
	[Server] [int] NULL,
	[AccountID] [varchar](10) NULL,
	[CharName] [varchar](10) NULL,
	[GiftKind] [int] NOT NULL,
	[Date_Give] [smalldatetime] NOT NULL,
	[Date_Reg] [smalldatetime] NULL,
	[RegCheck] [tinyint] NOT NULL,
 CONSTRAINT [PK_T_CC_OFFLINE_GIFT] PRIMARY KEY NONCLUSTERED 
(
	[Guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_T_CC_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_T_CC_OFFLINE_GIFT] ON [dbo].[T_CC_OFFLINE_GIFT]
(
	[Date_Give] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_CC_OFFLINE_GIFTNAME]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_CC_OFFLINE_GIFTNAME](
	[GiftKind] [int] NOT NULL,
	[GiftName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_T_CC_OFFLINE_GIFTNAME] PRIMARY KEY CLUSTERED 
(
	[GiftKind] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_CGuid]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_CGuid](
	[GUID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](10) NOT NULL,
 CONSTRAINT [PK_T_CGuid] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_CurCharName]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_CurCharName](
	[Name] [char](10) NOT NULL,
	[cDate] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_DL_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_DL_OFFLINE_GIFT](
	[Guid] [int] IDENTITY(1,1) NOT NULL,
	[Server] [int] NULL,
	[AccountID] [varchar](10) NULL,
	[CharName] [varchar](10) NULL,
	[GiftKind] [int] NOT NULL,
	[Date_Give] [smalldatetime] NOT NULL,
	[Date_Reg] [smalldatetime] NULL,
	[RegCheck] [tinyint] NOT NULL,
 CONSTRAINT [PK_T_DL_OFFLINE_GIFT] PRIMARY KEY NONCLUSTERED 
(
	[Guid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_T_DL_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:04 ******/
CREATE CLUSTERED INDEX [IX_T_DL_OFFLINE_GIFT] ON [dbo].[T_DL_OFFLINE_GIFT]
(
	[Date_Give] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_DL_OFFLINE_GIFTNAME]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_DL_OFFLINE_GIFTNAME](
	[GiftKind] [int] NOT NULL,
	[GiftName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_T_DL_OFFLINE_GIFTNAME] PRIMARY KEY CLUSTERED 
(
	[GiftKind] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_ENTER_CHECK_BC]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_ENTER_CHECK_BC](
	[AccountID] [varchar](10) NOT NULL,
	[CharName] [varchar](10) NOT NULL,
	[ServerCode] [smallint] NOT NULL,
	[ToDayEnterCount] [tinyint] NOT NULL,
	[LastEnterDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_T_ENTER_CHECK_DS] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC,
	[CharName] ASC,
	[ServerCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_FriendList]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_FriendList](
	[GUID] [int] NOT NULL,
	[FriendGuid] [int] NULL,
	[FriendName] [varchar](10) NULL,
	[Del] [tinyint] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_FriendMail]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_FriendMail](
	[MemoIndex] [int] NOT NULL,
	[GUID] [int] NOT NULL,
	[FriendName] [varchar](10) NULL,
	[wDate] [smalldatetime] NOT NULL,
	[Subject] [varchar](50) NULL,
	[bRead] [bit] NOT NULL,
	[Memo] [varbinary](1000) NULL,
	[Photo] [binary](13) NULL,
	[Dir] [tinyint] NULL,
	[Act] [tinyint] NULL,
 CONSTRAINT [PK_T_FriendMemo] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[MemoIndex] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_FriendMain]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_FriendMain](
	[GUID] [int] NOT NULL,
	[Name] [varchar](10) NOT NULL,
	[FriendCount] [tinyint] NULL,
	[MemoCount] [int] NULL,
	[MemoTotal] [int] NULL,
 CONSTRAINT [PK_T_FriendMain] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_FRIENDSHIP_SERVERRANK]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_FRIENDSHIP_SERVERRANK](
	[ServerCode] [smallint] NOT NULL,
	[FriendShipStoneCount] [int] NOT NULL,
 CONSTRAINT [PK_T_FRIENDSHIP_SERVERRANK] PRIMARY KEY CLUSTERED 
(
	[ServerCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_FRIENDSHIP_STONE]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_FRIENDSHIP_STONE](
	[AccountID] [varchar](10) NOT NULL,
	[ServerCode] [smallint] NOT NULL,
	[CharName] [varchar](10) NOT NULL,
	[FriendShipStoneCount] [int] NOT NULL,
	[Check_Code] [tinyint] NOT NULL,
 CONSTRAINT [PK_T_T] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC,
	[ServerCode] ASC,
	[CharName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_MU2003_EVENT]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_MU2003_EVENT](
	[AccountID] [varchar](50) NOT NULL,
	[EventChips] [smallint] NOT NULL,
	[MuttoIndex] [int] NOT NULL,
	[MuttoNumber] [int] NOT NULL,
	[Check_Code] [char](1) NOT NULL,
 CONSTRAINT [PK_T_MU2003_EVENT_1] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_PetItem_Info]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_PetItem_Info](
	[ItemSerial] [int] NOT NULL,
	[Pet_Level] [smallint] NULL,
	[Pet_Exp] [int] NULL,
 CONSTRAINT [PK_T_Pet_Info] PRIMARY KEY CLUSTERED 
(
	[ItemSerial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_RegCount_Check]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_RegCount_Check](
	[AccountID] [varchar](10) NOT NULL,
	[RegCount] [int] NOT NULL,
	[RegAlready] [bit] NOT NULL,
 CONSTRAINT [PK_T_RegCount_Check] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Register_Info]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Register_Info](
	[F_Register_Section] [smallint] NOT NULL,
	[F_Register_Name] [varchar](50) NOT NULL,
	[F_Register_TotalCount] [int] NOT NULL,
 CONSTRAINT [PK_T_Register_Info] PRIMARY KEY CLUSTERED 
(
	[F_Register_Section] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_RingAttackEvent_Gift]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_RingAttackEvent_Gift](
	[GUID] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [varchar](50) NOT NULL,
	[GiftKind] [tinyint] NOT NULL,
	[RegisterDate] [datetime] NOT NULL,
 CONSTRAINT [PK_T_RingAttackEvent_Gift] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[GiftKind] ASC,
	[RegisterDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_RingEvent_GiftName]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_RingEvent_GiftName](
	[GiftKind] [tinyint] NOT NULL,
	[GiftName] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_RingEvent_OfflineGift]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_RingEvent_OfflineGift](
	[GUID] [smallint] IDENTITY(1,1) NOT NULL,
	[AccountID] [varchar](10) NULL,
	[GiftSection] [tinyint] NOT NULL,
	[GiftKind] [tinyint] NOT NULL,
	[RegisterDate] [datetime] NULL,
 CONSTRAINT [PK_T_RingEvent_OfflineGift] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC,
	[GiftSection] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_Serial_Bank]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_Serial_Bank](
	[F_Serial_Guid] [int] IDENTITY(1,1) NOT NULL,
	[P_Serial_1] [char](4) NOT NULL,
	[P_Serial_2] [char](4) NOT NULL,
	[P_Serial_3] [char](4) NOT NULL,
	[F_Serial_Section] [smallint] NOT NULL,
	[F_Member_Guid] [int] NULL,
	[F_Member_Id] [char](10) NULL,
	[F_Register_Section] [smallint] NULL,
	[F_Register_Date] [smalldatetime] NULL,
	[F_Create_Date] [smalldatetime] NOT NULL,
	[F_RegisterCheck] [bit] NOT NULL,
 CONSTRAINT [PK_T_Serial_Bank] PRIMARY KEY CLUSTERED 
(
	[P_Serial_1] ASC,
	[P_Serial_2] ASC,
	[P_Serial_3] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[T_WaitFriend]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[T_WaitFriend](
	[GUID] [int] NOT NULL,
	[FriendGuid] [int] NOT NULL,
	[FriendName] [varchar](10) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TimeConnectLog ]    Script Date: 07-Jun-19 19:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TimeConnectLog ](
	[ConnectTime] [datetime] NOT NULL,
	[Server] [tinyint] NOT NULL,
	[SubServer] [tinyint] NOT NULL,
	[ConnectCount] [int] NOT NULL,
 CONSTRAINT [PK_TimeConnectLog ] PRIMARY KEY CLUSTERED 
(
	[Server] ASC,
	[ConnectTime] ASC,
	[SubServer] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[warehouse]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[warehouse](
	[AccountID] [varchar](10) NOT NULL,
	[Items] [varbinary](1200) NULL,
	[Money] [int] NULL,
	[EndUseDate] [smalldatetime] NULL,
	[DbVersion] [tinyint] NULL,
	[pw] [smallint] NULL,
	[MultiVault] [int] NOT NULL,
	[Items01] [varbinary](1200) NULL,
	[Items02] [varbinary](1200) NULL,
	[Items03] [varbinary](1200) NULL,
	[Items04] [varbinary](1200) NULL,
	[Items05] [varbinary](1200) NULL,
	[Items06] [varbinary](1200) NULL,
	[Items07] [varbinary](1200) NULL,
	[Items08] [varbinary](1200) NULL,
	[Items09] [varbinary](1200) NULL,
	[Items10] [varbinary](1200) NULL,
 CONSTRAINT [PK_warehouse] PRIMARY KEY CLUSTERED 
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Week_Statistics]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Week_Statistics](
	[Year] [smallint] NOT NULL,
	[Week] [tinyint] NOT NULL,
	[WeekDay] [tinyint] NOT NULL,
	[HalfDay] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_Week_Statistics] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[Week] ASC,
	[WeekDay] ASC,
	[HalfDay] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WeekDay_Statistics]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WeekDay_Statistics](
	[Year] [smallint] NOT NULL,
	[Week] [tinyint] NOT NULL,
	[WeekDay] [tinyint] NOT NULL,
	[Hour] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_WeekDay_Statistics] PRIMARY KEY CLUSTERED 
(
	[Week] ASC,
	[WeekDay] ASC,
	[Hour] ASC,
	[Year] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Year_Statistics]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Year_Statistics](
	[Year] [smallint] NOT NULL,
	[Month] [tinyint] NOT NULL,
	[Day] [tinyint] NOT NULL,
	[MaxCount] [int] NOT NULL,
	[MinCount] [int] NOT NULL,
	[AvgCount] [int] NOT NULL,
 CONSTRAINT [PK_Year_Statistics] PRIMARY KEY CLUSTERED 
(
	[Year] ASC,
	[Month] ASC,
	[Day] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ZenEvent]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZenEvent](
	[Guid] [int] IDENTITY(1,1) NOT NULL,
	[AccountID] [varchar](10) NOT NULL,
	[Zen] [int] NULL,
 CONSTRAINT [PK_ZenEvent] PRIMARY KEY CLUSTERED 
(
	[Guid] ASC,
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_BLOCKING]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_BLOCKING] ON [dbo].[BLOCKING]
(
	[char_name] DESC,
	[take_code] DESC,
	[appl_days] DESC,
	[rels_days] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EVENT_INFO_BC_5TH]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_EVENT_INFO_BC_5TH] ON [dbo].[EVENT_INFO_BC_5TH]
(
	[AlivePartyCount] ASC,
	[MinLeftTime] ASC,
	[RegDate] ASC,
	[Server] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_GUILD_G_RIVAL]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IDX_GUILD_G_RIVAL] ON [dbo].[Guild]
(
	[G_Rival] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_GUILD_G_UNION]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IDX_GUILD_G_UNION] ON [dbo].[Guild]
(
	[G_Union] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IDX_GUILD_NUMBER]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IDX_GUILD_NUMBER] ON [dbo].[Guild]
(
	[Number] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_GuildMember]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_GuildMember] ON [dbo].[GuildMember]
(
	[G_Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_MEMB_DETAIL]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_MEMB_DETAIL] ON [dbo].[MEMB_INFO]
(
	[sno__numb] DESC,
	[memb_name] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_MuCastle_MONEY_STATISTICS_NC]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_MuCastle_MONEY_STATISTICS_NC] ON [dbo].[MuCastle_MONEY_STATISTICS]
(
	[MAP_SVR_GROUP] ASC,
	[LOG_DATE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_CC_OFFLINE_GIFT_1]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_CC_OFFLINE_GIFT_1] ON [dbo].[T_CC_OFFLINE_GIFT]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_CGuid_Name]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_CGuid_Name] ON [dbo].[T_CGuid]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_DL_OFFLINE_GIFT_1]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_DL_OFFLINE_GIFT_1] ON [dbo].[T_DL_OFFLINE_GIFT]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_T_FriendList]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_FriendList] ON [dbo].[T_FriendList]
(
	[GUID] ASC,
	[FriendGuid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_FriendMain]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_FriendMain] ON [dbo].[T_FriendMain]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_RingAttackEvent_Gift]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_RingAttackEvent_Gift] ON [dbo].[T_RingAttackEvent_Gift]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_T_RingEvent_OfflineGift]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_RingEvent_OfflineGift] ON [dbo].[T_RingEvent_OfflineGift]
(
	[AccountID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_T_WaitFriend]    Script Date: 07-Jun-19 19:18:05 ******/
CREATE NONCLUSTERED INDEX [IX_T_WaitFriend] ON [dbo].[T_WaitFriend]
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AccountCharacter] ADD  CONSTRAINT [DF__AccountCh__MoveC__7A3223E8]  DEFAULT (0) FOR [MoveCnt]
GO
ALTER TABLE [dbo].[BLOCKING] ADD  CONSTRAINT [DF_BLOCKING_ctl1_code]  DEFAULT (5) FOR [ctl1_code]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_cLevel]  DEFAULT ((1)) FOR [cLevel]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_LevelUpPoint]  DEFAULT ((0)) FOR [LevelUpPoint]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Experience]  DEFAULT ((0)) FOR [Experience]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Money]  DEFAULT ((0)) FOR [Money]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_MapDir]  DEFAULT ((0)) FOR [MapDir]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkCount]  DEFAULT ((0)) FOR [PkCount]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkLevel]  DEFAULT ((3)) FOR [PkLevel]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_PkTime]  DEFAULT ((0)) FOR [PkTime]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_CtlCode]  DEFAULT ((0)) FOR [CtlCode]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__DbVer__3A4CA8FD]  DEFAULT ((0)) FOR [DbVersion]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Quest__40F9A68C]  DEFAULT ((0)) FOR [Quest]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Leade__6FB49575]  DEFAULT ((0)) FOR [Leadership]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__ChatL__70A8B9AE]  DEFAULT ((0)) FOR [ChatLimitTime]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_Resets]  DEFAULT ((0)) FOR [Resets]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_BlockPost]  DEFAULT ((0)) FOR [BanPost]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_IsMarried]  DEFAULT ((0)) FOR [IsMarried]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Grand__33F4B129]  DEFAULT ((0)) FOR [GrandResets]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Quest__34E8D562]  DEFAULT ((0)) FOR [QuestInProgress]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Quest__35DCF99B]  DEFAULT ((0)) FOR [QuestNumber]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__Quest__36D11DD4]  DEFAULT ((0)) FOR [QuestMonsters]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__SkyEv__37C5420D]  DEFAULT ((0)) FOR [SkyEventWins]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__IsVip__38B96646]  DEFAULT ((0)) FOR [IsVip]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF__Character__VipEx__39AD8A7F]  DEFAULT ((0)) FOR [VipExpirationTime]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_nyxPVP1]  DEFAULT ((0)) FOR [nyxPVP1]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_nyxPVP2]  DEFAULT ((0)) FOR [nyxPVP2]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_nyxPVP3]  DEFAULT ((0)) FOR [nyxPVP3]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_nyxEvents]  DEFAULT ((0)) FOR [nyxEventWins]
GO
ALTER TABLE [dbo].[Character] ADD  CONSTRAINT [DF_Character_profileViews]  DEFAULT ((0)) FOR [profileViews]
GO
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Leade__719CDDE7]  DEFAULT (0) FOR [Leadership]
GO
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Level__72910220]  DEFAULT (0) FOR [Level]
GO
ALTER TABLE [dbo].[DefaultClassType] ADD  CONSTRAINT [DF__DefaultCl__Level__73852659]  DEFAULT (0) FOR [LevelUpPoint]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC] ADD  CONSTRAINT [DF_EVENT_INFO_BC_PlayCount]  DEFAULT (1) FOR [PlayCount]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_3RD] ADD  CONSTRAINT [DF_EVENT_INFO_BC_3RD_SumLeftTime]  DEFAULT (0) FOR [SumLeftTime]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_4TH] ADD  CONSTRAINT [DF_EVENT_INFO_BC_4TH_SumLeftTime]  DEFAULT (0) FOR [SumLeftTime]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_4TH] ADD  CONSTRAINT [DF_EVENT_INFO_BC_4TH_MinLeftTime]  DEFAULT (0) FOR [MinLeftTime]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_5TH] ADD  CONSTRAINT [DF_EVENT_INFO_BC_5TH_SumLeftTime]  DEFAULT (0) FOR [SumLeftTime]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_5TH] ADD  CONSTRAINT [DF_EVENT_INFO_BC_5TH_MinLeftTime]  DEFAULT (0) FOR [MinLeftTime]
GO
ALTER TABLE [dbo].[EVENT_INFO_BC_5TH] ADD  CONSTRAINT [DF_EVENT_INFO_BC_5TH_MaxPointLeftTime]  DEFAULT (0) FOR [MaxPointLeftTime]
GO
ALTER TABLE [dbo].[GameServerInfo] ADD  CONSTRAINT [DF_GameServerInfo_Number]  DEFAULT (0) FOR [Number]
GO
ALTER TABLE [dbo].[GameServerInfo] ADD  CONSTRAINT [DF_GameServerInfo_ZenCount]  DEFAULT (0) FOR [ZenCount]
GO
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF_Guild_G_Score]  DEFAULT (0) FOR [G_Score]
GO
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Type__7EF6D905]  DEFAULT (0) FOR [G_Type]
GO
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Rival__7FEAFD3E]  DEFAULT (0) FOR [G_Rival]
GO
ALTER TABLE [dbo].[Guild] ADD  CONSTRAINT [DF__Guild__G_Union__00DF2177]  DEFAULT (0) FOR [G_Union]
GO
ALTER TABLE [dbo].[GuildMember] ADD  CONSTRAINT [DF__GuildMemb__G_Sta__01D345B0]  DEFAULT (0) FOR [G_Status]
GO
ALTER TABLE [dbo].[MEMB_CREDITS] ADD  CONSTRAINT [DF_MEMB_CREDITS_credits]  DEFAULT ((0)) FOR [credits]
GO
ALTER TABLE [dbo].[MEMB_INFO] ADD  CONSTRAINT [DF_MEMB_INFO_mail_chek]  DEFAULT ((0)) FOR [mail_chek]
GO
ALTER TABLE [dbo].[MEMB_INFO] ADD  CONSTRAINT [DF__MEMB_INFO__IsVip__3AA1AEB8]  DEFAULT ((0)) FOR [IsVip]
GO
ALTER TABLE [dbo].[MEMB_INFO] ADD  CONSTRAINT [DF__MEMB_INFO__VipEx__3B95D2F1]  DEFAULT ((0)) FOR [VipExpirationTime]
GO
ALTER TABLE [dbo].[MEMB_INFO] ADD  CONSTRAINT [DF_MEMB_INFO_requestInAction]  DEFAULT ((0)) FOR [requestInAction]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_SEIGE_ENDED]  DEFAULT (0) FOR [SIEGE_ENDED]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_CASTLE_OCCUPY]  DEFAULT (0) FOR [CASTLE_OCCUPY]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_MONEY]  DEFAULT (0) FOR [MONEY]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_Data_TAX_RATE]  DEFAULT (0) FOR [TAX_RATE_CHAOS]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_DATA_TAX_RATE_STORE]  DEFAULT (0) FOR [TAX_RATE_STORE]
GO
ALTER TABLE [dbo].[MuCastle_DATA] ADD  CONSTRAINT [DF_MuCastle_DATA_TAX_HUNT_ZONE]  DEFAULT (0) FOR [TAX_HUNT_ZONE]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_type]  DEFAULT ((0)) FOR [type]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_id]  DEFAULT ((0)) FOR [id]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_item_durability]  DEFAULT ((0)) FOR [durability]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_level]  DEFAULT ((0)) FOR [level]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_skill]  DEFAULT ((0)) FOR [skill]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_luck]  DEFAULT ((0)) FOR [luck]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_options]  DEFAULT ((0)) FOR [options]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_exo]  DEFAULT ((0)) FOR [exo]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_ancient]  DEFAULT ((0)) FOR [ancient]
GO
ALTER TABLE [dbo].[NYX_MARKET] ADD  CONSTRAINT [DF_NYX_MARKET_price_soul]  DEFAULT ((0)) FOR [price_amount]
GO
ALTER TABLE [dbo].[NYX_NEWS] ADD  CONSTRAINT [DF_NYX_NEWS_type]  DEFAULT (N'news') FOR [type]
GO
ALTER TABLE [dbo].[NYX_NEWS] ADD  CONSTRAINT [DF_NYX_NEWS_comments]  DEFAULT ((1)) FOR [comments]
GO
ALTER TABLE [dbo].[NYX_NEWS_COMMENTS] ADD  CONSTRAINT [DF_NYX_NEWS_COMMENTS_rate]  DEFAULT ((0)) FOR [rate]
GO
ALTER TABLE [dbo].[NYX_NEWS_COMMENTS] ADD  CONSTRAINT [DF_NYX_NEWS_COMMENTS_replay]  DEFAULT ((0)) FOR [reply]
GO
ALTER TABLE [dbo].[NYX_REQUESTS] ADD  CONSTRAINT [DF_NYX_REQUESTS_type]  DEFAULT (N'page') FOR [type]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_bless]  DEFAULT ((0)) FOR [bless]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_soul]  DEFAULT ((0)) FOR [soul]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos]  DEFAULT ((0)) FOR [chaos]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1]  DEFAULT ((0)) FOR [life]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_1]  DEFAULT ((0)) FOR [creation]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_apple]  DEFAULT ((0)) FOR [apple]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_2]  DEFAULT ((0)) FOR [box1]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_3]  DEFAULT ((0)) FOR [box2]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_4]  DEFAULT ((0)) FOR [box3]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_5]  DEFAULT ((0)) FOR [box4]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_6]  DEFAULT ((0)) FOR [box5]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_7]  DEFAULT ((0)) FOR [stone]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_8]  DEFAULT ((0)) FOR [rena]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_9]  DEFAULT ((0)) FOR [credits]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_chaos1_10]  DEFAULT ((0)) FOR [zen]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_boh]  DEFAULT ((0)) FOR [boh]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_hol]  DEFAULT ((0)) FOR [bol]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_hol_1]  DEFAULT ((0)) FOR [hol]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_heart]  DEFAULT ((0)) FOR [heart]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_craftCounter]  DEFAULT ((0)) FOR [craftCounter]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_QuestNumber]  DEFAULT ((1)) FOR [QuestNumber]
GO
ALTER TABLE [dbo].[NYX_RESOURCES] ADD  CONSTRAINT [DF_NYX_RESOURCES_recycleCounter]  DEFAULT ((0)) FOR [recycleCounter]
GO
ALTER TABLE [dbo].[OptionData] ADD  CONSTRAINT [DF__OptionDat__ChatW__4A8310C6]  DEFAULT (255) FOR [ChatWindow]
GO
ALTER TABLE [dbo].[T_BC_PCROOM_PLAYCOUNT] ADD  CONSTRAINT [DF_T_BC_PCROOM_PLAYCOUNT_PlayCount]  DEFAULT (1) FOR [PlayCount]
GO
ALTER TABLE [dbo].[T_BC_PCROOM_PLAYCOUNT] ADD  CONSTRAINT [DF_T_BC_PCROOM_PLAYCOUNT_Point]  DEFAULT (0) FOR [Point]
GO
ALTER TABLE [dbo].[T_BLOOD_CASTLE] ADD  CONSTRAINT [DF_T_BLOOD_CASTLE_StoneCount]  DEFAULT (0) FOR [StoneCount]
GO
ALTER TABLE [dbo].[T_BLOOD_CASTLE] ADD  CONSTRAINT [DF_T_BLOOD_CASTLE_Check_Code]  DEFAULT (0) FOR [Check_Code]
GO
ALTER TABLE [dbo].[T_CC_OFFLINE_GIFT] ADD  CONSTRAINT [DF_T_CC_OFFLINE_GIFT_Date_Reg]  DEFAULT (getdate()) FOR [Date_Reg]
GO
ALTER TABLE [dbo].[T_CC_OFFLINE_GIFT] ADD  CONSTRAINT [DF_T_CC_OFFLINE_GIFT_RegCheck]  DEFAULT (0) FOR [RegCheck]
GO
ALTER TABLE [dbo].[T_CurCharName] ADD  CONSTRAINT [DF__T_CurChar__cDate__6BE40491]  DEFAULT (getdate()) FOR [cDate]
GO
ALTER TABLE [dbo].[T_DL_OFFLINE_GIFT] ADD  CONSTRAINT [DF_T_DL_OFFLINE_GIFT_Date_Reg]  DEFAULT (getdate()) FOR [Date_Reg]
GO
ALTER TABLE [dbo].[T_DL_OFFLINE_GIFT] ADD  CONSTRAINT [DF_T_DL_OFFLINE_GIFT_RegCheck]  DEFAULT (0) FOR [RegCheck]
GO
ALTER TABLE [dbo].[T_ENTER_CHECK_BC] ADD  CONSTRAINT [DF_T_ENTER_CHECK_DS_ToDayEnterCheck]  DEFAULT (0) FOR [ToDayEnterCount]
GO
ALTER TABLE [dbo].[T_ENTER_CHECK_BC] ADD  CONSTRAINT [DF_T_ENTER_CHECK_BC_LastEnterDate]  DEFAULT (getdate()) FOR [LastEnterDate]
GO
ALTER TABLE [dbo].[T_FriendList] ADD  CONSTRAINT [DF_T_FriendList_Del]  DEFAULT (0) FOR [Del]
GO
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_MemoIndex]  DEFAULT (10) FOR [MemoIndex]
GO
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_wDate]  DEFAULT (getdate()) FOR [wDate]
GO
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_MemoRead]  DEFAULT (0) FOR [bRead]
GO
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_Dir]  DEFAULT (0) FOR [Dir]
GO
ALTER TABLE [dbo].[T_FriendMail] ADD  CONSTRAINT [DF_T_FriendMemo_Action]  DEFAULT (0) FOR [Act]
GO
ALTER TABLE [dbo].[T_FriendMain] ADD  CONSTRAINT [DF_T_FriendMain_MemoCount]  DEFAULT (10) FOR [MemoCount]
GO
ALTER TABLE [dbo].[T_FriendMain] ADD  CONSTRAINT [DF_T_FriendMain_MemoTotal]  DEFAULT (0) FOR [MemoTotal]
GO
ALTER TABLE [dbo].[T_FRIENDSHIP_SERVERRANK] ADD  CONSTRAINT [DF_T_FRIENDSHIP_SERVERRANK_FriendShipStoneCount]  DEFAULT (0) FOR [FriendShipStoneCount]
GO
ALTER TABLE [dbo].[T_FRIENDSHIP_STONE] ADD  CONSTRAINT [DF_T_T_FriendShipStoneCount]  DEFAULT (0) FOR [FriendShipStoneCount]
GO
ALTER TABLE [dbo].[T_FRIENDSHIP_STONE] ADD  CONSTRAINT [DF_T_T_Check_Code]  DEFAULT (0) FOR [Check_Code]
GO
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_EventChips_1]  DEFAULT (0) FOR [EventChips]
GO
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_MuttoIndex_1]  DEFAULT ((-1)) FOR [MuttoIndex]
GO
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_MuttoNumber_1]  DEFAULT (0) FOR [MuttoNumber]
GO
ALTER TABLE [dbo].[T_MU2003_EVENT] ADD  CONSTRAINT [DF_T_MU2003_EVENT_Check_Code]  DEFAULT (0) FOR [Check_Code]
GO
ALTER TABLE [dbo].[T_PetItem_Info] ADD  CONSTRAINT [DF_T_Pet_Info_Pet_Level]  DEFAULT (0) FOR [Pet_Level]
GO
ALTER TABLE [dbo].[T_PetItem_Info] ADD  CONSTRAINT [DF_T_Pet_Info_Pet_Exp]  DEFAULT (0) FOR [Pet_Exp]
GO
ALTER TABLE [dbo].[T_RegCount_Check] ADD  CONSTRAINT [DF_T_RegCount_Check_RegCount]  DEFAULT (1) FOR [RegCount]
GO
ALTER TABLE [dbo].[T_RegCount_Check] ADD  CONSTRAINT [DF_T_RegCount_Check_RegAlready]  DEFAULT (0) FOR [RegAlready]
GO
ALTER TABLE [dbo].[T_RingAttackEvent_Gift] ADD  CONSTRAINT [DF_T_RingAttackEvent_Gift_RegisterDate]  DEFAULT (getdate()) FOR [RegisterDate]
GO
ALTER TABLE [dbo].[T_Serial_Bank] ADD  CONSTRAINT [DF_T_Serial_Bank_F_Create_Date]  DEFAULT (getdate()) FOR [F_Create_Date]
GO
ALTER TABLE [dbo].[T_Serial_Bank] ADD  CONSTRAINT [DF_T_Serial_Bank_F_RegisterCheck]  DEFAULT (0) FOR [F_RegisterCheck]
GO
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF_warehouse_Money]  DEFAULT (0) FOR [Money]
GO
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF__warehouse__DbVer__3B40CD36]  DEFAULT (0) FOR [DbVersion]
GO
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF__warehouse__pw__40058253]  DEFAULT (0) FOR [pw]
GO
ALTER TABLE [dbo].[warehouse] ADD  CONSTRAINT [DF_warehouse_WHInUse]  DEFAULT (1) FOR [MultiVault]
GO
ALTER TABLE [dbo].[ZenEvent] ADD  CONSTRAINT [DF_ZenEvent_Zen]  DEFAULT (0) FOR [Zen]
GO
ALTER TABLE [dbo].[GuildMember]  WITH CHECK ADD  CONSTRAINT [FK_GuildMember_Guild] FOREIGN KEY([G_Name])
REFERENCES [dbo].[Guild] ([G_Name])
GO
ALTER TABLE [dbo].[GuildMember] CHECK CONSTRAINT [FK_GuildMember_Guild]
GO
ALTER TABLE [dbo].[MEMB_DETA]  WITH CHECK ADD  CONSTRAINT [FK_MEMB_DETA_MEMB_INFO] FOREIGN KEY([memb_guid])
REFERENCES [dbo].[MEMB_INFO] ([memb_guid])
GO
ALTER TABLE [dbo].[MEMB_DETA] CHECK CONSTRAINT [FK_MEMB_DETA_MEMB_INFO]
GO
/****** Object:  StoredProcedure [dbo].[_SP_ENTER_CHECK_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



--//************************************************************************
--// і»   їл 	: єн·ЇµеДіЅЅ 1АП АФАеБ¦ЗС АъАеЗБ·ОЅГАъ
--// єО   ј­ 	: °ФАУ°і№ЯЖА 
--// ёёµзіЇ 	: 2004.03.08
--// ёёµзАМ 	: БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[_SP_ENTER_CHECK_BC]
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Server		smallint		-- ј­№ц
As
Begin
	DECLARE	@iMaxEnterCheck	INT
	DECLARE	@iNowEnterCheck	INT

	SET		@iMaxEnterCheck	= 6		-- АПАП ГЦґл АФАе°ЎґЙ ИЅјц
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT AccountID FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
				WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName )
	BEGIN
		SELECT @iNowEnterCheck = ToDayEnterCount 
		FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
		WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName

		IF (@iNowEnterCheck >= @iMaxEnterCheck)
		BEGIN
			SELECT 0 As EnterResult	-- єн·ЇµеДіЅЅ АФАеЅЗЖР
		END
		ELSE
		BEGIN
			UPDATE T_ENTER_CHECK_BC 
			SET ToDayEnterCount = ToDayEnterCount + 1, LastEnterDate = GetDate()
			WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName

			SELECT 1 As EnterResult	-- єн·ЇµеДіЅЅ АФАејє°ш
		END
		
	END
	ELSE
	BEGIN
		INSERT INTO T_ENTER_CHECK_BC ( AccountID, CharName, ServerCode, ToDayEnterCount, LastEnterDate ) VALUES (
			@AccountID,
			@CharacterName,
			@Server,
			1,
			DEFAULT
		)
	
		SELECT 1 As EnterResult		-- єн·ЇµеДіЅЅ АФАејє°ш
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[_SP_POINT_ACCM_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.08. 16
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[_SP_POINT_ACCM_BC]
	@Server		smallint,		-- ј­№ц 
	@Bridge		tinyint,		-- ґЩё®
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Class			tinyint ,		-- Е¬·ЎЅє №шИЈ 	(0-Ижё¶№э»з, 1-јТїпё¶ЅєЕН, 16-Иж±в»з, 17-єн·№АМµеіЄАМЖ®, 32-їдБ¤, 33-№ВБої¤ЗБ, 48-ё¶°Л»з)
	@Point			int,		-- ЖчАОЖ®
	@PCRoomGUID	int		-- PC№ж GUID (0АМёй µо·ПµИ PC№ж ѕЖґФ?)
As
Begin
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT CharacterName FROM EVENT_INFO_BC  WITH (READUNCOMMITTED) 
				WHERE  Server = @Server AND AccountID = @AccountID AND CharacterName = @CharacterName )
		Begin			
			DECLARE @iiiPoint	int
			SELECT @iiiPoint = Point FROM EVENT_INFO_BC
			WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName

			IF (@iiiPoint + @Point < 0)
			BEGIN
				UPDATE EVENT_INFO_BC
				SET Point = 0 , Bridge = @Bridge, PlayCount = PlayCount+1 										 										
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END	
			ELSE
			BEGIN
				UPDATE EVENT_INFO_BC
				SET Point = Point + @Point , Bridge = @Bridge, PlayCount = PlayCount+1 										
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END
		End
	ELSE
		Begin
			INSERT INTO EVENT_INFO_BC ( Server,  Bridge, AccountID, CharacterName, Class, Point, PlayCount ) VALUES (
				@Server,
				@Bridge,
				@AccountID,
				@CharacterName,
				@Class,
				@Point,
				default	
			)
		End


	IF (@PCRoomGUID <> 0)
	Begin
		IF EXISTS (SELECT AccountID FROM T_BC_PCROOM_PLAYCOUNT  WITH (READUNCOMMITTED) 
				WHERE  PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID)
		Begin
			UPDATE T_BC_PCROOM_PLAYCOUNT
			SET PlayCount = PlayCount + 1, Point = Point + @Point
			WHERE PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID		
		End
		ELSE
		Begin
			INSERT INTO T_BC_PCROOM_PLAYCOUNT
			VALUES (@PCRoomGUID, @AccountID, default, @Point)
		End
	End


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[CItemExeSP]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[CItemExeSP] @Name VARCHAR(10)
AS
BEGIN
	select cName from CItemExt where Name=@Name
END

GO
/****** Object:  StoredProcedure [dbo].[DupeMaker]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DupeMaker]
AS
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[MuMakerDupeFinder]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1)
 TRUNCATE TABLE [dbo].[MuMakerDupeFinder]
ELSE
 CREATE TABLE [dbo].[MuMakerDupeFinder] ( [ID] [bigint] NOT NULL Primary Key IDENTITY, [AC] [varchar] (10) NOT NULL, [PJ] [varchar] (10) NOT NULL, [Item] [varchar] (32) NOT NULL, [ItemID] [varchar] (2) NOT NULL, [ItemTP] [varchar] (2) NOT NULL, [Serial] [varchar] (8) NOT NULL, [Posicion] [smallint] NOT NULL, [Vault] [smallint] NOT NULL, [Invent] [smallint] NOT NULL, [ExtraW] [smallint] NOT NULL, [NumExt] [smallint] NOT NULL, [Items] [varchar] (4000) NOT NULL)
GO
/****** Object:  StoredProcedure [dbo].[ItemMakerInventary]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemMakerInventary] (@AccountID VARCHAR(10), @Name VARCHAR(10))
AS
UPDATE Character
SET Inventory=0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
WHERE AccountID=@AccountID AND Name=@Name
GO
/****** Object:  StoredProcedure [dbo].[ItemMakerWareHous]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ItemMakerWareHous] (@AccountID VARCHAR(10))
AS
INSERT INTO warehouse (AccountID, Items, Money) VALUES (@AccountID, 0xFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF, 0)
GO
/****** Object:  StoredProcedure [dbo].[MMK_ItemMakerInventory]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MMK_ItemMakerInventory] (@AccountID VARCHAR(10), @Name VARCHAR(10))
AS
UPDATE Character
SET Inventory=0x00000000000000000000
WHERE AccountID=@AccountID AND Name=@Name
GO
/****** Object:  StoredProcedure [dbo].[MMK_ItemMakerWareHous]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MMK_ItemMakerWareHous] (@AccountID VARCHAR(10))
AS
UPDATE warehouse
SET Items=0x1AFF9598FC7A007F0000AD0000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
WHERE AccountID=@AccountID
GO
/****** Object:  StoredProcedure [dbo].[QuestMaker]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[QuestMaker] (@AccountID VARCHAR(10), @Name VARCHAR(10))
AS
UPDATE Character
SET Quest=0xFFFFFF
WHERE AccountID=@AccountID AND Name=@Name
GO
/****** Object:  StoredProcedure [dbo].[ShowResourceInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ShowResourceInfo]
    AS
    SET NOCOUNT ON
    print '*********************************'
    print ' 5ГК°Ј »зїлµИ ЅГЅєЕЫ АЪїш·®АФґПґЩ.'
    print '*********************************'
    SELECT spid, cpu, physical_io 
    INTO #Temp1 FROM master..sysprocesses
    WAITFOR DELAY '0:00:05'  
    SELECT P.spid, P.cpu-T.cpu AS 'cpu »зїл·®(ms)', 
    P.physical_io-T.physical_io AS 'Disk R/W·®(Page)',
    P.nt_username ' NT »зїлАЪён', P.program_name 'ААїлЗБ·О±Ч·Ґ', 
    P.hostname, P.cmd
    FROM master..sysprocesses P, #Temp1 T
    WHERE P.spid=T.spid 
    AND P.cpu-T.cpu+P.physical_io-T.physical_io+P.memusage > 0
    DROP TABLE #Temp1
    Return
    


GO
/****** Object:  StoredProcedure [dbo].[SkillMaker]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SkillMaker] (@AccountID VARCHAR(10), @Name VARCHAR(10))
AS
UPDATE Character
SET MagicList=0xFF0000
WHERE AccountID=@AccountID AND Name=@Name
GO
/****** Object:  StoredProcedure [dbo].[SP_CHECK_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ 1АП АФАеБ¦ЗС
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.08. 16
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_CHECK_BC]
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Server		smallint		-- ј­№ц
As
Begin
	DECLARE	@iMaxEnterCheck	INT
	DECLARE	@iNowEnterCheck	INT

	SET		@iMaxEnterCheck	= 6		-- АПАП ГЦґл АФАе°ЎґЙ ИЅјц
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT AccountID FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
				WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName )
	BEGIN
		SELECT @iNowEnterCheck = ToDayEnterCount 
		FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
		WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName

		IF (@iNowEnterCheck >= @iMaxEnterCheck)
		BEGIN
			SELECT 0 As EnterResult	-- єн·ЇµеДіЅЅ АФАе єТ°ЎґЙ
		END
		ELSE
		BEGIN
			SELECT 1 As EnterResult	-- єн·ЇµеДіЅЅ АФАе °ЎґЙ
		END
		
	END
	ELSE
	BEGIN
		SELECT 1 As EnterResult		-- єн·ЇµеДіЅЅ АФАе °ЎґЙ
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_DAY_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[SP_DAY_SELECT]
(
	@Year	smallint,
	@Month	tinyint,
	@Day	tinyint,
	@SearchOption	tinyint,		-- °Л»ц їЙјЗ 
	@Server	tinyint				-- Server Б¤єё 			
	
)
As
Begin
	SET NOCOUNT ON

	Declare	 @SearchDate	Varchar(50)
	Declare	 @StartDate	Varchar(50)
	Declare	 @DistServer	tinyint
	Declare	 @DistServer_sub		tinyint
	
	Set	@SearchDate = Convert(varchar, datepart( year ,  Getdate()   ))   + '-'+ 
			   Convert( varchar ,  datepart( month ,  Getdate()   ) )  + '-'+ 
			   Convert( varchar, datepart( day , Getdate()   )  )		

	Set	@StartDate = Convert(varchar, @Year)  + '-'+  Convert(varchar, @Month)  + '-'+ Convert(varchar, @Day  )	

	--//  ј­№ц№шИЈ = ј­№цДЪµе / 20 + 1
	--//  єОј­№ц№шИЈ = ј­№цДЪµе % 20 + 1	
	Set	@DistServer =  ( @Server / 20 ) + 1
	if( @Server >= 240 )
		Set	@DistServer = 12 	-- Test Server 

	Set	@DistServer_sub  =  ( @Server % 20 ) + 1

	if(@SearchOption = 0)
		begin
			-- ЕлЗХµИ Б¤єё їдГ» 
			if ( @Year <>  0 )  
				-- ЗцАзАЗ µїБў Б¤єёё¦ »кГвЗСґЩ. 
				begin 
					select 
						datePart( year, ConnectTime) As [Year] ,
						datePart( month, ConnectTime) As [Month],
						datePart( day, ConnectTime ) As [Day] ,
						datePart( hour, ConnectTime) As [Hour] ,			
						datePart( Minute, ConnectTime) As [Minute] ,			
						TotalCount 
					from dbo.Day_Statistics 
					With( READUNCOMMITTED )
					Where 			
					ConnectTime Between Convert( datetime , @StartDate + '  00:00:00' )  and  Convert( datetime,   @StartDate + '  23:59:59' )
				end
			else
				-- їдГ»ЗС µїБўБ¤єёё¦ »кГвЗСґЩ. 
				begin		
					select 
						datePart( year, ConnectTime) As [Year] ,
						datePart( month, ConnectTime) As [Month],
						datePart( day, ConnectTime ) As [Day] ,
						datePart( hour, ConnectTime) As [Hour] ,			
						datePart( Minute, ConnectTime) As [Minute] ,			
						TotalCount 
					from dbo.Day_Statistics 
					With( READUNCOMMITTED )
					where
			          		 ConnectTime  Between  Convert( datetime , @SearchDate + '  00:00:00' )  and  Convert( datetime,   @SearchDate + '  23:59:59' )
				end 
		end 
	else
		begin
			--// ј­№цє° БўјУ Б¤єёё¦ Return ЗСґЩ. 
			select 
				datePart( year, ConnectTime) As [Year] ,
				datePart( month, ConnectTime) As [Month],
				datePart( day, ConnectTime ) As [Day] ,
				datePart( hour, ConnectTime) As [Hour] ,			
				datePart( Minute, ConnectTime) As [Minute] ,			
				ConnectCount As TotalCount 
			from SameTimeConnectLog 
			With( READUNCOMMITTED )
			where
				Server = @DistServer AND SubServer = @DistServer_sub
	     			AND ConnectTime  Between  Convert( datetime , @StartDate + '  00:00:00' )  and  Convert( datetime,   @StartDate + '  23:59:59' )							
		end
	SET NOCOUNT OFF
end 


GO
/****** Object:  StoredProcedure [dbo].[SP_DIST_DAY_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_DIST_DAY_SELECT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ЗцАзіЇВҐ ¶ЗґВ ј±ЕГЗС іЇВҐїЎ ґлЗС µїБўЗцИІА» Дхё®ЗПґВ ЗБ·ОЅГБ® 
		
*/

CREATE PROCEDURE [dbo].[SP_DIST_DAY_SELECT]
(
	@Year	smallint,
	@Month	tinyint,
	@Day	tinyint,
	@Server	tinyint				-- Server Б¤єё 				
)
As
Begin
	SET NOCOUNT ON

	Declare	 @SearchDate	Varchar(50)
	Declare	 @StartDate	Varchar(50)
	Declare	 @DistServer	tinyint
	Declare	 @DistServer_sub		tinyint
	
	Set	@StartDate = Convert(varchar, @Year)  + '-'+  Convert(varchar, @Month)  + '-'+ Convert(varchar, @Day  )	

	select 
		datePart( year, ConnectTime) As [Year] ,
		datePart( month, ConnectTime) As [Month],
		datePart( day, ConnectTime ) As [Day] ,
		datePart( hour, ConnectTime) As [Hour] ,			
		datePart( Minute, ConnectTime) As [Minute] ,			
		ConnectCount As TotalCount,
		SubServer 
	from SameTimeConnectLog 
	With( READUNCOMMITTED )
	where
		Server = @Server
			AND ConnectTime  Between  Convert( datetime , @StartDate + '  00:00:00' )  
		and  Convert( datetime,   @StartDate + '  23:59:59' )
	order by SubServer , ConnectTime asc 		
	
	SET NOCOUNT OFF

End

GO
/****** Object:  StoredProcedure [dbo].[SP_DIST_LOG_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_DIST_LOG_INSERT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		 БўјУј­№цїЎј­ АьґЮµИ ј­№цє° єРё®µИ Б¤єё АъАеЗПґВ ЗБ·ОЅГБ® .  
		
*/

CREATE PROCEDURE [dbo].[SP_DIST_LOG_INSERT]
(
	@Insert_Date	datetime,
	@Server	tinyint,
	@SubServer	tinyint,
	@ConnectCount	int		
) 
As
Begin
	
	SET NOCOUNT ON
	
	Begin Transaction

	Insert into SameTimeConnectLog(ConnectTime, Server, SubServer, ConnectCount) Values (
	@Insert_Date,
	@Server,
	@SubServer,
	@ConnectCount
	)
	
	if(@@Error <> 0 )
		RollBack Tran
	else	
		Commit Tran

	SET NOCOUNT OFF
			
End

GO
/****** Object:  StoredProcedure [dbo].[SP_DT_DAY_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) PROCEDURE NAME : SP_DT_DAY_INSERT
	2) JOB DATE : 2003. 3. 25.
	3) PROGRAMMER :  Chu Souk
	4) Job Information
					
*/
Create Procedure	[dbo].[SP_DT_DAY_INSERT]
	@CurrentDateTime	datetime,		-- ±в°Ј
	@Type	tinyint,						-- ЕёАФ PC 0 , °іАО 1
	@TotalCount	int						-- БўјУБ¤єё 
As
Begin
	
	SET NOCOUNT ON
	BEGIN TRANSACTION

	INSERT DT_Day_Statistics ( ConnectTime, Type, TotalCount ) 
	VALUES( @CurrentDateTime, @Type, @TotalCount)

	IF(@@error <> 0 )
		ROLLBACK TRANSACTION
	ELSE
		COMMIT TRANSACTION	
	

	SET NOCOUNT OFF

End 


GO
/****** Object:  StoredProcedure [dbo].[SP_DT_DAY_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) PROCEDURE NAME : SP_DT_DAY_INSERT
	2) JOB DATE : 2003. 3. 25.
	3) PROGRAMMER :  Chu Souk
	4) Job Information
					
*/
CREATE Procedure [dbo].[SP_DT_DAY_SELECT]
(
	@Year	smallint,
	@Month	tinyint,
	@Day	tinyint,
	@SearchOption	tinyint		-- °Л»ц їЙјЗ  ( ЕёАФ PC 0 , °іАО 1, АьГј 2) 
)
As
Begin
	SET NOCOUNT ON

	Declare	 @SearchDate	Varchar(50)
	Declare	 @StartDate	Varchar(50)
	Declare	 @DistServer	tinyint
	Declare	 @DistServer_sub		tinyint

	Set	@StartDate = Convert(varchar, @Year)  + '-'+  Convert(varchar, @Month)  + '-'+ Convert(varchar, @Day  )	

	IF ( @SearchOption = 2 )
		Begin
			select 
				datePart( year, ConnectTime) As [Year] ,
				datePart( month, ConnectTime) As [Month],
				datePart( day, ConnectTime ) As [Day] ,
				datePart( hour, ConnectTime) As [Hour] ,			
				datePart( Minute, ConnectTime) As [Minute] ,			
				Type, TotalCount		
			from DT_Day_Statistics 
			With( READUNCOMMITTED )
			where  ConnectTime  Between  Convert( datetime , @StartDate + '  00:00:00' )  
				and  Convert( datetime,   @StartDate + '  23:59:59' )							
		End 
	Else 	
		Begin
			select 
				datePart( year, ConnectTime) As [Year] ,
				datePart( month, ConnectTime) As [Month],
				datePart( day, ConnectTime ) As [Day] ,
				datePart( hour, ConnectTime) As [Hour] ,			
				datePart( Minute, ConnectTime) As [Minute] ,			
				Type, TotalCount		
			from DT_Day_Statistics 
			With( READUNCOMMITTED )
			where  ConnectTime  Between  Convert( datetime , @StartDate + '  00:00:00' )  
				and  Convert( datetime,   @StartDate + '  23:59:59' )	
				and Type = 	@SearchOption
		End
	


	SET NOCOUNT OFF
End


GO
/****** Object:  StoredProcedure [dbo].[SP_DT_STATISTICS]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_DT_STATISTICS
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ЗцАз(°іАО°ъ PC№ж µїБў) БўјУБ¤єё LOG µїБўБ¤єё Ел°и
		
*/

CREATE PROCEDURE [dbo].[SP_DT_STATISTICS]
As
Begin	
	SET NOCOUNT ON	

	BEGIN TRANSACTION

	DECLARE @YesterDay	datetime
	DECLARE @SearchDay	char(10)
	DECLARE @Week		tinyint
	DECLARE @WeekDay	tinyint
	DECLARE @Year		smallint

	SET		@YesterDay	= DateAdd( day, -1, getdate())		-- ЗцАз ЅГЅєЕЫАЗ ѕоБ¦ іЇВҐё¦ ±ёЗСґЩ
	SET		@SearchDay = Convert(varchar, datepart( year ,  @YesterDay  ))   + '-'+ 
						   Convert( varchar ,  datepart( month ,  @YesterDay  ) )  + '-'+ 
						   Convert( varchar, datepart( day , @YesterDay  )  )		
	SET		 @Week	= DatePart( Week, @YesterDay)
	SET		 @WeekDay	= DatePart( Weekday, @YesterDay)
	SET		 @Year		= DatePart( Year, @YesterDay)

	
	--/////////////////////////////////////////////////////////////////////////
	--// БЦґЬА§ µїБўБ¤єё Ел°и ГЯГв( ЗСЅГ°Ј ґЬА§АЗ Ел°иБ¤єёё¦ ГЯГв)		 		
	--/////////////////////////////////////////////////////////////////////////
	INSERT INTO DT_WeekDay_Statistics( 
		Year, Week, WeekDay, Type,  [Hour], MaxCount, MinCount, AvgCount
	)   SELECT  
		@Year		As Year,
		@Week	As Week,
		@WeekDay	As WeekDay,			
		Type, 
		datepart(Hour, ConnectTime) As Hour,		
		Max(TotalCount) As MaxCount,
		Min(TotalCount) As MinCount,
		Avg(TotalCount) As AvgCount
    	FROM  DT_Day_Statistics
	WHERE ConnectTime Between Convert( datetime , @SearchDay + '  00:00:00' )  
		and  Convert( datetime,   @SearchDay + '  23:59:59' )
	group by datepart(hour, ConnectTime), Type			


	--/////////////////////////////////////////////////////////////////////////
	--// їщґЬА§ µїБўБ¤єё Ел°и ГЯГв( ЗСЅГ°Ј ґЬА§АЗ Ел°иБ¤єёё¦ ГЯГв)		 		
	--/////////////////////////////////////////////////////////////////////////
	DECLARE	@Step		tinyint	
	DECLARE	@StartHour	tinyint
	DECLARE	@StrStartHour	varchar(15)
	DECLARE	@StrEndHour	varchar(15)
	DECLARE	@EndHour	tinyint
	SET @Step = 1
	

	WHILE( @Step < 7 ) 
	Begin
		
		if( @Step = 1) 
			SET @StartHour = 0
		else 	
			SET @StartHour = ((@Step-1)*4)-1

		SET @EndHour = (@Step*4)-1
		
		SET @StrStartHour = ' ' +  CONVERT(varchar, @StartHour) + ':00:00'				
		SET @StrEndHour = ' ' +  CONVERT(varchar, @EndHour) + ':59:59' 

		INSERT INTO DT_Week_Statistics( 
			Year, Week, WeekDay,  HalfDay, Type,  MaxCount, MinCount, AvgCount		
		)   SELECT  
			@Year		As Year, 
			@Week	As Week,
			@WeekDay	As WeekDay,
			(@Step-1)	As HalfDay, 
			Type, 
			Max(TotalCount) As MaxCount,
			Min(TotalCount) As MinCount,
			Avg(TotalCount) As AvgCount
    	    	FROM  DT_Day_Statistics
		WHERE ConnectTime Between Convert( datetime , @SearchDay + @StrStartHour )  
			and  Convert( datetime,   @SearchDay + @StrEndHour  )
		group by Type
		having Max(TotalCount) > 0 

		SET @Step = @Step + 1
	End


	--/////////////////////////////////////////////////////////////////////////
	--// івґЬА§ µїБўБ¤єё Ел°и ГЯГв( АПАП ґЬА§АЗ Ел°иБ¤єёё¦ ГЯГв)		 		
	--/////////////////////////////////////////////////////////////////////////
	INSERT INTO DT_Year_Statistics( 
		Year, Month,  Day, Type,  MaxCount, MinCount, AvgCount		
	)   SELECT  
		Convert(varchar, datepart( year ,  @YesterDay  )) As Year,
		Convert(varchar, datepart( month ,  @YesterDay  )) As month,
		Convert(varchar, datepart( day ,  @YesterDay  )) As day,
		Type, 
		Max(TotalCount) As MaxCount,
		Min(TotalCount) As MinCount,
		Avg(TotalCount) As AvgCount	
 	FROM  DT_Day_Statistics
	WHERE ConnectTime Between Convert( datetime , @SearchDay + '  00:00:00' )  
			and  Convert( datetime,   @SearchDay + '  23:59:59' )	Group by Type
	
	if(@@ERROR <> 0 )
		ROLLBACK TRAN
	else	
		COMMIT TRAN


	SET NOCOUNT OFF
			
End

GO
/****** Object:  StoredProcedure [dbo].[SP_DT_WEEK_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) PROCEDURE NAME : SP_DT_WEEK_SELECT
	2) JOB DATE : 2003. 3. 25.
	3) PROGRAMMER :  Chu Souk
	4) Job Information
					
*/

CREATE PROCEDURE [dbo].[SP_DT_WEEK_SELECT]
(
	@Year		char(4),			-- ЖЇБ¤ їщ 
	@Month		char(2),			-- ЖЇБ¤ ґЮ 
	@Type		tinyint			-- ЕёАФБ¤єё
) 
As
Begin

	--//////////////////////////////////////////////////////////////////////////////////////////
	-- БЦґЬА§АЗ Б¤єёё¦ АУЅГЕЧАМєнїЎ АъАе 
	Create Table #Temp(		
		WeekIndex	smallint,		-- АОµ¦ЅєЕ°
		Week		smallint,		-- БЦБ¤єёё¦ »кГв
		WeekDay	smallint,		-- ЅГ°ЈБ¤єё (12ЅГ°Ј ґЬА§)
		Type		tinyint, 
		MaxCount	int,
		MinCount	int, 
		AvgCount	int		
	)  	
	--//////////////////////////////////////////////////////////////////////////////////////////

	
	SET NOCOUNT ON
		
	Declare	@SetDate	datetime
	Declare  @StartWeek	tinyint	--	1 Byte ( 0 ~  255) 
	Declare  @Count		int

	-- ЗцАз іС°ЬБш ЖД¶уёЮЕёАЗ °ЄИ®АО 
	if( @Year = 0 )
	begin
		Set	@Year = Datepart( Year, getdate())
		Set	@Month = Datepart( Month, getdate())
	end

	-- ЖЇБ¤ґЮїЎ ґлЗС Г№В°іЇА» »кГв 
	Set @SetDate = ConVert(datetime, @Year + '-' + @Month + '-01 00:00:00')
	
	-- ЅГАЫµЗґВ БЦБ¤єёё¦ ѕтѕоµ·ґЩ. 	
	Set @StartWeek = DatePart(Week, @SetDate)	
	Set @Count = 0
	
	-- БЦБ¤єёё¦ °Л»цЗСґЩ. (ЗШґз ґЮАЗ 5В°БЦ Б¤єёё¦ ѕтѕоїВґЩ.)
	While(@Count < 5)
	begin
		
		--  Д«їоЖ®°Є Бх°Ў
		Set  @Count =  @Count + 1	
			
		--  ЗШґз Б¤єё »кГв 
		If( @Type =  2 )
			begin
				INSERT INTO #Temp( WeekIndex, Week, WeekDay, Type,  MaxCount, MinCount, AvgCount)
					SELECT 
						@Count As WeekIndex , 
						Week, 
						WeekDay, 	
						Type, 
						MaxCount,  				
						MinCount, 
						AvgCount  
					From DT_Week_Statistics 	
					Where Week = @StartWeek											
			end
		else 
			begin
				INSERT INTO #Temp( WeekIndex, Week, WeekDay, Type,  MaxCount, MinCount, AvgCount)
					SELECT 
						@Count As WeekIndex , 
						Week, 
						WeekDay, 	
						Type, 
						MaxCount,  				
						MinCount, 
						AvgCount  
					From DT_Week_Statistics 	
					Where Week = @StartWeek	
						And Type = @Type	
			end 

		Set	@StartWeek = @StartWeek + 1

	end 	

	Select * from #Temp
	Drop Table #Temp	
	
	SET NOCOUNT OFF
			
End


GO
/****** Object:  StoredProcedure [dbo].[SP_DT_WEEKDAY_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) PROCEDURE NAME : SP_DT_WEEKDAY_SELECT
	2) JOB DATE : 2003. 3. 25.
	3) PROGRAMMER :  Chu Souk
	4) Job Information
					
*/
CREATE Procedure [dbo].[SP_DT_WEEKDAY_SELECT]
(
	@Year		smallint, 
	@WEEK		smallint,
	@Type		tinyint
)
As
Begin
	SET NOCOUNT ON

	-- ЗцАз БЦїЎ ґлЗС 	
	if ( @WEEK = 0 )
	begin
		SET	@WEEK = DatePart( wk, getdate() )  
		SET	@Year =  DatePart( year, getdate() )  
	End	
		
	IF (@Type = 2) 
		Begin
			SELECT WeekDay, [Hour], MaxCount, MinCount, AvgCount FROM DT_WeekDay_Statistics
			WHERE Week = @WEEK and Year = @Year
		End
	ELSE
		Begin
			SELECT WeekDay, [Hour], MaxCount, MinCount, AvgCount FROM DT_WeekDay_Statistics
			WHERE Week = @WEEK and Year = @Year and Type = @Type
		End				

	SET NOCOUNT OFF
end



GO
/****** Object:  StoredProcedure [dbo].[SP_DT_YEAR_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	1) PROCEDURE NAME : SP_DT_YEAR_SELECT
	2) JOB DATE : 2003. 3. 25.
	3) PROGRAMMER :  Chu Souk
	4) Job Information
					
*/
CREATE Procedure [dbo].[SP_DT_YEAR_SELECT]
(
	@YEAR	smallint,
	@Type	tinyint
)
As
Begin
	SET NOCOUNT ON

	-- ЗцАз івµµїЎ ґлЗС Б¤єёё¦ °ЎБ®їВґЩ. 	
	if ( @YEAR = 0 )
	begin
		SET	@YEAR = DatePart( year, getdate() )  
	End	
	
	If( @Type = 2 ) 
		Begin
			SELECT Month, Day,   MaxCount, MinCount, AvgCount FROM DT_Year_Statistics
			WHERE Year = @YEAR
		end 
	else 
		Begin
			SELECT Month, Day,   MaxCount, MinCount, AvgCount FROM DT_Year_Statistics
			WHERE Year = @YEAR and Type = @Type 
		End 
	SET NOCOUNT OFF
end



GO
/****** Object:  StoredProcedure [dbo].[SP_ENTER_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ 1АП АФАеБ¦ЗС
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.08. 16
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_ENTER_BC]
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Server		smallint		-- ј­№ц
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT AccountID FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
				WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName )
	BEGIN
		UPDATE T_ENTER_CHECK_BC 
		SET ToDayEnterCount = ToDayEnterCount + 1, LastEnterDate = GetDate()
		WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName
	END
	ELSE
	BEGIN
		INSERT INTO T_ENTER_CHECK_BC ( AccountID, CharName, ServerCode, ToDayEnterCount, LastEnterDate ) VALUES (
			@AccountID,
			@CharacterName,
			@Server,
			1,
			DEFAULT
		)
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[SP_LEFT_ENTERCOUNT_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ 1АП іІАє АФАеИЅјц Б¶Иё
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.04. 02
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_LEFT_ENTERCOUNT_BC]
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Server		smallint		-- ј­№ц
As
Begin
	DECLARE	@iMaxEnterCheck	INT
	DECLARE	@iNowEnterCheck	INT

	SET		@iMaxEnterCheck	= 6		-- АПАП ГЦґл АФАе°ЎґЙ ИЅјц
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT AccountID FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED) 
				WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName )
	BEGIN
		SELECT @iMaxEnterCheck - ToDayEnterCount As LeftEnterCount FROM T_ENTER_CHECK_BC  WITH (READUNCOMMITTED)	-- єн·ЇµеДіЅЅ іІАє АФАеИЅјц
		WHERE  AccountID = @AccountID AND ServerCode = @Server AND CharName = @CharacterName
	END
	ELSE
	BEGIN
		INSERT INTO T_ENTER_CHECK_BC ( AccountID, CharName, ServerCode, ToDayEnterCount, LastEnterDate ) VALUES (
			@AccountID,
			@CharacterName,
			@Server,
			0,
			GetDate()
		)
	
		SELECT @iMaxEnterCheck As LeftEnterCount		-- єн·ЇµеДіЅЅ іІАє АФАеИЅјц
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[SP_MD5_ENCODE_VALUE]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[SP_MD5_ENCODE_VALUE] 

	@btInStr		VARCHAR(10),
	@btInStrIndex		VARCHAR(10)

--RETURNS BINARY(16)
--Created By WebZen
--Adapted By [CzF]Deathway
AS  
BEGIN 
	DECLARE	@btOutVal	BINARY(16)

	EXEC master..XP_MD5_EncodeKeyVal @btInStr, @btInStrIndex, @btOutVal OUT
	UPDATE MEMB_INFO SET memb__pwd = @btOutVal WHERE memb___id = @btInStrIndex
	RETURN 	@btOutVal
END
GO
/****** Object:  StoredProcedure [dbo].[SP_POINT_ACCM_BC]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.08. 16
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_POINT_ACCM_BC]
	@Server		smallint,		-- ј­№ц 
	@Bridge		tinyint,		-- ґЩё®
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Class			tinyint ,		-- Е¬·ЎЅє №шИЈ 	(0-Ижё¶№э»з, 1-јТїпё¶ЅєЕН, 16-Иж±в»з, 17-єн·№АМµеіЄАМЖ®, 32-їдБ¤, 33-№ВБої¤ЗБ, 48-ё¶°Л»з)
	@Point			int,		-- ЖчАОЖ®
	@PCRoomGUID	int		-- PC№ж GUID (0АМёй µо·ПµИ PC№ж ѕЖґФ?)
As
Begin
	DECLARE	@TEMP	INT
End	


GO
/****** Object:  StoredProcedure [dbo].[SP_POINT_ACCM_BC_3RD]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.04.02
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_POINT_ACCM_BC_3RD]
	@Server		smallint,		-- ј­№ц 
	@Bridge		tinyint,		-- ґЩё®
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Class			tinyint ,		-- Е¬·ЎЅє №шИЈ 	(0-Ижё¶№э»з, 1-јТїпё¶ЅєЕН, 16-Иж±в»з, 17-єн·№АМµеіЄАМЖ®, 32-їдБ¤, 33-№ВБої¤ЗБ, 48-ё¶°Л»з)
	@Point			int,		-- ЖчАОЖ®
	@PCRoomGUID	int,		-- PC№ж GUID (0АМёй µо·ПµИ PC№ж ѕЖґФ?)
	@LeftTime		int		-- єн·ЇµеДіЅЅ °ж±в іІАєЅГ°Ј ґ©Аы
As
Begin
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	
/*
	IF EXISTS ( SELECT CharacterName FROM EVENT_INFO_BC_3RD  WITH (READUNCOMMITTED) 
				WHERE  Server = @Server AND AccountID = @AccountID AND CharacterName = @CharacterName )
		Begin			
			DECLARE @iiiPoint	int
			SELECT @iiiPoint = Point FROM EVENT_INFO_BC_3RD
			WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName

			IF (@iiiPoint + @Point < 0)
			BEGIN
				UPDATE EVENT_INFO_BC_3RD
				SET Point = 0 , Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime  										 										
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END	
			ELSE
			BEGIN
				UPDATE EVENT_INFO_BC_3RD
				SET Point = Point + @Point , Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime 
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END
		End
	ELSE
		Begin
			INSERT INTO EVENT_INFO_BC_3RD ( Server,  Bridge, AccountID, CharacterName, Class, Point, PlayCount, SumLeftTime ) VALUES (
				@Server,
				@Bridge,
				@AccountID,
				@CharacterName,
				@Class,
				@Point,
				1,
				@LeftTime	
			)
		End


	IF (@PCRoomGUID <> 0)
	Begin
		IF EXISTS (SELECT AccountID FROM T_BC_PCROOM_PLAYCOUNT  WITH (READUNCOMMITTED) 
				WHERE  PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID)
		Begin
			UPDATE T_BC_PCROOM_PLAYCOUNT
			SET PlayCount = PlayCount + 1, Point = Point + @Point
			WHERE PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID		
		End
		ELSE
		Begin
			INSERT INTO T_BC_PCROOM_PLAYCOUNT
			VALUES (@PCRoomGUID, @AccountID, default, @Point)
		End
	End

*/
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_POINT_ACCM_BC_4TH]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы (4Вч)
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.05. 31
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_POINT_ACCM_BC_4TH]
	@Server		smallint,		-- ј­№ц 
	@Bridge		tinyint,		-- ґЩё®
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Class			tinyint ,		-- Е¬·ЎЅє №шИЈ 	(0-Ижё¶№э»з, 1-јТїпё¶ЅєЕН, 16-Иж±в»з, 17-єн·№АМµеіЄАМЖ®, 32-їдБ¤, 33-№ВБої¤ЗБ, 48-ё¶°Л»з)
	@Point			int,		-- ЖчАОЖ®
	@PCRoomGUID	int,		-- PC№ж GUID (0АМёй µо·ПµИ PC№ж ѕЖґФ?)
	@LeftTime		int		-- єн·ЇµеДіЅЅ °ж±в іІАєЅГ°Ј ґ©Аы
As
Begin
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	
/*
	IF EXISTS ( SELECT CharacterName FROM EVENT_INFO_BC_4TH  WITH (READUNCOMMITTED) 
				WHERE  Server = @Server AND AccountID = @AccountID AND CharacterName = @CharacterName )
		Begin			
			DECLARE @iiiPoint		int
			DECLARE @iMinLeftTime	int
			SELECT @iiiPoint = Point, @iMinLeftTime = MinLeftTime FROM EVENT_INFO_BC_4TH
			WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName

			IF (@iMinLeftTime < @LeftTime)
			BEGIN
				IF (@Point > 0)			-- ДіЅЅїЎ јє°ш ЗЯА» ¶§ ёё ГЦґЬЅГ°Ј БЎјц°Ў °»ЅЕµЙ °НАМґЩ.
				BEGIN
					SET @iMinLeftTime = @LeftTime
				END
			END

			IF (@iiiPoint + @Point < 0)
			BEGIN
				UPDATE EVENT_INFO_BC_4TH
				SET Point = 0 , Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime, MinLeftTime = @iMinLeftTime 
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END	
			ELSE
			BEGIN
				UPDATE EVENT_INFO_BC_4TH
				SET Point = Point + @Point , Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime, MinLeftTime = @iMinLeftTime 
				WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END
		End
	ELSE
		Begin
			INSERT INTO EVENT_INFO_BC_4TH ( Server,  Bridge, AccountID, CharacterName, Class, Point, PlayCount, SumLeftTime, MinLeftTime ) VALUES (
				@Server,
				@Bridge,
				@AccountID,
				@CharacterName,
				@Class,
				@Point,
				1,
				@LeftTime,
				@LeftTime
			)
		End


	IF (@PCRoomGUID <> 0)
	Begin
		IF EXISTS (SELECT AccountID FROM T_BC_PCROOM_PLAYCOUNT  WITH (READUNCOMMITTED) 
				WHERE  PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID)
		Begin
			UPDATE T_BC_PCROOM_PLAYCOUNT
			SET PlayCount = PlayCount + 1, Point = Point + @Point
			WHERE PCROOM_GUID = @PCRoomGUID AND AccountID = @AccountID		
		End
		ELSE
		Begin
			INSERT INTO T_BC_PCROOM_PLAYCOUNT
			VALUES (@PCRoomGUID, @AccountID, default, @Point)
		End
	End
*/

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_POINT_ACCM_BC_5TH]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



--//************************************************************************
--// і»   їл : єн·ЇµеДіЅЅ АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы (5Вч)
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.05. 31
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_POINT_ACCM_BC_5TH]
	@Server		smallint,		-- ј­№ц 
	@Bridge		tinyint,		-- ґЩё®
	@AccountID		varchar(10),	-- °иБ¤ён
	@CharacterName	varchar(10),	-- ДЙёЇён
	@Class			tinyint ,		-- Е¬·ЎЅє №шИЈ 	(0-Ижё¶№э»з, 1-јТїпё¶ЅєЕН, 16-Иж±в»з, 17-єн·№АМµеіЄАМЖ®, 32-їдБ¤, 33-№ВБої¤ЗБ, 48-ё¶°Л»з)
	@Point			int,		-- ЖчАОЖ®
	@LeftTime		int,		-- єн·ЇµеДіЅЅ °ж±в іІАєЅГ°Ј ґ©Аы
	@AlivePartyCount	int		-- єн·ЇµеДіЅЅ °ж±в іІАєЅГ°Ј ґ©Аы
As
BEGIN
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	DECLARE @RegDate	SMALLDATETIME
	SET @RegDate = CAST(DATEPART(YY, GetDate()) AS VARCHAR(4)) + '-' + CAST(DATEPART(MM, GetDate()) AS VARCHAR(2)) + '-' + CAST(DATEPART(DD, GetDate()) AS VARCHAR(2)) + ' 00:00:00'

	IF EXISTS ( SELECT CharacterName FROM EVENT_INFO_BC_5TH  WITH (READUNCOMMITTED) 
				WHERE RegDate = @RegDate AND Server = @Server AND AccountID = @AccountID AND CharacterName = @CharacterName )
		BEGIN			
			DECLARE @iiiPoint		int
			DECLARE @iMinLeftTime	int
			DECLARE @iAlivePartyCount	int
			DECLARE @iMaxPointLeftTime	int

			SELECT @iiiPoint = Point, @iMinLeftTime = MinLeftTime, @iAlivePartyCount = AlivePartyCount, @iMaxPointLeftTime = MaxPointLeftTime
			FROM EVENT_INFO_BC_5TH
			WHERE  RegDate = @RegDate AND Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName

			-- БЎјцґВ °ЎАе іфАє БЎјц ЗПіЄёё АъАеЗСґЩ.
			-- ГЦ°н БЎјцё¦ №ЮѕТА» ¶§ ±Ч ЅГ°ЈА» °°АМ °»ЅЕЗСґЩ.
			IF (@iiiPoint < @Point)
			BEGIN
				IF (@Point > 0)			-- ДіЅЅїЎ јє°ш ЗЯА» ¶§ ёё ГЦ°н БЎјц°Ў °»ЅЕµЙ °НАМґЩ.
				BEGIN
					SET @iiiPoint = @Point
					SET @iMaxPointLeftTime = @LeftTime
				END
			END

			-- БЎјц°Ў °°А» ¶§ґВ ґ©АыЅГ°ЈАМ Е«ВКА» АъАеЗСґЩ.
			IF (@iiiPoint = @Point)
			BEGIN
				IF (@Point > 0)			-- ДіЅЅїЎ јє°ш ЗЯА» ¶§ ёё ГЦ°н БЎјц°Ў °»ЅЕµЙ °НАМґЩ.
				BEGIN
					IF (@iMaxPointLeftTime < @LeftTime)
					BEGIN
						SET @iMaxPointLeftTime = @LeftTime
					END
				END
			END

			-- ЖДЖјїшАє °ЎАе ё№Ає °Н И¤Ає °°Ає °НАП °жїмёё АъАеЗСґЩ.
			-- ЖДЖјїшјц°Ў °»ЅЕµЙ ¶§ ГЦґЬЅГ°Јµµ °°АМ °»ЅЕµИґЩ.
			IF (@iAlivePartyCount <= @AlivePartyCount)
			BEGIN
				IF (@iMinLeftTime < @LeftTime)
				BEGIN
					IF (@Point > 0)			-- ДіЅЅїЎ јє°ш ЗЯА» ¶§ ёё ГЦґЬЅГ°Ј БЎјц°Ў °»ЅЕµЙ °НАМґЩ.
					BEGIN
						SET @iMinLeftTime = @LeftTime
					END
				END
				SET @iAlivePartyCount = @AlivePartyCount
			END

			IF (@iiiPoint < 0)
			BEGIN
				UPDATE EVENT_INFO_BC_5TH
				SET Point = 0 , Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime, MinLeftTime = @iMinLeftTime, AlivePartyCount = @iAlivePartyCount, MaxPointLeftTime = @iMaxPointLeftTime
				WHERE  RegDate = @RegDate AND Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END	
			ELSE
			BEGIN
				UPDATE EVENT_INFO_BC_5TH
				SET Point = @iiiPoint, Bridge = @Bridge, PlayCount = PlayCount+1, SumLeftTime = SumLeftTime + @LeftTime, MinLeftTime = @iMinLeftTime, AlivePartyCount = @iAlivePartyCount, MaxPointLeftTime = @iMaxPointLeftTime
				WHERE  RegDate = @RegDate AND Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName	
			END
		END
	ELSE
		BEGIN
			IF (@Point < 0)
			BEGIN
				INSERT INTO EVENT_INFO_BC_5TH ( Server,  Bridge, AccountID, CharacterName, Class, Point, PlayCount, SumLeftTime, MinLeftTime, RegDate, AlivePartyCount, MaxPointLeftTime ) VALUES (
					@Server,
					@Bridge,
					@AccountID,
					@CharacterName,
					@Class,
					0,
					1,
					0,
					0,
					@RegDate,
					0,
					0
				)
			END
			ELSE
			BEGIN
				INSERT INTO EVENT_INFO_BC_5TH ( Server,  Bridge, AccountID, CharacterName, Class, Point, PlayCount, SumLeftTime, MinLeftTime, RegDate, AlivePartyCount, MaxPointLeftTime ) VALUES (
					@Server,
					@Bridge,
					@AccountID,
					@CharacterName,
					@Class,
					@Point,
					1,
					@LeftTime,
					@LeftTime,
					@RegDate,
					@AlivePartyCount,
					@LeftTime
				)
			END

		END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
END

GO
/****** Object:  StoredProcedure [dbo].[SP_POINT_ACCUMULATION]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--//************************************************************************
--// і»   їл : µҐєф АМєҐЖ® ЖчАОЖ® Б¤єё ґ©Аы
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.01. 21
--// ёёµйАМ : ГЯјч
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_POINT_ACCUMULATION]
	@Server		smallint,	-- ј­№ц 
	@Square		tinyint,	-- ±¤Ае 

	@AccountID		varchar(10),
	@CharacterName	varchar(10),
	@Class			tinyint ,
	@Point			int
As
Begin
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT CharacterName FROM EVENT_INFO  WITH (READUNCOMMITTED) 
				WHERE  Server = @Server AND AccountID = @AccountID AND CharacterName = @CharacterName )
		Begin			
			UPDATE EVENT_INFO
			SET Point = Point + @Point , Square = @Square 										
			WHERE  Server = @Server  AND AccountID = @AccountID AND CharacterName = @CharacterName		
		End
	ELSE
		Begin
			INSERT INTO EVENT_INFO ( Server, 	Square, 	AccountID, CharacterName, Class, Point ) VALUES (
				@Server,
				@Square,
				@AccountID,
				@CharacterName,
				@Class,
				@Point	
			)	
		End 


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_REG_CC_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO


--//************************************************************************
--// і»   їл : №В №ЭБцАМєҐЖ® °жЗ°µо·П
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.05.05
--// ёёµйАМ : БШАП
--// 
--// ё®ЕП°Є јіён
--// 
--// 	јє°шДЪµе :	0 : »уЗ°µо·П ЅЗЖР (ѕшАЅ)
--//			1 : »уЗ°µо·П јє°ш
--// 	ј±№°АМё§ :	T_CC_OFFLINE_GIFTNAME °ъ GiftKind ё¦ JOIN ЗС °б°ъ
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_CC_OFFLINE_GIFT]
	@AccountID		varchar(10),	
	@CharName		varchar(10),	
	@ServerCode		int
As
Begin
	BEGIN TRANSACTION

	DECLARE @iGIFT_GUID	INT		-- №ЮА» »уЗ°АЗ GUID
	DECLARE @iGIFT_KIND	INT		-- №ЮА» »уЗ°АЗ Бѕ·щ
	DECLARE @iGIFT_NAME	VARCHAR(50)	-- №ЮА» »уЗ°АЗ АМё§
	
	SET NOCOUNT ON

	IF EXISTS (SELECT AccountID Guid FROM T_CC_OFFLINE_GIFT WHERE AccountID = @AccountID)
	BEGIN
		-- АМ »з¶чАє АМАьїЎ ЗС№ш ґзГ·µИ »з¶чАМ№З·О ¶З °жЗ°А» БЩ јц ѕшґЩ.
		SELECT 0 As ResultCode, '' As GiftName
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT TOP 1 Guid FROM T_CC_OFFLINE_GIFT WHERE Date_Give < GetDate() and RegCheck = 0 ORDER BY Guid ASC)
		BEGIN
			-- їАґГ БЩ јц АЦґВ °жЗ°АМ іІѕЖАЦґЩ.
			SELECT TOP 1 @iGIFT_GUID = Guid, @iGIFT_KIND = GiftKind  FROM T_CC_OFFLINE_GIFT WHERE Date_Give < GetDate() and RegCheck = 0 ORDER BY Guid ASC
	
			-- ї©±вј­ ЗШґз »зїлАЪАЗ °жЗ°Аё·О БцБ¤ЗШ БШґЩ.
			UPDATE T_CC_OFFLINE_GIFT SET Server = @ServerCode, AccountID = @AccountID, CharName = @CharName, Date_Reg = GetDate(), RegCheck = 1 WHERE Guid = @iGIFT_GUID
	
			SELECT @iGIFT_NAME = GiftName FROM T_CC_OFFLINE_GIFTNAME WHERE GiftKind = @iGIFT_KIND
	
			SELECT 1 As ResultCode, @iGIFT_NAME As GiftName
		END
		ELSE
		BEGIN
			-- їАґГ БЩ јц АЦґВ °жЗ°АМ ЗПіЄµµ ѕшґЩ.
			SELECT 0 As ResultCode, '' As GiftName
		END
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF
End
GO
/****** Object:  StoredProcedure [dbo].[SP_REG_DL_OFFLINE_GIFT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--//************************************************************************
--// і»   їл : ґЩЕ©·ОµеАЗ ё¶АЅ АМєҐЖ® °жЗ°µо·П
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.08.11
--// ёёµйАМ : БШАП
--// 
--// ё®ЕП°Є јіён
--// 
--// 	јє°шДЪµе :	0 : »уЗ°µо·П ЅЗЖР (ѕшАЅ)
--//			1 : »уЗ°µо·П јє°ш
--// 	ј±№°АМё§ :	T_DL_OFFLINE_GIFTNAME °ъ GiftKind ё¦ JOIN ЗС °б°ъ
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_DL_OFFLINE_GIFT]
	@AccountID		varchar(10),	
	@CharName		varchar(10),	
	@ServerCode		int
As
Begin
	BEGIN TRANSACTION

	DECLARE @iGIFT_GUID	INT		-- №ЮА» »уЗ°АЗ GUID
	DECLARE @iGIFT_KIND	INT		-- №ЮА» »уЗ°АЗ Бѕ·щ
	DECLARE @iGIFT_NAME	VARCHAR(50)	-- №ЮА» »уЗ°АЗ АМё§
	
	SET NOCOUNT ON

	IF EXISTS (SELECT AccountID Guid FROM T_DL_OFFLINE_GIFT WHERE AccountID = @AccountID)
	BEGIN
		-- АМ »з¶чАє АМАьїЎ ЗС№ш ґзГ·µИ »з¶чАМ№З·О ¶З °жЗ°А» БЩ јц ѕшґЩ.
		SELECT 0 As ResultCode, '' As GiftName
	END
	ELSE
	BEGIN
		IF EXISTS (SELECT TOP 1 Guid FROM T_DL_OFFLINE_GIFT WHERE Date_Give < GetDate() and RegCheck = CAST(0 AS BIT) ORDER BY Guid ASC)
		BEGIN
			-- їАґГ БЩ јц АЦґВ °жЗ°АМ іІѕЖАЦґЩ.
			SELECT TOP 1 @iGIFT_GUID = Guid, @iGIFT_KIND = GiftKind  FROM T_DL_OFFLINE_GIFT WHERE Date_Give < GetDate() and RegCheck = CAST(0 AS BIT) ORDER BY Guid ASC
	
			-- ї©±вј­ ЗШґз »зїлАЪАЗ °жЗ°Аё·О БцБ¤ЗШ БШґЩ.
			UPDATE T_DL_OFFLINE_GIFT SET Server = @ServerCode, AccountID = @AccountID, CharName = @CharName, Date_Reg = GetDate(), RegCheck = CAST(1 AS BIT) WHERE Guid = @iGIFT_GUID
	
			SELECT @iGIFT_NAME = GiftName FROM T_DL_OFFLINE_GIFTNAME WHERE GiftKind = @iGIFT_KIND
	
			SELECT 1 As ResultCode, @iGIFT_NAME As GiftName
		END
		ELSE
		BEGIN
			-- їАґГ БЩ јц АЦґВ °жЗ°АМ ЗПіЄµµ ѕшґЩ.
			SELECT 0 As ResultCode, '' As GiftName
		END
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF
End

GO
/****** Object:  StoredProcedure [dbo].[SP_REG_FRIEND_STONE]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO




--//************************************************************************
--// і»   їл : №В ДЈ±ёАМєҐЖ®
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2004.02.11
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_FRIEND_STONE]
	@AccountID		varchar(10),	-- °иБ¤ён
	@ServerCode		smallint,	-- ј­№цДЪµе
	@CharName		varchar(10)	-- ДЙёЇён
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	-- 1 . ЗШґз °иБ¤АЗ ЅєЕж°іјцё¦ Бх°ЎЅГЕґ
	IF NOT EXISTS (SELECT FriendShipStoneCount FROM T_FRIENDSHIP_STONE WHERE AccountID = @AccountID and ServerCode = @ServerCode and CharName = @CharName)
	BEGIN
		INSERT T_FRIENDSHIP_STONE (AccountID, ServerCode, CharName, FriendShipStoneCount) VALUES (@AccountID, @ServerCode, @CharName, 1)
	END
	ELSE
	BEGIN
		UPDATE T_FRIENDSHIP_STONE 
		SET FriendShipStoneCount = FriendShipStoneCount + 1
		WHERE AccountID = @AccountID and ServerCode = @ServerCode and CharName = @CharName
	END

	-- 2 . ј­№цє° ЅєЕж°іјц Бх°Ў
	IF NOT EXISTS (SELECT FriendShipStoneCount FROM T_FRIENDSHIP_SERVERRANK WHERE ServerCode = @ServerCode)
	BEGIN
		INSERT T_FRIENDSHIP_SERVERRANK VALUES (@ServerCode, 1)
	END
	ELSE
	BEGIN
		UPDATE T_FRIENDSHIP_SERVERRANK 
		SET FriendShipStoneCount = FriendShipStoneCount + 1
		WHERE ServerCode = @ServerCode
	END

	SELECT FriendShipStoneCount FROM T_FRIENDSHIP_STONE WHERE  AccountID = @AccountID and ServerCode = @ServerCode and CharName = @CharName
	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF
End

GO
/****** Object:  StoredProcedure [dbo].[SP_REG_RINGGIFT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--//************************************************************************
--// і»   їл : №В №ЭБцАМєҐЖ® °жЗ°µо·П
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.12.05
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_RINGGIFT]
	@AccountID		varchar(10),	-- °иБ¤ён
	@GiftSection		tinyint		-- »уЗ°АЗ Бѕ·щ (0:їл»зАЗ№ЭБцµе·У, 1:їАЕ©БЧАУ)
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON
	
	DECLARE	@ToDay		DATETIME
	DECLARE	@strToDayStart		VARCHAR(32)
	DECLARE	@strToDayEnd		VARCHAR(32)
	DECLARE	@iToDayRegCount	INT
	DECLARE	@iMaxAllGiftNum	INT		-- ЗШґз №шИЈ »уЗ°АЗ ёрµз °іјц
	DECLARE	@iMaxTodayGiftNum	INT		-- ЗШґз №шИЈ »уЗ°АЗ їАґГАЗ °іјц

	DECLARE	@iLeftGifts		INT		-- іІАє »уЗ° °іјц
	DECLARE	@iGUID		INT		-- GUID
	DECLARE	@iGiftKind		INT		-- ґзГ·µИ »уЗ°№шИЈ

	IF @GiftSection = 0
	BEGIN
		SET @iMaxAllGiftNum = 80		
		SET @iMaxTodayGiftNum = 2		
	END	
	ELSE
	BEGIN
		SET @iMaxAllGiftNum = 160		
		SET @iMaxTodayGiftNum = 5		
	END

	-- 1> . АМБ¦ ЗШґз °жЗ°Бѕ·щАЗ іІАє °жЗ°АМ АЦґВБц И®АОЗСґЩ.
	SELECT @iLeftGifts = COUNT(*) FROM T_RingEvent_OfflineGift WHERE RegisterDate IS NULL AND GiftSection = @GiftSection

	IF @iLeftGifts = 0
	BEGIN
		SELECT 3 As RegResult, @AccountID, 0 As GiftKind	-- АМБ¦ ґх АМ»у µо·ПЗТ јц ѕшґЩёй іЄ°ЈґЩ.
	END
	ELSE
	BEGIN
		-- 2> . їАґГ µо·ПЗТ јц АЦґВ Иёјц АМ»уАё·О µо·П µЗѕъґВБц ГјЕ©
		SET @ToDay = GetDate()
		
		SET @strToDayStart = CONVERT(char(4), YEAR(@ToDay)) + '-' + CONVERT(char(2), MONTH(@ToDay)) + '-' + CONVERT(char(2), DAY(@ToDay)) + ' 00:00:00'
		SET @strToDayEnd = CONVERT(char(4), YEAR(@ToDay)) + '-' + CONVERT(char(2), MONTH(@ToDay)) + '-' + CONVERT(char(2), DAY(@ToDay)) + ' 23:59:59'
		
		SELECT @iToDayRegCount = COUNT(*)
		FROM T_RingEvent_OfflineGift
		WHERE RegisterDate between @strToDayStart and @strToDayEnd
			and @GiftSection = GiftSection
		
		IF @iToDayRegCount >= @iMaxTodayGiftNum		-- їАґГАЗ ГЦґл »уЗ° µо·П јцё¦ ГК°ъЗПёй іЄ°ЈґЩ.
		BEGIN
			SELECT 3 As RegResult, @AccountID, 0 As GiftKind
		END
		ELSE
		BEGIN
			-- 3> . АМ№М µо·ПµИ °иБ¤АОБц И®АОЗСґЩ.
			IF EXISTS ( SELECT * FROM T_RingEvent_OfflineGift  WITH (READUNCOMMITTED) 
						WHERE AccountID = @AccountID)
			BEGIN	
				-- АМ№М µо·ПµИ »з¶чАМ№З·О ±Ч»з¶чАЗ °иБ¤ён Б¤µµёё єёіЅґЩ.
				SELECT 2 As RegResult, @AccountID, 0 As GiftKind
			END
			ELSE
			BEGIN
				SELECT TOP 1 @iGUID = GUID, @iGiftKind = GiftKind
				FROM T_RingEvent_OfflineGift
				WHERE @GiftSection = GiftSection and RegisterDate IS NULL
				ORDER BY GUID

				UPDATE T_RingEvent_OfflineGift
				SET AccountID=@AccountID, RegisterDate=GetDate()
				WHERE @iGUID = GUID
		
				-- »х·О µо·ПЗПґВ »з¶чАМёз µо·П јє°ш
				SELECT 1 As RegResult, @AccountID, @iGiftKind As GiftKind
			END
	
		END
	END
	
	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF
End
GO
/****** Object:  StoredProcedure [dbo].[SP_REG_RINGGIFT_TEMP]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO



-- АъАе ЗБ·ОЅГАъ »эјє

--//************************************************************************
--// і»   їл : №В №ЭБцАМєҐЖ® °жЗ°µо·П
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.12.05
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_RINGGIFT_TEMP]
	@AccountID		varchar(10),	-- °иБ¤ён
	@GiftKind		tinyint		-- »уЗ°АЗ Бѕ·щ (0:їл»зАЗ№ЭБцµе·У, 1:їАЕ©БЧАУ)
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	DECLARE	@ToDay		DATETIME
	DECLARE	@strToDayStart		VARCHAR(32)
	DECLARE	@strToDayEnd		VARCHAR(32)
	DECLARE	@iToDayRegCount	INT
	DECLARE	@iMaxAllGiftNum	INT		-- ЗШґз №шИЈ »уЗ°АЗ ёрµз °іјц
	DECLARE	@iMaxTodayGiftNum	INT		-- ЗШґз №шИЈ »уЗ°АЗ їАґГАЗ °іјц

	IF @GiftKind = 0
	BEGIN
		SET @iMaxAllGiftNum = 80		
		SET @iMaxTodayGiftNum = 2		
	END	
	ELSE
	BEGIN
		SET @iMaxAllGiftNum = 160		
		SET @iMaxTodayGiftNum = 3		
	END

	SELECT @iToDayRegCount = COUNT(*)
	FROM T_RingAttackEvent_Gift
	WHERE @GiftKind = GiftKind

	IF @iToDayRegCount >= @iMaxAllGiftNum		-- АьГј ГЦґл »уЗ° µо·П јцё¦ ГК°ъЗПёй іЄ°ЈґЩ.
	BEGIN
		SELECT 3 As RegResult, @AccountID
	END
	ELSE
	BEGIN
		SET @ToDay = GetDate()
		
		SET @strToDayStart = CONVERT(char(4), YEAR(@ToDay)) + '-' + CONVERT(char(2), MONTH(@ToDay)) + '-' + CONVERT(char(2), DAY(@ToDay)) + ' 00:00:00'
		SET @strToDayEnd = CONVERT(char(4), YEAR(@ToDay)) + '-' + CONVERT(char(2), MONTH(@ToDay)) + '-' + CONVERT(char(2), DAY(@ToDay)) + ' 23:59:59'
		
		SELECT @iToDayRegCount = COUNT(*)
		FROM T_RingAttackEvent_Gift
		WHERE RegisterDate between @strToDayStart and @strToDayEnd
			and @GiftKind = GiftKind
	
		IF @iToDayRegCount >= @iMaxTodayGiftNum	-- їАґГАЗ ГЦґл »уЗ° µо·П јцё¦ ГК°ъЗПёй іЄ°ЈґЩ.
		BEGIN
			SELECT 3 As RegResult, @AccountID
		END
		ELSE
		BEGIN
			IF EXISTS ( SELECT * FROM T_RingAttackEvent_Gift  WITH (READUNCOMMITTED) 
						WHERE AccountID = @AccountID)
			BEGIN	
				-- АМ№М µо·ПµИ »з¶чАМ№З·О ±Ч»з¶чАЗ °иБ¤ён Б¤µµёё єёіЅґЩ.
				SELECT 2 As RegResult, @AccountID
			END
			ELSE
			BEGIN
				INSERT INTO T_RingAttackEvent_Gift
				VALUES (@AccountID, @GiftKind, default)
		
				-- »х·О µо·ПЗПґВ »з¶чАМёз µо·П јє°ш
				SELECT 1 As RegResult, @AccountID
			END
		END
	END

	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF
End
GO
/****** Object:  StoredProcedure [dbo].[SP_REG_SERIAL]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO



--//************************************************************************
--// і»   їл : №В »уїлИ­ 2БЦів ±від є№±З АМєҐЖ® °ь·Г - ЅГё®ѕу µо·П
--// єО   ј­ : °ФАУ°і№ЯЖА 
--// ёёµйАП : 2003.10.20
--// ёёµйАМ : БШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[SP_REG_SERIAL]
	@AccountID		varchar(10),	-- °иБ¤ён
	@MembGUID		int,		-- GUID
	@SERIAL1		varchar(4),	-- ЅГё®ѕу1
	@SERIAL2		varchar(4),	-- ЅГё®ѕу2
	@SERIAL3		varchar(4)	-- ЅГё®ѕу3
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	-- 1 . T_RegCount_Check ЕЧАМєнїЎј­ ЗШґз °иБ¤АМ АМ№М µо·ПЗЯґВБц, Д«їоЖ®ё¦ іСѕъґВБц ГјЕ©ЗСґЩ.
	DECLARE @MAX_REGCOUNT	INT
	DECLARE @iREG_COUNT	INT
	DECLARE @iREG_ALREADY	BIT

	SET @MAX_REGCOUNT 	= 20
	SET @iREG_ALREADY		= 0
	
	IF EXISTS ( SELECT * FROM T_RegCount_Check  WITH (READUNCOMMITTED) 
				WHERE AccountID = @AccountID)
	BEGIN	
		-- T_RegCount_Check їЎґВ АМ№М °иБ¤ёнАМ АЦА» °НАМ№З·О є°µµАЗ °ЛБхѕшАМ µо·Пї©єОё¦ єЇ°жЗСґЩ.
		SELECT @iREG_ALREADY = RegAlready FROM T_RegCount_Check WHERE AccountID = @AccountID
		
/*		IF (@iREG_ALREADY = 1 )
		BEGIN
			SELECT 5 As RegResult, 0 As F_Register_Section
		END
		ELSE
		BEGIN
*/			SELECT @iREG_COUNT = RegCount 
			FROM T_RegCount_Check
			WHERE AccountID = @AccountID
		
			IF (@iREG_COUNT >= @MAX_REGCOUNT)
			BEGIN
				SET @iREG_ALREADY = 1

				SELECT 2 As RegResult, 0 As F_Register_Section
			END
			ELSE
			BEGIN
				UPDATE T_RegCount_Check
				SET RegCount = RegCount + 1
				WHERE AccountID = @AccountID
			END
--		END
	END
	ELSE
	BEGIN
		INSERT INTO T_RegCount_Check
		VALUES (@AccountID, default, default)
	END


/*	IF (@iREG_ALREADY =1)
	BEGIN
		IF(@@Error <> 0 )
			ROLLBACK TRANSACTION
		ELSE	
			COMMIT TRANSACTION
		
		SET NOCOUNT OFF	

		RETURN
	END
*/	

	-- 2 . T_Serial_Bank ЕЧАМєнїЎ ЅГё®ѕуА» µо·ПЗСґЩ.
	DECLARE @REG_CHECK	BIT
	
	IF EXISTS ( SELECT F_RegisterCheck FROM T_Serial_Bank  WITH (READUNCOMMITTED) 
		WHERE  P_Serial_1 = @SERIAL1 and P_Serial_2 = @SERIAL2 and P_Serial_3 = @SERIAL3)
	BEGIN	
		SELECT @REG_CHECK = F_RegisterCheck FROM T_Serial_Bank  WITH (READUNCOMMITTED) 
		WHERE  P_Serial_1 = @SERIAL1 and P_Serial_2 = @SERIAL2 and P_Serial_3 = @SERIAL3

		IF (@REG_CHECK = 0)
		BEGIN
			UPDATE T_Serial_Bank
			SET F_Member_Guid = @MembGUID, F_Member_Id = @AccountID, F_RegisterCheck = 1, F_Register_Date = GetDate()
			WHERE  P_Serial_1 = @SERIAL1 and P_Serial_2 = @SERIAL2 and P_Serial_3 = @SERIAL3

			-- T_RegCount_Check їЎґВ АМ№М °иБ¤ёнАМ АЦА» °НАМ№З·О є°µµАЗ °ЛБхѕшАМ µо·Пї©єОё¦ єЇ°жЗСґЩ.
			UPDATE T_RegCount_Check
			SET RegAlready = 1
			WHERE AccountID = @AccountID
			
			SELECT 0 As RegResult, F_Register_Section
			FROM T_Serial_Bank
			WHERE  P_Serial_1 = @SERIAL1 and P_Serial_2 = @SERIAL2 and P_Serial_3 = @SERIAL3
		END
		ELSE
		BEGIN	-- АМ№М µо·ПµЗѕъґЩ.
			SELECT 1 As RegResult, 0 As F_Register_Section
		END

	END
	ELSE
	BEGIN	-- ЗШґз ЅГё®ѕуАє БёАзЗПБц ѕКґВґЩ.
		SELECT 3 As RegResult, 0  As F_Register_Section
	END

	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION
	
	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_SUM_LOG_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_SUM_LOG_INSERT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		 ЅГ°Јє° ГС БўјУБ¤єёё¦ АъАеЗПґВ ЗБ·ОЅГБ®   
		
*/

CREATE PROCEDURE [dbo].[SP_SUM_LOG_INSERT]
(
	@Insert_Date	datetime,
	@ConnectCount	int		
) 
As
Begin

	SET NOCOUNT ON
	
	Begin Transaction

	Insert into Day_Statistics(ConnectTime, TotalCount) Values (
	@Insert_Date,
	@ConnectCount
	)
	
	if(@@Error <> 0 )
		RollBack Tran
	else	
		Commit Tran

	SET NOCOUNT OFF			
End

GO
/****** Object:  StoredProcedure [dbo].[SP_TIME_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_Time_ConnectLog 
	2) АЫѕчАП : 2003. 2. 5.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		 ЅГ°Ј ґЬА§є° µїБў ЗцИІ µҐАМЕёё¦ ГЯГвЗПї© АъАеЗСґЩ. 
		БЦ±вАыАО ЅГ°Ј ЅєДЙБм·Ї( 1 ЅГ°Ј ґЬА§ ) 
		
*/
CREATE Procedure [dbo].[SP_TIME_INSERT]
As
Begin	
	DECLARE	@PrevOnetime	datetime
	DECLARE	@SearchDay		char(15)
	DECLARE	@SaveTime		datetime 
	
	SET NOCOUNT ON 
	-- іЇВҐБ¤єё Б¤ё® 
	SET	  @PrevOnetime	 	= DateAdd(Hour, -1, getdate() ) 
	SET	  @SearchDay		 = Convert(varchar, datepart( year ,  @PrevOnetime  ))   + '-'+ 
						   	 Convert( varchar ,  datepart( month ,  @PrevOnetime  ) )  + '-'+ 
						  	 Convert( varchar, datepart( day , @PrevOnetime  )  )	+ '  ' +
							 Convert( varchar, datepart( Hour , @PrevOnetime  )  )	


	SELECT  Top 1 @SaveTime =  ConnectTime 
	FROM	SameTimeConnectLog  	With( READUNCOMMITTED )
	WHERE ConnectTime Between   Convert( datetime , @SearchDay + ':00:00' )  
		and  Convert( datetime,   @SearchDay + ':59:59' ) Order By ConnectTime Asc

	IF  EXISTS  ( SELECT  @SaveTime )
	Begin 		
		Begin Tran

		-- їдГ»µИ Б¤єё°Ў БёАзЗПёй 
		INSERT INTO TimeConnectLog (
			ConnectTime,
			Server,
			SubServer,
			ConnectCount			
		)  SELECT
			ConnectTime,
			Server,
			SubServer,
			ConnectCount		
			FROM SameTimeConnectLog
			With( READUNCOMMITTED )
		WHERE 	ConnectTime = @SaveTime 									
		
		
		if(@@error > 0 )
			rollback tran
		else	
			commit tran

	End 

	SET NOCOUNT OFF
End
GO
/****** Object:  StoredProcedure [dbo].[SP_TIME_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_TIME_SELECT
	2) АЫѕчАП : 2003. 2. 5.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ЖЇБ¤АПїЎ ґлЗС Б¤єёё¦ ґЩїо №ЮґВґЩ 
		
*/
CREATE Procedure [dbo].[SP_TIME_SELECT]
	@Year	char(4),
	@Month	char(2),
	@Day	char(2)	
As
Begin

	SET NOCOUNT ON 	

	DECLARE	@SearchDay		char(15)		
	SET	  @SearchDay	 =     Convert(varchar, @Year)   + '-'+  Convert( varchar ,  @Month )  + '-' + Convert( varchar, @Day )						

	SELECT
		DatePart( Hour, ConnectTime )  As Hour ,
		DatePart( Minute, ConnectTime )  As Minute ,
		Server,
		SubServer,
		ConnectCount		
	FROM TimeConnectLog 	With( READUNCOMMITTED )
	WHERE 	ConnectTime Between   Convert( datetime , @SearchDay +  '  00:00:00' )  
	and  Convert( datetime,   @SearchDay + '  23:59:59' ) 
	Order By Server, Hour, SubServer

	SET NOCOUNT OFF

End
GO
/****** Object:  StoredProcedure [dbo].[SP_WEEK_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_WEEKDAY_INSERT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ѕоБ¦їЎ ґлЗС 1ЅГ°Ј ґЬА§АЗ БўјУБ¤єёё¦ ГЯГвЗПґВ ЗБ·ОЅГБ®  
		
*/

CREATE PROCEDURE	 [dbo].[SP_WEEK_INSERT]
AS
Begin
	
	
	Declare @YesterDay	datetime
	Declare @SearchDay	char(10)
	Declare @Week		tinyint
	Declare @WeekDay	tinyint
	Declare @Year		smallint

	SET		@YesterDay	= DateAdd( day, -1, getdate())		-- ЗцАз ЅГЅєЕЫАЗ ѕоБ¦ іЇВҐё¦ ±ёЗСґЩ
	SET		@SearchDay = Convert(varchar, datepart( year ,  @YesterDay  ))   + '-'+ 
						   Convert( varchar ,  datepart( month ,  @YesterDay  ) )  + '-'+ 
						   Convert( varchar, datepart( day , @YesterDay  )  )		
	SET		 @Week	= DatePart( Week, @YesterDay)
	SET		 @WeekDay	= DatePart( Weekday, @YesterDay)
	SET		@Year		= DatePart( Year, @YesterDay)
			

	SET NOCOUNT ON		
	
	Begin Tran
		
	-- 0~12 ЅГ±оБцАЗ Б¤єё 		
		INSERT INTO Week_Statistics( 
			Year, Week, WeekDay,  HalfDay, MaxCount, MinCount, AvgCount			
		)   SELECT  
			@Year		As Year, 
			@Week	As Week,
			@WeekDay 	As WeekDay,
			1 		As HalfDay, 
			Max(TotalCount) As MaxCount,
			Min(TotalCount) As MinCount,
			Avg(TotalCount) As AvgCount			
        	    	FROM  Day_Statistics
		WHERE ConnectTime Between Convert( datetime , @SearchDay + '  00:00:00' )  and  Convert( datetime,   @SearchDay + '  23:59:59' )
		having Max(TotalCount) > 0

	-- 12~24 ЅГ±оБцАЗ Б¤єё 		
		INSERT INTO Week_Statistics( 
			Year, Week, WeekDay,  HalfDay, MaxCount, MinCount, AvgCount			
		)   SELECT  
			@Year		As Year, 
			@Week	As Week,
			@WeekDay 	As WeekDay,
			2 		As HalfDay, 
			Max(TotalCount) As MaxCount,
			Min(TotalCount) As MinCount,
			Avg(TotalCount) As AvgCount			
        	    	FROM  Day_Statistics
		WHERE ConnectTime Between Convert( datetime , @SearchDay + '  12:00:00' )  and  Convert( datetime,   @SearchDay + '  23:59:59' )
		having Max(TotalCount) > 0


		if(@@error > 0 )
			rollback tran
		else	
			commit tran
	
	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_WEEK_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_WEEK_SELECT 
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		 ЖЇБ¤ ґЮїЎ ґлЗС  µїБў Б¤єёё¦ ГЯГв
		
*/

CREATE PROCEDURE [dbo].[SP_WEEK_SELECT]
(
	@Year		char(4),			-- ЖЇБ¤ їщ 
	@Month		char(2)		-- ЖЇБ¤ ґЮ 
) 
As
Begin

	--//////////////////////////////////////////////////////////////////////////////////////////
	-- БЦґЬА§АЗ Б¤єёё¦ АУЅГЕЧАМєнїЎ АъАе 
	Create Table #Temp(		
		WeekIndex	smallint,		-- АОµ¦ЅєЕ°
		Week		smallint,		-- БЦБ¤єёё¦ »кГв
		WeekDay	smallint,		-- ЅГ°ЈБ¤єё (12ЅГ°Ј ґЬА§)
		MaxCount	int,
		MinCount	int, 
		AvgCount	int		
	)  	
	--//////////////////////////////////////////////////////////////////////////////////////////

	
	SET NOCOUNT ON
		
	Declare	@SetDate	datetime
	Declare  @StartWeek	tinyint	--	1 Byte ( 0 ~  255) 
	Declare  @Count		int

	-- ЗцАз іС°ЬБш ЖД¶уёЮЕёАЗ °ЄИ®АО 
	if( @Year = 0 )
	begin
		Set	@Year = Datepart( Year, getdate())
		Set	@Month = Datepart( Month, getdate())
	end

	-- ЖЇБ¤ґЮїЎ ґлЗС Г№В°іЇА» »кГв 
	Set @SetDate = ConVert(datetime, @Year + '-' + @Month + '-01 00:00:00')
	
	-- ЅГАЫµЗґВ БЦБ¤єёё¦ ѕтѕоµ·ґЩ. 	
	Set @StartWeek = DatePart(Week, @SetDate)	
	Set @Count = 0
	
	-- БЦБ¤єёё¦ °Л»цЗСґЩ. (ЗШґз ґЮАЗ 5В°БЦ Б¤єёё¦ ѕтѕоїВґЩ.)
	While(@Count < 5)
	begin
		
		--  Д«їоЖ®°Є Бх°Ў
		Set  @Count =  @Count + 1	
			
		--  ЗШґз Б¤єё »кГв 
		INSERT INTO #Temp( WeekIndex, Week, WeekDay, MaxCount, MinCount, AvgCount)
			SELECT 
				@Count As WeekIndex , 
				Week, 
				WeekDay, 	
				MaxCount,  				
				MinCount, 
				AvgCount  
			From Week_Statistics 	
			Where Week = @StartWeek

		print @StartWeek
		
		Set	@StartWeek = @StartWeek + 1

	end 	

	Select  WeekIndex, Week, WeekDay As HalfDay , MaxCount, MinCount, AvgCount  from #Temp 
	Drop Table #Temp	
	
	SET NOCOUNT OFF
			
End
GO
/****** Object:  StoredProcedure [dbo].[SP_WEEKDAY_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_WEEKDAY_INSERT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ѕоБ¦їЎ ґлЗС 1ЅГ°Ј ґЬА§АЗ БўјУБ¤єёё¦ ГЯГвЗПґВ ЗБ·ОЅГБ®  
		
*/

CREATE PROCEDURE	 [dbo].[SP_WEEKDAY_INSERT]
AS
Begin
	
	
	Declare @YesterDay	datetime
	Declare @SearchDay	char(10)
	Declare @Week		tinyint
	Declare @WeekDay	tinyint
	Declare @Year		smallint

	SET		@YesterDay	= DateAdd( day, -1, getdate())		-- ЗцАз ЅГЅєЕЫАЗ ѕоБ¦ іЇВҐё¦ ±ёЗСґЩ
	SET		@SearchDay = Convert(varchar, datepart( year ,  @YesterDay  ))   + '-'+ 
						   Convert( varchar ,  datepart( month ,  @YesterDay  ) )  + '-'+ 
						   Convert( varchar, datepart( day , @YesterDay  )  )		
	SET		 @Week	= DatePart( Week, @YesterDay)
	SET		 @WeekDay	= DatePart( Weekday, @YesterDay)
	SET		 @Year		= DatePart( Year, @YesterDay)

			

	SET NOCOUNT ON		
	
	Begin Tran
		
		INSERT INTO WeekDay_Statistics( 
			Year, Week, WeekDay, [Hour], MaxCount, MinCount, AvgCount			
		)   SELECT  
			@Year		As Year,
			@Week	As Week,
			@WeekDay	As WeekDay,			
			datepart(Hour, ConnectTime) As Hour,
			Max(TotalCount) As MaxCount,
			Min(TotalCount) As MinCount,
			Avg(TotalCount) As AvgCount			
        	    	FROM  Day_Statistics
		WHERE ConnectTime Between Convert( datetime , @SearchDay + '  00:00:00' )  and  Convert( datetime,   @SearchDay + '  23:59:59' )
		group by datepart(hour, ConnectTime)			
		order by Hour


		if(@@error > 0 )
			rollback tran
		else	
			commit tran
	
	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[SP_WEEKDAY_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_WEEKDAY_SELECT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ЗцАз БЦїЎ ґлЗС µїБўЕл°и Б¤єё »кГв 
		
*/

CREATE Procedure [dbo].[SP_WEEKDAY_SELECT]
(
	@Year		smallint, 
	@WEEK	smallint
)
As
Begin
	SET NOCOUNT ON

	-- ЗцАз БЦїЎ ґлЗС 	
	if ( @WEEK = 0 )
	begin
		SET	@WEEK = DatePart( wk, getdate() )  
		SET	@Year =  DatePart( year, getdate() )  
	End	
		
	SELECT WeekDay, [Hour], MaxCount, MinCount, AvgCount FROM WeekDay_Statistics
	WHERE Week = @WEEK and Year = @Year
		

	SET NOCOUNT OFF
end

GO
/****** Object:  StoredProcedure [dbo].[SP_YEAR_INSERT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_YEAR_INSERT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ѕоБ¦їЎ ґлЗС ЗП·з ґЬА§АЗ БўјУБ¤єёё¦ ГЯГвЗПґВ ЗБ·ОЅГБ®  
		
*/

CREATE PROCEDURE	 [dbo].[SP_YEAR_INSERT]
AS
Begin
	
	
	Declare @YesterDay	datetime
	Declare @SearchDay	char(10)

	SET		@YesterDay	= DateAdd( day, -1, getdate())		-- ЗцАз ЅГЅєЕЫАЗ ѕоБ¦ іЇВҐё¦ ±ёЗСґЩ
	SET		@SearchDay = Convert(varchar, datepart( year ,  @YesterDay  ))   + '-'+ 
						   Convert( varchar ,  datepart( month ,  @YesterDay  ) )  + '-'+ 
						   Convert( varchar, datepart( day , @YesterDay  )  )		
			

	SET NOCOUNT ON		
	
	Begin Tran
		
		INSERT INTO Year_Statistics( 
			Year, Month,  Day, MaxCount, MinCount, AvgCount			
		)   SELECT  
			Convert(varchar, datepart( year ,  @YesterDay  )) As Year,
			Convert(varchar, datepart( month ,  @YesterDay  )) As month,
			Convert(varchar, datepart( day ,  @YesterDay  )) As day,
			Max(TotalCount) As MaxCount,
			Min(TotalCount) As MinCount,
			Avg(TotalCount) As AvgCount			
        	    	FROM  Day_Statistics
		WHERE ConnectTime Between Convert( datetime , @SearchDay + '  00:00:00' )  and  Convert( datetime,   @SearchDay + '  23:59:59' )


		if(@@error > 0 )
			rollback tran
		else	
			commit tran
	
	SET NOCOUNT OFF	
End
GO
/****** Object:  StoredProcedure [dbo].[SP_YEAR_SELECT]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЅєЕдѕоµе ЗБ·ОЅГБ®ён : SP_YEAR_SELECT
	2) АЫѕчАП : 2002. 10. 10.
	3) АЫѕчАЪён :  ГЯјч 
	4) АЫѕчБ¤єё
	
		ЗцАзівµµїЎ ґлЗС µїБўЗцИІА» єёї©БШґЩ. 
		
*/

CREATE Procedure [dbo].[SP_YEAR_SELECT]
(
	@YEAR	smallint
)
As
Begin
	SET NOCOUNT ON

	-- ЗцАз івµµїЎ ґлЗС Б¤єёё¦ °ЎБ®їВґЩ. 	
	if ( @YEAR = 0 )
	begin
		SET	@YEAR = DatePart( year, getdate() )  
	End	
	
	SELECT Month, Day,   MaxCount, MinCount, AvgCount FROM Year_Statistics
	WHERE Year = @YEAR

	SET NOCOUNT OFF
end

GO
/****** Object:  StoredProcedure [dbo].[UP_EMP6]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROC [dbo].[UP_EMP6] @JOB VARCHAR(4), @YEAR CHAR(4), @SAL BIGINT
AS
	SELECT * FROM EMP
		WHERE JOB = @JOB AND DATEPART(YY,EDATE) = @YEAR
			AND SAL > @SAL


GO
/****** Object:  StoredProcedure [dbo].[USP_Block_CanCel]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=======================================================================--
--	ЅГ  Ѕє  ЕЫ  :  єн·°ЗШБ¦ 
--	ЗБ·ОБ§Ж®  :   ME_Muonline
--	ЗФ  јц  ён  : USP_Block_CanCel
--	і»      їл  :   єн·°°­Б¦ ЗШБ¦ 
--	№Э  ИЇ  °Є  :  
--	АЫ  јє  АЪ  :  ГЯјч 
--	јц  Б¤  АП  :  2002ів 09їщ 03АП
--=======================================================================--
CREATE PROCEDURE [dbo].[USP_Block_CanCel]
(	
	
	-- »зїлАЪ ёЕ°ієЇјцј±ѕр
	--========================================================================--
	@block_guid			 int ,	
	@memb_guid			int,
	@rels_days			char(8),
	@hand_meth			varchar(1000),
	@func_guid			int,
	@func_name			varchar(50),
	@Result_Data 		      	TINYINT OUTPUT	-- №ЭИЇµЗґВ »уЕВ°Є (ЅЗЖРДЪµе:9  јє°шДЪµе:1)
	--========================================================================-------------------------
	
)
AS
BEGIN	
	SET NOCOUNT ON

	BEGIN TRAN

            
        --========================================================================--=====================================
	-- T_Event_Drawing  ЕЧАМєнїЎј­ і»їл °ЎБ®їА±в
	--========================================================================--=====================================
	
	UPDATE BLOCKING
		
		Set ctl1_code = '9',
		      rels_days = @rels_days ,
		      take_cont  = @hand_meth
                           WHERE block_guid = @block_guid

	
	UPDATE MEMB_INFO SET bloc_code ='0'  WHERE memb_guid = @memb_guid


	INSERT INTO BLOCKING_LOG(appl_days, admin_guid, block_guid, dist_code,admin_name) 
		
		VALUES(getdate(), @func_guid , @block_guid ,'88',@func_name)
			

		
           --========================================================================--=====================================
	-- їЎ·ЇДЪµе ё®ЕПЗП±в  9 : їЎ·ЇДЪµе  Б¤»уДЪµе: 1
 	--========================================================================--=====================================
	IF (@@ERROR <> 0)
	        BEGIN
		SET @Result_Data = 9
		ROLLBACK TRAN
	        END
	ELSE
	          BEGIN
		SET @Result_Data = 1
		COMMIT TRAN
	           END
 --========================================================================--=====================================
	SET NOCOUNT OFF
 --========================================================================--=====================================
END

GO
/****** Object:  StoredProcedure [dbo].[USP_BloodCastle5_Ranking]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*******************************************************************************************************
 ЗБ·ОБ§Ж®ён : №ВїВ¶уАО ver3.0
 »зАМЖ®ён : event.muonline.co.kr
 ЗБ·ОЅГАъён : USP_BloodCastle5_Ranking
 ±вґЙ : ЅГ°Јє° ·©Е·А» °ЎБ®їВґЩ.
 ЖдАМБцён : 
 АФ·В : @BridgeSearch
 №ЭИЇ : 
 АМ·В : 2005.06.21 АМЗэ°ж
********************************************************************************************************/
CREATE PROCEDURE [dbo].[USP_BloodCastle5_Ranking]

	@BridgeSearch	TINYINT

AS

SET NOCOUNT ON

	SELECT TOP 50  T1.CharacterName, T1.Server, 0 AS Point, MAX(T1.MinLeftTime) as MinLeftTime, T1.Bridge, 

	(SELECT TOP 1 T2.AlivePartyCount FROM EVENT_INFO_BC_5TH T2 WHERE T1.CharacterName = T2.CharacterName AND T1.Server = T2.Server 
	 AND T1.Bridge = T2.Bridge AND T1.AccountID = T2.AccountID AND T2.AlivePartyCount >= 5 ORDER BY T2.MinLeftTime DESC) AS AlivePartyCount,

	(SELECT TOP 1 Convert(char(8),T3.RegDate,112) FROM EVENT_INFO_BC_5TH T3 WHERE T1.CharacterName = T3.CharacterName AND T1.Server = T3.Server 
	 AND T1.Bridge = T3.Bridge AND T1.AccountID = T3.AccountID AND T3.AlivePartyCount >= 5 ORDER BY T3.MinLeftTime DESC, T3.RegDate) AS RegDate
	FROM EVENT_INFO_BC_5TH  T1 WHERE AlivePartyCount > 4 AND T1.Server <> 13 

	GROUP BY T1.CharacterName,T1.Server, T1.Bridge,T1.AccountID
	Having  T1.Bridge = @BridgeSearch

	ORDER BY AlivePartyCount DESC, MinLeftTime DESC, RegDate, T1.Server


	


			
SET NOCOUNT OFF
GO
/****** Object:  StoredProcedure [dbo].[USP_DAY_SELECT_ALL_SERVER]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


-- БЦј­№ц(ё¶ѕЯ,А§±Ыµо)є° µїБў ЗцИІ °Л»ц
-- БЦј­№цє° µїБўА» °ў°ў °Л»ц ИД  ёрµО Гв·В
-- ј­єкј­№ц °н·БЗПБц ѕКАЅ
-- ЅГАЫ іЇВҐїН іЎіЇВҐАЗ ВчАМґВ ГЦґл 2~3АП·О °н·Б µК

-- ѕИБШј®
-- 2003.08.01 

CREATE PROCEDURE [dbo].[USP_DAY_SELECT_ALL_SERVER]
(
	@S_Year		smallint,
	@S_Month		tinyint,
	@S_Day		tinyint,

	@E_Year		smallint,
	@E_Month		tinyint,
	@E_Day		tinyint
)
As
Begin
	SET NOCOUNT ON

	Declare	 @StartDate	Varchar(50)
	Declare  @EndDate	Varchar(50)
	
	Set	@StartDate = Convert(varchar, @S_Year)  + '-'+  Convert(varchar, @S_Month)  + '-'+ Convert(varchar, @S_Day  )
	Set	@EndDate = Convert(varchar, @E_Year)  + '-'+  Convert(varchar, @E_Month)  + '-'+ Convert(varchar, @E_Day  )	

	select 
		datePart( year, ConnectTime) As [Year] ,
		datePart( month, ConnectTime) As [Month],
		datePart( day, ConnectTime ) As [Day] ,
		datePart( hour, ConnectTime) As [Hour] ,			
		datePart( Minute, ConnectTime) As [Minute] ,
		--Server,			
		sum(ConnectCount)  As TotalCount

	from SameTimeConnectLog 
	With( READUNCOMMITTED )
	where
		 ConnectTime  Between  Convert( datetime , @StartDate + '  00:00:00' )  
			  	     and  Convert( datetime,   @EndDate + '  23:59:59' )
	
	--group by Server, ConnectTime 	
	--order by Server , ConnectTime asc
	group by ConnectTime 	
	order by ConnectTime asc  	
	
	SET NOCOUNT OFF

End
GO
/****** Object:  StoredProcedure [dbo].[USP_DAY_SELECT_EACH_SERVER]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


-- БЦј­№ц(ё¶ѕЯ,А§±Ыµо)є° µїБў ЗцИІ °Л»ц
-- ЖЇБ¤ БЦј­№ц µїБў °Л»ц
-- ј­єкј­№ц °н·БЗПБц ѕКАЅ
-- ЅГАЫ іЇВҐїН іЎіЇВҐАЗ ВчАМґВ ГЦґл 2~3АП·О °н·Б µК

-- ѕИБШј®
-- 2003.08.01

CREATE PROCEDURE [dbo].[USP_DAY_SELECT_EACH_SERVER]
(
	@S_Year		smallint,
	@S_Month		tinyint,
	@S_Day		tinyint,

	@E_Year		smallint,
	@E_Month		tinyint,
	@E_Day		tinyint,
	@Server		tinyint
)
As
Begin
	SET NOCOUNT ON

	Declare	 @StartDate	Varchar(50)
	Declare  @EndDate	Varchar(50)
	
	Set	@StartDate = Convert(varchar, @S_Year)  + '-'+  Convert(varchar, @S_Month)  + '-'+ Convert(varchar, @S_Day  )
	Set	@EndDate = Convert(varchar, @E_Year)  + '-'+  Convert(varchar, @E_Month)  + '-'+ Convert(varchar, @E_Day  )	

	select 
		datePart( year, ConnectTime) As [Year] ,
		datePart( month, ConnectTime) As [Month],
		datePart( day, ConnectTime ) As [Day] ,
		datePart( hour, ConnectTime) As [Hour] ,			
		datePart( Minute, ConnectTime) As [Minute] ,
		Server,			
		sum(ConnectCount)  As TotalCount

	from SameTimeConnectLog 
	With( READUNCOMMITTED )
	where
		Server = @Server
		AND ConnectTime 
			 Between  Convert( datetime , @StartDate + '  00:00:00' )  
			and  Convert( datetime,   @EndDate + '  23:59:59' )
	
	group by Server, ConnectTime 	
	order by Server , ConnectTime asc 	
	
	SET NOCOUNT OFF

End
GO
/****** Object:  StoredProcedure [dbo].[USP_DAY_SELECT_EACH_SUBSERVER]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


-- БЦј­№ц(ё¶ѕЯ,А§±Ыµо)є° µїБў ЗцИІ °Л»ц
-- ЖЇБ¤ БЦј­№ц µїБў °Л»ц
-- ј­єкј­№ц °н·БЗПБц ѕКАЅ
-- ЅГАЫ іЇВҐїН іЎіЇВҐАЗ ВчАМґВ ГЦґл 2~3АП·О °н·Б µК

-- ѕИБШј®
-- 2003.08.01

CREATE PROCEDURE [dbo].[USP_DAY_SELECT_EACH_SUBSERVER]
(
	@S_Year		smallint,
	@S_Month		tinyint,
	@S_Day		tinyint,

	@E_Year		smallint,
	@E_Month		tinyint,
	@E_Day		tinyint,

	@Server		tinyint
)
As
Begin
	SET NOCOUNT ON

	Declare	 @StartDate	Varchar(50)
	Declare  @EndDate	Varchar(50)
	
	Set	@StartDate = Convert(varchar, @S_Year)  + '-'+  Convert(varchar, @S_Month)  + '-'+ Convert(varchar, @S_Day  )
	Set	@EndDate = Convert(varchar, @E_Year)  + '-'+  Convert(varchar, @E_Month)  + '-'+ Convert(varchar, @E_Day  )	

	select 
		datePart( year, ConnectTime) As [Year] ,
		datePart( month, ConnectTime) As [Month],
		datePart( day, ConnectTime ) As [Day] ,
		datePart( hour, ConnectTime) As [Hour] ,			
		datePart( Minute, ConnectTime) As [Minute] ,
		Server,
		SubServer,			
		sum(ConnectCount)  As TotalCount

	from SameTimeConnectLog 
	With( READUNCOMMITTED )
	where
		Server = @Server
		AND ConnectTime 
			 Between  Convert( datetime , @StartDate + '  00:00:00' )  
			and  Convert( datetime,   @EndDate + '  23:59:59' )
	
	group by  ConnectTime, Server, SubServer 	
	order by ConnectTime, Server , SubServer  asc 	
	
	SET NOCOUNT OFF

End
GO
/****** Object:  StoredProcedure [dbo].[USP_SelectAdminFriendMail]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/*********************************************************************
	АЫјєАП:		2005-05-10		
	АЫјєАЪ:		Б¤БѕЗц
	SPён:		USP_SelectAdminFriendMail
	SPі»їл:
			-ј­№ц»уАЗ ДЈ±ё ёЮАПА» °ЎБ®їА°н ёЮАПАє »иБ¦ Гіё® ЗСґЩ.
			
	ї№Б¦:
			EXEC dbo.USP_SelectAdminFriendMail
	АФ·ВєЇјц(°Є):

	№ЭИЇєЇјц(°Є):
			[MemoIndex]
		,	[GUID]
		,	[FriendName]
		,	[wDate]
		,	[Subject]
		,	[bRead]
		,	[Memo]


	ВьБ¶№®ј­:

	єс°н:

	EDIT HISTORY:
			
			050510	ГК±в±ёЗц (ї¬±ё°і№Я Б¤БѕЗц)

**********************************************************************/


CREATE  PROC [dbo].[USP_SelectAdminFriendMail]
	@i_MailAdminGuid	INT

AS
SET NOCOUNT ON

	
	DECLARE @i_AdminMailCount INT
	
	--ЗцАз ёЮАП °№јц И®АО
	SELECT @i_AdminMailCount = MemoCount FROM T_FriendMain WHERE GUID = @i_MailAdminGuid
	
	IF(@i_AdminMailCount > 0)
	BEGIN
		SELECT 
			MemoIndex, GUID, FriendName, wDate, Subject, bRead,Memo 
		FROM 
			T_FriendMail 
		WHERE 
			GUID =  @i_MailAdminGuid



		/* Ж®·ЈАијЗ ЅГАЫ */
		BEGIN TRANSACTION
		
			/*°ьё®АЪ ёЮАП ёрµО »иБ¦ */
			DELETE 
			FROM
				T_FriendMail 
			WHERE 
				GUID = @i_MailAdminGuid
		
			/* ЗцАз №ЮАє ГС ёЮАПјц ГК±вИ­ */
			UPDATE
				T_FriendMain
			SET
				MemoTotal = 0
			WHERE
				GUID = @i_MailAdminGuid

		IF @@ERROR = 0 
		BEGIN
			COMMIT TRANSACTION
		END
		ELSE
		BEGIN
			ROLLBACK TRANSACTION
		END



	END			
	


SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [dbo].[USP_SelectAdminFriendMailGuid]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


/*********************************************************************
	АЫјєАП:		2005-05-10		
	АЫјєАЪ:		Б¤БѕЗц
	SPён:		USP_SelectAdminFriendMailGuid
	SPі»їл:
			-ј­№ц»уАЗ °ьё®АЪАЗ GUID ё¦ °ЎБ®їВґЩ.
			
	ї№Б¦:
			EXEC dbo.USP_SelectAdminFriendMailGuid 'webzen'
	АФ·ВєЇјц(°Є):

	№ЭИЇєЇјц(°Є):
			[AdminName]

	ВьБ¶№®ј­:

	єс°н:

	EDIT HISTORY:
			
			050510	ГК±в±ёЗц (ї¬±ё°і№Я Б¤БѕЗц)

**********************************************************************/


CREATE  PROC [dbo].[USP_SelectAdminFriendMailGuid]
	@vc_AdminName	VARCHAR(10)

AS
SET NOCOUNT ON

	

	/* °ьё®АЪ GUID °ЎБ®їА±в*/
	SELECT GUID from T_FriendMain where Name = @vc_AdminName;	
	


SET NOCOUNT OFF

GO
/****** Object:  StoredProcedure [dbo].[WZ_CharMoveReset]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[WZ_CharMoveReset] 
	@AccountID			varchar(10),		-- °иБ¤ён 
	@Name				varchar(10)			-- ДіёЇЕНён
AS
Begin

	SET NOCOUNT ON
	SET	XACT_ABORT ON					-- їЎ·Ї№Я»эЅГ ёрµз Ж®·ЈАијЗ ГлјТ  
	
	DECLARE	@Result				tinyint	
	DECLARE	@ResultLowCount		int
	DECLARE	@Class				tinyint
	DECLARE	@Ctl1_Code			tinyint
	DECLARE	@SQLEXEC			varchar(1000)
	DECLARE	@ErrorCheck			INT
	DECLARE 	@g1 varchar(10), @g2 varchar(10), @g3 varchar(10), @g4 varchar(10), @g5 varchar(10)
	DECLARE 	@MoveCnt tinyint		
	DECLARE 	@ChangeMoveCnt	tinyint		
	DECLARE	@SqlStmt			VARCHAR(700)		
	DECLARE	@SqlStmt2			VARCHAR(700)		

	SET LOCK_TIMEOUT	1000			-- Ж®·ЈАијЗ Ае±в Аб±ЭГіё®ё¦ ё·±в А§ЗШј­ 	
	SET @Result = 0x00	
	SET @ErrorCheck = 0x00

	--====================================================================================
	-- ДіёЇЕН БёАзї©єО И®АО 
	--====================================================================================
	SELECT @Class = Class, @Ctl1_Code = CtlCode  FROM  Character  WHERE Name = @Name
	
	-- Гіё®°б°ъ ГјЕ© 	
	SELECT @ResultLowCount = @@rowcount, @ErrorCheck = @@error

	-- ДіёЇЕН°Ў БёАзЗПБц ѕКґВґЩ.  						
	IF @ResultLowCount = 0 
	begin
		SET @Result	= 0x02			
		GOTO ON_ERROR						
	end

	-- їЎ·Ї№Я»э(Ж®·ЈАијЗ Аб±ЭАМіЄ ¶ЗґВ ±вЕё №®Б¦ №Я»эЅГ Гіё®) 
	IF @ErrorCheck  <> 0 GOTO ON_ERROR

	-------------------------------------------------------------------------------------
	-- ДіёЇЕНАЗ »уЕВ°Ў АМµїЗТ јц ѕшґВ »уЕВ(0x80)
	-- АМµї»уЕВАЗ ДіёЇЕН ¶ЗґВ ±вЕё єн·°»уЕВАЗ ДіёЇЕНАЗ °жїм АМµїАМ єТ°ЎґЙ 							
	-------------------------------------------------------------------------------------
	if  ( (@Ctl1_Code & 127 ) > 0 )
	BEGIN
		SET @Result	= 0x03			
		GOTO ON_ERROR						
	END 

	--====================================================================================
	-- їшє» ДіёЇЕН ЅЅ·ФБ¤єё ГјЕ© 
	--====================================================================================
	SELECT  @g1=GameID1, @g2=GameID2, @g3=GameID3, @g4=GameID4, @g5=GameID5, @MoveCnt = MoveCnt 
	FROM dbo.AccountCharacter 	Where Id = @AccountID 		
	
	-- Гіё®°б°ъ ГјЕ© 	
	SELECT @ResultLowCount = @@rowcount, @ErrorCheck = @@error

	--// °иБ¤АМ БёАзЗПБц ѕКґВґЩ. 
	if @ResultLowCount = 0 
	begin
		SET @Result	= 0x02			
		GOTO ON_ERROR						
	end

	-- їЎ·Ї№Я»э(Ж®·ЈАијЗ Аб±ЭАМіЄ ±віЄ №®Б¦ №Я»эЅГ Гіё®) 
	IF @ErrorCheck  <> 0 GOTO ON_ERROR


	-------------------------------------------------------------------------------------
	-- АМµїИЅјц ГјЕ© 
	-------------------------------------------------------------------------------------
	SET @MoveCnt =  0

	-------------------------------------------------------------------------------------
	-- їшє» ДіёЇЕН Е¬ё®ѕо  & АМµїИЅјц UPDATE 
	-------------------------------------------------------------------------------------
	SET @SqlStmt = 'UPDATE AccountCharacter  '

	IF ( @g1 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  '
	ELSE IF ( @g2 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  '
	ELSE IF ( @g3 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  '
	ELSE IF ( @g4 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  '
	ELSE IF ( @g5 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  '
	ELSE 				
		SET @Result	= 0x05--// ЗШґз °иБ¤їЎ ґлЗС ДіёЇЕН°Ў БёАз ЗПБц ѕКґВґЩ. 

	IF ( @Result <> 0 )
		GOTO ON_ERROR

	SET @SqlStmt = @SqlStmt + ' MoveCnt =  ' + CONVERT(VARCHAR, @MoveCnt )					
	SET @SqlStmt = @SqlStmt + ' WHERE Id =  ''' + @AccountID	 + ''''				

	--====================================================================================
	-- ДіёЇЕНАЗ єЇ°ж 
	--====================================================================================
	SET @SqlStmt2 = 'UPDATE Character '
	SET @SqlStmt2 = @SqlStmt2 + 'SET  '
	SET @SqlStmt2 = @SqlStmt2 + 'CtlCode = ' + CONVERT(VARCHAR, @Ctl1_Code & 127	)	-- ДіёЇЕН АМµї»уЕВё¦ їшє»Аё·О єЇ°ж[ АМµїДіёЇ ДЪµеґВ 0x80 ]
	SET @SqlStmt2 = @SqlStmt2 + ' WHERE Name = ''' +  @Name + ''''


	--====================================================================================
	--Ж®·ЈАијЗ АЫѕч Гіё®  
	--====================================================================================
	BEGIN TRANSACTION 

	-- °иБ¤ДіёЇЕН Б¤єё єЇ°ж Гіё® 
	EXEC(@SqlStmt)
	SELECT @ResultLowCount = @@rowcount,  @ErrorCheck = @@error
	IF  @ResultLowCount = 0  GOTO ON_TRN_ERROR
	IF  @ErrorCheck  <> 0 GOTO ON_TRN_ERROR

	-- ДіёЇЕН Б¤єё єЇ°ж Гіё® 
	EXEC(@SqlStmt2)
	SELECT @ResultLowCount = @@rowcount,  @ErrorCheck = @@error
	IF  @ResultLowCount = 0  GOTO ON_TRN_ERROR
	IF  @ErrorCheck  <> 0 GOTO ON_TRN_ERROR

	--====================================================================================
	-- Ж®·ЈАијЗ °б°ъ Гіё® 
	--====================================================================================
ON_TRN_ERROR:
	IF ( @Result  <> 0 ) OR (@ErrorCheck <> 0)
	BEGIN
		IF @Result = 0 
			SET @Result = 0x09 		--// DB Error

		ROLLBACK TRAN
	END
	ELSE
		COMMIT	TRAN

ON_ERROR:
	IF @ErrorCheck <> 0
	BEGIN
		SET @Result = 0x09 			--// DB Error 
	END 


	SELECT @Result	

	SET NOCOUNT OFF
	SET	XACT_ABORT OFF
END


GO
/****** Object:  StoredProcedure [dbo].[WZ_CONNECT_MEMB]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WZ_CONNECT_MEMB]
@memb___id varchar(10),
@ServerName  varchar(20),
@IP varchar(15)	
 AS
Begin	
set nocount on
	Declare  @find_id varchar(10)	
	Declare  @ConnectStat tinyint
	Set @find_id = 'NOT'
	Set @ConnectStat = 1		-- БўјУ »уЕВ °Є 1 = БўјУ, 0 = БўјУX

	select @find_id = S.memb___id from MEMB_STAT S INNER JOIN MEMB_INFO I ON S.memb___id = I.memb___id 
	       where I.memb___id = @memb___id

	if( @find_id = 'NOT' )
	begin		
		insert into MEMB_STAT (memb___id,ConnectStat,ServerName,IP,ConnectTM)
		values(@memb___id,  @ConnectStat, @ServerName, @IP, getdate())
	end
	else		
		update MEMB_STAT set ConnectStat = @ConnectStat,
					 ServerName = @ServerName,IP = @IP,
					 ConnectTM = getdate()
       	 where memb___id = @memb___id
end
GO
/****** Object:  StoredProcedure [dbo].[WZ_CreateCharacter]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
	1) ЗБ·ОЅГБ®ён : WZ_CreateCharacter
	2) АЫѕчАП : 2003. 09. 15.
	3) АЫѕчАЪён :  ГЯјч 
	4) АьґЮАОАЪ :
			°иБ¤Б¤єё, ДіёЇЕНён, Е¬·ЎЅє ЕёАФ, јє°шї©єО №ЭИЇБ¤єё 
	5) №ЭИЇАОАЪ(Result) :
			1 : µїАП ДіёЇЕНён БёАз 			
			2 : єуЅЅ·ФАМ БёАз ЗПБц ѕКАЅ
			3 єОЕН  ~ : SQL Error Code
	6) Е¬·ЎЅє ЕёАФ
			0 : Ижё¶№э»з
			16 : Иж±в»з
			32 : їдБ¤ 
			48 : ё¶°Л»з
			64:ґЩЕ©·Оµе	
	7) VERSION
		1 

	8) Last Update Date : 2004.06.15
	
2003.10.16  : Ж®·ЈАијЗ А§ДЎ їА·щ  (SaemSong)
	BEGIN TRAN №®АМ µйѕо°Ў±в АьїЎ µїАПДіёЇён БёАзЅГ ProcEnd ·О GOTO ЗСґЩ 
   	АМ¶§, ProcEndїЎј± ·С№йАМіЄ Дї№ФА» ЅЗЗаЗП№З·О АОЗШ
	TranProcEndё¦ ГЯ°ЎЗПї© Ж®·ЈБ§јЗ Гіё®ё¦ ЗПБц ѕКґВ °чАё·О GOTOЗСґЩ. 
*/

CREATE Procedure [dbo].[WZ_CreateCharacter] 
	@AccountID		varchar(10),		--// °иБ¤ Б¤єё 
	@Name			varchar(10),		--// ДіёЇЕН 
	@Class			tinyint			--// Class Type
AS
Begin

	SET NOCOUNT ON
	SET	XACT_ABORT ON
	DECLARE		@Result		tinyint

	--//  °б°ъ°Є ГК±вИ­ 
	SET @Result = 0x00	

	--====================================================================================
	--	 ДіёЇЕН БёАзї©єО И®АО 
	--====================================================================================
	If EXISTS ( SELECT  Name  FROM  Character WHERE Name = @Name )
	begin
		SET @Result	= 0x01				--// µїАП ДіёЇЕНён БёАз 						
		GOTO ProcEnd						
	end 

	BEGIN TRAN
	--====================================================================================
	--	 °иБ¤ БёАз ї©єО И®АО  №Ч єу ЅЅ·Ф Б¤єё И®АОЗПї© АъАе 		
	--====================================================================================
	If NOT EXISTS ( SELECT  Id  FROM  AccountCharacter WHERE Id = @AccountID )
		begin
			INSERT INTO dbo.AccountCharacter(Id, GameID1, GameID2, GameID3, GameID4, GameID5, GameIDC) 
			VALUES(@AccountID, @Name, NULL, NULL, NULL, NULL, NULL)

			SET @Result  = @@Error
		end 
	else
		begin
			--// ДіёЇЕН єу ЅЅ·Ф јіБ¤ 
			Declare @g1 varchar(10), @g2 varchar(10), @g3 varchar(10), @g4 varchar(10), @g5 varchar(10)						
			SELECT @g1=GameID1, @g2=GameID2, @g3=GameID3, @g4=GameID4, @g5=GameID5 FROM dbo.AccountCharacter Where Id = @AccountID 			

			if( ( @g1 Is NULL) OR (Len(@g1) = 0))
				begin
					UPDATE AccountCharacter SET  GameID1 = @Name
					WHERE Id = @AccountID
										
					SET @Result  = @@Error
				end 
			else	 if( @g2  Is NULL OR Len(@g2) = 0)
				begin
					UPDATE AccountCharacter SET  GameID2 = @Name
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else	 if( @g3  Is NULL OR Len(@g3) = 0)
				begin			
					UPDATE AccountCharacter SET  GameID3 = @Name
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else	 if( @g4 Is NULL OR Len(@g4) = 0)
				begin
					UPDATE AccountCharacter SET  GameID4 = @Name
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else	 if( @g5 Is NULL OR Len(@g5) = 0)
				begin
					UPDATE AccountCharacter SET  GameID5 = @Name
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 		
			else 
				--// ЗШґз єу ЅЅ·Ф Б¤єё°Ў БёАз ЗПБц ѕКґЩ. 	
				begin					
					SET @Result	= 0x03							
					GOTO TranProcEnd								
				end 			 
		end 

	
	

	--====================================================================================
	--	 ДіёЇЕН Б¤єё АъАе 
	--====================================================================================
	if( @Result <> 0 )
		begin
			GOTO TranProcEnd		
		end 
	else
		begin
			INSERT INTO dbo.Character(AccountID, Name, cLevel, LevelUpPoint, Class, Strength, Dexterity, Vitality, Energy, Inventory,MagicList, 
					Life, MaxLife, Mana, MaxMana, MapNumber, MapPosX, MapPosY,  MDate, LDate, Quest, DbVersion, Leadership )
			SELECT @AccountID As AccountID, @Name As Name, Level, LevelUpPoint, @Class As Class, 
				Strength, Dexterity, Vitality, Energy, Inventory,MagicList,  Life, MaxLife, Mana, MaxMana, MapNumber, MapPosX, MapPosY,
				getdate() As MDate, getdate() As LDate, Quest, DbVersion, Leadership
			FROM  DefaultClassType WHERE Class = @Class					

			SET @Result  = @@Error
		end 

TranProcEnd:	-- GOTO
	IF ( @Result  <> 0 )
		ROLLBACK TRAN
	ELSE
		COMMIT	TRAN

ProcEnd:
	SET NOCOUNT OFF
	SET	XACT_ABORT OFF

	--====================================================================================
	--  °б°ъ°Є №ЭИЇ Гіё® 
	-- 0x00 : ДіёЇЕН БёАз, 0x01 : јє°шїП·б, 0x02 : ДіёЇЕН »эјє ЅЗЖР , 0x03 : єуЅЅ·Ф БёАзЗПБц ѕКґВґЩ   
	--====================================================================================
	SELECT
	   CASE @Result
	      WHEN 0x00 THEN 0x01		--// јє°ш №ЭИЇ 
	      WHEN 0x01 THEN 0x00		--// ДіёЇЕН БёАз 
	      WHEN 0x03 THEN 0x03		--// єуЅЅ·ФАМ БёАзЗПБц ѕКґВґЩ. 
	      ELSE 0x02				--// ±вЕё їЎ·ЇДЪµеґВ »эјє »шЖР №ЭИЇ  
	   END AS Result 
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CreateCharacter_GetVersion]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



--//***********************************************************************************************************************
--// Version Procedure 
/*
	1) ЗБ·ОЅГБ®ён : WZ_CreateCharacter_Version
	2) АЫѕчАП : 2003. 10. 15.
	3) АЫѕчАЪён :  ГЯјч 
	4) АьґЮАОАЪ :			
	5) №ЭИЇАОАЪ(Result) : №цБЇ		
	6) Last Update Date : 2003.10.20	
*/
CREATE PROCEDURE [dbo].[WZ_CreateCharacter_GetVersion]
AS
BEGIN
	SELECT 1
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_CheckSiegeGuildList]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: °шјєј­№цё®ЅєЖ®їЎ АЦґВБц °Л»зЗСґЩ.
--// єОј­	: MuStudio 
--// ёёµзіЇ	: 2005.01.11
--// јцБ¤АП	: 2005.03.15
--// ёёµйАМ	: ѕИБШј®
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_CheckSiegeGuildList]
	@szGuildName		varchar(8)
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	DECLARE @iEnd INT

	SELECT @iEnd = SIEGE_ENDED FROM MuCastle_DATA

	IF @iEnd = 1
	BEGIN
		SELECT 0 As QueryResult
	END
	ELSE IF EXISTS ( SELECT GUILD_NAME FROM MuCastle_SIEGE_GUILDLIST  WITH (READUNCOMMITTED) 
				WHERE GUILD_NAME = @szGuildName)
	BEGIN
		SELECT 1 As QueryResult	
	END
	ELSE
	BEGIN
		IF EXISTS ( SELECT REG_SIEGE_GUILD FROM MuCastle_REG_SIEGE WITH (READUNCOMMITTED) 
				WHERE REG_SIEGE_GUILD = @szGuildName AND IS_GIVEUP = 0)
		BEGIN
			SELECT 1 As QueryResult
		END
		ELSE
		BEGIN
			SELECT 0 As QueryResult	
		END
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetAllGuildMarkRegInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ёрµз °шјєГш ±жµеАЗ ё¶Е©µо·П Б¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetAllGuildMarkRegInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	SELECT TOP 100 * FROM MuCastle_REG_SIEGE WITH (READUNCOMMITTED)
	WHERE MAP_SVR_GROUP = @iMapSvrGroup
	ORDER BY SEQ_NUM DESC

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCalcRegGuildList]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјє ЅЕГ» ±жµеµйАЗ ёрµз ЗКїдЗС Б¤єёµйА» Б¤ё®ЗШј­ ё®ЅєЖ®ё¦ ёёµйѕоБШґЩ. (»уА§ N°іёё)
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCalcRegGuildList]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	DECLARE T_CURSOR CURSOR FAST_FORWARD
	FOR SELECT TOP 100 * FROM MuCastle_REG_SIEGE	WHERE MAP_SVR_GROUP = @iMapSvrGroup AND IS_GIVEUP = 0 ORDER BY SEQ_NUM DESC
	
	OPEN T_CURSOR
	
	DECLARE	@iMapSvrNum			INT
	DECLARE	@szRegGuild			VARCHAR(8)
	DECLARE	@iRegMarks			INT
	DECLARE	@iIsGiveUp			INT
	DECLARE	@iSeqNum			INT

	DECLARE	@iGuildMemberCount		INT
	DECLARE	@iGuildMasterLevel		INT

	CREATE TABLE #T_REG_GUILDLIST  (
		[REG_SIEGE_GUILD] [varchar] (8) NOT NULL ,
		[REG_MARKS] [int] NOT NULL ,
		[GUILD_MEMBER] [int] NOT NULL ,
		[GM_LEVEL] [int] NOT NULL ,
		[SEQ_NUM] [int] NOT NULL 
	) ON [PRIMARY]
	
	FETCH FROM T_CURSOR INTO @iMapSvrNum, @szRegGuild, @iRegMarks, @iIsGiveUp, @iSeqNum
	WHILE (@@FETCH_STATUS = 0)
	BEGIN
		IF EXISTS ( SELECT G_Name FROM Guild  WITH (READUNCOMMITTED) WHERE G_Name = @szRegGuild)
		BEGIN
			DECLARE @szGuildMaster	VARCHAR(10)
			SELECT @szGuildMaster = G_Master FROM Guild  WHERE G_Name = @szRegGuild

			IF EXISTS ( SELECT Name FROM Character WITH (READUNCOMMITTED) WHERE Name = @szGuildMaster)
			BEGIN
				-- ї©±в±оБц їАёй ±жµе°Ў Б¤»уАыАё·О БёАзЗПґВ °НАМ№З·О Б¤єёё¦ ѕтґВґЩ.
				SELECT @iGuildMemberCount = COUNT(*) FROM GuildMember WHERE G_Name = @szRegGuild
				SELECT @iGuildMasterLevel = cLevel FROM Character WHERE Name = @szGuildMaster

				INSERT INTO #T_REG_GUILDLIST VALUES (@szRegGuild, @iRegMarks, @iGuildMemberCount, @iGuildMasterLevel, @iSeqNum)
			END
		END
		
		FETCH FROM T_CURSOR INTO @iMapSvrGroup, @szRegGuild, @iRegMarks, @iIsGiveUp, @iSeqNum
	END
	
	CLOSE T_CURSOR
	
	DEALLOCATE T_CURSOR

	SELECT * FROM #T_REG_GUILDLIST

	DROP TABLE #T_REG_GUILDLIST

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCastleMoneySts]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ іЇВҐАЗ јјАІ Ел°иБ¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2005.02.22
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCastleMoneySts]
	@iMapSvrGroup		SMALLINT,		-- ёКј­№ц ±Ч·м
	@iTaxDate		DATETIME
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON
	
	DECLARE	@iTaxInc		MONEY
	DECLARE	@iTaxDec		MONEY
	
	DECLARE	@iYEAR		INT
	DECLARE	@iMONTH		INT
	DECLARE	@iDAY			INT
	
	DECLARE	@dtLogDateStart	DATETIME
	DECLARE	@dtLogDateEnd	DATETIME
	
	SELECT	@iYEAR		= DATEPART(YY, @iTaxDate)
	SELECT	@iMONTH		= DATEPART(MM, @iTaxDate)
	SELECT	@iDAY			= DATEPART(DD, @iTaxDate)
	
	SET		@dtLogDateStart	= CAST(@iYEAR AS VARCHAR(4)) + '-' + CAST(@iMONTH AS VARCHAR(2))  + '-' + CAST(@iDAY AS VARCHAR(4)) + ' 00:00:00'
	SET		@dtLogDateEnd	= CAST(@iYEAR AS VARCHAR(4)) + '-' + CAST(@iMONTH AS VARCHAR(2))  + '-' + CAST(@iDAY AS VARCHAR(4)) + ' 23:59:59'
	
	SELECT @iTaxInc = SUM(MONEY_CHANGE) FROM MuCastle_MONEY_STATISTICS  WITH (READUNCOMMITTED) 
	WHERE MAP_SVR_GROUP = 0 and LOG_DATE BETWEEN @dtLogDateStart AND @dtLogDateEnd and MONEY_CHANGE >= 0
	
	SELECT @iTaxDec = SUM(MONEY_CHANGE) FROM MuCastle_MONEY_STATISTICS  WITH (READUNCOMMITTED) 
	WHERE MAP_SVR_GROUP = 0 and LOG_DATE BETWEEN @dtLogDateStart AND @dtLogDateEnd and MONEY_CHANGE < 0
	
	IF @iTaxInc IS NULL
		SET @iTaxInc = 0
	IF @iTaxDec IS NULL
		SET @iTaxDec = 0

	SELECT @dtLogDateStart As TaxDate, @iTaxInc As TaxInc, @iTaxDec As TaxDec

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCastleMoneyStsRange]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO




--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ іЇВҐАЗ јјАІ Ел°иБ¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2005.02.22
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCastleMoneyStsRange]
	@iMapSvrGroup		SMALLINT,		-- ёКј­№ц ±Ч·м
	@iTaxDateStart		DATETIME,
	@iTaxDateEnd		DATETIME
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	CREATE TABLE #T_REG_TAXSTT  (
		[TaxDate] [datetime] NOT NULL ,
		[TaxInc] [money] NOT NULL ,
		[TaxDec] [money] NOT NULL 
	) ON [PRIMARY]
	
	IF (@iTaxDateStart <= @iTaxDateEnd)
	BEGIN
		DECLARE	@iTaxDate		DATETIME
		SET		@iTaxDate		= @iTaxDateStart

		WHILE(@iTaxDate <= @iTaxDateEnd)
		BEGIN
			DECLARE	@dtLogDateStart	DATETIME
			DECLARE	@dtLogDateEnd	DATETIME

			DECLARE	@iTaxInc		MONEY
			DECLARE	@iTaxDec		MONEY
		
			DECLARE	@iYEAR		INT
			DECLARE	@iMONTH		INT
			DECLARE	@iDAY			INT
		
			SELECT	@iYEAR		= DATEPART(YY, @iTaxDate)
			SELECT	@iMONTH		= DATEPART(MM, @iTaxDate)
			SELECT	@iDAY			= DATEPART(DD, @iTaxDate)
			
			SET		@dtLogDateStart	= CAST(@iYEAR AS VARCHAR(4)) + '-' + CAST(@iMONTH AS VARCHAR(2))  + '-' + CAST(@iDAY AS VARCHAR(4)) + ' 00:00:00'
			SET		@dtLogDateEnd	= CAST(@iYEAR AS VARCHAR(4)) + '-' + CAST(@iMONTH AS VARCHAR(2))  + '-' + CAST(@iDAY AS VARCHAR(4)) + ' 23:59:59'
					
			SELECT @iTaxInc = SUM(MONEY_CHANGE) FROM MuCastle_MONEY_STATISTICS  WITH (READUNCOMMITTED) 
			WHERE MAP_SVR_GROUP = 0 and LOG_DATE BETWEEN @dtLogDateStart AND @dtLogDateEnd and MONEY_CHANGE >= 0
			
			SELECT @iTaxDec = SUM(MONEY_CHANGE) FROM MuCastle_MONEY_STATISTICS  WITH (READUNCOMMITTED) 
			WHERE MAP_SVR_GROUP = 0 and LOG_DATE BETWEEN @dtLogDateStart AND @dtLogDateEnd and MONEY_CHANGE < 0

			IF @iTaxInc IS NULL
				SET @iTaxInc = 0
			IF @iTaxDec IS NULL
				SET @iTaxDec = 0
						
			INSERT INTO #T_REG_TAXSTT VALUES (@dtLogDateStart, @iTaxInc, @iTaxDec)

			SET @iTaxDate				= DATEADD(DD, 1, @iTaxDate)
		END
	END
	
	SELECT * FROM #T_REG_TAXSTT

	DROP TABLE #T_REG_TAXSTT

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCastleNpcInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ёрµз јцјєГш NPC Б¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCastleNpcInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	SELECT * FROM MuCastle_NPC WITH (READUNCOMMITTED)
	WHERE MAP_SVR_GROUP = @iMapSvrGroup

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCastleTaxInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јјАІБ¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCastleTaxInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	SELECT MONEY, TAX_RATE_CHAOS, TAX_RATE_STORE, TAX_HUNT_ZONE FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
	WHERE MAP_SVR_GROUP = @iMapSvrGroup

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCastleTotalInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ АьГјБ¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCastleTotalInfo]
	@iMapSvrGroup		SMALLINT,			-- ёКј­№ц ±Ч·м
	@iCastleEventCycle	INT				-- °шјєАь БЦ±в
As
Begin
	DECLARE	@iCastleSiegeTerm			INT
	SET		@iCastleSiegeTerm			= @iCastleEventCycle		-- °шјєАь БЦ±в (Жт±Х 14АП)
	DECLARE	@iFirstCreate				INT
	SET		@iFirstCreate				= 0
	
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF NOT EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		DECLARE	@dtStartDate			datetime
		DECLARE	@dtEndDate			datetime
		DECLARE	@dtStartDateString		varchar(32)
		DECLARE	@dtEndDateString		varchar(32)

		SET		@dtStartDate			= GetDate()
		SET		@dtEndDate			= DATEADD(dd, @iCastleSiegeTerm, GetDate())
		SET		@dtStartDateString		= CAST(DATEPART(YY, @dtStartDate) AS char(4)) + '-' + CAST(DATEPART(MM, @dtStartDate) AS char(2)) + '-' + CAST(DATEPART(DD, @dtStartDate) AS char(2)) + ' 00:00:00'
		SET		@dtEndDateString		= CAST(DATEPART(YY, @dtEndDate) AS char(4)) + '-' + CAST(DATEPART(MM, @dtEndDate) AS char(2)) + '-' + CAST(DATEPART(DD, @dtEndDate) AS char(2)) + ' 00:00:00'


		INSERT INTO MuCastle_DATA  VALUES (
			@iMapSvrGroup,			-- MAP_SVR_GROUP
			@dtStartDateString,			-- SIEGE_START_DATE
			@dtEndDateString,			-- SIEGE_END_DATE
			0,					-- SIEGE_GUILDLIST_SETTED
			0,					-- SIEGE_ENDED
			0,					-- CASTLE_OCCUPY
			'',					-- OWNER_GUILD
			0,					-- MONEY
			0,					-- TAX_RATE_CHAOS
			0,					-- TAX_RATE_STORE
			0					-- TAX_HUNT_ZONE
		)

		SET @iFirstCreate				= 1
	END

	SELECT	 MAP_SVR_GROUP, 
			DATEPART(YY,SIEGE_START_DATE)	As SYEAR, 
			DATEPART(MM,SIEGE_START_DATE)	As SMONTH, 
			DATEPART(DD,SIEGE_START_DATE)	As SDAY, 
			DATEPART(YY,SIEGE_END_DATE)	As EYEAR, 
			DATEPART(MM,SIEGE_END_DATE)	As EMONTH, 
			DATEPART(DD,SIEGE_END_DATE)	As EDAY, 
			SIEGE_GUILDLIST_SETTED, 
			SIEGE_ENDED, 
			CASTLE_OCCUPY, 
			OWNER_GUILD, 
			MONEY, 
			TAX_RATE_CHAOS,
			TAX_RATE_STORE,
			TAX_HUNT_ZONE,
			@iFirstCreate As FIRST_CREATE
	FROM MuCastle_DATA  WITH (READUNCOMMITTED)
	WHERE MAP_SVR_GROUP = @iMapSvrGroup

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetCsGuildUnionInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ ±жµеАЗ ї¬ЗХ±жµеАЗ ё®ЅєЖ®ё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.03
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetCsGuildUnionInfo]
	@szGuildName		VARCHAR(8)	-- ї¬ЗХБ¤єёё¦ ѕЛ°нАЪ ЗПґВ ±жµеён
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	DECLARE	@iG_Union	INT
	SET		@iG_Union	= -1	-- БёАзЗТ јц ѕшґВ ї¬ЗХ°ЄАё·О ±вє»јјЖГ
	
	IF EXISTS ( SELECT G_Name FROM Guild  WITH (READUNCOMMITTED) 
				WHERE G_Name = @szGuildName)
	BEGIN				-- ЗШґз ±жµеАЗ Б¤єё°Ў БёАзЗСґЩ.
		SELECT @iG_Union = G_Union
		FROM Guild WITH (READUNCOMMITTED) 
		WHERE G_Name = @szGuildName
	END

	-- ЖЇБ¤ ±жµеАЗ ї¬ЗХ±жµе АМё§µйА» ѕтѕоїВґЩ.
	IF (@iG_Union = 0)
	BEGIN
		SELECT @szGuildName As GUILD_NAME
	END
	ELSE
	BEGIN
		-- ї¬ЗХАМ БёАзЗП№З·О ї¬ЗХАЗ ±жµеАМё§А» ѕтѕоїВґЩ.
		SELECT G_Name As GUILD_NAME
		FROM Guild WITH (READUNCOMMITTED) 
		WHERE G_Union = @iG_Union
	END
	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetGuildMarkRegInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ °шјєГш ±жµеАЗ ё¶Е©µо·П Б¤єёё¦ ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetGuildMarkRegInfo]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8)	-- °шјєµо·ПА» ЗС ±жµеАМё§
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	SELECT * FROM MuCastle_REG_SIEGE WITH (READUNCOMMITTED)
	WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName
	ORDER BY SEQ_NUM ASC

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetOwnerGuildMaster]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш ±жµеАЗ ±жµеё¶ЅєЕН АМё§А» ѕтѕоїВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.22
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetOwnerGuildMaster]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON	

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN				-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗСґЩ.
		DECLARE	@iIsCastleOccupied	TINYINT
		DECLARE	@szGuildName		VARCHAR(8)

		SELECT @iIsCastleOccupied = CASTLE_OCCUPY, @szGuildName = OWNER_GUILD FROM MuCastle_DATA WHERE MAP_SVR_GROUP = @iMapSvrGroup

		IF (@iIsCastleOccupied = 1)
		BEGIN
			IF (@szGuildName <> '')			
			BEGIN		-- АМБ¦ ±жµе°Ў ЅЗАз·О БёАзЗПґВБц И®АОЗСґЩ.
				IF EXISTS ( SELECT G_Master FROM Guild  WITH (READUNCOMMITTED)
							WHERE G_Name = @szGuildName)
				BEGIN
					SELECT 1 As QueryResult, @szGuildName As OwnerGuild, G_Master As OwnerGuildMaster FROM Guild  WITH (READUNCOMMITTED) WHERE G_Name = @szGuildName
				END
				ELSE
				BEGIN	-- БёАзЗШѕЯ ЗТ ±жµе°Ў БёАзЗПБц ѕКґВґЩ. (±жµе°Ў »иБ¦µЗґВ µо)
					SELECT 4 As QueryResult, '' As OwnerGuild, '' As OwnerGuildMaster
				END
			END
			ELSE
			BEGIN		-- јєµҐАМЕН°Ў АЯёшµЗѕъґЩ.
				SELECT 3 As QueryResult, '' As OwnerGuild, '' As OwnerGuildMaster
			END
		END
		ELSE
		BEGIN			-- јєАЗ БЦАОАМ ѕшґЩ.
			SELECT 2 As QueryResult, '' As OwnerGuild, '' As OwnerGuildMaster
		END
	END
	ELSE
	BEGIN				-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКґВґЩ.
		SELECT 0 As QueryResult, '' As OwnerGuild, '' As OwnerGuildMaster
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_GetSiegeGuildInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ГЦБѕ °шјє°ь·Г ±жµеµйАЗ Б¤єёё¦ °ЎБ®їВґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.04
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_GetSiegeGuildInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	SELECT * 
	FROM MuCastle_SIEGE_GUILDLIST  WITH (READUNCOMMITTED) 
	WHERE MAP_SVR_GROUP = @iMapSvrGroup

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyCastleOwnerInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јТАЇБЦ Б¤єёё¦ єЇ°жЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyCastleOwnerInfo]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iCastleOccupied	INT,		-- јєАЗ »зїлАЪ јТАЇї©єО (0:NPC јТАЇ)
	@szOwnGuildName	VARCHAR(8)	-- јєА» јТАЇЗС ±жµеАЗ АМё§
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		UPDATE MuCastle_DATA 
		SET CASTLE_OCCUPY = @iCastleOccupied, OWNER_GUILD = @szOwnGuildName
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyCastleSchedule]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјєЅєДЙБм (ЅГАЫіЇВҐ, Бѕ·біЇВҐ) А» єЇ°жЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyCastleSchedule]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@dtStartDate		DATETIME,	-- ЅГАЫіЇВҐ
	@dtEndDate		DATETIME	-- Бѕ·біЇВҐ
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		UPDATE MuCastle_DATA 
		SET SIEGE_START_DATE = @dtStartDate, SIEGE_END_DATE = @dtEndDate
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyGuildGiveUp]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ ±жµеАЗ °шјєµо·ПАЗ Жч±в »уЕВё¦ єЇ°жЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyGuildGiveUp]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8),	-- °шјєµо·ПА» ЗС ±жµеАМё§
	@iIsGiveUp		INT		-- °шјє Жч±в ї©єО
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName)
	BEGIN
		DECLARE	@iMarkCount	INT
		SELECT @iMarkCount = REG_MARKS FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName

		UPDATE MuCastle_REG_SIEGE 
		SET IS_GIVEUP = @iIsGiveUp, REG_MARKS = 0
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName

		SELECT 1 As QueryResult, @iMarkCount As DEL_MARKS	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 2 As QueryResult, 0 As DEL_MARKS			-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyGuildMarkRegCount]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ ±жµеАЗ ±жµеё¶Е© µо·П°іјцё¦ Б¶Б¤ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyGuildMarkRegCount]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8),	-- °шјєµо·ПА» ЗС ±жµеАМё§
	@iMarkCount		INT		-- ±жµеё¶Е© °іјц
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName)
	BEGIN
		UPDATE MuCastle_REG_SIEGE 
		SET REG_MARKS = @iMarkCount
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	--ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyGuildMarkReset]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ЖЇБ¤ ±жµеАЗ ±жµеё¶Е© µо·П°іјцё¦ ГК±вИ­ ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyGuildMarkReset]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8)	-- °шјєµо·ПА» ЗС ±жµеАМё§
As
Begin
	BEGIN TRANSACTION

	DECLARE		@iMarkCount	INT	-- ±жµеё¶Е© °іјц
	DECLARE		@bIsGiveUp	INT	-- °шјє Жч±вї©єО

	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName)
	BEGIN
		SELECT @iMarkCount = REG_MARKS, @bIsGiveUp = IS_GIVEUP
		FROM MuCastle_REG_SIEGE
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName

		IF (@iMarkCount > 0)
		BEGIN
			IF (@bIsGiveUp = 0)
			BEGIN
				UPDATE MuCastle_REG_SIEGE 
				SET REG_MARKS = 0
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName
		
				SELECT 1 As QueryResult, @iMarkCount As DEL_MARKS	--ё¶Е©°іјцё¦ ГК±вИ­ ЗП°н µо·ПµИ ё¶Е©ё¦ ГК±вИ­ ЗСґЩ.
			END
			ELSE
			BEGIN
				SELECT 2 As QueryResult, 0 As DEL_MARKS			--АМ№М ё¶Е©°Ў БцєТµЗѕъґЩ. (Жч±в -> БцєТ µК)
			END
		END
		ELSE
		BEGIN
			SELECT 1 As QueryResult, 0 As DEL_MARKS				--ё¶Е©°Ў ѕшґВ°НАП »УАМ№З·О ЗцАз »уЕВё¦ јє°шАё·О АОЅД
		END
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult, 0 As DEL_MARKS					--ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyMoney]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ µ· ѕЧјцё¦ јцБ¤ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyMoney]
	@iMapSvrGroup		SMALLINT,			-- ёКј­№ц ±Ч·м
	@iMoneyChange	MONEY				-- »уґлјцДЎАО єЇИ­µЙ µ·
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		DECLARE	@iTotMoney	MONEY
		SELECT @iTotMoney = MONEY FROM MuCastle_DATA
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		IF (@iTotMoney + @iMoneyChange < 0)
		BEGIN
			SELECT 2 As QueryResult, MONEY	--  ЗцАз АЦґВ µ·АЗ ѕЧјц°Ў іК№« АЫАё№З·О єЇ°жЗПБц ѕКґВґЩ.
			FROM MuCastle_DATA
			WHERE MAP_SVR_GROUP = @iMapSvrGroup		
		END
		ELSE
		BEGIN
			UPDATE MuCastle_DATA 
			SET MONEY = @iTotMoney + @iMoneyChange
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
	
			SELECT 1 As QueryResult, MONEY	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПёз µ·°и»к ЅГ їЎ·Ї°Ў ѕшАё№З·О ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
			FROM MuCastle_DATA
			WHERE MAP_SVR_GROUP = @iMapSvrGroup		
		END

		-- ёёѕа јєАЗ АЪ±ЭЕл°иё¦ і»Бц ѕКґВґЩёй АМ єОєРАє БЦј® Гіё®
		INSERT MuCastle_MONEY_STATISTICS VALUES (@iMapSvrGroup, GetDate(), @iMoneyChange)
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult, 0 As MONEY		-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifySiegeEnd]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјє Бѕ·б ї©єОё¦ єЇ°жЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifySiegeEnd]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iSiegeEnded		INT		-- °шјє Бѕ·б ї©єО
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		UPDATE MuCastle_DATA 
		SET SIEGE_ENDED = @iSiegeEnded
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ModifyTaxRate]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јјАІА» Б¶Б¤ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ModifyTaxRate]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iTaxKind		INT,		-- јјАІАЗ Бѕ·щ (1:Д«їАЅєБ¶ЗХ / 2:»уБЎ)
	@iTaxRate		INT		-- єЇ°жµЙ јјАІ
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF (@iTaxKind = 1)			-- Д«їАЅє Б¶ЗХ јјАІБ¶Б¤
	BEGIN
		IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
					WHERE MAP_SVR_GROUP = @iMapSvrGroup)
		BEGIN
			UPDATE MuCastle_DATA 
			SET TAX_RATE_CHAOS = @iTaxRate
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
	
			SELECT @iTaxKind As TaxKind, 1 As QueryResult, TAX_RATE_CHAOS As TaxRate	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
			FROM MuCastle_DATA
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
		END
		ELSE
		BEGIN
			SELECT @iTaxKind As TaxKind, 0 As QueryResult, 0 As TaxRate				-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
		END
	END
	ELSE IF (@iTaxKind = 2)			-- »уБЎ јјАІ Б¶Б¤
	BEGIN
		IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
					WHERE MAP_SVR_GROUP = @iMapSvrGroup)
		BEGIN
			UPDATE MuCastle_DATA 
			SET TAX_RATE_STORE = @iTaxRate
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
	
			SELECT @iTaxKind As TaxKind, 1 As QueryResult, TAX_RATE_STORE As TaxRate	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
			FROM MuCastle_DATA
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
		END
		ELSE
		BEGIN
			SELECT @iTaxKind As TaxKind, 0 As QueryResult, 0 As TaxRate				-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
		END
	END
	ELSE IF (@iTaxKind = 3)			-- »зіЙЕН АФАе·б јјАІ Б¶Б¤
	BEGIN
		IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
					WHERE MAP_SVR_GROUP = @iMapSvrGroup)
		BEGIN
			UPDATE MuCastle_DATA 
			SET TAX_HUNT_ZONE = @iTaxRate
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
	
			SELECT @iTaxKind As TaxKind, 1 As QueryResult, TAX_HUNT_ZONE As TaxRate	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
			FROM MuCastle_DATA
			WHERE MAP_SVR_GROUP = @iMapSvrGroup
		END
		ELSE
		BEGIN
			SELECT @iTaxKind As TaxKind, 0 As QueryResult, 0 As TaxRate				-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
		END
	END
	ELSE
	BEGIN
		SELECT @iTaxKind As TaxKind, 0 As QueryResult, 0 As TaxRate					-- јјАІБѕ·щ°Ў БёАзЗПБц ѕКґВґЩ.
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqNpcBuy]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш NPCё¦ ±ёАФЗСґЩ. (NPC Б¤єёё¦ »рАФЗСґЩ.)
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqNpcBuy]
	@iMapSvrGroup		SMALLINT,		-- ёКј­№ц ±Ч·м
	@iNpcNumber		INT,			-- NPC№шИЈ
	@iNpcIndex		INT,			-- NPCАОµ¦Ѕє
	@iNpcDfLevel		INT,			-- №жѕо·В ·№є§
	@iNpcRgLevel		INT,			-- »эёнИёє№ ·№є§
	@iNpcMaxHp		INT,			-- ГЦґл HP
	@iNpcHp		INT,			-- HP
	@btNpcX		TINYINT,		-- БВЗҐ - X
	@btNpcY		TINYINT,		-- БВЗҐ - Y
	@btNpcDIR		TINYINT		-- №жЗв
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex)
	BEGIN
		SELECT 4 As QueryResult		-- ЗШґз NPC°Ў АМ№М БёАзЗП°н АЦґЩ.
	END
	ELSE
	BEGIN
		INSERT INTO MuCastle_NPC VALUES (
			@iMapSvrGroup	,		-- MAP_SVR_GROUP
			@iNpcNumber,			-- NPC_NUMBER
			@iNpcIndex,			-- NPC_INDEX
			@iNpcDfLevel,			-- NPC_DF_LEVEL
			@iNpcRgLevel,			-- NPC_RG_LEVEL
			@iNpcMaxHp,			-- NPC_MAXHP
			@iNpcHp,			-- NPC_HP
			@btNpcX,			-- NPC_X
			@btNpcY,			-- NPC_Y
			@btNpcDIR,			-- NPC_DIR
			GetDate()			-- NPC_CREATEDATE
		)
		
		SELECT 1 As QueryResult		-- NPCГЯ°Ў јє°ш
	END



	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqNpcRemove]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш NPCё¦ Б¦°ЕЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.26
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqNpcRemove]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iNpcNumber		INT,		-- NPC№шИЈ
	@iNpcIndex		INT		-- NPCАОµ¦Ѕє
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex)
	BEGIN
		DELETE MuCastle_NPC
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex

		SELECT 1 As QueryResult	-- NPC°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 2 As QueryResult	-- NPC°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqNpcRepair]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш NPCё¦ јцё®ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqNpcRepair]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iNpcNumber		INT,		-- NPC№шИЈ
	@iNpcIndex		INT		-- NPCАОµ¦Ѕє
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex)
	BEGIN
		UPDATE MuCastle_NPC 
		SET  NPC_HP = NPC_MAXHP
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex

		SELECT 1 As QueryResult, NPC_HP, NPC_MAXHP		-- NPC°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
		FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex
	END
	ELSE
	BEGIN
		SELECT 2 As QueryResult, 0 As NPC_HP, 0 As NPC_MAXHP	-- NPC°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End




GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqNpcUpdate]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш NPC Б¤єёё¦ °»ЅЕЗСґЩ -> NPC Б¤єё°Ў ѕшґЩёй ГЯ°ЎЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.15
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqNpcUpdate]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@iNpcNumber		INT,		-- NPC№шИЈ
	@iNpcIndex		INT,		-- NPCАОµ¦Ѕє
	@iNpcDfLevel		INT,		-- №жѕо·В ·№є§
	@iNpcRgLevel		INT,		-- »эёнИёє№ ·№є§
	@iNpcMaxHp		INT,		-- ГЦґл HP
	@iNpcHp		INT,		-- HP
	@btNpcX		TINYINT,	-- БВЗҐ - X
	@btNpcY		TINYINT,	-- БВЗҐ - Y
	@btNpcDIR		TINYINT	-- №жЗв
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex)
	BEGIN
		-- ЗШґз NPC°Ў БёАзЗСґЩ -> Б¤єё °»ЅЕ
		
		UPDATE MuCastle_NPC
		SET	NPC_NUMBER		= @iNpcNumber, 
			NPC_INDEX		= @iNpcIndex, 
			NPC_DF_LEVEL	= @iNpcDfLevel, 
			NPC_RG_LEVEL	= @iNpcRgLevel, 
			NPC_MAXHP		= @iNpcMaxHp, 
			NPC_HP		= @iNpcHp,
			NPC_X			= @btNpcX,
			NPC_Y			= @btNpcY, 
			NPC_DIR		= @btNpcDIR
		WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex
	END
	ELSE
	BEGIN
		-- ЗШґз NPC°Ў БёАзЗПБц ѕКґВґЩ -> Б¤єё ГЯ°Ў

		INSERT INTO MuCastle_NPC VALUES (
			@iMapSvrGroup	,	-- MAP_SVR_GROUP
			@iNpcNumber,		-- NPC_NUMBER
			@iNpcIndex,		-- NPC_INDEX
			@iNpcDfLevel,		-- NPC_DF_LEVEL
			@iNpcRgLevel,		-- NPC_RG_LEVEL
			@iNpcMaxHp,		-- NPC_MAXHP
			@iNpcHp,		-- NPC_HP
			@btNpcX,		-- NPC_X
			@btNpcY,		-- NPC_Y
			@btNpcDIR,		-- NPC_DIR
			GetDate()		-- NPC_CREATEDATE
		)
	END



	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqNpcUpgrade]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ јцјєГш NPCё¦ ѕч±Ч·№АМµе ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqNpcUpgrade]
	@iMapSvrGroup		SMALLINT,		-- ёКј­№ц ±Ч·м
	@iNpcNumber		INT,			-- NPC№шИЈ
	@iNpcIndex		INT,			-- NPCАОµ¦Ѕє
	@iNpcUpType		INT,			-- NPCѕч±Ч·№АМµе ЕёАФ (1:№жѕо·В/2:Иёє№·В/3:ГЦґлHP)
	@iNpcUpValue		INT			-- NPCѕч±Ч·№АМµе јцДЎ
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_NPC  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex)
	BEGIN
		IF (@iNpcUpType = 1)			-- №жѕо·В
		BEGIN
			UPDATE MuCastle_NPC 
			SET NPC_DF_LEVEL = @iNpcUpValue
			WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex
	
			SELECT 1 As QueryResult	-- NPC°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
		END
		ELSE IF (@iNpcUpType = 2) 		-- Иёє№·В
		BEGIN
			UPDATE MuCastle_NPC 
			SET NPC_RG_LEVEL = @iNpcUpValue
			WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex
	
			SELECT 1 As QueryResult	-- NPC°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
		END
		ELSE IF (@iNpcUpType = 3) 		-- ГЦґлHP
		BEGIN
			UPDATE MuCastle_NPC 
			SET NPC_MAXHP = @iNpcUpValue, NPC_HP = @iNpcUpValue
			WHERE MAP_SVR_GROUP = @iMapSvrGroup and NPC_NUMBER = @iNpcNumber and NPC_INDEX = @iNpcIndex
	
			SELECT 1 As QueryResult	-- NPC°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
		END
		ELSE
		BEGIN
			SELECT 2 As QueryResult	-- ѕч±Ч·№АМµе ЕёАФАМ АЯёшµЗѕъґЩ.
		END
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult		-- NPC°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqRegAttackGuild]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјєЅЕГ» ±жµеАЗ °шјєАь µо·ПА» ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.22
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqRegAttackGuild]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8)	-- °шјєµо·ПА» ЗС ±жµеАМё§
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	DECLARE	@iMaxRegGuildCount	INT					-- °шјєїЎ ГЦґл µо·ПЗТ јц АЦґВ ±жµе јц
	DECLARE	@iCurRegGuildCount	INT					-- ЗцАз °шјєїЎ µо·ПЗС ±жµе јц
	SET 		@iMaxRegGuildCount	= 100

	SELECT @iCurRegGuildCount = COUNT(*) FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED)  WHERE MAP_SVR_GROUP = @iMapSvrGroup
	IF (@iCurRegGuildCount >= @iMaxRegGuildCount)
	BEGIN
			SELECT 6 As QueryResult					-- µо·ПЗС ±жµе°Ў АМ№М N°іё¦ іСѕъАЅ
	END
	ELSE
	BEGIN
		IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
					WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName)
		BEGIN
			SELECT 2 As QueryResult					-- АМ№М µо·ПµЗѕо АЦАЅ
		END
		ELSE
		BEGIN
			DECLARE @szOwnGuildName		VARCHAR(8)
			SELECT @szOwnGuildName = OWNER_GUILD FROM MuCastle_DATA WHERE MAP_SVR_GROUP = @iMapSvrGroup
	
			-- И¤ЅГ јцјєГш ±жµе°Ў ѕЖґСБц БЎ°Л
			IF (@szOwnGuildName = @szGuildName)
			BEGIN
				SELECT 3 As QueryResult				-- јцјєГш ±жµеАУ
			END
			ELSE
			BEGIN
				IF NOT EXISTS ( SELECT G_Name FROM Guild  WITH (READUNCOMMITTED) WHERE G_Name = @szGuildName)
				BEGIN
					SELECT 4 As QueryResult			-- ±жµеБ¤єё°Ў БёАзЗПБц ѕКАЅ
				END
				ELSE
				BEGIN
					DECLARE @szGuildMaster			VARCHAR(10)
					DECLARE @iGuildMasterLevel			INT
					DECLARE @iGuildMemberCount			INT
					
					SELECT @szGuildMaster = G_Master FROM Guild WHERE G_Name = @szGuildName
					SELECT @iGuildMasterLevel = cLevel FROM Character WHERE Name = @szGuildMaster
					SELECT @iGuildMemberCount = COUNT(*) FROM GuildMember WHERE G_Name = @szGuildName
					
					IF (@iGuildMasterLevel < 200)
					BEGIN
						SELECT 5 As QueryResult		-- ±жµеё¶ЅєЕНАЗ ·№є§АМ і·АЅ
					END
					ELSE
					BEGIN
						IF (@iGuildMemberCount < 20)
						BEGIN
							SELECT 8 As QueryResult	-- ±жµеїш јц°Ў єОБ·
						END
						ELSE
						BEGIN
							DECLARE @iMAX_SEQNUM	INT	-- ЗцАз ГЦґл µо·П№шИЈ
							DECLARE @iNXT_SEQNUM	INT	-- ґЩАЅ µо·П№шИЈ
							SELECT @iMAX_SEQNUM = MAX(SEQ_NUM) FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED)  WHERE MAP_SVR_GROUP = @iMapSvrGroup
							
							IF (@iMAX_SEQNUM IS NULL)
								SET @iNXT_SEQNUM	= 1
							ELSE
								SET @iNXT_SEQNUM	= @iMAX_SEQNUM + 1

							INSERT INTO MuCastle_REG_SIEGE 
							VALUES (@iMapSvrGroup, @szGuildName, 0, 0, @iNXT_SEQNUM)
					
							SELECT 1 As QueryResult	-- µо·П јє°ш
						END
					END
				END
			END
		END
	END

	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ReqRegGuildMark]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјєЅЕГ» ±жµеАЗ ё¶Е©ё¦ µо·ПЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ReqRegGuildMark]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8)	-- °шјєµо·ПА» ЗС ±жµеАМё§
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName)
	BEGIN
		DECLARE	@bIS_GIVEUP	INT
		SELECT @bIS_GIVEUP = IS_GIVEUP FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName

		-- °шјєЖч±в ±жµеґВ µо·ПАМ µЗёй ѕКµИґЩ.
		IF (@bIS_GIVEUP = 0)
		BEGIN
			UPDATE MuCastle_REG_SIEGE 
			SET REG_MARKS = REG_MARKS + 1
			WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName
	
			SELECT 1 As QueryResult, REG_MARKS
			FROM MuCastle_REG_SIEGE  WITH (READUNCOMMITTED)
			WHERE MAP_SVR_GROUP = @iMapSvrGroup and REG_SIEGE_GUILD = @szGuildName
		END
		ELSE
		BEGIN
			SELECT 0 As QueryResult, 0 As REG_MARKS
		END
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult, 0 As REG_MARKS
	END


	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ResetCastleSiege]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјєБЦ±вё¦ ГК±вИ­ЗСґЩ (»х·О ЅГАЫЗТ БШєсё¦ ЗФ)
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ResetCastleSiege]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		-- 1 . °шјє ЅГ°ЈА» єЇ°жЗП°н ЗКїдЗС Б¤єёµйА» ГК±вИ­ ЗСґЩ.
		UPDATE MuCastle_DATA 
		SET 	SIEGE_GUILDLIST_SETTED = 0,
			SIEGE_ENDED = 0
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		-- 2 . °шјє µо·П±жµе Б¤єёё¦ ГК±вИ­ ЗСґЩ.
		-- ЅГАЫЗПАЪ ё¶АЪ ГК±вИ­ ЅГЕі ¶§ ИЈГвАМ µЗ№З·О АМ ±ё№®Ає єьБ®ѕЯ ЗСґЩ -> °шјєБЦ±в Бѕ·б ¶§ µы·О Бцїт
--		DELETE MuCastle_REG_SIEGE
--		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		--DBCC CHECKIDENT ('MuCastle_REG_SIEGE', RESEED, 0)

		-- 3 . °шјє°ь·Г ±жµеё®ЅєЖ® Б¤єёё¦ ГК±вИ­ ЗСґЩ.
		DELETE MuCastle_SIEGE_GUILDLIST
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ResetCastleTaxInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ µ·°ъ јјАІА» ГК±вИ­ ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.21
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ResetCastleTaxInfo]
	@iMapSvrGroup		SMALLINT			-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		UPDATE MuCastle_DATA 
		SET MONEY = 0, TAX_RATE_CHAOS = 0, TAX_RATE_STORE = 0, TAX_HUNT_ZONE = 0
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult			-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult			-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ResetRegSiegeInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјє Вьї©ЅЕГ» ±жµе ё®ЅєЖ®ё¦ ГК±вИ­ ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ResetRegSiegeInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		-- °шјє°ь·Г ±жµеё®ЅєЖ® Б¤єёё¦ ГК±вИ­ ЗСґЩ.
		DELETE MuCastle_REG_SIEGE
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_ResetSiegeGuildInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO







--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ °шјє Вьї©±жµе ё®ЅєЖ®ё¦ ГК±вИ­ ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.11.09
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_ResetSiegeGuildInfo]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN
		-- °шјє°ь·Г ±жµеё®ЅєЖ® Б¤єёё¦ ГК±вИ­ ЗСґЩ.
		DELETE MuCastle_SIEGE_GUILDLIST
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_SetSiegeGuildInfo]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ГЦБѕ °шјє°ь·Г ±жµеµйАЗ Б¤єёё¦ јјЖГЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.04
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_SetSiegeGuildInfo]
	@iMapSvrGroup		SMALLINT,	-- ёКј­№ц ±Ч·м
	@szGuildName		VARCHAR(8),	-- °шјєµо·ПА» ЗС ±жµеАМё§
	@iGuildID		INT,		-- °ш/јцАЗ ѕоґА ЖнАОБц ?
	@iGuildInvolved		INT		-- °шјєїЎ БчБў °іАФµИ ґз»зАЪАОБц ?
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	INSERT INTO MuCastle_SIEGE_GUILDLIST
	VALUES (@iMapSvrGroup, @szGuildName, @iGuildID, @iGuildInvolved)
	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_CS_SetSiegeGuildOK]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO






--//************************************************************************
--// << №В °шјєАь - АъАе ЗБ·ОЅГАъ >>
--// 
--// і»їл	: ЖЇБ¤ јє (ёКј­№ц±є) АЗ ГЦБѕ °шјє°ь·Г ±жµеµйАЗ Б¤єёё¦ јє°шАыАё·О јјЖГЗЯґЩґВ ГјЕ©ё¦ ЗСґЩ.
--// єОј­	: °ФАУ°і№Я 1ЖА 
--// ёёµзіЇ	: 2004.12.04
--// ёёµйАМ	: ИІБШАП
--// 
--//************************************************************************

CREATE PROCEDURE	[dbo].[WZ_CS_SetSiegeGuildOK]
	@iMapSvrGroup		SMALLINT	-- ёКј­№ц ±Ч·м
As
Begin
	BEGIN TRANSACTION
	
	SET NOCOUNT ON

	IF EXISTS ( SELECT MAP_SVR_GROUP FROM MuCastle_DATA  WITH (READUNCOMMITTED) 
				WHERE MAP_SVR_GROUP = @iMapSvrGroup)
	BEGIN					-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗСґЩ.
		UPDATE MuCastle_DATA
		SET SIEGE_GUILDLIST_SETTED = 1
		WHERE MAP_SVR_GROUP = @iMapSvrGroup

		SELECT 1 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПї© ѕчµҐАМЖ®їЎ јє°шЗЯґЩ.
	END
	ELSE
	BEGIN
		SELECT 0 As QueryResult	-- ёКј­№ц±єАЗ Б¤єё°Ў БёАзЗПБц ѕКАё№З·О ѕчµҐАМЖ®їЎ ЅЗЖРЗЯґЩ.
	END

	
	IF(@@Error <> 0 )
		ROLLBACK TRANSACTION
	ELSE	
		COMMIT TRANSACTION

	SET NOCOUNT OFF	
End



GO
/****** Object:  StoredProcedure [dbo].[WZ_DelMail]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[WZ_DelMail]
	@Name varchar(10),  @MemoIndex int
as 

BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int

	SET	XACT_ABORT ON
	Set	nocount on 	

	SET @ErrorCode = 0

	-- Name GUID°Ў БёАзЗПґВ°Ў?
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@Name)
	  BEGIN
		SET @ErrorCode = 2
		GOTO EndProc

	  END
	else
	  BEGIN
		select @UserGuid = GUID FROM T_FriendMain where Name=@Name
	
		IF ( @@Error  <> 0   )
		begin
			SET @ErrorCode = 3
			GOTO EndProc
		end
	
	  END

	if NOT EXISTS ( select MemoIndex FROM T_FriendMail  where MemoIndex=@MemoIndex AND GUID=@UserGuid)
	  BEGIN
		SET @ErrorCode = 4
		GOTO EndProc
	  END

	BEGIN TRAN

	-- ёЮАПА» »иБ¦ЅГЕІґЩ.
	DELETE FROM T_FriendMail where MemoIndex=@MemoIndex AND GUID=@UserGuid
	IF ( @@Error  <> 0 )
		SET @ErrorCode	= 5
	else 
	  BEGIN
		update T_FriendMain set MemoTotal=MemoTotal-1 where GUID = @UserGuid
		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode	= 6
		end
	  END

	IF ( @ErrorCode  <> 0 )
	  BEGIN
		ROLLBACK TRAN
	  END
	ELSE
	  BEGIN
		COMMIT TRAN
		SET @ErrorCode	= 1
	  END
	
EndProc:

	SET	XACT_ABORT OFF
	Set		nocount off
	select @ErrorCode
END


GO
/****** Object:  StoredProcedure [dbo].[WZ_DISCONNECT_MEMB]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[WZ_DISCONNECT_MEMB]
@memb___id varchar(10)
 AS
Begin	
set nocount on
	Declare  @find_id varchar(10)	
	Declare @ConnectStat tinyint
	Set @ConnectStat = 0		-- БўјУ »уЕВ °Є 1 = БўјУ, 0 = БўјУX
	Set @find_id = 'NOT'
	select @find_id = S.memb___id from MEMB_STAT S INNER JOIN MEMB_INFO I ON S.memb___id = I.memb___id 
	       where I.memb___id = @memb___id

	if( @find_id <> 'NOT' )	-- БўјУ Бѕ·б Гіё®ґВ БўјУ Гіё®ё¦ ЗЯА»¶§ёё АЇИїЗП°Ф ЗСґЩ
	begin		
		update MEMB_STAT set ConnectStat = @ConnectStat, DisConnectTM = getdate()
		 where memb___id = @memb___id
	end
end
GO
/****** Object:  StoredProcedure [dbo].[WZ_FriendAdd]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ДЈ±ё ёс·ПїЎ ГЯ°ЎЗСґЩ.
/* їЎ·Ї : 
 1 : јє°ш
 2 : АМ№М ДЈ±ё ё®ЅєЖ®їЎ µо·ПµЗѕо АЦґЩ
 3 : іЄАЗ  GUID°Ў БёАзЗПБц ѕКґВґЩ.
 4 : іЄАЗ GUIDё¦ ѕтґВµҐ ЅЗЖРЗЯґЩ.
 5 : ДЈ±ёАЗ GUID°Ў БёАзЗПБц ѕКґВґЩ.
 6 : ДЈ±ёАЗ GUIDё¦ ѕтґВµҐ ЅЗЖРЗЯґЩ.
*/
CREATE procedure [dbo].[WZ_FriendAdd]
	@Name varchar(10),  @FriendName varchar(10)

as 

BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int
	DECLARE @FriendGuid  int

	Set		nocount on 	
	SET	XACT_ABORT ON


	SET @ErrorCode = 0

	-- FriendName GUID°Ў БёАзЗПґВ°Ў?
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@FriendName )
	  BEGIN
		SET @ErrorCode = 5
		GOTO EndProc
	  END
 	ELSE
	  BEGIN
		select @FriendGuid = GUID FROM T_FriendMain where Name=@FriendName

		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode = 6
			GOTO EndProc
		end

	end


	-- Name GUID°Ў БёАзЗПґВ°Ў?
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@Name)
	  BEGIN
		SET @ErrorCode = 3
		GOTO EndProc

	  END
	else
	  BEGIN
		select @UserGuid = GUID FROM T_FriendMain where Name=@Name
	
		IF ( @@Error  <> 0   )
		begin
			SET @ErrorCode = 4
			GOTO EndProc
		end
	
	  END

	-- АМ№М ДЈ±ёё®ЅєЖ®їЎ µо·ПµЗѕо АЦґЩёй ГлјТЅГЕІґЩ.
	if EXISTS (SELECT GUID FROM T_FriendList where GUID = @UserGuid AND FriendGuid = @FriendGuid  )
	begin
		SET @ErrorCode = 2
		GOTO EndProc
	end

	/*-- АМ№М ДЈ±ёё®ЅєЖ®їЎ µо·ПµЗѕо АЦґЩёй ГлјТЅГЕІґЩ.
	if EXISTS (SELECT GUID FROM T_FriendList where GUID = @FriendGuid AND FriendGuid = @UserGuid  )
	begin
		SET @ErrorCode = 2
		GOTO EndProc
	end
	*/
	BEGIN TRAN

	-- ДЈ±ёё¦ ГЯ°ЎЅГЕІґЩ.
	INSERT INTO T_FriendList (GUID, FriendGuid, FriendName ) 
		VALUES ( @UserGuid, @FriendGuid, @FriendName)
	
	IF ( @@Error  <> 0 )
		SET @ErrorCode	= @@Error
	else 
	  BEGIN
		DELETE FROM T_WaitFriend where GUID = @UserGuid AND FriendGuid = @FriendGuid
		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode	= @@Error
		end
	  END

	/*-- ґЩёҐ ДЈ±ёїЎ ГЯ°ЎЅГЕІґЩ.
	INSERT INTO T_FriendList (GUID, FriendGuid, FriendName ) 
		VALUES ( @FriendGuid, @UserGuid,  @Name)

	IF ( @@Error  <> 0 )
		SET @ErrorCode	= @@Error
	else 
	  BEGIN
		DELETE FROM T_WaitFriend where GUID = @FriendGuid AND FriendGuid = @UserGuid
		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode	= @@Error
		end
	  END
*/
--EndTranProc:
	IF ( @ErrorCode  <> 0 )
	  BEGIN
		ROLLBACK TRAN
	  END
	ELSE
	  BEGIN
		COMMIT TRAN
		SET @ErrorCode	= 1
	  END
	


EndProc:

	SET	XACT_ABORT OFF
	Set		nocount off
	select @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[WZ_FriendDel]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


-- ДЈ±ёё¦ ёс·ПїЎј­ »иБ¦ЗСґЩ.
CREATE procedure [dbo].[WZ_FriendDel]
	@Name varchar(10),  @FriendName varchar(10)

as 

BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int
	DECLARE @FriendGuid  int

	Set		nocount on

	SET @ErrorCode = 0

	-- іЄАЗ GUIDё¦ ѕтґВґЩ.
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@Name)
	  BEGIN
		SET @ErrorCode = 3
		GOTO EndProc

	  END
	else
	  BEGIN
		select @UserGuid = GUID FROM T_FriendMain where Name=@Name
	
		IF ( @@Error  <> 0   )
		begin
			SET @ErrorCode = 4
		end

	  END

	-- ДЈ±ёАЗ GUIDё¦ ѕтґВґЩ.
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@FriendName )
	  BEGIN
		SET @ErrorCode = 5
		GOTO EndProc
	  END
 	ELSE
	  BEGIN
		select @FriendGuid = GUID FROM T_FriendMain where Name=@FriendName

		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode = 6
		end

	end

	-- ДЈ±ёё®ЅєЖ®їЎ µо·ПµЗѕо АЦґВБц ГјЕ©ЗСґЩ.
	if NOT EXISTS (SELECT GUID FROM T_FriendList where GUID = @UserGuid AND FriendGuid = @FriendGuid  )
	begin
		SET @ErrorCode = 2
		GOTO EndProc
	end
	
	-- »иБ¦ЗСґЩ.
	DELETE FROM T_FriendList where GUID = @UserGuid AND FriendGuid = @FriendGuid  
	
	IF ( @@Error  <> 0 )
		SET @ErrorCode	= @@Error
	else SET @ErrorCode	= 1

	IF( @ErrorCode = 1 )
	BEGIN
		UPDATE T_FriendList SET Del=1 where GUID=@FriendGuid AND FriendGuid=@UserGuid
	END

EndProc:

	Set		nocount off
	select @ErrorCode
END
GO
/****** Object:  StoredProcedure [dbo].[WZ_GetItemSerial]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO


CREATE procedure [dbo].[WZ_GetItemSerial]
as
BEGIN	
	DECLARE @ItemSerial	int
	set nocount on
	begin transaction

		update GameServerInfo set @ItemSerial = ItemCount = ItemCount+1
			
		if ( @@error  <> 0 )
		begin	
			rollback transaction
			select -1
		end 
		else
		begin
			commit transaction				
			select @ItemSerial
		end
END
GO
/****** Object:  StoredProcedure [dbo].[WZ_GetItemSerial2]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[WZ_GetItemSerial2]
	@iAddSerialCount	int
as
BEGIN	
	DECLARE @ItemSerial	int

	set nocount on
	begin transaction

	update GameServerInfo 
	set @ItemSerial = ItemCount = ItemCount+@iAddSerialCount
		
	if ( @@error  <> 0 )
	begin	
		rollback transaction
		select -1
	end 
	else
	begin
		commit transaction				
		select @ItemSerial-@iAddSerialCount+1
	end

	set nocount off	
END

GO
/****** Object:  StoredProcedure [dbo].[WZ_GuildCreate]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[WZ_GuildCreate]
	@GuildName	varchar(8),
	@MasterName 	varchar(10)
as 
BEGIN
	DECLARE @ErrorCode int

	SET @ErrorCode = 0
	SET XACT_ABORT ON

	Set		nocount on 		
	begin transaction

	-- ±жµеё¦ »эјєЗСґЩ.	
	INSERT	INTO Guild (G_Name, G_Master) VALUES (@GuildName, @MasterName)
	IF ( @@Error  <> 0 )
	BEGIN
		SET @ErrorCode	= 1
	END

       	-- ±жµеё¦ Б¤»уАыАё·О »эјє ЗЯґЩёй
	IF ( @ErrorCode  =  0 )
	BEGIN
		-- ±жµе ё¶ЅєЕН·О ГЯ°ЎЗСґЩ.
		INSERT GuildMember (Name, G_Name, G_Level) VALUES (@MasterName, @GuildName, 1)
		IF ( @@Error  <> 0 )
		BEGIN
			SET @ErrorCode	= 2
		END
	END

	IF ( @ErrorCode  <> 0 )
		rollback transaction
	ELSE
		commit transaction

	select @ErrorCode

	Set 	nocount off 
	SET XACT_ABORT OFF
END
GO
/****** Object:  StoredProcedure [dbo].[WZ_MoveCharacter]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/*
	1) ЗБ·ОЅГБ®ён : WZ_MoveCharacter
	2) АЫѕчАП : 2004. 07. 30.
	3) АЫѕчАЪён :  ГЯјч 
	4) АьґЮАОАЪ :
			°иБ¤їЎ ґлЗС ДіёЇЕН АМµї 
	5) №ЭИЇАОАЪ(Result) :
		0x01 : °ФАУі»їЎ БўјУБЯ
		0x02 : °иБ¤ OR ДіёЇЕН°Ў БёАзЗПБц ѕКґВґЩ.s
		0x03 : ДіёЇЕНАЗ »уЕВ°Ў АМµїЗТ јц ѕшґВ »уЕВ		 
		0x04 : АМµїИЅјц ГК°ъ 
		0x05 : ДіёЇЕН°Ў °иБ¤ДіёЇЕН ЅЅ·ФїЎ БёАзЗПБц ѕКґВґЩ.
		0x06 : ГЯ°ЎЗТ °иБ¤ДіёЇЕН єу ЅЅ·ФАМ БёАзЗПБц ѕКґВґЩ.
		0x07 : ґЩЕ©·Оµе АМµї єТ°ЎґЙ
		0x08 : ·№є§ Б¦ЗС, АОєҐАЗ ѕЖАМЕЫ, БЁ 
		0x09 : DB Гіё® ЅЗЖР 
		0x10 : ±жµе °ЎАФµЗѕо АЦґЩ.
		
	6) VERSION
		1 

	7) Last Update Date : 2005.03.10 whatthehell(ґЩЕ©·Оµе АМµї °ЎґЙЗП°Ф єЇ°ж)

	8) DESC
	    Аб±ЭїЎ ґлЗС ГЦјТґЬА§ 1000(1ГК) јіБ¤. ёёѕа 1ГКѕИїЎ ѕИ іЎіЇ°жїм ёрµзАЫѕчА» ·С№йГіё® ЗСґЩ. 
           °ў АЫѕчїЎ ґлЗС їЎ·ЇБ¤єё °Л»зЗСґЩ. ёёѕа їЎ·Ї °Л»з ѕИЗП°н іСѕо°Ґ °жїм єн·ЇЕ· °Йё± И®·ьАМ іфґЩ. 
	    ї©±вј­ґВ @ErrorCheck єЇјцё¦ АМїлЗПї© АЫѕч 

*/
CREATE Procedure [dbo].[WZ_MoveCharacter] 
	@AccountID			varchar(10),		-- °иБ¤ён 
	@Name				varchar(10),		-- АМАь ДіёЇЕН 
	@ChangeAccountID	varchar(10), 		-- єЇ°жЗТ °иБ¤ён 
	@IsOriginalAccountID  bit				-- їшє» °иБ¤ї©єО ГјЕ© 
AS
Begin

	SET NOCOUNT ON
	SET	XACT_ABORT ON					-- їЎ·Ї№Я»эЅГ ёрµз Ж®·ЈАијЗ ГлјТ  
	
	DECLARE	@Result				tinyint	
	DECLARE	@ResultLowCount		int
	DECLARE	@Class				tinyint
	DECLARE	@Ctl1_Code			tinyint
	DECLARE	@SQLEXEC			varchar(1000)
	DECLARE	@ErrorCheck			INT
	DECLARE 	@g1 varchar(10), @g2 varchar(10), @g3 varchar(10), @g4 varchar(10), @g5 varchar(10)
	DECLARE 	@MoveCnt tinyint		
	DECLARE 	@ChangeMoveCnt	tinyint		
	DECLARE	@SqlStmt			VARCHAR(700)		
	DECLARE	@SqlStmt2			VARCHAR(700)		
	DECLARE	@SqlStmt3			VARCHAR(700)		


	SET LOCK_TIMEOUT	1000			-- Ж®·ЈАијЗ Ае±в Аб±ЭГіё®ё¦ ё·±в А§ЗШј­ 	
	SET @Result = 0x00	
	SET @ErrorCheck = 0x00


	If EXISTS( SELECT Name FROM GuildMember   WHERE Name = @Name )
	BEGIN
		SET @Result	= 0x10			--// °ЎАФµИ ±жµе БёАз 
		GOTO ON_ERROR
	End	

	--====================================================================================
	-- ДіёЇЕН БёАзї©єО И®АО 
	--====================================================================================
	SELECT @Class = Class, @Ctl1_Code = CtlCode  FROM  Character  WHERE Name = @Name
	
	-- Гіё®°б°ъ ГјЕ© 	
	SELECT @ResultLowCount = @@rowcount, @ErrorCheck = @@error

	-- ДіёЇЕН°Ў БёАзЗПБц ѕКґВґЩ.  						
	IF @ResultLowCount = 0 
	begin
		SET @Result	= 0x02			
		GOTO ON_ERROR						
	end

	-- їЎ·Ї№Я»э(Ж®·ЈАијЗ Аб±ЭАМіЄ ¶ЗґВ ±вЕё №®Б¦ №Я»эЅГ Гіё®) 
	IF @ErrorCheck  <> 0 GOTO ON_ERROR

	-------------------------------------------------------------------------------------
	-- ДіёЇЕНАЗ »уЕВ°Ў АМµїЗТ јц ѕшґВ »уЕВ(0x80)
	-- АМµї»уЕВАЗ ДіёЇЕН ¶ЗґВ ±вЕё єн·°»уЕВАЗ ДіёЇЕНАЗ °жїм АМµїАМ єТ°ЎґЙ 							
	-------------------------------------------------------------------------------------
	if  ( (@Ctl1_Code & 127 ) > 0 )
	BEGIN
		SET @Result	= 0x03			
		GOTO ON_ERROR						
	END 

	--====================================================================================
	-- їшє» ДіёЇЕН ЅЅ·ФБ¤єё ГјЕ© 
	--====================================================================================
	SELECT  @g1=GameID1, @g2=GameID2, @g3=GameID3, @g4=GameID4, @g5=GameID5, @MoveCnt = MoveCnt 
	FROM dbo.AccountCharacter 	Where Id = @AccountID 		
	
	-- Гіё®°б°ъ ГјЕ© 	
	SELECT @ResultLowCount = @@rowcount, @ErrorCheck = @@error

	--// °иБ¤АМ БёАзЗПБц ѕКґВґЩ. 
	if @ResultLowCount = 0 
	begin
		SET @Result	= 0x02			
		GOTO ON_ERROR						
	end

	-- їЎ·Ї№Я»э(Ж®·ЈАијЗ Аб±ЭАМіЄ ±віЄ №®Б¦ №Я»эЅГ Гіё®) 
	IF @ErrorCheck  <> 0 GOTO ON_ERROR


	-------------------------------------------------------------------------------------
	-- АМµїИЅјц ГјЕ© 
	-------------------------------------------------------------------------------------
	IF @MoveCnt IS NULL 
	BEGIN
		SET @MoveCnt  =0 
	END
	
	-- ё¶°Л»з ДіёЇЕН(»уА§ 4єсЖ®)	
	IF  @Class = 48 
		BEGIN
			IF ((@MoveCnt&240) > 0) 
				BEGIN	
					SET @Result	= 0x04			-- АМµїЗТ јц ѕшґВ »уЕВ(АМ№М АМАьїЎ АМµїЗС »уЕВ) 					
					GOTO ON_ERROR				
				END 
			ELSE
				BEGIN
					SET @MoveCnt =  @MoveCnt | 16	
				END 
		END
	ELSE
		BEGIN
			-- ґЩЕ©·Оµе(»уА§ 4єсЖ®) - ё¶°Л»зїН µїАПЗП°Ф Гл±Ю(2005.03.10 whatthehell)
			IF @Class = 64
				BEGIN
					IF ((@MoveCnt&240) > 0) 
						BEGIN	
							SET @Result	= 0x04			-- АМµїЗТ јц ѕшґВ »уЕВ(АМ№М АМАьїЎ АМµїЗС »уЕВ) 					
						GOTO ON_ERROR				
						END 
					ELSE
						BEGIN
							SET @MoveCnt =  @MoveCnt | 16	
						END 
				END
			ELSE 	-- АП№Э ДіёЇЕН(ЗПА§ 4єсЖ®)
				BEGIN
					IF  ((@MoveCnt&15) > 0)
						BEGIN	
							SET @Result	= 0x04			-- АМµїЗТ јц ѕшґВ »уЕВ(АМ№М АМАьїЎ АМµїЗС »уЕВ) 					
							GOTO ON_ERROR				
						END 
					ELSE
						BEGIN
							SET @MoveCnt =  @MoveCnt | 1	
						END 
				END 
		END


	-------------------------------------------------------------------------------------
	-- їшє» ДіёЇЕН Е¬ё®ѕо  & АМµїИЅјц UPDATE 
	-------------------------------------------------------------------------------------
	SET @SqlStmt = 'UPDATE AccountCharacter  '

	IF ( @g1 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  GameID1 = NULL,'
	ELSE IF ( @g2 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  GameID2 = NULL,'
	ELSE IF ( @g3 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  GameID3 = NULL,'
	ELSE IF ( @g4 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  GameID4 = NULL,'
	ELSE IF ( @g5 = @Name )
		SET @SqlStmt = @SqlStmt + ' SET  GameID5 = NULL,'
	ELSE 				
		SET @Result	= 0x05--// ЗШґз °иБ¤їЎ ґлЗС ДіёЇЕН°Ў БёАз ЗПБц ѕКґВґЩ. 

	IF ( @Result <> 0 )
		GOTO ON_ERROR

	SET @SqlStmt = @SqlStmt + ' MoveCnt =  ' + CONVERT(VARCHAR, @MoveCnt )					
	SET @SqlStmt = @SqlStmt + ' WHERE Id =  ''' + @AccountID	 + ''''				

	--====================================================================================
	--   АМµї  ДіёЇЕН ЅЅ·ФБ¤єё ГјЕ© 
	--====================================================================================
	SELECT  @g1=GameID1, @g2=GameID2, @g3=GameID3, @g4=GameID4, @g5=GameID5, @ChangeMoveCnt = MoveCnt  
	FROM dbo.AccountCharacter  Where Id = @ChangeAccountID 			

	-- Гіё®°б°ъ ГјЕ© 	
	SELECT @ResultLowCount = @@rowcount, @ErrorCheck = @@error

	-- їЎ·Ї№Я»э(Ж®·ЈАијЗ Аб±ЭАМіЄ ±віЄ №®Б¦ №Я»эЅГ Гіё®) 
	IF @ErrorCheck  <> 0 GOTO ON_ERROR

	IF @ResultLowCount = 0 
	BEGIN
		-------------------------------------------------------------------------------------
		-- АМµї °иБ¤ДіёЇЕН БёАзЗПБц ѕКА» °жїм AccountCharacter »эјє
		-------------------------------------------------------------------------------------
		SET @SqlStmt2 ='INSERT INTO dbo.AccountCharacter(Id, GameID1, GameID2, GameID3, GameID4, GameID5, GameIDC) '
		SET @SqlStmt2 = @SqlStmt2 + ' VALUES( ''' +  @ChangeAccountID + ''', '
		SET @SqlStmt2 = @SqlStmt2 + '''' + @Name + ''', '
		SET @SqlStmt2 = @SqlStmt2 +  ' NULL, NULL, NULL, NULL, NULL) '

		PRINT @SqlStmt2
	END
	ELSE
	BEGIN
		-------------------------------------------------------------------------------------
		-- АМµї °иБ¤ДіёЇЕН Б¤єё ѕчµҐАМЖ® 
		-------------------------------------------------------------------------------------
		SET @SqlStmt2 = 'UPDATE AccountCharacter SET '
	
		IF( ( @g1 Is NULL) OR (Len(@g1) = 0))
			SET @SqlStmt2 = @SqlStmt2 + '  GameID1 = '
		ELSE IF ( @g2  Is NULL OR Len(@g2) = 0)
			SET @SqlStmt2 = @SqlStmt2 + '  GameID2 = '
		ELSE IF ( @g3 Is NULL OR Len(@g3) = 0)
			SET @SqlStmt2 = @SqlStmt2 + '  GameID3 = ' 
		ELSE IF ( @g4 Is NULL OR Len(@g4) = 0)
			SET @SqlStmt2 = @SqlStmt2 + '  GameID4 = '
		ELSE IF ( @g5 Is NULL OR Len(@g5) = 0)
			SET @SqlStmt2 = @SqlStmt2 + '  GameID5 = '
		ELSE 		
			SET @Result	= 0x06			-- ЗШґз єу ЅЅ·Ф Б¤єё°Ў БёАз ЗПБц ѕКґЩ. 							
	
		if( @Result <> 0 )
			GOTO ON_ERROR
		
		SET @SqlStmt2 = @SqlStmt2 +  '''' + @Name + ''''
		SET @SqlStmt2 = @SqlStmt2 + ' WHERE Id =  ''' + @ChangeAccountID + ''''
	END

	--====================================================================================
	-- ДіёЇЕНАЗ єЇ°ж 
	--====================================================================================
	SET @SqlStmt3 = 'UPDATE Character '
	SET @SqlStmt3 = @SqlStmt3 + 'SET  AccountID = ''' + @ChangeAccountID + ''''
	
	IF @IsOriginalAccountID = 1
		SET @SqlStmt3 = @SqlStmt3 + ', CtlCode = ' + CONVERT(VARCHAR, @Ctl1_Code & 127	)	-- ДіёЇЕН АМµї»уЕВё¦ їшє»Аё·О єЇ°ж[ АМµїДіёЇ ДЪµеґВ 0x80 ]
	ELSE
		SET @SqlStmt3 = @SqlStmt3 + ', CtlCode = ' + CONVERT(VARCHAR,  @Ctl1_Code | 128	)	-- АМµїДіёЇЕН »уЕВ·О єЇ°ж 
	
	SET @SqlStmt3 = @SqlStmt3 + ' WHERE Name = ''' +  @Name + ''''


	--====================================================================================
	--Ж®·ЈАијЗ АЫѕч Гіё®  
	--====================================================================================
	BEGIN TRANSACTION 

	-- їшє» °иБ¤ДіёЇЕН Б¤єё єЇ°ж Гіё® 
	EXEC(@SqlStmt)
	SELECT @ResultLowCount = @@rowcount,  @ErrorCheck = @@error
	IF  @ResultLowCount = 0  GOTO ON_TRN_ERROR
	IF  @ErrorCheck  <> 0 GOTO ON_TRN_ERROR

	-- АМµї °иБ¤ДіёЇЕН Б¤єё єЇ°ж Гіё® 
	EXEC(@SqlStmt2)
	SELECT @ResultLowCount = @@rowcount,  @ErrorCheck = @@error
	IF  @ResultLowCount = 0  GOTO ON_TRN_ERROR
	IF  @ErrorCheck  <> 0 GOTO ON_TRN_ERROR
	
	-- ДіёЇЕН Б¤єё єЇ°ж Гіё® 
	EXEC(@SqlStmt3)
	SELECT @ResultLowCount = @@rowcount,  @ErrorCheck = @@error
	IF  @ResultLowCount = 0  GOTO ON_TRN_ERROR
	IF  @ErrorCheck  <> 0 GOTO ON_TRN_ERROR

	--====================================================================================
	-- Ж®·ЈАијЗ °б°ъ Гіё® 
	--====================================================================================
ON_TRN_ERROR:
	IF ( @Result  <> 0 ) OR (@ErrorCheck <> 0)
	BEGIN
		IF @Result = 0 
			SET @Result = 0x09 		--// DB Error

		ROLLBACK TRAN
	END
	ELSE
		COMMIT	TRAN

ON_ERROR:
	IF @ErrorCheck <> 0
	BEGIN
		SET @Result = 0x09 			--// DB Error 
	END 


	SELECT @Result	

	SET NOCOUNT OFF
	SET	XACT_ABORT OFF
END

GO
/****** Object:  StoredProcedure [dbo].[WZ_RenameCharacter]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









--////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
	1) ЗБ·ОЅГБ®ён : WZ_RenameCharacter
	2) јцБ¤АП : 2005. 03. 31.
	3) АЫѕчАЪён :  ±ЗµОЗС 
	4) АьґЮАОАЪ :
			ј­№цДЪµе, ДіёЇЕНён
	5) №ЭИЇАОАЪ(Result) :
			0x01 : ДіёЇЕН°Ў БёАзЗПБц ѕКґВґЩ
			0x02 : єЇ°жЗТ ДіёЇЕН БёАз
			0x03 : °ЎАФµИ ±жµе БёАз
			0x04 : °ФБ¤ Б¤єё°Ў БёАзЗПБц ѕКґВґЩ.
			0x05 : єЇ°жЗТ ДіёЇЕНАЗ ЗШґз ЅЅ·ФАМ БёАзЗПБц ѕКґВґЩ. 
			2 : ДіёЇЕНїЎ ґлЗС ЗШґз °иБ¤АМ БёАзЗПБц ѕКґВґЩ 
			
	6) VERSION
		3 

	7) Last Update Date : 2005.03.31
	
*/
CREATE Procedure [dbo].[WZ_RenameCharacter] 
	@ServerCode	smallint,					--// ј­№цДЪµе 
	@AccountID		varchar(10),				--// °иБ¤ён 
	@Name			varchar(10),				--// АМАь ДіёЇЕН 
	@ChangeName			varchar(10)		--// єЇ°жЗТ  ДіёЇЕН 
AS
Begin

	SET NOCOUNT ON
	SET	XACT_ABORT ON			-- їЎ·Ї№Я»эЅГ ёрµО ГлјТ 
	DECLARE		@Result		tinyint, 		@GUID 		int,@CGUID	int

	--// Ж®·ЈАијЗ ¶фГіё® ЅГ°Ј 10ГК (АМ±вБѕ ј­№ц°ЈАЗ ЕлЅЕА» А§ЗШј­ ЅГ°ЈјіБ¤)
	SET LOCK_TIMEOUT	5000

	--//  °б°ъ°Є ГК±вИ­ 
	SET @Result = 0x00	

	--====================================================================================
	-- ДіёЇЕН БёАзї©єО И®АО 
	--====================================================================================
	If NOT EXISTS ( SELECT  Name  FROM  Character  (READUNCOMMITTED) WHERE Name = @Name )
	begin
		SET @Result	= 0x01				--// ДіёЇЕН°Ў БёАзЗПБц ѕКґВґЩ.  						
		GOTO ProcEnd						
	end 

	--====================================================================================
	-- єЇ°жЗТ ДіёЇЕН БёАзї©єО И®АО 
	--====================================================================================
	If  not EXISTS ( SELECT  Name  FROM  Character (READUNCOMMITTED)  WHERE Name = @ChangeName )
	begin
		SET @Result	= 0x02				--// єЇ°жЗТ ДіёЇЕН БёАз   						
		GOTO ProcEnd						
	end 

	--====================================================================================
	-- ±жµеµо·П ї©єО И®АО 
	--====================================================================================
	If EXISTS( SELECT Name FROM GuildMember (READUNCOMMITTED)  WHERE Name = @Name )
	BEGIN
		SET @Result	= 0x03			--// °ЎАФµИ ±жµе БёАз 
		GOTO ProcEnd
	End			
	
	select @GUID=GUID from T_CGuid where Name=@Name
	--************************************************************************************
	--// єР»к Ж®·ЈАијЗ ЅЗЗа 

	declare 	@cLevel int,@LevelUpPoint int,@Class tinyint  ,@Experience int ,@Strength smallint  ,@Dexterity smallint  ,@Vitality smallint  ,@Energy smallint  ,@Inventory varbinary (1080)  ,@MagicList varbinary (60)  ,@Money int  ,@Life real  ,@MaxLife real  ,@Mana real  ,@MaxMana real  ,@MapNumber smallint  ,@MapPosX smallint  ,@MapPosY smallint  ,@MapDir tinyint,@PkCount int ,@PkLevel int ,@PkTime int ,@MDate smalldatetime  ,@LDate smalldatetime  ,@CtlCode tinyint ,@DbVersion tinyint,@Quest varbinary (50)  ,@Leadership smallint,@ChatLimitTime smallint
	declare @O_Name varchar(10),@O_SkillKey binary(10),@O_GameOption tinyint,@O_Qkey tinyint,@O_Wkey tinyint,@O_Ekey tinyint,@O_ChatWindow tinyint
	declare @F_Name varchar(10),@F_FriendCount tinyint,@F_MemoCount int,@F_MemoTotal int

	select 	@cLevel=cLevel ,@LevelUpPoint=LevelUpPoint ,@Class=Class,@Experience=Experience  ,@Strength=Strength,@Dexterity=Dexterity,@Vitality=Vitality,@Energy=Energy,
		@Inventory=Inventory  ,@MagicList=MagicList  ,@Money=Money   ,@Life=Life   ,
		@MaxLife=MaxLife   ,@Mana=Mana   ,@MaxMana=MaxMana   ,@MapNumber=MapNumber,
		@MapPosX=MapPosX   ,@MapPosY=MapPosY   ,@MapDir=MapDir ,@PkCount=PkCount  ,
		@PkLevel=PkLevel  ,@PkTime=PkTime  ,@MDate=MDate   ,@LDate=LDate   ,@CtlCode=CtlCode,
		@DbVersion=DbVersion ,@Quest=Quest  ,@Leadership=Leadership ,@ChatLimitTime=ChatLimitTime 
		from Character where Name=@Name

	select 	@O_Name=Name, @O_SkillKey=SkillKey, @O_GameOption=GameOption,  @O_Qkey=Qkey, @O_Wkey=Wkey, @O_Ekey=Ekey ,@O_ChatWindow=ChatWindow 
		from OptionData where Name=@Name

	--select @F_Name =Name,@F_FriendCount =FriendCount, @F_MemoCount =MemoCount  from T_FriendMain where GUID=@GUID

	BEGIN DISTRIBUTED TRAN

	--====================================================================================
	-- °иБ¤ БёАзї©єО И®АО
	-- ЗКїдјє : єРЗТј­№ц·О ДіёЇЕН АМµїЅГ AccountCharacter АЗ µҐАМЕёё¦ »иБ¦ ЅГЕ°±в ¶§№®їЎ И®АО ЗП±в А§ЗС 
	-- ДіёЇЕНАМё§їЎ ЗШґз ЅЅ·ФїЎ єЇ°жЗТ ДіёЇЕНёнАё·О јцБ¤ 
	--====================================================================================
	If NOT EXISTS ( SELECT  Id  FROM  AccountCharacter  WHERE Id = @AccountID )
		begin						
			SET @Result  = 0x04			--// °иБ¤Б¤єё°Ў БёАз ЗПБц ѕКґВґЩ 
			GOTO ProcTrnEnd
		end 
	else
		begin
			--// АМАь ДіёЇЕН°Ў БёАзЗПґш ЅЅ·ФїЎ єЇ°жЗТ ДіёЇЕНєґА» јцБ¤Гіё® ЗСґЩ. 
			Declare @g1 varchar(10), @g2 varchar(10), @g3 varchar(10), @g4 varchar(10), @g5 varchar(10)						
			SELECT @g1=GameID1, @g2=GameID2, @g3=GameID3, @g4=GameID4, @g5=GameID5 FROM dbo.AccountCharacter Where Id = @AccountID 			

			if( @g1 = @Name )
				begin
					UPDATE AccountCharacter SET  GameID1 = @ChangeName
					WHERE Id = @AccountID
		
					SET @Result  = @@Error
				end 
			else if( @g2 = @Name )
				begin
					UPDATE AccountCharacter SET  GameID2 = @ChangeName
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g3 = @Name )
				begin			
					UPDATE AccountCharacter SET  GameID3 = @ChangeName
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g4 = @Name )
				begin
					UPDATE AccountCharacter SET  GameID4 = @ChangeName
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g5 = @Name )
				begin
					UPDATE AccountCharacter SET  GameID5 = @ChangeName
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 		
			else 				
				begin					
					SET @Result	= 0x05	--// ЗШґз ДіёЇЕНїЎ ґлЗС ЅЅ·ФАМ БёАз ЗПБц ѕКґВґЩ. 						
					GOTO ProcTrnEnd								
				end 		

			if( @g1 = @ChangeName )
				begin
					UPDATE AccountCharacter SET  GameID1 = ''
					WHERE Id = @AccountID
		
					SET @Result  = @@Error
				end 
			else if( @g2 = @ChangeName )
				begin
					UPDATE AccountCharacter SET  GameID2 = ''
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g3 = @ChangeName )
				begin			
					UPDATE AccountCharacter SET  GameID3 = ''
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g4 = @ChangeName )
				begin
					UPDATE AccountCharacter SET  GameID4 = ''
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 
			else if( @g5 = @ChangeName )
				begin
					UPDATE AccountCharacter SET  GameID5 = ''
					WHERE Id = @AccountID

					SET @Result  = @@Error
				end 		
			else 				
				begin					
					SET @Result	= 0x05	--// ЗШґз ДіёЇЕНїЎ ґлЗС ЅЅ·ФАМ БёАз ЗПБц ѕКґВґЩ. 						
					GOTO ProcTrnEnd								
				end 			

	 
		end 

	--====================================================================================
	-- Гіё®µИ Б¤єё°Ў №ц±Ч°Ў АЦА» °жїм №ЭИЇГіё®  
	--====================================================================================
	If ( @Result <> 0 )
	begin
		GOTO ProcTrnEnd	
	end

	--====================================================================================
	-- ДіёЇЕН ±вє»Б¤єё єЇ°ж 
	--====================================================================================


	If ( @Result = 0 )
	begin
		
		UPDATE Character	
		SET
		cLevel=@cLevel ,LevelUpPoint=@LevelUpPoint ,Class=@Class,Experience=@Experience  ,
		Strength=@Strength,Dexterity=@Dexterity,Vitality=@Vitality,Energy=@Energy,
		Inventory=@Inventory  ,MagicList=@MagicList  ,Money=@Money   ,Life=@Life   ,
		MaxLife=@MaxLife   ,Mana=@Mana   ,MaxMana=@MaxMana   ,MapNumber=@MapNumber,
		MapPosX=@MapPosX   ,MapPosY=@MapPosY   ,MapDir=@MapDir ,PkCount=@PkCount  ,
		PkLevel=@PkLevel  ,PkTime=@PkTime  ,MDate=@MDate   ,LDate=@LDate   ,CtlCode=@CtlCode,
		DbVersion=@DbVersion ,Quest=@Quest  ,Leadership=@Leadership ,ChatLimitTime=@ChatLimitTime 
		from Character where Name=@ChangeName


		update OptionData set SkillKey=@O_SkillKey,GameOption= @O_GameOption, Qkey= @O_Qkey, Wkey=@O_Wkey, Ekey=@O_Ekey ,ChatWindow=@O_ChatWindow 
		where Name=@ChangeName
		
		
		--єЇ°ж ДіёЇЕН јцБ¤ ---------------------------------
--		update T_FriendMain set FriendCount=@F_FriendCount, MemoCount=@F_MemoCount, MemoTotal=@F_MemoTotal where Name=@ChangeName


		SET @Result =  @@Error
		IF @Result <> 0 
			GOTO ProcTrnEnd	
	end 



ProcTrnEnd:
	IF ( @Result  <> 0 )
		ROLLBACK TRAN
	ELSE
		COMMIT	TRAN


ProcEnd:


	--// °б°ъБ¤єёё¦ №ЭИЇ Гіё® 
	SELECT @Result	

	SET NOCOUNT OFF
	SET	XACT_ABORT OFF
	
End








GO
/****** Object:  StoredProcedure [dbo].[WZ_SetGuildDelete]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[WZ_SetGuildDelete]
	@GuildName		varchar(10)
As
Begin
	SET NOCOUNT ON
	Declare		@Result		int
	Set @Result	= 1 

	Begin Transaction									
		--// Guild member db delete
		DELETE GuildMember WHERE G_Name = @GuildName		
		If @@Error <> 0 
		begin			
			Set @Result	= 0 -- DB Error 
			goto PROBLEM	
		end

		--// Guild  Main DBё¦ »иБ¦
		DELETE Guild WHERE G_Name = @GuildName		
		If @@Error <> 0 
			begin
				Set @Result	= 0  --  DB Error
				goto PROBLEM	
			end	
		else goto SUCESS

	PROBLEM:
		rollback transaction
 
	SUCESS:
   		commit transaction		
				
	-- °б°ъё¦ RETURN
	SELECT @Result  As Result
	SET NOCOUNT OFF
End

GO
/****** Object:  StoredProcedure [dbo].[WZ_UserGuidCreate]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*
-- »зїлАЪ °нАЇ GUID°Ў БёАзЗПБц ѕКґВґЩёй GUIDё¦ »эјєЗСґЩ.

*/
CREATE procedure [dbo].[WZ_UserGuidCreate]
	@Name varchar(10)
as 
BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int

	SET @ErrorCode = 1
	SET @UserGuid = -1

	SET	XACT_ABORT ON
	Set		nocount on 		


	-- GUID°Ў БёАзЗПґВ°Ў?
	if EXISTS ( select GUID FROM T_CGuid where Name=@Name ) 
	  BEGIN
		SET @ErrorCode = 0
		goto EndProc
	  END

	BEGIN TRAN

	-- БёАзЗПБц ѕКА»°жїмїЈ GUIDё¦ »эјєЗСґЩ.
	INSERT INTO T_CGuid (Name) VALUES(@Name)

	IF ( @@Error  <> 0 )
	  BEGIN
		SET @ErrorCode	= 2
	  END
	ELSE 
	  BEGIN
		select @UserGuid = GUID FROM T_CGuid where Name=@Name
		IF ( @@Error  <> 0 )
		  BEGIN
			SET @ErrorCode	= 3
		  END
		ELSE 
		  BEGIN
			INSERT INTO T_FriendMain ( GUID, Name, FriendCount, MemoCount) VALUES(@UserGuid, @Name,1,10)
			IF ( @@Error  <> 0 )
				SET @ErrorCode	= 4
		  END
	  END

EndTranProc:
	IF ( @@Error  <> 0 )
		ROLLBACK TRAN
	ELSE COMMIT TRAN

EndProc:
	select @ErrorCode
	SET	XACT_ABORT OFF

	Set		nocount off
END

GO
/****** Object:  StoredProcedure [dbo].[WZ_WaitFriendAdd]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


/*  ДЈ±ё ґл±в ёс·ПїЎ АЪЅЕА» ГЯ°ЎЗСґЩ.
-- Name :АЪ±в АМё§ 
-- FriendName : ДЈ±ё АМё§

return : 
	0 : јє°ш,
 	2 : АЇАъ GUID АР±в ЅЗЖР
       	3 : ДЈ±ё GUID АР±в ЅЗЖР
	4 : ДЈ±ё ГЯ°Ў ЅЗЖР
	5 : ДЈ±ё ГЈ±в ЅЗЖР
	6 : ДЈ±ё ·№є§АМ 6 АМЗПґЩ

-- ** БЯє№ ГЯ°ЎµЗґВ №®Б¦ ЗШ°бЗШѕЯ ЗФ
*/
CREATE procedure [dbo].[WZ_WaitFriendAdd]
	@Name varchar(10), @FriendName varchar(10)
as 
BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int
	DECLARE @FriendGuid  int
	DECLARE @FriendLevel  int

	SET NOCOUNT ON
	SET	XACT_ABORT ON
	

	SET @ErrorCode = 0

	-- GUIDё¦ ѕтґВґЩ.
	if EXISTS (select  GUID FROM T_FriendMain where Name=@Name)
  	  BEGIN
		select @UserGuid = GUID FROM T_FriendMain where Name=@Name
		IF ( @@Error  <> 0 )
		BEGIN
			SET @ErrorCode	= 2
			GOTO EndProc
		END
	  END
	else
	  BEGIN
		SET @ErrorCode	= 3
		GOTO EndProc
	  END

	-- GUIDё¦ ѕтґВґЩ.
	if EXISTS (select  GUID FROM T_FriendMain where Name=@FriendName)
	  BEGIN
		select @FriendGuid = GUID FROM T_FriendMain where Name=@FriendName
		IF ( @@Error  <> 0 )
		BEGIN
			SET @ErrorCode	= 4
			GOTO EndProc
		END
  	  END
	else
	  BEGIN
		SET @ErrorCode	= 5
		GOTO EndProc
  	  END

	-- ДЈ±ёАЗ ·№є§А» ѕтґВґЩ. 
	select  @FriendLevel=cLevel FROM Character where Name=@FriendName
	IF( @@Error <> 0 )
	  begin
		SET @ErrorCode = 5		
		GOTO EndProc
	  end
	ELSE 
	  BEGIN
		-- ·№є§АМ 6єёґЩ АЫґЩёй 
		if( @FriendLevel < 6 )
		  begin
			SET @ErrorCode = 6
			GOTO EndProc
		  end
	  END

	BEGIN TRAN

	-- іЄАЗ ДЈ±ё ё®ЅєЖ®їЎ ГЯ°ЎЅГЕІґЩ.
	INSERT INTO T_FriendList (GUID, FriendGuid, FriendName ) 
		VALUES ( @UserGuid, @FriendGuid, @FriendName)
	
	IF ( @@Error  <> 0 )
	BEGIN
		SET @ErrorCode	= 7
		GOTO EndTranProc
	END

	-- ДЈ±ёАЗ ёс·ПїЎ АМ№М µо·ПµЗѕо АЦґЩёй..
	if EXISTS (SELECT GUID FROM T_FriendList where GUID = @FriendGuid AND FriendGuid =  @UserGuid )
	begin
		UPDATE T_FriendList SET Del=0 where GUID=@FriendGuid AND FriendGuid=@UserGuid
		SET @ErrorCode = 8
		GOTO EndTranProc
	end	

	-- ДЈ±ёАЗ ґл±в ёс·ПїЎ µо·ПЗШ іхґВґЩ
	INSERT INTO T_WaitFriend (GUID, FriendName, FriendGuid ) 
				VALUES ( @FriendGuid, @Name, @UserGuid)
	
	IF ( @@Error  <> 0 )
	BEGIN
		SET @ErrorCode	= 6
		GOTO EndTranProc
	END


EndTranProc:
	IF ( (@ErrorCode  = 0) OR (@ErrorCode  = 8) )
	  BEGIN
		COMMIT TRAN
	  END
	ELSE
	  BEGIN
		ROLLBACK TRAN
	  END
	
EndProc:

	SET	XACT_ABORT OFF

	SET NOCOUNT OFF

	select @ErrorCode
END

GO
/****** Object:  StoredProcedure [dbo].[WZ_WaitFriendDel]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ДЈ±ёё¦ ёс·ПїЎј­ »иБ¦ЗСґЩ.
CREATE procedure [dbo].[WZ_WaitFriendDel]
	@Name varchar(10),  @FriendName varchar(10)

as 

BEGIN
	DECLARE @ErrorCode int
	DECLARE @UserGuid  int
	DECLARE @FriendGuid  int

	Set		nocount on 	

	SET @ErrorCode = 0

	-- іЄАЗ GUIDё¦ ѕтґВґЩ.
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@Name)
	  BEGIN
		SET @ErrorCode = 3
		GOTO EndProc

	  END
	else
	  BEGIN
		select @UserGuid = GUID FROM T_FriendMain where Name=@Name
	
		IF ( @@Error  <> 0   )
		begin
			SET @ErrorCode = 4
		end

	  END

	-- ДЈ±ёАЗ GUIDё¦ ѕтґВґЩ.
	if NOT EXISTS ( select GUID FROM T_FriendMain where Name=@FriendName )
	  BEGIN
		SET @ErrorCode = 5
		GOTO EndProc
	  END
 	ELSE
	  BEGIN
		select @FriendGuid = GUID FROM T_FriendMain where Name=@FriendName

		IF ( @@Error  <> 0 )
		begin
			SET @ErrorCode = 6
		end

	end

	-- ДЈ±ёё®ЅєЖ®їЎ µо·ПµЗѕо АЦґВБц ГјЕ©ЗСґЩ.
	if NOT EXISTS (SELECT GUID FROM T_WaitFriend where GUID = @UserGuid AND FriendGuid = @FriendGuid  )
	begin
		SET @ErrorCode = 2
		GOTO EndProc
	end
	
	-- »иБ¦ЗСґЩ.
	DELETE FROM T_WaitFriend where GUID = @UserGuid AND FriendGuid = @FriendGuid  
	
	IF ( @@Error  <> 0 )
		SET @ErrorCode	= @@Error

	-- Ўб #BUG_FIX_20040421_03
	IF( @ErrorCode = 0 )
	BEGIN
		UPDATE T_FriendList SET Del=1 where GUID=@FriendGuid AND FriendGuid=@UserGuid
	END
	-- Ўб #BUG_FIX_20040421_03	

EndProc:

	Set		nocount off
	select @ErrorCode
END

GO
/****** Object:  StoredProcedure [dbo].[WZ_WriteMail]    Script Date: 07-Jun-19 19:18:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO



-- ЖнБцё¦ АъАеЗСґЩ.
/*
 return 
 
 2 : №ЮґВ »зїлАЪАЗ GUID°Ў БёАзЗПБц ѕКґВґЩ.
 3 : №ЮґВ »зїлАЪАЗ ЖнБц Д«їоЖ®ё¦ ѕчµҐАМЖ® ЗТ јц ѕшґЩ.
 4 : ЖнБцё¦ ГЯ°ЎЗПґВµҐ ЅЗЖРЗЯґЩ
 5 : ЖнБцАЗ ГЦґл °іјцё¦ ГК°ъЗПїґґЩ.
 6 : »уґл°Ў ·№є§ 6 АМЗПґЩ
 10 АМ»у : јє°ш
*/
CREATE procedure [dbo].[WZ_WriteMail]
	@SendName 	varchar(10), 
	@RecvName 	varchar(10),
	@Subject	varchar(32),
	@Dir		tinyint,
	@Act		tinyint

as
BEGIN
	Set		nocount on 	

	DECLARE 	@userguid	int
	DECLARE 	@memocount	int
	DECLARE	@return		int
	DECLARE	@MemoTotal	int
	DECLARE 	@FriendLevel  	int

	SET	XACT_ABORT ON

	SET @return	= 0

	-- №ЮґВ »зїлАЪАЗ GUIDё¦ ѕтґВґЩ
	SELECT @userguid=GUID, @MemoTotal=MemoTotal FROM T_FriendMain where Name= @RecvName
	if( @@ROWCOUNT < 1 )
	BEGIN
		SET @return = 2
		GOTO EndProc
	END

	IF( @MemoTotal > 49 )
	BEGIN
		SET @return = 5
		GOTO EndProc
	END

	-- ДЈ±ёАЗ ·№є§А» ѕтґВґЩ. 
	select  @FriendLevel=cLevel FROM Character where Name=@RecvName
	IF( @@Error <> 0 )
	  begin
		SET @return = 2		
		GOTO EndProc
	  end
	ELSE 
	  BEGIN
		-- ·№є§АМ 6єёґЩ АЫґЩёй 
		if( @FriendLevel < 6 )
		  begin
			SET @return = 6
			GOTO EndProc
		  end
	  END

	begin transaction

	-- №ЮґВ »зїлАЪАЗ ЖнБц №шИЈё¦ ѕтґВґЩ.
	update T_FriendMain set @memocount = MemoCount = MemoCount+1, MemoTotal=MemoTotal+1 where Name = @RecvName
	if( @@error <> 0 )
	BEGIN
		SET @return = 3
		GOTO EndProcTran
	END	

		
	-- ЖнБцё¦ ГЯ°ЎЗСґЩ.
	INSERT INTO T_FriendMail (MemoIndex, GUID, FriendName, wDate, Subject,bRead,  Dir,  Act) VALUES(@memocount,@userguid, @SendName, getdate(), @Subject, 0,  @Dir, @Act)
	if( @@error <> 0 )
	BEGIN
		SET @return = 4
		GOTO EndProcTran
	END

EndProcTran:
	if ( @return  <> 0 )
	begin	
		rollback transaction
	end 
	else
	begin
		commit transaction
		SET @return = @memocount
	end	
	
EndProc:
	SET	XACT_ABORT OFF
	Set		nocount off
	
	SELECT @return, @userguid
End

GO
USE [master]
GO
ALTER DATABASE [MuOnline] SET  READ_WRITE 
GO
