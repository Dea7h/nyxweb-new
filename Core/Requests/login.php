<?php

use Core\Tools\Post;
use Core\Tools\Crypto;
use Core\Tools\Cookie;
use Core\Tools\Notice;
use Core\Main\User;

if (defined('access')) {
    $account = Post::Get('account', '_var');
    $password = Post::Get('password', '_var');

    if ($account && $password) {
        if (strlen($account) < 4 || strlen($account) > 10 || strlen($password) < 4 || strlen($password) > 10) {
            print Notice::Request($lang->Phrase('login', 'fields-length'));
        } else {
            if ($sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE memb___id=? AND [memb__pwd]=?", [$account, $password]) != 1) {
                print Notice::Request($lang->Phrase('login', 'no-match'));
            } else {
                Cookie::Create('username', $account, 360);
                Cookie::Create('password', Crypto::Encrypt($password), 360);

                $user = new User;
                $user->Log([
                    'account' => $account,
                    'module' => 'login',
                    'message' => 'Logged in successfully'
                ]);

                print Notice::Request($lang->Phrase('login', 'success') . ", <b>{$account}</b>!<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
            }
        }
    } else {
        print Notice::Request($lang->Phrase('login', 'fields-empty'));
    }
}