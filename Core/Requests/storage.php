<?php

use Core\Tools\Cookie;
use Core\Tools\Post;
use Core\Tools\JSON;
use Core\Tools\Config;
use Core\Tools\Notice;
use Core\Main\User;
use Core\Main\Item\Decode;
use Core\Main\Item\getFreeSlot;

if (defined('access')) {
    $user = new User;
    $decode = new Decode;
    $freeSlot = new getFreeSlot;
    $config = Config::Get('Items');
    $account = Cookie::Get('username');

    $data = JSON::Get(Post::GetRaw('data'));


    if (!$user->Access()) {
        //Checking if the user is logged in
        print Notice::Request($lang->Phrase('request', 'not-logged'));
    } else if ($sql->Count("SELECT COUNT(*) FROM [MEMB_STAT] WHERE [ConnectStat]='1' AND [memb___id]=?", [$account]) > 0) {
        //Checking user status
        print Notice::Request($lang->Phrase('request', 'online'));
    } else {
        if (isset($data['item'])) {
            $xPlode = explode('-', $data['item']);
            if (isset($xPlode[0]) && isset($xPlode[1]) && ctype_alpha($xPlode[0]) && ctype_digit($xPlode[1])) {
                if ($xPlode[0] == 'right' || ($xPlode[0] == 'left' && isset($data['toSlot']))) {
                    //To Warehouse

                    $warehouse = bin2hex($sql->Fetch("SELECT [Items] FROM [warehouse] WHERE [AccountID]=?", [$account])->Items);
                    $storage = $sql->Fetch("SELECT [items] FROM [NYX_RESOURCES] WHERE [account]=?", [$account])->items;

                    //From WebStorage to Warehouse
                    if ($xPlode[0] == 'right') {
                        //Getting the item hex
                        $itemHex = substr($storage, $config['hexLength'] * $xPlode[1], $config['hexLength']);

                        if (strtolower($itemHex) != str_repeat('f', $config['hexLength']) && strlen($itemHex) == $config['hexLength']) {

                            $decode = $decode->hexDecode($itemHex);
                            if ($decode) {
                                if (isset($data['toSlot'])) {
                                    $slot = $freeSlot->checkSlot($warehouse, $decode->itemInfo['x'], $decode->itemInfo['y'], $data['toSlot'] - 1);
                                } else {
                                    $slot = $freeSlot->Warehouse($warehouse, $decode->itemInfo['x'], $decode->itemInfo['y']);
                                }

                                if ($slot !== false) {
                                    $sql->Query("UPDATE [NYX_RESOURCES] SET [items]=? WHERE [account]=?", [preg_replace("/{$itemHex}/i", '', $storage), $account]);

                                    $newWarehouse = preg_replace("/{$itemHex}/i", str_repeat('f', $config['hexLength']), $warehouse);
                                    $newWarehouse = substr_replace($newWarehouse, $itemHex, $slot * $config['hexLength'], $config['hexLength']);

                                    $sql->Query("UPDATE [warehouse] SET [Items]=CONVERT(varbinary(1200), '{$newWarehouse}', 2) WHERE [AccountID]=?", [$account]);

                                    print Notice::Request($lang->Phrase('account-storage', 'success') . "<script>loader(location.pathname);</script>", 'success');
                                } else {
                                    print Notice::Request($lang->Phrase('account-storage', 'no-space'));
                                }
                            }

                        }

                        //From Warehouse to Warehouse
                    } else {
                        $itemHex = substr($warehouse, $config['hexLength'] * $xPlode[1], $config['hexLength']);

                        if (strtolower($itemHex) != str_repeat('f', $config['hexLength']) && strlen($itemHex) == $config['hexLength']) {
                            $decode = $decode->hexDecode($itemHex);
                            if ($decode) {
                                if (isset($data['toSlot'])) {
                                    $slot = $freeSlot->checkSlot($warehouse, $decode->itemInfo['x'], $decode->itemInfo['y'], $data['toSlot'] - 1);

                                    if ($slot !== false) {
                                        $newWarehouse = preg_replace("/{$itemHex}/i", str_repeat('f', $config['hexLength']), $warehouse);
                                        $newWarehouse = substr_replace($newWarehouse, $itemHex, $slot * $config['hexLength'], $config['hexLength']);

                                        $sql->Query("UPDATE [warehouse] SET [Items]=CONVERT(varbinary(1200), '{$newWarehouse}', 2) WHERE [AccountID]=?", [$account]);

                                        print Notice::Request($lang->Phrase('account-storage', 'success') . "<script>loader(location.pathname);</script>", 'success');
                                    } else {
                                        print Notice::Request($lang->Phrase('account-storage', 'no-space'));
                                    }
                                }
                            }
                        }
                    }
                } else {
                    //To WebStorage

                    $warehouse = bin2hex($sql->Fetch("SELECT [Items] FROM [warehouse] WHERE [AccountID]=?", [$account])->Items);
                    $storage = $sql->Fetch("SELECT [items] FROM [NYX_RESOURCES] WHERE [account]=?", [$account])->items;

                    $itemHex = substr($warehouse, $config['hexLength'] * $xPlode[1], $config['hexLength']);

                    if (strtolower($itemHex) != str_repeat('f', $config['hexLength']) && strlen($itemHex) == $config['hexLength']) {
                        if (strlen($storage) / $config['hexLength'] <= 150) {
                            $newWarehouse = preg_replace("/{$itemHex}/i", str_repeat('f', $config['hexLength']), $warehouse);
                            $sql->Query("UPDATE [warehouse] SET [Items]=CONVERT(varbinary(1200), '{$newWarehouse}', 2) WHERE [AccountID]=?", [$account]);

                            $newStorage = $storage . $itemHex;
                            $sql->Query("UPDATE [NYX_RESOURCES] SET [items]=? WHERE [account]=?", [$newStorage, $account]);

                            print Notice::Request($lang->Phrase('account-storage', 'success') . "<script>loader(location.pathname);</script>", 'success');
                        } else {
                            print Notice::Request($lang->Phrase('account-storage', 'no-space'));
                        }
                    } else {
                        print Notice::Request($lang->Phrase('account-storage', 'item-gone'));
                    }
                }
            }
        }
    }
}