<?php

use Core\Tools\Post;
use Core\Tools\Purify;
use Core\Tools\Crypto;
use Core\Tools\Cookie;
use Core\Tools\Notice;
use Core\Main\User;

if (defined('access')) {
    $account = Post::Get('account', '_var');
    $password = Post::Get('password', '_var');
    $repassword = Post::Get('re-password', '_var');
    $email = Post::Get('email', '_email');

    if ($account && $password && $repassword && $email) {
        if ($config->Get('NYX')['regLimit'] <= $sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE [phon_numb]=?", [$user->IP()])) {
            print Notice::Request($lang->Phrase('register', 'ip-limit', ['{limit}' => $config->Get('NYX')['regLimit']]));
        } else if (strlen($account) < 4 || strlen($account) > 10 || strlen($password) < 4 || strlen($password) > 10) {
            print Notice::Request($lang->Phrase('register', 'fields-length'));
        } else if ($password !== $repassword) {
            print Notice::Request($lang->Phrase('register', 'passwords-dont-match'));
        } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            print Notice::Request($lang->Phrase('register', 'invalid-email'));
        } else {
            if ($sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE upper(memb___id)=?", [strtoupper($account)]) != 0) {
                print Notice::Request($lang->Phrase('register', 'account-taken'));
            } else if ($sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE upper(mail_addr)=?", [strtoupper($email)]) != 0) {
                print Notice::Request($lang->Phrase('register', 'email-taken'));
            } else {
                $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
                $code = empty($ipdat->geoplugin_countryCode) ? 'unknown' : Purify::_var(strtolower($ipdat->geoplugin_countryCode));
                $sql->Query("INSERT INTO [MEMB_INFO] (memb___id,memb__pwd,memb_name,sno__numb,phon_numb,mail_addr,fpas_ques,fpas_answ,bloc_code,ctl1_code) VALUES(?,?,?,?,?,?,?,?,?,?)", [$account, $password, time(), $code, $user->IP(), $email, 'question', 'answer', '0', '0']);
                $sql->Query("INSERT INTO [NYX_RESOURCES] (account) VALUES (?)", [$account]);
                $sql->Query("INSERT INTO [warehouse] (AccountID,Items,MultiVault) VALUES (?,CONVERT(varbinary(1200), '" . str_repeat('f', 2400) . "', 2), 1)", [$account]);

                Cookie::Create('username', $account, 360);
                Cookie::Create('password', Crypto::Encrypt($password), 360);

                $user = new User;
                $user->Log([
                    'account' => $account,
                    'module' => 'register',
                    'message' => "<b>{$account}</b> has joined us <b>" . date('Y/m/d H:i:s') . "</b>"
                ]);

                print Notice::Request($lang->Phrase('register', 'success') . "<script>quickLoad('user-area');</script>", 'success');
            }
        }
    } else {
        print Notice::Request($lang->Phrase('register', 'fields-empty'));
    }
}