<?php

use Core\Tools\Cookie;
use Core\Tools\Post;
use Core\Tools\JSON;
use Core\Tools\Notice;
use Core\Main\User;

if (defined('access')) {
    $user = new User;
    $account = Cookie::Get('username');

    $character = Post::Get('character', '_post', 10);

    if ($user->Access()) {
        if ($character) {
            $fetch = $sql->Fetch("SELECT [Name] FROM [Character] WHERE [AccountID]=? AND [Name]=?", [$account, $character]);
            if (!empty($fetch)) {
                $check = $sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE [memb___id]=? AND [mainCharacter]=?", [$account, $fetch->Name]);
                if ($check != 1) {
                    $sql->Query("UPDATE [MEMB_INFO] SET [mainCharacter]=? WHERE [memb___id]=?", [$fetch->Name, $account]);
                    print Notice::Request($lang->Phrase('character-main', 'success', ['{char}' => $fetch->Name]) . "<script>loader(location.pathname);</script>", 'success');
                } else {
                    print Notice::Request($lang->Phrase('character-main', 'already', ['{char}' => $fetch->Name]));
                }
            } else {
                print Notice::Request($lang->Phrase('character-main', 'error'));
            }
        }
    } else {
        print Notice::Request($lang->Phrase('request', 'not-logged'));
    }
}