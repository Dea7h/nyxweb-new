<?php

use Core\Tools\Cookie;
use Core\Tools\Post;
use Core\Tools\JSON;
use Core\Tools\Config;
use Core\Tools\Notice;
use Core\Main\User;
use Core\Main\Item\Count;
use Core\Main\Item\getFreeSlot;
use Core\Main\Item\Decode;
use Core\Main\Item\Generate;

if (defined('access')) {
    $user = new User;
    $count = new Count;
    $getSlot = new getFreeSlot;
    $hexDecode = new Decode;
    $generate = new Generate;
    $itemsConfig = Config::Get('Items');
    $config = Config::Get('Resources');
    $account = Cookie::Get('username');

    $ware = $sql->Fetch("SELECT [Items] FROM [warehouse] WHERE [AccountID]=?", [$account]);

    $data = JSON::Get(Post::GetRaw('data'));

    if ($user->Access()) {
        if ($sql->Count("SELECT COUNT(*) FROM [MEMB_STAT] WHERE [ConnectStat]='1' AND [memb___id]=?", [$account]) != 1) {
            if (isset($data['deposit'])) {
                //Deposit from warehouse to web bank
                if (isset($config[$data['deposit']])) {
                    $res = $config[$data['deposit']];
                    $count = $count->Item(bin2hex($ware->Items), $res['props']);

                    if ($count['count'] > 0) {
                        $sql->Query("UPDATE [warehouse] SET [Items]=CONVERT(varbinary(1200), '{$count['hex']}', 2) WHERE [AccountID]=?", [$account]);
                        $sql->Query("UPDATE [NYX_RESOURCES] SET [{$data['deposit']}]=[{$data['deposit']}]+{$count['count']} WHERE [account]=?", [$account]);
                        print Notice::Request($lang->Phrase('account-resources', 'success', ['{count}' => $count['count'], '{resource}' => $res['name']]) . "<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
                    } else {
                        print Notice::Request($lang->Phrase('account-resources', 'not-found', ['{resource}' => $res['name']]));
                    }
                }
            } elseif (isset($data['withdraw']) && isset($data['amount']) && ctype_digit($data['amount']) && $data['amount'] > 0) {
                //Withdraw resources
                if (isset($config[$data['withdraw']])) {
                    $fetch = $sql->Fetch("SELECT * FROM [NYX_RESOURCES] WHERE [account]=?", [$account]);
                    $res = $config[$data['withdraw']];
                    if (isset($fetch->{$data['withdraw']}) && $fetch->{$data['withdraw']} >= $data['amount']) {
                        $count = $data['amount'];
                        $count2 = $data['amount'];
                        $items = bin2hex($ware->Items);
                        $space = true;

                        if ($data['withdraw'] == 'bless' || $data['withdraw'] == 'soul') {
                            if ($count >= 30) {
                                for ($i = 0; $i < floor($count2 / 30); $i++) {
                                    $props = $res['props'][3];
                                    $hex = $generate->Hex($props);
                                    $decode = $hexDecode->hexDecode($hex);
                                    $slot = $getSlot->Warehouse($items, $decode->itemInfo['x'], $decode->itemInfo['y']);

                                    if ($slot) {
                                        $items = substr_replace($items, $hex, $slot * $itemsConfig['hexLength'], $itemsConfig['hexLength']);
                                        $count -= 30;
                                    } else {
                                        $space = false;
                                        break;
                                    }
                                }
                            }

                            if ($count >= 20 && $space) {
                                $props = $res['props'][2];
                                $hex = $generate->Hex($props);
                                $decode = $hexDecode->hexDecode($hex);
                                $slot = $getSlot->Warehouse($items, $decode->itemInfo['x'], $decode->itemInfo['y']);

                                if ($slot) {
                                    $items = substr_replace($items, $hex, $slot * $itemsConfig['hexLength'], $itemsConfig['hexLength']);
                                    $count -= 20;
                                } else {
                                    $space = false;
                                }
                            }

                            if ($count >= 10 && $space) {
                                $props = $res['props'][1];
                                $hex = $generate->Hex($props);
                                $decode = $hexDecode->hexDecode($hex);
                                $slot = $getSlot->Warehouse($items, $decode->itemInfo['x'], $decode->itemInfo['y']);

                                if ($slot) {
                                    $items = substr_replace($items, $hex, $slot * $itemsConfig['hexLength'], $itemsConfig['hexLength']);
                                    $count -= 10;
                                } else {
                                    $space = false;
                                }
                            }
                        }


                        if ($count > 0 && $space) {
                            $count2 = $count;
                            $props = $res['props'][0];
                            for ($i = 0; $i < $count2; $i++) {
                                $hex = $generate->Hex($props);
                                $decode = $hexDecode->hexDecode($hex);
                                $slot = $getSlot->Warehouse($items, $decode->itemInfo['x'], $decode->itemInfo['y']);

                                if ($slot !== false) {
                                    $items = substr_replace($items, $hex, $slot * $itemsConfig['hexLength'], $itemsConfig['hexLength']);
                                    $count -= 1;
                                } else {
                                    break;
                                }
                            }
                        }

                        if ($count == $data['amount']) {
                            print Notice::Request($lang->Phrase('account-resources', 'no-space'));
                        } else {
                            $sql->Query("UPDATE [NYX_RESOURCES] SET [{$data['withdraw']}]=[{$data['withdraw']}]-" . ($data['amount'] - $count) . " WHERE [account]=?", [$account]);
                            $sql->Query("UPDATE [warehouse] SET [Items]=CONVERT(varbinary(1200), '{$items}', 2) WHERE [AccountID]=?", [$account]);

                            if ($count != 0) {
                                print Notice::Request($lang->Phrase('account-resources', 'success-not-all', ['{count}' => $data['amount'] - $count, '{count2}' => $data['amount'], '{resource}' => $res['name']]) . "<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
                            } else {
                                print Notice::Request($lang->Phrase('account-resources', 'success-all', ['{count}' => $data['amount'], '{resource}' => $res['name']]) . "<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
                            }
                        }
                    } else {
                        print Notice::Request($lang->Phrase('account-resources', 'not-enough', ['{resource}' => $res['name']]));
                    }
                }
            } else if (isset($data['depositZen']) && isset($data['character']) && ctype_digit($data['character']) && isset($data['zen']) && ctype_digit($data['zen'])) {
                $charID = strlen($data['character']) < 6 ? $data['character'] : 0;

                if ($data['zen'] <= 2000000000) {
                    $char = $sql->Fetch("SELECT [Money] FROM [Character] WHERE [id]=? AND [AccountID]=?", [$data['character'], $account]);

                    if (!empty($char)) {
                        if ($char->Money >= $data['zen']) {
                            $sql->Query("UPDATE [Character] SET [Money]=[Money]-{$data['zen']} WHERE [id]=? AND [AccountID]=?", [$data['character'], $account]);
                            $sql->Query("UPDATE [NYX_RESOURCES] SET [zen]=[zen]+{$data['zen']} WHERE [account]=?", [$account]);

                            print Notice::Request($lang->Phrase('account-resources', 'success-deposit-zen', ['{count}' => number_format($data['zen'])]) . "<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
                        } else {
                            print Notice::Request($lang->Phrase('account-resources', 'deposit-not-enough'));
                        }
                    }
                } else {
                    print Notice::Request($lang->Phrase('account-resources', 'deposit-big'));
                }
            } else if (isset($data['withdrawZen']) && isset($data['character']) && ctype_digit($data['character']) && isset($data['zen']) && ctype_digit($data['zen'])) {
                $charID = strlen($data['character']) < 6 ? $data['character'] : 0;

                if ($data['zen'] <= 2000000000) {
                    $char = $sql->Fetch("SELECT [Money] FROM [Character] WHERE [id]=? AND [AccountID]=?", [$data['character'], $account]);
                    $res = $sql->Fetch("SELECT [zen] FROM [NYX_RESOURCES] WHERE [account]=?", [$account]);

                    if (!empty($char)) {
                        if ($res->zen >= $data['zen']) {
                            if ($char->Money + $data['zen'] <= 2000000000) {
                                $sql->Query("UPDATE [Character] SET [Money]=[Money]+{$data['zen']} WHERE [id]=? AND [AccountID]=?", [$data['character'], $account]);
                                $sql->Query("UPDATE [NYX_RESOURCES] SET [zen]=[zen]-{$data['zen']} WHERE [account]=?", [$account]);

                                print Notice::Request($lang->Phrase('account-resources', 'success-withdraw-zen', ['{count}' => number_format($data['zen'])]) . "<script>loader(location.pathname); quickLoad('user-area');</script>", 'success');
                            } else {
                                print Notice::Request($lang->Phrase('account-resources', 'withdraw-too-much'));
                            }
                        } else {
                            print Notice::Request($lang->Phrase('account-resources', 'withdraw-not-enough'));
                        }
                    }
                } else {
                    print Notice::Request($lang->Phrase('account-resources', 'withdraw-big'));
                }
            } else {
                print Notice::Request($lang->Phrase('account-resources', 'only-digits'));
            }
        } else {
            print Notice::Request($lang->Phrase('request', 'online'));
        }
    } else {
        print Notice::Request($lang->Phrase('request', 'not-logged'));
    }
}