<?php

use Core\Tools\Post;
use Core\Tools\Crypto;
use Core\Tools\Cookie;
use Core\Tools\Notice;
use Core\Main\User;

if (defined('access')) {
    $user = new User;

    $account = Cookie::Get('username');
    $password = Post::Get('password', '_var');
    $newpassword = Post::Get('newpassword', '_var');
    $email = Post::Get('email', '_email');

    if ($user->Access()) {
        if ($account && $password && $email && $newpassword) {
            if (strlen($newpassword) < 4 || strlen($newpassword) > 10 || strlen($password) < 4 || strlen($password) > 10 || strlen($email) < 5 || strlen($email) > 35) {
                print Notice::Request($lang->Phrase('account-password', 'fields-length'));
            } else {
                if ($sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE [memb___id]=? AND [memb__pwd]=? AND [mail_addr]=?", [$account, $password, $email]) != 1) {
                    print Notice::Request($lang->Phrase('account-password', 'no-match'));
                } else {
                    $sql->Query("UPDATE [MEMB_INFO] SET [memb__pwd]=? WHERE [memb___id]=? AND [memb__pwd]=?", [$newpassword, $account, $password]);
                    Cookie::Create('username', $account, 360);
                    Cookie::Create('password', Crypto::Encrypt($newpassword), 360);

                    $user->Log([
                        'account' => $account,
                        'module' => 'password',
                        'message' => 'Changed password successfully'
                    ]);

                    print Notice::Request($lang->Phrase('account-password', 'success') . ", <b>{$account}</b>!", 'success');
                }
            }
        } else {
            print Notice::Request($lang->Phrase('request', 'fields-empty'));
        }
    } else {
        print Notice::Request($lang->Phrase('request', 'not-logged'));
    }
}