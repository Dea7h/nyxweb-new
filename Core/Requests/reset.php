<?php

use Core\Main\User;

if (defined('access')) {
    $user = new User;

    if ($user->Access()) {

    } else {
        print $lang->Phrase('request', 'not-logged');
    }
}