<?php

use Core\Tools\Cookie;
use Core\Tools\Post;
use Core\Tools\Purify;
use Core\Tools\Config;
use Core\Tools\Notice;
use Core\Main\User;

if (defined('access')) {
    $user = new User;
    $config = Config::Get('News');
    $account = Cookie::Get('username');

    if ($user->Access()) {
        $data = Purify::_mass(json_decode(isset($_POST['data']) ? $_POST['data'] : false));

        if (isset($data->comment) && ctype_digit($data->comment) && isset($data->vote) && ($data->vote == 'up' || $data->vote == 'down')) {
            if ($sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS] WHERE [id]=?", [$data->comment]) != 1) {
                print Notice::Request($lang->Phrase('news', 'invalid-id'));
            } else if ($sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS_VOTES] WHERE [id]=? AND [account]=?", [$data->comment, $account]) > 0) {
                print Notice::Request($lang->Phrase('news', 'voted-already'));
            } else {
                $sql->Query("INSERT INTO [NYX_NEWS_COMMENTS_VOTES] VALUES (?,?,?,?,?)", [$data->comment, $account, $data->vote, time(), $user->IP()]);
                $sql->Query("UPDATE [NYX_NEWS_COMMENTS] SET [rate]=[rate]" . ($data->vote == 'up' ? '+1' : '-1') . " WHERE [id]=?", [$data->comment]);
                print Notice::Request($lang->Phrase('news', 'vote-success') . "<script>loader(location.pathname);</script>", 'success');
            }
        } else if (Post::Exists('message') && Post::Exists('news') && !empty(Post::Get('message'))) {
            $message = htmlspecialchars(Post::Get('message', '_raw', 500));
            $news = Post::Get('news', '_num');
            $reply = Post::Get('reply', '_num');

            if (strlen($message) > 500 || strlen($message) < 1) {
                print Notice::Request($lang->Phrase('news', 'comment-message'));
            } else if ($config['allComments'] <= $sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS] WHERE [article_id]=?", [$news])) {
                print Notice::Request($lang->Phrase('news', 'max-comments', ['{amount}' => $config['allComments']]));
            } else if ($config['commentsPerUser'] <= $sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS] WHERE [article_id]=? AND [author]=?", [$news, $account])) {
                print Notice::Request($lang->Phrase('news', 'comments-per-user', ['{amount}' => $config['commentsPerUser']]));
            } else {
                if ($sql->Count("SELECT COUNT(*) FROM [NYX_NEWS] WHERE [id]=? AND [comments]='1'", [$news]) != 1) {
                    print Notice::Request($lang->Phrase('news', 'comments-disabled'));
                } else {
                    if($reply) {
                        if($sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS] WHERE [id]=?", [$reply]) != 1) {
                            print Notice::Request($lang->Phrase('news', 'wrong-reply'));
                        } else {
                            $sql->Query("INSERT INTO [NYX_NEWS_COMMENTS] (article_id,message,author,time,reply) VALUES (?,?,?,?,?)", [$news, $message, $account, time(), $reply]);
                            print Notice::Request($lang->Phrase('news', 'comment-success') . "<script>loader(location.pathname);</script>", 'success');
                        }
                    } else {
                        $sql->Query("INSERT INTO [NYX_NEWS_COMMENTS] (article_id,message,author,time,reply) VALUES (?,?,?,?,?)", [$news, $message, $account, time(), '']);
                        print Notice::Request($lang->Phrase('news', 'comment-success') . "<script>loader(location.pathname);</script>", 'success');
                    }
                }
            }
        } else {
            print Notice::Request($lang->Phrase('news', 'empty-fields'));
        }
    } else {
        print Notice::Request($lang->Phrase('news', 'not-logged'));
    }

}