<?php

namespace Core\User;

use Core\Tools\Config;
use Core\Tools\Cookie;
use Core\Tools\SQL;
use Core\Main\Item\Render;

class Storage {

    protected $_sql, $_user, $_config, $_fetch, $_render;

    public function __construct() {
        $this->_sql = new SQL;
        $this->_user = Cookie::Get('username');
        $this->_config = new Config;
        $this->_render = new Render;
    }

    public function Render($data = []) {
        $config = $this->_config::Get('Items');
        $this->_user = isset($data['account']) ? $data['account'] : $this->_user;
        $this->_fetch = $this->_sql->Fetch("SELECT [{$data['items_column']}] FROM [{$data['table']}] WHERE [{$data['user_column']}]=?", [$this->_user]);

        $hex = $data['real_size'] ? bin2hex($this->_fetch->{$data['items_column']}) : $this->_fetch->{$data['items_column']};

        $output = '';
        for ($i = 0; $i < strlen($hex) / $config['hexLength']; $i++) {
            $item = substr($hex, $i * $config['hexLength'], $config['hexLength']);
            if (strtolower($item) != str_repeat('f', $config['hexLength'])) {
                $render = $this->_render->Item($item);
                if ($render !== false) {
                    $row = floor($i / $data['x']);
                    $col = $i - $row * $data['x'];
                    $width = ($data['real_size'] ? $render->properties->itemInfo['x'] * 26 : 26) - 3;
                    $height = ($data['real_size'] ? $render->properties->itemInfo['y'] * 26 : 26) - 3;

                    $output .= "<div id='{$data['side']}-{$i}' data-item='{$render->properties->itemInfo['x']}.{$render->properties->itemInfo['y']}' onclick=\"request($(this).parent().attr('target'), {item: '{$data['side']}-{$i}'})\" class='draggable-item' draggable='true' style=\"background: rgba(0,0,0,0.4) url('{$render->image}') no-repeat center center/contain; left: " . $col * 26 . "px; top: " . $row * 26 . "px; width: {$width}px; height: {$height}px;\" title=\"{$render->options}\"></div>";
                }
            }
        }

        return $output;
    }

}
