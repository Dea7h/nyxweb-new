<?php

namespace Core\Tools;

class Post extends Purify {

    static function Get($key, $purify = '_post', $length = 50) {
        return self::Exists($key) ? Purify::{$purify}($_POST[$key], $length) : false;
    }

    static function Exists($key) {
        return isset($_POST[$key]) ? true : false;
    }

    static function GetRaw($key) {
        return self::Exists($key) ? $_POST[$key] : false;
    }

}
