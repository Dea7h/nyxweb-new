<?php

namespace Core\Tools;

use PDO;

class SQL extends Config {

    private $_conn, $_config, $_query;

    public function __construct() {
        if (empty($this->_conn)) {
            $this->_config = Config::Get('Server');

            try {
                $this->_conn = new PDO("sqlsrv:Server={$this->_config->server};Database={$this->_config->database}", $this->_config->user, $this->_config->password, ($this->_config->errors ? [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION] : []));
            } catch (\PDOException $e) {
                exit('<b>SQL Error:</b> ' . $e->getMessage());
            }
        }
    }

    public function Query($query, $data = []) {
        $this->_query = $this->_conn->prepare($query);
        if (!$this->_query->execute($data)) {
            exit('<b>SQL Error:</b> A Fetch query has failed to execute.');
        }

        return $this->_query;
    }

    public function Fetch($query, $data = []) {
        $this->_query = $this->_conn->prepare($query);
        if (!$this->_query->execute($data)) {
            exit('<b>SQL Error:</b> A Fetch query has failed to execute.');
        }
        return $this->_query->fetch(PDO::FETCH_OBJ);
    }

    public function FetchAll($query, $data = []) {
        $this->_query = $this->_conn->prepare($query);
        if (!$this->_query->execute($data)) {
            exit('<b>SQL Error:</b> A FetchAll query has failed to execute.');
        }
        return $this->_query->fetchAll(PDO::FETCH_OBJ);
    }

    public function Count($query, $data = []) {
        $this->_query = $this->_conn->prepare($query);
        if (!$this->_query->execute($data)) {
            exit('<b>SQL Error:</b> A Count query has failed to execute.');
        }
        return $this->_query->fetchColumn();
    }

}
