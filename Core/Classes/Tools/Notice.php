<?php

namespace Core\Tools;

class Notice {

    public static function Request($message, $type = 'error') {
        return "<div class='notice {$type}'>{$message}</div>";
    }

    public static function Page($message, $type = 'error') {
        return "<div class='notice {$type}'>{$message}</div>";
    }

}
