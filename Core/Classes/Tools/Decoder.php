<?php

namespace Core\Tools;

class Decoder {

    public function __construct() {
        
    }

    public function resourceName($key) {
        switch ($key) {
            case 'chaos':   return 'Jewel of Chaos';
            case 'bless':   return 'Jewel of Bless';
            case 'soul':    return 'Jewel of Soul';
            case 'life':    return 'Jewel of Life';
            case 'creation':return 'Jewel of Creation';
            case 'apple':   return 'Apple';
            case 'box1':    return 'Box of Kundun +1';
            case 'box2':    return 'Box of Kundun +2';
            case 'box3':    return 'Box of Kundun +3';
            case 'box4':    return 'Box of Kundun +4';
            case 'box5':    return 'Box of Kundun +5';
            case 'stone':   return 'Stone';
            case 'rena':    return 'Rena';
            case 'credits': return 'Credits';
            case 'zen':     return 'Zen';
            default:        return 'unknown';
        }
    }

    public static function characterClass($key) {
        switch ($key) {
            case 0:
                return (Object) [
                            'img' => 'sm',
                            'full' => 'Dark Wizard',
                            'mini' => 'DW'
                ];
            case 1:
                return (Object) [
                            'img' => 'sm',
                            'full' => 'Soul Master',
                            'mini' => 'SM'
                ];
            case 16:
                return (Object) [
                            'img' => 'bk',
                            'full' => 'Dark Knight',
                            'mini' => 'DK'
                ];
            case 17:
                return (Object) [
                            'img' => 'bk',
                            'full' => 'Blade Knight',
                            'mini' => 'BK'
                ];
            case 32:
                return (Object) [
                            'img' => 'me',
                            'full' => 'Fairy Elf',
                            'mini' => 'ELF'
                ];
            case 33:
                return (Object) [
                            'img' => 'me',
                            'full' => 'Muse Elf',
                            'mini' => 'ME'
                ];
            case 48:
                return (Object) [
                            'img' => 'mg',
                            'full' => 'Magic Gladiator',
                            'mini' => 'MG'
                ];
            case 64:
                return (Object) [
                            'img' => 'dl',
                            'full' => 'Dark Lord',
                            'mini' => 'DL'
                ];
            default:
                return (Object) [
                            'img' => 'unknown',
                            'full' => 'Unknown',
                            'mini' => 'X'
                ];
        }
    }

    public static function decodeMap($key) {
        switch ($key) {
            case 0:
                return "<font color='#e3e48b'>Lorencia</font>";
            case 1:
                return "<font color='#cccccc'>Dungeon</font>";
            case 2:
                return "<font color='#66ccd8'>Davias</font>";
            case 3:
                return "<font color='#4e9c46'>Noria</font>";
            case 4:
                return "Lost Tower";
            case 5:
                return "Exile";
            case 6:
                return "Stadium";
            case 7:
                return "Atlans";
            case 8:
                return "Tarkan";
            case 9:
            case 32:
                return "Devil Square";
            case 10:
                return "Icarus";
            case 11:
                return "Blood Castle 1";
            case 12:
                return "Blood Castle 2";
            case 13:
                return "Blood Castle 3";
            case 14:
                return "Blood Castle 4";
            case 15:
                return "Blood Castle 5";
            case 16:
                return "Blood Castle 6";
            case 17:
                return "Blood Castle 7";
            case 18:
                return "Chaos Castle 1";
            case 19:
                return "Chaos Castle 2";
            case 20:
                return "Chaos Castle 3";
            case 21:
                return "Chaos Castle 4";
            case 22:
                return "Chaos Castle 5";
            case 23:
                return "Chaos Castle 6";
            case 24:
                return "Kalima 1";
            case 25:
                return "Kalima 2";
            case 26:
                return "Kalima 3";
            case 27:
                return "Kalima 4";
            case 28:
                return "Kalima 5";
            case 29:
                return "Kalima 6";
            case 36:
                return "Kalima 7";
            case 30:
                return "Valley Of Loren";
            case 31:
                return "Land of Trials";
            case 33:
                return "Aida";
            case 34:
                return "Crywolf Fortress";
            case 37:
                return "Kantru 1";
            case 38:
                return "Kantru 2";
            case 39:
                return "Kantru 3";
            case 40:
                return "Silent Map";
            case 41:
                return "Barracks of Balgass";
            case 42:
                return "Balgass Refuge";
            case 45:
                return "Illusion Temple 1";
            case 46:
                return "Illusion Temple 2";
            case 47:
                return "Illusion Temple 3";
            case 48:
                return "Illusion Temple 4";
            case 49:
                return "Illusion Temple 5";
            case 50:
                return "Illusion Temple 6";
            case 51:
                return "Elbeland";
            case 52:
                return "Blood Castle 8";
            case 53:
                return "Chaos Castle 7";
            case 55:
                return "Valley Of Loren";
            case 56:
                return "Swamp of Calmness";
            case 57:
                return "Raklion";
            case 58:
                return "Raklion Boss";
            case 62:
                return "Villages Santa";
            case 63:
                return "Vulcanus";
            case 64:
                return "Duel Arena";
            case 65:
            case 66:
            case 67:
            case 68:
                return "Doppelganger";
            case 69:
            case 70:
            case 71:
            case 72:
                return "Empire Fortress";
            default:
                return "Unknown";
        }
    }

//
//    public function Inventory($hex = '') {
//        $item = new Item();
//        $hex = empty($hex) ? str_repeat('F', 1200) : bin2hex($hex);
//        $hex = $item->Inventory($hex);
//
//        return [
//            'helm' => $item->Decode($hex['helm']),
//            'wings' => $item->Decode($hex['wings']),
//            'leftWeapon' => $item->Decode($hex['leftWeapon']),
//            'rightWeapon' => $item->Decode($hex['rightWeapon']),
//            'armor' => $item->Decode($hex['armor']),
//            'gloves' => $item->Decode($hex['gloves']),
//            'pants' => $item->Decode($hex['pants']),
//            'boots' => $item->Decode($hex['boots']),
//            'pendant' => $item->Decode($hex['pendant']),
//            'leftRing' => $item->Decode($hex['leftRing']),
//            'rightRing' => $item->Decode($hex['rightRing']),
//            'pet' => $item->Decode($hex['pet'])
//        ];
//    }
}
