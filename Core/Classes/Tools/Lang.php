<?php

namespace Core\Tools;

use Core\Tools\Config;

class Lang extends Cookie {

    private $_phrases, $_message;

    public function __construct() {
        if (empty($this->_phrases)) {
            $this->_phrases = include 'Core/Languages/' . (Cookie::Exists('lang') && file_exists('Core/Languages/' . Cookie::Get('lang') . '.php') ? Cookie::Get('lang') : Config::Get('Language')->default) . '.php';
        }
    }

    public function Phrase($category = 'undefined', $phrase = 'undefined', $replace = []) {
        if (isset($this->_phrases[$category][$phrase])) {
            $this->_message = $this->_phrases[$category][$phrase];
            if (!empty($replace)) {
                foreach ($replace as $find => $value) {
                    $this->_message = preg_replace("/{$find}/", $value, $this->_message);
                }
            }
            return $this->_message;
        }
        return $category . '_' . $phrase;
    }

}
