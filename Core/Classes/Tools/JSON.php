<?php

namespace Core\Tools;

class JSON extends Purify {

    public static function Get($string) {
        $json = is_object(json_decode($string)) ? json_decode($string, true) : [];

        foreach ($json as $key => $value) {
            $json[$key] = Purify::_userA($value);
        }

        return $json;
    }

}
