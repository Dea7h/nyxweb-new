<?php

namespace Core\Tools;

class Session extends Purify {

    static function Get($key) {
        return self::Exists($key) ? Purify::_post($_SESSION[$key]) : false;
    }

    static function Exists($key) {
        return isset($_SESSION[$key]) ? true : false;
    }
    
    static function Create($key, $value) {
        $_SESSION[$key] = $value;
    }

}
