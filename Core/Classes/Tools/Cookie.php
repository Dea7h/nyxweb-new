<?php

namespace Core\Tools;

class Cookie extends Purify {

    public static function Get($key, $purify = '_var') {
        return self::Exists($key) ? Purify::{$purify}($_COOKIE[$key]) : false;
    }

    public static function Exists($key) {
        return isset($_COOKIE[$key]) ? true : false;
    }

    public static function Create($key, $value, $days = 30) {
        setcookie($key, $value, time() + 86400 * $days, '/');
    }

    public static function Destroy($key) {
        setcookie($key, '', -1, '/');
    }

}
