<?php

namespace Core\Tools;

use Core\Main\User;

class RequestHandler
{

    protected $_sql, $_lang, $_config;

    public function __construct()
    {
        $this->_sql = new SQL;
        $this->_lang = new Lang;
        $this->_config = Config::Get('NYX');
    }

    public function PageCheck()
    {
        $time = round(microtime(true) * 1000);
        $user = new User;
        $ip = $user->IP();


        $fetch = $this->_sql->Fetch("SELECT [time] FROM [NYX_REQUESTS] WHERE [ip]=? AND [type]=?", [$ip, 'page']);
        $this->_sql->Query("DELETE FROM [NYX_REQUESTS] WHERE [ip]=? AND [type]=?", [$ip, 'page']);
        $this->_sql->Query("INSERT INTO [NYX_REQUESTS] VALUES (?,?,?)", [$ip, $time, 'page']);
        if (isset($fetch->time) && ($time - $fetch->time) < $this->_config['requestTime']) {
            print Notice::Page($this->_lang->Phrase('request', 'too-fast', ['{time}' => $this->_config['requestTime'] / 1000]));
            exit;
        }
    }

    public function RequestStart()
    {
        $fetch = $this->_sql->Fetch("SELECT [requestInAction] FROM [MEMB_INFO] WHERE [memb___id]=?", [Cookie::Get('username')]);

        if ($fetch->requestInAction == 1) {
            print Notice::Request($this->_lang->Phrase('request', 'too-fast'));
            exit;
        } else {
            $this->_sql->Query("UPDATE [MEMB_INFO] SET [requestInAction]='1' WHERE [memb___id]=?", [Cookie::Get('username')]);
        }
    }

    public function RequestEnd()
    {
        $this->_sql->Query("UPDATE [MEMB_INFO] SET [requestInAction]='0' WHERE [memb___id]=?", [Cookie::Get('username')]);
    }

}
