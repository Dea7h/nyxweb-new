<?php

namespace Core\Tools;

class Purify {

    public static function _var($var, $length = 30) {
        return preg_match('/^[a-z0-9]*$/i', $var) ? strlen($var) <= $length ? $var : false : false;
    }

    public static function _post($var, $length = 30) {
        return preg_match('/^[a-z0-9., !@|:;(*)\n<?_\/-]*$/i', $var) ? strlen($var) <= $length ? $var : false : false;
    }

    public static function _num($int, $length = 10) {
        return preg_match('/^[0-9]*$/', $int) ? strlen($int) <= $length ? $int : false : false;
    }

    public static function _email($var, $length = 35) {
        return preg_match('/^[a-z0-9_.-@]*$/i', $var) ? strlen($var) <= $length ? $var : false : false;
    }

    public static function _userA($var, $length = 35) {
        return preg_match('/^[a-z0-9-]*$/i', $var) ? strlen($var) <= $length ? $var : false : false;
    }

    public static function _raw($var) {
        return $var;
    }

    public static function _mass($data = [], $purify = '_var') {
        if (is_array($data) || is_object($data)) {
            $result = [];
            foreach ($data as $key => $value) {
                $result[$key] = self::{$purify}($value);
            }

            return is_array($data) ? $result : (Object) $result;
        }

        return (Object) [];
    }

    public static function _custom($var, $pattern = 'a-z0-9', $length = 10) {
        return preg_match('/^[' . $pattern . ']*$/i', $var) ? strlen($var) <= $length ? $var : false : false;
    }

}
