<?php

namespace Core\Tools;

class Crypto extends Config {

    public static function Encrypt($value) {
        $config = Config::Get('NYX');
        $iv = random_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        return base64_encode(openssl_encrypt($value, 'aes-256-cbc', $config['cryptoKEY'], 0, $iv)) . $config['cryptoSEP'] . bin2hex($iv);
    }

    public static function Decrypt($value) {
        $config = Config::Get('NYX');
        list($value, $iv) = explode($config['cryptoSEP'], $value, 2);
        return openssl_decrypt(base64_decode($value), 'aes-256-cbc', $config['cryptoKEY'], 0, hex2bin($iv));
    }

}
