<?php

namespace Core\Tools;

class Config {

    private static $_content = [];

    public static function Get($key) {
        if (!isset(self::$_content[$key])) {
            if (is_file('Core/Configs/' . $key . '.php')) {
                self::$_content[$key] = include 'Core/Configs/' . $key . '.php';
            } else {
                exit('<b>Config Error:</b> Config file <b>' . $key . '</b> could not be found.');
            }
        }

        return self::$_content[$key];
    }

}
