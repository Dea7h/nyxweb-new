<?php

namespace Core\Main;

use Core\Tools\Cookie;
use Core\Tools\Crypto;
use Core\Tools\Purify;
use Core\Tools\SQL;

class User {

    protected $_sql;

    public function __construct() {
        $this->_sql = new SQL;
    }

    public function Access() {
        if (Cookie::Exists('username') && Cookie::Exists('password')) {
            $password = Purify::_var(Crypto::Decrypt(Cookie::Get('password', '_raw')));
            $check = $this->_sql->Count("SELECT COUNT(*) FROM [MEMB_INFO] WHERE [memb___id]=? AND [memb__pwd]=?", [Cookie::Get('username'), $password]);
            if ($check == 1) {
                return true;
            }
        }
        return false;
    }

    public function Log($data = ['module' => 'unknown']) {
        $account = isset($data['account']) ? $data['account'] : Cookie::Get('username');

        //Saving log in the DataBase
        $this->_sql->Query("INSERT INTO [NYX_LOGS] (account, module, message, time, ip, hidden_info) VALUES (?,?,?,?,?,?)", [$account, $data['module'], $data['message'], time(), $this->IP(), isset($data['hidden_info']) ? $data['hidden_info'] : '']);

        //Saving log in .txt file
        $content = '[ ' . date('d/m/Y H:i:s') . ' ] :: [ ' . $this->IP() . ' ] :: [ ' . $data['module'] . ' ] - [ ' . $data['message'] . ' ] - [ ' . (isset($data['hidden_info']) ? $data['hidden_info'] : '-') . ' ]' . "\n";
        $handle = fopen("Core/Logs/{$account}.txt", 'a');
        fwrite($handle, $content);
        fclose($handle);
    }

    public function IP() {
        if (!empty($_SERVER['HTTP_X_REAL_IP'])) {
            return $_SERVER['HTTP_X_REAL_IP'];
        } else if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            return $_SERVER['HTTP_CLIENT_IP'];
        } else if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

}
