<?php

namespace Core\Main;

use Core\Tools\SQL;
use Core\Tools\Decoder;
use Core\Tools\Config;
use PDO;

class Item {

    private $_sql, $_config;

    public function __construct() {
        $this->_sql = new SQL;
        $this->_config = new Config;
    }

    public function CategoryDecoder($category) {
        switch ($category) {
            case 0: return 'Swords';
            case 1: return 'Axes';
            case 2: return 'Maces';
            case 3: return 'Spears';
            case 4: return 'Bows';
            case 5: return 'Staffs';
            case 6: return 'Shields';
            case 7: return 'Helmets';
            case 8: return 'Armors';
            case 9: return 'Pants';
            case 10: return 'Gloves';
            case 11: return 'Boots';
            case 12: return 'Items 1';
            case 13: return 'Items 2';
            case 14: return 'Items 3';
            case 15: return 'Scrolls';
            default: return 'Unknown';
        }
    }

    public function countItem($hex, $props = []) {
        $config = $this->_config->Get('Items');
        $count = 0;
        $items = bin2hex($hex);

        for ($i = 0; $i < (strlen($items) / $config['length']); $i++) {
            $item = substr($items, $i * $config['length'], $config['length']);
            $decode = $this->hexDecode($item);
            if ($decode !== false) {
                $check = true;

                if (count($props) > 1) {
                    foreach ($props as $id => $prop) {
                        $check = true;
                        $val = $prop['count'];
                        unset($prop['count']);
                        unset($prop['durability']);
                        foreach ($prop as $key => $value) {
                            if ($decode->{$key} != $value) {
                                $check = false;
                                break;
                            }
                        }

                        if ($check) {
                            $items = preg_replace("/{$item}/i", str_repeat('f', $config['length']), $items);
                            $count += $val;
                        }
                    }
                } else {
                    unset($props[0]['durability']);
                    foreach ($props[0] as $key => $value) {
                        if ($decode->{$key} != $value) {
                            $check = false;
                            break;
                        }
                    }

                    if ($check) {
                        $items = preg_replace("/{$item}/i", str_repeat('f', $config['length']), $items);
                        $count += 1;
                    }
                }
            }
        }

        return [
            'count' => $count,
            'hex' => $items
        ];
    }

//    public function addToWarehouse($warehouse, $item) {
//        $hex_length = $this->_config->Get('Items')['length'];
//        $decode = $this->hexDecode($item);
//        $slot = $this->getFreeSlot($warehouse, $decode['x'], $decode['y']);
//        if ($slot !== false) {
//            return substr_replace($warehouse, $item, ($slot * $hex_length), $hex_length);
//        }
//        return false;
//    }

    public function getFreeSlot($hex, $_x, $_y) {
        $hex_length = $this->_config->Get('Items')['length'];
        $slots_count = 120;
        $slots = str_repeat(0, $slots_count);   //Creating all slots as 0's (empty)

        for ($i = 0; $i < $slots_count; $i++) { //Setting all taken slots to 1
            $item = substr($hex, $i * $hex_length, $hex_length);
            if (strtolower($item) != str_repeat('f', $hex_length)) {
                $decode = $this->hexDecode($item);
                $slots = substr_replace($slots, str_repeat(1, $decode->item['x']), $i, $decode->item['x']);
                if ($decode->item['y'] > 1) {
                    for ($r = 0; $r < $decode->item['y'] - 1; $r++) {
                        $slots = substr_replace($slots, str_repeat(1, $decode->item['x']), $i + 8 + $r * 8, $decode->item['x']);
                    }
                }
            }
        }

        for ($i = 0; $i < $slots_count; $i++) { //Going through $slots and checking if there are any empty slots that matches our item
            if ((ceil(($i + 1) / 8) * 8) >= ($i + $_x) && substr($slots, $i, $_x) === str_repeat(0, $_x)) { //First empty row found
                if ($_y > 1) { //If the item is bigger than 1 slot vertically (Y)
                    if ((ceil(($i + 1) / 8) + $_y - 1) <= 15) {
                        $row = 0;
                        for ($r = 0; $r < $_y - 1; $r++) {
                            if (substr($slots, $i + 8 + $r * 8, $_x) === str_repeat(0, $_x)) {
                                $row++;
                            }
                        }

                        if ($row == $_y - 1) {
                            return $i;
                        }
                    }
                } else {
                    return $i;
                }
            }
        }
        return false;
    }

    public function generateHex($prop = []) {
        if (!isset($prop['type']) || !isset($prop['id'])) {
            return false;
        }

        $item = include "Core/DB/items/{$prop['type']}.php";
        $item = $item[$prop['id']];

        //Defining base values
        $options = isset($prop['options']) && $prop['options'] !== false ? $prop['options'] : 0;
        $skill = isset($prop['skill']) && $prop['skill'] !== false && $prop['skill'] != 0 && !empty($prop['skill']) ? 128 : 0;
        $level = isset($prop['level']) && $prop['level'] !== false ? $prop['level'] : 0;
        $luck = isset($prop['luck']) && ($prop['luck'] == 1 || $prop['luck'] === true) ? 1 : 0;
        $serial = isset($prop['serial']) ? 0 : $this->_sql->Query("EXEC WZ_GetItemSerial")->fetch(PDO::FETCH_COLUMN);
        $durability = isset($prop['durability']) && $prop['durability'] !== false ? $prop['durability'] : 0;
        $ancient = isset($prop['ancient']) && $prop['ancient'] !== false && isset($item['ancient']) ? $prop['ancient'] == 1 ? 5 : 9 : 0;
        $hex = '';

        //Part 1
        $part1 = $prop['type'] * 32 + $prop['id'];
        $hex .= $part1 >= 255 ? substr(sprintf("%02X", $part1, 0), 1, 2) : sprintf("%02X", $part1, 0);

        //Part 2
        if ($options > 3) {
            $options -= 4;
            $exl = 64;
        } else {
            $exl = 0;
        }

        $part2 = $level * 8 + $options + $skill + $luck * 4;
        $hex .= sprintf("%02X", $part2 < 0 ? 0 : $part2, 0);

        //Part 3
        $hex .= sprintf("%02X", $durability, 0);

        //Part 4
        $hex .= sprintf("%08X", $serial, 0);

        //Part 5
        if (isset($prop['exo'])) {
            $exl += $prop['exo'];
        }

        if ($prop['type'] * 2 >= 16) {
            $exl += 128;
        }

        $hex .= sprintf("%02X", $exl, 0);

        //Part 6
        $hex .= sprintf("%02X", $ancient, 0);

        //Part 7
        if ($prop['type'] == 14) {
            $prop['type'] = 12;
        } elseif ($prop['type'] == 12) {
            $prop['type'] = 13;
        } elseif ($prop['type'] == 13) {
            $prop['type'] = 14;
        }

        $hex .= sprintf("%02X", $prop['type'] * 16, 0);

        return substr_replace($hex, '77', 6, 2);
    }

    public function Render($hex, $settings = []) {
        if (is_array($hex)) {
            $hex = $this->generateHex($hex);
        } else if (strtoupper($hex) == str_repeat('F', 20) || empty($hex) || strlen($hex) !== 20) {
            return false;
        }

        $decode = $this->hexDecode($hex);

        if (isset($decode->type) && file_exists("Core/DB/items/{$decode->type}.php")) {
            $item = include "Core/DB/items/{$decode->type}.php";
            if (!isset($item[$decode->id])) {
                return false;
            }
        } else {
            return false;
        }

        $exo = $decode->exo;

        //Excellent option 6
        $decode->exo = [];
        $decode->exo[5] = false;
        if ($exo >= 32) {
            $decode->exo[5] = true;
            $exo -= 32;
        }

        //Excellent option 5
        $decode->exo[4] = false;
        if ($exo >= 16) {
            $decode->exo[4] = true;
            $exo -= 16;
        }

        //Excellent option 4
        $decode->exo[3] = false;
        if ($exo >= 8) {
            $decode->exo[3] = true;
            $exo -= 8;
        }

        //Excellent option 3
        $decode->exo[2] = false;
        if ($exo >= 4) {
            $decode->exo[2] = true;
            $exo -= 4;
        }

        //Excellent option 2
        $decode->exo[1] = false;
        if ($exo >= 2) {
            $decode->exo[1] = true;
            $exo -= 2;
        }

        //Excellent option 1
        $decode->exo[0] = false;
        if ($exo >= 1) {
            $decode->exo[0] = true;
            $exo -= 1;
        }


        $special = false;
        $item = $item[$decode->id];
        if (isset($item['special'])) {
            $item = $item['special'][$decode->level];
            $special = true;
        }

        //Settings image path
        $image = "/assets/images/items/{$decode->type}/" . ($special ? $item['image'] : $decode->id . ($decode->ancient ? 'C' : '') . '.gif');

        $view = '';
        if (isset($item['exo'])) {
            $exolist = include "Core/DB/optionsList.php";
            $opts = $exolist[$item['exo']];
        }

        //Item Name
        $wings = ($decode->type == 13 && $decode->id == 30) || ($decode->type == 12 && $decode->id <= 6) ? true : false;
        if ($decode->ancient) {
            $view .= "<div style='background: #3d3cc8; color: #43c38a; text-align: center;'>{$item['ancient']} {$item['name']}" . ($decode->level > 0 ? " +" . $decode->level : '') . "</div>";
            $name = "<font style='color: #3d3cc8;'>" . $item['ancient'] . ' ' . $item['name'] . ($decode->level > 0 ? " +" . $decode->level : '') . '</font>';
        } else {
            $name = "<font style='color: #" . (in_array(true, $decode->exo) && !$wings ? '439f6b' : ($decode->level > 6 ? 'ceb948' : '96b0d3')) . ";'>{$item['name']} " . (($decode->level > 0 && !$special) ? " +" . $decode->level : '') . "</font>";
            $view .= "<div style='text-align: center'>$name</div>";
        }

        if (isset($settings['image']) && $settings['image'] !== false) {
            $view .= "<div style='text-align: center; margin: 5px 0 5px 0; height: " . 32 * $item['y'] . "px; background: url({$image}) no-repeat center center;'></div>";
        }

        //Durability
        $view .= "<div style='margin: 5px 0 5px 0; text-align: center; color: #FFFFFF;'>" . ($special && isset($item['dur']) ? $item['dur'] : "Durability [{$decode->durability}]") . "</div>";


        //Allowed to equip
        if (isset($item['class']) && $item['class'] !== false) {
            $view .= "<div style='margin-bottom: 5px; background: #a00000; color: #FFFFFF; text-align: center;'>";

            foreach ($item['class'] as $class) {
                $view .= "<div>Can be equiped by " . Decoder::characterClass($class)->full . "</div>";
            }

            $view .= "</div>";
        }

        //Adding Ancient Stamina opt
        if (isset($item['ancient']) && $decode->ancient !== false) {
            $view .= "<div style='text-align: center; color: #96b0d3;'>Increase Stamina +{$decode->ancient}</div>";
        }

        if (isset($settings['random_exo']) && $settings['random_exo'] !== false) {
            $view .= "<div style='text-align: center; color: #96b0d3;'>[ {$settings['random_exo']} ]</div>";
        } else {
            //Skill
            if ($decode->skill) {
                $view .= "<div style='text-align: center; color: #96b0d3;'><i>This item has a special skill</i></div>";
            }

            //Luck
            if ($decode->luck) {
                $view .= "<div style='text-align: center; color: #96b0d3;'>Luck (success rate of Jewel of Soul +25%)<br />Luck (critical damage rate +5%)</div>";
            }

            //Check if item is Cape of Lord
            if ($item['exo'] == 4) {
                $view .= "<div style='text-align: center; color: #96b0d3;'>Increase defensive skill +" . (15 + ($decode->level * 2)) . "<br />Increase " . (20 + ($decode->level * 2)) . "% of Damage</div>";
            }

            //Add Option
            if ($decode->options > 0) {
                $view .= "<div style='text-align: center; color: #96b0d3;'>";

                if ($decode->type != 12 && $decode->type != 13) {
                    if ($item['exo'] == 2) {
                        $view .= $opts['adds'] . ' ' . ($decode->type != 6 ? '+' . ($decode->options * 4) . '%' : 'rate +' . ($decode->options * 5));
                    } else {
                        $view .= $opts['adds'] . ' +' . ($decode->options * 4);
                    }
                } else if ($decode->type == 13 && $decode->id != 30) {
                    //Dinorat
                    if ($decode->id == 3) {
                        switch ($decode->options) {
                            case 1:
                                $view .= '5% of damage transfered as Mana';
                                break;
                            case 2:
                                $view .= '+50 of damage transfered as Life';
                                break;
                            case 3:
                                $view .= '5% of damage transfered as Mana<br />+50 of damage transfered as Life';
                                break;
                            default:
                                $view .= 'Increase Attacking(wizardry)speed +5';
                                break;
                        }
                    } else {
                        $view .= $opts['reco'] . " +{$decode->options}%";
                    }
                } else {
                    switch ($decode->id) {
                        case 3:
                        case 5:
                            $view .= $decode->exo[5] ? "{$opts['adds']} +" . ($decode->options * 4) : "{$opts['reco']} +{$decode->options}%";
                            break;
                        case 4:
                            $view .= $decode->exo[5] ? "{$opts['wiza']} +" . ($decode->options * 4) : "{$opts['reco']} +{$decode->options}%";
                            break;
                        case 6:
                            $view .= $decode->exo[5] ? "{$opts['adds']} +" . ($decode->options * 4) : "{$opts['wiza']} +" . ($decode->options * 4);
                            break;
                        default:
                            $view .= "{$opts['adds']} +" . ($decode->options * 4);
                            break;
                    }
                }
                $view .= '</div>';
            }

            //Excellent Options
            if ($item['exo'] == 3 || $item['exo'] == 4) {
                $opts['exo1'] = preg_replace('/{amount}/', (50 + ($decode->level * 5)), $opts['exo1']);
                $opts['exo2'] = preg_replace('/{amount}/', (50 + ($decode->level * 5)), $opts['exo2']);
            }

            foreach ($decode->exo as $id => $exo) {
                if ($exo === true) {
                    $view .= "<div style='text-align: center; color: #96b0d3;'>{$opts['exo' . ($id + 1)]}</div>";
                }
            }
        }

        //Ancient Optinos
        if ($decode->ancient) {
            $ancOpts = include 'Core/DB/itemsAncient.php';
            if (isset($ancOpts[$item['ancient']])) {
                $view .= "<div style='text-align: center; color: #666666;'>";
                $view .= "<div style='margin: 10px 0 10px 0; color: #e3c972;'>Set</div>";
                $view .= $ancOpts[$item['ancient']];
                $view .= "</div>";
            }
        }

        return (Object) [
                    'name' => $name,
                    'options' => $view,
                    'image' => $image,
                    'size' => (Object) ['x' => $item['x'], 'y' => $item['y']]
        ];
    }

    public function hexDecode($hex) {
        if (strtolower($hex) == str_repeat('f', 20)) {
            return false;
        }

        $ops = hexdec(substr($hex, 2, 2));
        $exo = hexdec(substr($hex, 14, 2)); //Excellent options
        $dur = hexdec(substr($hex, 4, 2)); //Durability
        $anc = hexdec(substr($hex, 16, 2)); //Ancient Option
        $serial = substr($hex, 6, 8); //Serial
        //Item Skill
        $skill = false;
        if ($ops > 128) {
            $skill = true;
            $ops -= 128;
        }

        //Item Level
        $lvl = floor($ops / 8);
        $ops -= $lvl * 8;

        //Luck Option
        $luck = false;
        if ($ops >= 4) {
            $luck = true;
            $ops -= 4;
        }

        $level = substr($hex, 0, 1);
        $id = substr($hex, 1, 1);
        $opt = hexdec(substr($hex, 14, 2));
        $type = hexdec($level);
        if (($type % 2) <> 0) {
            $id = "1" . $id;
            $type--;
        }

        if ($opt >= 128) {
            $type += 16;
        }

        //Item id and type
        $type /= 2;
        $id = hexdec($id);

        //Excellent options
        if ($exo - 128 >= 64) {
            $ops += 4;
        }

        if ($exo - 128 >= 0) {
            $exo -= 128;
        }

        if ($exo >= 64) {
            $exo -= 64;
        }

        $level = $lvl > 13 ? 0 : $lvl;

        if (file_exists("Core/DB/items/{$type}.php")) {
            $item = include "Core/DB/items/{$type}.php";
            if (!isset($item[$id])) {
                return false;
            }
        } else {
            return false;
        }

        $item = $item[$id];
        if (isset($item['special'])) {
            $item = $item['special'][$level];
        }

        return (Object) [
                    'id' => $id,
                    'type' => $type,
                    'durability' => $dur,
                    'serial' => $serial,
                    'level' => $level,
                    'skill' => $skill,
                    'luck' => $luck,
                    'options' => $ops,
                    'exo' => $exo,
                    'ancient' => ($anc == 5 || $anc == 9) ? ($anc == 5 ? 5 : 10) : false,
                    'item' => $item
        ];
    }

//    public function Inventory($hex) {
//        $items = bin2hex($hex);
//
//        return [
//            'wings' => substr($items, 140, 20),
//            'helm' => substr($items, 40, 20),
//            'armor' => substr($items, 60, 20),
//            'pants' => substr($items, 80, 20),
//            'gloves' => substr($items, 100, 20),
//            'boots' => substr($items, 120, 20),
//            'leftWeapon' => substr($items, 0, 20),
//            'rightWeapon' => substr($items, 20, 20),
//            'pendant' => substr($items, 180, 20),
//            'leftRing' => substr($items, 200, 20),
//            'rightRing' => substr($items, 220, 20),
//            'pet' => substr($items, 160, 20)
//        ];
//    }

}
