<?php

namespace Core\Main\Item;

use Core\Tools\SQL,
    Core\Tools\Config;

class Generate {

    protected $_config, $_decode, $_sql;

    public function __construct() {
        $this->_config = new Config;
        $this->_decode = new Decode;
        $this->_sql = new SQL;
    }

    public function Hex($props = []) {
        /*
         * Still using Savoy's code here, to be re-made
         */

        if (!isset($props['group']) || !isset($props['id'])) {
            return false;
        }

        //Defining base values

        //Item additional options
        $options = isset($props['options']) ? $props['options'] : 0;

        //Item skill
        $skill = isset($props['skill']) ? 128 : 0;

        //Item level
        $level = isset($props['level']) ? $props['level'] : 0;

        //Item luck
        $luck = isset($props['luck']) ? 1 : 0;

        //Item Serial
        $serial = file_get_contents('Core/DB/GameServerInfo.php');
        file_put_contents('Core/DB/GameServerInfo.php', $serial + 1);

        //Item durability
        $durability = isset($props['durability']) ? $props['durability'] : 0;

        //Item ancient
        $ancient = isset($props['ancient']) ? $props['ancient'] : 0;

        $hex = '';

        //Part 1
        $part1 = $props['group'] * 32 + $props['id'];
        $hex .= $part1 >= 255 ? substr(sprintf("%02X", $part1, 0), 1, 2) : sprintf("%02X", $part1, 0);

        //Part 2
        if ($options > 3) {
            $options -= 4;
            $exl = 64;
        } else {
            $exl = 0;
        }

        $part2 = $level * 8 + $options + $skill + $luck * 4;
        $hex .= sprintf("%02X", $part2 < 0 ? 0 : $part2, 0);

        //Part 3
        $hex .= sprintf("%02X", $durability, 0);

        //Part 4
        $hex .= sprintf("%08X", $serial, 0);

        //Part 5
        if (isset($props['exo'])) {
            $exl += $props['exo'];
        }

        if ($props['group'] * 2 >= 16) {
            $exl += 128;
        }

        $hex .= sprintf("%02X", $exl, 0);

        //Part 6
        $hex .= sprintf("%02X", $ancient, 0);

        //Part 7
        if ($props['group'] == 14) {
            $props['group'] = 12;
        } elseif ($props['group'] == 12) {
            $props['group'] = 13;
        } elseif ($props['group'] == 13) {
            $props['group'] = 14;
        }

        $hex .= sprintf("%02X", $props['group'] * 16, 0);

        return substr_replace($hex, '77', 6, 2);
    }

}
