<?php

namespace Core\Main\Item;

use Core\Tools\Config;

class getFreeSlot
{

    protected $_config, $_decode;

    public function __construct()
    {
        $this->_config = new Config;
        $this->_decode = new Decode;
    }

    public function getSlots($hex) {
        $config = $this->_config->Get('Items');
        $slots_count = 120;
        $slots = str_repeat(0, $slots_count);   //Setting all slots as 0's (empty)

        for ($i = 0; $i < $slots_count; $i++) { //Setting all taken slots to 1
            $item = substr($hex, $i * $config['hexLength'], $config['hexLength']);
            if (strtolower($item) != str_repeat('f', $config['hexLength'])) {
                $decode = $this->_decode->hexDecode($item);
                $slots = substr_replace($slots, str_repeat(1, $decode->itemInfo['x']), $i, $decode->itemInfo['x']);
                if ($decode->itemInfo['y'] > 1) {
                    for ($r = 0; $r < $decode->itemInfo['y'] - 1; $r++) {
                        $slots = substr_replace($slots, str_repeat(1, $decode->itemInfo['x']), $i + 8 + $r * 8, $decode->itemInfo['x']);
                    }
                }
            }
        }

        return $slots;
    }

    public function Warehouse($hex, $_x, $_y)
    {
        $slots_count = 120;
        $slots = $this->getSlots($hex);

        for ($i = 0; $i < $slots_count; $i++) { //Going through $slots and checking if there are any empty slots that matches our item
            if ((ceil(($i + 1) / 8) * 8) >= ($i + $_x) && substr($slots, $i, $_x) === str_repeat(0, $_x)) { //First empty row found
                if ($_y > 1) { //If the item is bigger than 1 slot vertically (Y)
                    if ((ceil(($i + 1) / 8) + $_y - 1) <= 15) {
                        $row = 0;
                        for ($r = 0; $r < $_y - 1; $r++) {
                            if (substr($slots, $i + 8 + $r * 8, $_x) === str_repeat(0, $_x)) {
                                $row++;
                            }
                        }

                        if ($row == $_y - 1) {
                            return $i;
                        }
                    }
                } else {
                    return $i;
                }
            }
        }
        return false;
    }

    public function checkSlot($hex, $_x, $_y, $_slot)
    {
        $slots = $this->getSlots($hex);

            if ((ceil(($_slot + 1) / 8) * 8) >= ($_slot + $_x) && substr($slots, $_slot, $_x) === str_repeat(0, $_x)) { //First empty row found
                if ($_y > 1) { //If the item is bigger than 1 slot vertically (Y)
                    if ((ceil(($_slot + 1) / 8) + $_y - 1) <= 15) {
                        $row = 0;
                        for ($r = 0; $r < $_y - 1; $r++) {
                            if (substr($slots, $_slot + 8 + $r * 8, $_x) === str_repeat(0, $_x)) {
                                $row++;
                            }
                        }

                        if ($row == $_y - 1) {
                            return $_slot;
                        }
                    }
                } else {
                    return $_slot;
                }
            }
        return false;
    }

}
