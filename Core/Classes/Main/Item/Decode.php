<?php

namespace Core\Main\Item;

use Core\Tools\Config,
    Core\Tools\SQL;

class Decode {

    protected $_config, $_output, $_sql;

    public function __construct() {
        $this->_config = Config::Get('Items');
        $this->_output = new \stdClass;
        $this->_sql = new SQL;
    }

    public function hexDecode($hex) {
        if (strtolower($hex) === str_repeat('f', $this->_config['hexLength'])) {
            return false;
        }

        if (!in_array(strlen($hex), $this->_config['possibleHexLengths']) || strlen($hex) !== $this->_config['hexLength']) {
            exit('<b>Item/Decode:</b> Invalid item hex length');
        }

        $exo = hexdec(substr($hex, 14, 2));

        if ($this->_config['hexLength'] < 32) {
            $this->_output->version = false;

            //Item group & id (old versions 20 hex length)
            $group = hexdec(substr($hex, 0, 1));
            $this->_output->group = ($group | $exo >> 3 & 0b10000) >> 1;

            $id = hexdec(substr($hex, 1, 1));
            $this->_output->id = $id | ($group % 2 ? 0b10000 : 0);
        } else {
            $this->_output->version = true;

            //Item group & id (newer versions 32 hex length)
            $this->_output->group = hexdec(substr($hex, 18, 1));
            $this->_output->id = hexdec(substr($hex, 0, 2));

            //Refinery Option
            $this->_output->refinery = substr($hex, 19, 1) ? true : false;

            //Harmony Option
            $this->_output->harmony = hexdec(substr($hex, 20, 1));
            $this->_output->harmony_level = hexdec(substr($hex, 21, 1));

            //Socket options
            $sockets = substr($hex, 22, 10);
            $this->_output->socket = [
                1 => hexdec(substr($sockets, 0, 2)),
                2 => hexdec(substr($sockets, 2, 2)),
                3 => hexdec(substr($sockets, 4, 2)),
                4 => hexdec(substr($sockets, 6, 2)),
                5 => hexdec(substr($sockets, 8, 2))
            ];
        }

        //Skill, Luck, Level, Additional
        $options = hexdec(substr($hex, 2, 2));

        //Skill
        $this->_output->skill = $options >> 7 & 0b1;

        //Level
        $this->_output->level = $options >> 3 & 0b1111;

        //Luck
        $this->_output->luck = $options >> 2 & 0b1;

        //Durability
        $this->_output->durability = hexdec(substr($hex, 4, 2));

        //Serial
        $this->_output->serial = substr($hex, 6, 8);

        //Excellent options
        $this->_output->excellent = [
            1 => $exo & 0b1,
            2 => $exo >> 1 & 0b1,
            3 => $exo >> 2 & 0b1,
            4 => $exo >> 3 & 0b1,
            5 => $exo >> 4 & 0b1,
            6 => $exo >> 5 & 0b1
        ];

        //Additional dmg/def etc..
        $this->_output->additional = $options & 0b11 | (($exo >> 6 & 0b1) << 2);

        //Ancient
        $ancient = hexdec(substr($hex, 17, 1));
        $this->_output->ancient = $ancient & 0b100 | $ancient & 0b1000;

        //Item default properties
        if(is_file("Core/DB/items/{$this->_output->group}/{$this->_output->id}.php")) {
            $this->_output->itemInfo = include "Core/DB/items/{$this->_output->group}/{$this->_output->id}.php";
            if(isset($this->_output->itemInfo['level'])) {
                $trueLevel = isset($this->_output->itemInfo['level'][$this->_output->level]) ? $this->_output->level : 0;
                $this->_output->itemInfo += $this->_output->itemInfo['level'][$trueLevel];
                $this->_output->itemInfo['trueLevel'] = $trueLevel;
            }
        } else {
            exit("<b>Item/Decode:</b> Item <b>{$this->_output->group} {$this->_output->id}</b> could not be found");
        }

        return $this->_output;
    }

}
