<?php

namespace Core\Main\Item;

use Core\Main\Item\Decode,
    Core\Tools\Config;

class Count {

    protected $_config, $_decode;

    public function __construct() {
        $this->_config = new Config;
        $this->_decode = new Decode;
    }

    public function Item($items, $props) {
        $config = $this->_config->Get('Items');
        $count = 0;
        $items = strtolower($items);

        for ($i = 0; $i < (strlen($items) / $config['hexLength']); $i++) {
            $item = substr($items, $i * $config['hexLength'], $config['hexLength']);

            if ($item != str_repeat('f', $config['hexLength'])) {
                $decode = $this->_decode->hexDecode($item);
                if ($decode !== false) {
                    $check = true;

                    if (count($props) > 1) {
                        foreach ($props as $id => $prop) {
                            $check = true;
                            $val = $prop['count'];
                            unset($prop['count']);
                            unset($prop['durability']);
                            foreach ($prop as $key => $value) {
                                if ($decode->{$key} != $value) {
                                    $check = false;
                                    break;
                                }
                            }

                            if ($check) {
                                $items = preg_replace("/{$item}/i", str_repeat('f', $config['hexLength']), $items);
                                $count += $val;
                            }
                        }
                    } else {
                        unset($props[0]['durability']);
                        foreach ($props[0] as $key => $value) {
                            if ($decode->{$key} != $value) {
                                $check = false;
                                break;
                            }
                        }

                        if ($check) {
                            $items = preg_replace("/{$item}/i", str_repeat('f', $config['hexLength']), $items);
                            $count += 1;
                        }
                    }
                }
            }
        }

        return [
            'count' => $count,
            'hex' => $items
        ];
    }

}
