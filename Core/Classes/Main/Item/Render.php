<?php

namespace Core\Main\Item;

use Core\Tools\SQL;
use Core\Tools\Decoder;

class Render
{
    protected $_sql, $_config, $_decode, $_output, $_itemName, $_itemImage, $_itemOptions;

    public function __construct()
    {
        $this->_sql = new SQL;
    }

    public function Item($hex)
    {
        $decode = new Decode;
        $this->_decode = $decode->hexDecode($hex);
        if (!$this->_decode) {
            return (Object)[
                'name' => '',
                'image' => '',
                'options' => '',
                'properties' => []
            ];
        }

        $itemsOptions = include 'Core/DB/items/itemsOptions.php';
        $itemsAncient = include 'Core/DB/items/itemsAncient.php';

        if ($this->_decode->version) {
            $sockets = array_filter($this->_decode->socket, function ($value) {
                if ($value !== 0) {
                    return true;
                }
            });
            $sockets = array_values($sockets);
        } else {
            $sockets = [];
        }

        $ancient = isset($this->_decode->itemInfo['ancient']) && $this->_decode->ancient > 0 ? true : false;

        //Item image
        $this->_itemImage = "/assets/images/itemImage/{$this->_decode->group}/{$this->_decode->id}" . (isset($this->_decode->itemInfo['trueLevel']) ? "-{$this->_decode->itemInfo['trueLevel']}" : '') . ($ancient ? 'C' : '') . ".gif";

        //Item name
        $this->_itemName = "<div style='margin-bottom: 4px; background: " . ($ancient ? '#3d3cc8' : 'transparent') . "; color: " . (count($sockets) > 0 ? '#bc45a8' : ($ancient ? '#43c38a' : (in_array(1, $this->_decode->excellent) && $this->_decode->group != 12 ? '#439f6b' : ($this->_decode->level > 6 ? '#ceb948' : '#96b0d3')))) . "; text-align: center;'>";

        if ($ancient) {
            $this->_itemName .= $this->_decode->itemInfo['ancient'] . ' ';
        }

        $this->_itemName .= $this->_decode->itemInfo['name'];

        if ($this->_decode->group == 13 && $this->_decode->id == 37) {
            if ($this->_decode->excellent[1]) {
                $this->_itemName .= " +Destroy";
            } else if ($this->_decode->excellent[2]) {
                $this->_itemName .= " +Defense";
            } else if ($this->_decode->excellent[3]) {
                $this->_itemName = "<div style='color: #f4cb3f; text-align: center;'>Golden Fenrir</div>";
            }
        }

        if ($this->_decode->level > 0 && !isset($this->_decode->itemInfo['level'])) {
            $this->_itemName .= " +" . $this->_decode->level;
        }

        $this->_itemName .= "</div>";

        //Item name
        $this->_itemOptions = $this->_itemName;

        //Item durability
        $this->_itemOptions .= "<div style='padding: 2px; color: #ffffff; text-align: center;'>";

        if (isset($this->_decode->itemInfo['dur'])) {
            $this->_decode->itemInfo['dur'] = preg_replace('/{durability}/', $this->_decode->durability, $this->_decode->itemInfo['dur']);
            $this->_itemOptions .= $this->_decode->itemInfo['dur'];
        } else {
            $this->_itemOptions .= "Durability {$this->_decode->durability}";
        }

        $this->_itemOptions .= "</div>";

        //Allowed to equip
        if (isset($this->_decode->itemInfo['class'])) {
            $this->_itemOptions .= "<div style='margin-bottom: 5px; background: #a00000; color: #FFFFFF; text-align: center;'>";

            foreach ($this->_decode->itemInfo['class'] as $class) {
                $this->_itemOptions .= "<div style='padding: 0 4px 0 4px;'>Can be equipped by " . Decoder::characterClass($class)->full . "</div>";
            }

            $this->_itemOptions .= "</div>";
        }

        if ($this->_decode->version) {
            //Refinery option
            if ($this->_decode->refinery) {
                $this->_itemOptions .= "<div style='padding: 5px; text-align: center; color: #ffbcfb;'>Additional Damage +200<br />Attack success rate increase +10</div>";
            }

            //Harmony option
//            if ($this->_decode->harmony) {
//                $fetch = $this->_sql->Fetch("SELECT * FROM [ITEMS_Harmony] WHERE [harmony_type]=? AND [harmony_id]=?", [$this->_decode->itemInfo->harmony, $this->_decode->harmony]);
//                if (!empty($fetch)) {
//                    $this->_itemOptions .= "<div style='padding: 5px; text-align: center; color: #e2d58a;'>{$fetch->harmony_option} +" . $fetch->{'lvl_' . $this->_decode->harmony_level} . "</div>";
//                }
//            }
        }

        //Item Skill
        if ($this->_decode->group == 13 && $this->_decode->id == 37) {
            $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>Plasma storm skill (Mana: 50)</div>";
        } else {
            if ($this->_decode->skill && isset($this->_decode->itemInfo['skill'])) {
                if (isset($itemsOptions['skill'][$this->_decode->itemInfo['skill']])) {
                    $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>{$itemsOptions['skill'][$this->_decode->itemInfo['skill']]}</div>";
                }
            }
        }

        //Item Luck
        if ($this->_decode->luck && isset($this->_decode->itemInfo['luck'])) {
            $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>Luck (success rate of Jewel of Soul +25%)<br />Luck (critical damage rate +5%)</div>";
        }

        //Additional DEFF/DMG
        if ($this->_decode->additional && isset($this->_decode->itemInfo['add'])) {
            $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>";

            $add = $this->_decode->itemInfo['add'];
            $this->_itemOptions .= $itemsOptions['add'][$add] . ' +' . (in_array($add, ['dmg', 'deff', 'wiz', 'dmgwiz']) ? $this->_decode->additional * 4 : $this->_decode->additional . '%');

            $this->_itemOptions .= "</div>";

//            if (in_array($this->_decode->group, [0, 1, 2, 3, 4, 12, 13])) {
//                if ($this->_decode->group == 12 && $this->_decode->id > 2) {
//                    //BK, ELF
//                    if (in_array($this->_decode->id, [3, 5, 36, 38])) {
//                        $this->_itemOptions .= $this->_decode->excellent[5] ? "Additional Dmg +" . $this->_decode->additional * 4 : 'Automatic HP Recovery +' . $this->_decode->additional . '%';
//                    }
//                    //SM, SUM
//                    else if (in_array($this->_decode->id, [4, 37, 41, 42, 43])) {
//                        $this->_itemOptions .= $this->_decode->excellent[5] ? "Additional Wizardy dmg +" . $this->_decode->additional * 4 : 'Automatic HP Recovery +' . $this->_decode->additional . '%';
//                    }
//                    //MG
//                    else if (in_array($this->_decode->id, [6])) {
//                        $this->_itemOptions .= $this->_decode->excellent[5] ? "Additional dmg +" . $this->_decode->additional * 4 : "Additional Wizardy Dmg +" . $this->_decode->additional * 4;
//                    }
//                } else {
//                    $this->_itemOptions .= "Additional dmg +" . $this->_decode->additional * 4;
//                }
//            } else if ($this->_decode->group == 5) {
//                $this->_itemOptions .= "Additional Wizardy Dmg +" . $this->_decode->additional * 4;
//            } else if ($this->_decode->group == 6) {
//                $this->_itemOptions .= "Additional Defense Rate +" . $this->_decode->additional * 5;
//            } else if (in_array($this->_decode->group, [7, 8, 9, 10, 11])) {
//                $this->_itemOptions .= "Additional Defense +" . $this->_decode->additional * 4 . '%';
//            }

        }

        //Excellent options
        if (in_array(1, $this->_decode->excellent)) {
            foreach ($this->_decode->excellent as $id => $exo) {
                if ($exo === 1) {
                    $excOpt = preg_replace('/{wingsLife}/', $this->_decode->level > 0 ? 50 * $this->_decode->level : 50, $itemsOptions['exo'][$this->_decode->itemInfo['exo']][$id]);
                    $excOpt = preg_replace('/{wingsMana}/', $this->_decode->level > 0 ? 50 * $this->_decode->level : 50, $excOpt);
                    $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>{$excOpt}</div>";
                }
            }
        }

        //Ancient options
        if ($ancient && isset($itemsAncient[$this->_decode->itemInfo['ancient']])) {
            //Ancient items options
            $this->_itemOptions .= "<div style='text-align: center; color: #f4c842; padding: 7px;'>Set Item option info</div>";
            $this->_itemOptions .= "<div style='text-align: center; color: #81858c;'>{$itemsAncient[$this->_decode->itemInfo['ancient']]['options']}</div>";

            //Ancient items parts
            if (isset($itemsAncient[$this->_decode->itemInfo['ancient']]['parts'])) {
                $this->_itemOptions .= "<div style='text-align: center; color: #f4c842; padding: 7px;'>Set Item parts</div>";
                $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>";

                foreach ($itemsAncient[$this->_decode->itemInfo['ancient']]['parts'] as $part) {
                    $this->_itemOptions .= $this->_decode->itemInfo['ancient'] . ' ' . $part . '<br />';
                }

                $this->_itemOptions .= "</div>";
            }
        }

        //Socket options
//        if (count($sockets) > 0) {
//            $this->_itemOptions .= "<div style='text-align: center; color: #dd66c9; padding: 7px;'>Socket Options</div>";
//
//            foreach ($sockets as $id => $socket) {
//                if ($socket === 255) {
//                    $this->_itemOptions .= "<div style='text-align: center; color: #707070;'>Socket " . ($id + 1) . ": No item application</div>";
//                } else {
//                    $level = 1;
//                    while ($socket > 50) {
//                        $socket -= 50;
//                        $level++;
//                    }
//
//                    $fetch = $this->_sql->Fetch("SELECT * FROM [ITEMS_Sockets] WHERE [socket_id]=?", [$socket]);
//                    $socketOption = $fetch->socket_option;
//                    $this->_itemOptions .= "<div style='text-align: center; color: #96b0d3;'>Socket " . ($id + 1) . ": {$fetch->socket_type}({$socketOption} + " . $fetch->{"lvl_{$level}"} . ")</div>";
//                }
//            }
//        }

        /*
         * Item Options end
         */

        return (Object)[
            'name' => $this->_itemName,
            'image' => $this->_itemImage,
            'options' => $this->_itemOptions,
            'properties' => $this->_decode
        ];
    }

}
