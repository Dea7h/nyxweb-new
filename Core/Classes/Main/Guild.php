<?php

namespace Core\Main;

use Core\Tools\SQL;

class Guild {

    private $_sql;

    public function __construct() {
        $this->_sql = new SQL;
    }

    public function Info($name, $logoSize = 40) {
        $output = ['name' => false, 'logo' => false];
        $fetch = $this->_sql->Fetch("SELECT [G_Name] FROM [GuildMember] WHERE [Name]=?", [$name]);
        if (!empty($fetch)) {
            $fetch = $this->_sql->Fetch("SELECT [G_Name], [G_Mark] FROM [Guild] WHERE [G_Name]=?", [$fetch->G_Name]);
            if (!empty($fetch)) {
                $output = [
                    'name' => $fetch->G_Name,
                    'logo' => $this->markDecode(bin2hex($fetch->G_Mark), $logoSize)
                ];
            }
        }

        return (Object) $output;
    }

    private function markDecode($mark, $size = 50) {
        $logo = "<div style='width: " . $size . "px; height: " . $size . "px; overflow: hidden;'>";

        for ($id = 0; $id < 64; $id++) {
            $color = substr($mark, $id, 1);
            $logo .= "<div style='background-color: " . $this->colorDecode($color) . "; width: " . ($size / 8) . "px; height: " . ($size / 8) . "px; float: left;'></div>";
        }

        return $logo . '</div>';
    }

    private function colorDecode($key) {
        switch ($key) {
            case '0': return 'transparent';
            case '1': return '#000000';
            case '2': return '#8c8a8d';
            case '3': return '#ffffff';
            case '4': return '#fe0000';
            case '5': return '#ff8a00';
            case '6': return '#ffff00';
            case '7': return '#8cff01';
            case '8': return '#00ff00';
            case '9': return '#01ff8d';
            case 'A': return '#00ffff';
            case 'B': return '#008aff';
            case 'C': return '#0000fe';
            case 'D': return '#8c00ff';
            case 'E': return '#ff00fe';
            case 'F': return '#ff0080';
        }
    }

}
