<?php

if (!defined('access')) {
    exit;
}

use Core\Tools\Purify,
    Core\Tools\Notice,
    Core\Main\User;

$user = new User;
$request = Purify::_mass($GLOBALS['request'], '_userA');

if ($user->Access()) {
    if (isset($request[0]) && isset($request[1]) && isset($request[2]) && is_file("Core/Views/{$request[0]}/{$request[1]}/{$request[2]}.php")) {
        include "Core/Views/{$request[0]}/{$request[1]}/{$request[2]}.php";
    } else {
        print "<div style='border: 1px solid #1b1b1b; margin: 5px;'><img src='/assets/images/others/404.jpg' style='width: 100%;' /></div>";
    }
} else {
    print Notice::Request($lang->Phrase('request', 'not-logged'));
}