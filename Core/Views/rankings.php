<?php
if (!defined('access')) {
    exit;
}

use Core\Main\Guild;
use Core\Main\Character;
?>
<div class="padding">
    <table class="table-classic">
        <tr>
            <th>#</th>
            <th></th>
            <th><?= $lang->Phrase('rankings', 'name') ?></th>
            <th><?= $lang->Phrase('rankings', 'level') ?></th>
            <th><?= $lang->Phrase('rankings', 'map') ?></th>
            <th class="dissapearOnMobile"><?= $lang->Phrase('rankings', 'guild') ?></th>
            <th class="dissapearOnMobile"></th>
        </tr>
        <?php
        $nguild = new Guild;
        $char = new Character;
        $settings = $config->Get('Rankings');

        $chars = $sql->Count("SELECT COUNT(*) FROM [Character]");
        $total = ceil($chars / $settings['perPage']);
        $page = isset($GLOBALS['request'][1]) && $GLOBALS['request'][1] > 0 && ctype_digit($GLOBALS['request'][1]) && $GLOBALS['request'][1] <= $total ? $GLOBALS['request'][1] : 1;
        $offset = ($page - 1) * $settings['perPage'];

        $fetch = $sql->FetchAll("SELECT * FROM [Character] ORDER BY [Resets] DESC, [cLevel] DESC, [Name] DESC OFFSET {$offset} ROWS FETCH NEXT {$settings['perPage']} ROWS ONLY");
        foreach ($fetch as $id => $obj) {
            $id++;
            $real_id = $id + ($page - 1) * $settings['perPage'];

            //Country Flag
            $user = $sql->Fetch("SELECT [sno__numb] FROM [MEMB_INFO] WHERE [memb___id]=?", [$obj->AccountID]);
            $flag = "<img src='/assets/images/icons/flags/" . (!empty($user->sno__numb) ? $user->sno__numb : 'unknown') . ".png' />";

            //Character Status
            $status = "<img src='/assets/images/icons/" . ($char->Status($obj->Name) ? 'on' : 'off') . ".gif' />";

            //Guild
            $guild = $nguild->Info($obj->Name, 26);
            if ($guild->name === false) {
                $guild->name = "<font style='color: #727272;'>{$lang->Phrase('rankings', 'no-guild')}</font>";
            }

            //Experience bar
            if ($obj->cLevel > 255) {
                $exp = (($obj->cLevel - 255) + 9) * ($obj->cLevel - 255) * ($obj->cLevel - 255) * 1000;
            } else {
                $exp = ($obj->cLevel + 9) * $obj->cLevel * $obj->cLevel * 10;
            }

            $expPercent = $obj->Experience > $exp ? 100 : ($obj->Experience < 0 ? 0 : floor(($obj->Experience / $exp) * 100));

            print "<tr><td>{$real_id}</td><td class='rankings-charimg' style=\"background-image: url('/assets/images/others/characters/rankings/{$char->_Class($obj->Class)['image']}.gif');\"></td><td>{$flag} {$status} <font ajax='/profile/{$obj->Name}'>{$obj->Name}</font></td><td style='text-align: center;'>{$obj->cLevel} ( <b>" . $expPercent . "%</b> )</td><td style='text-align: center;'>{$char->Map($obj->MapNumber)} ( $obj->MapPosX/$obj->MapPosY )</td><td style='text-align: right;' class='dissapearOnMobile'>{$guild->name}</td><td class='rankings-guild dissapearOnMobile'>{$guild->logo}</td></tr>";
        }
        ?>
        <tr>
            <th colspan="7" style="overflow: hidden; line-height: 19px;">
                <div class="paging-left"><?= $lang->Phrase('rankings', 'page') ?> <b><?= $page ?></b> <?= $lang->Phrase('rankings', 'of') ?> <b><?= $total ?></b></div>
                <div style="display: inline-block;">
                    <div onclick="loader('/rankings/<?= ($page - 1 < 1 ? 1 : $page - 1) ?>');" class="paging-button">&#8592;</div>
                    <select onchange="loader('/rankings/' + this.value);" class="paging-select">
                        <?php
                        for ($i = 1; $i <= $total; $i++) {
                            print "<option " . ($i == $page ? 'selected' : '') . ">{$i}</option>";
                        }
                        ?>
                    </select>
                    <div onclick="loader('/rankings/<?= ($page + 1 > $total ? $total : $page + 1) ?>');" class="paging-button">&#8594;</div>
                </div>
                <div class="paging-right"><?= $lang->Phrase('rankings', 'total-of') ?> <b><?= $chars ?></b> <?= $lang->Phrase('rankings', 'results') ?></div>
            </th>
        </tr>
    </table>
</div>