<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\User\Storage,
    Core\Main\Item\getFreeSlot;

$user = new User;
$storage = new Storage;
$getSlots = new getFreeSlot;
$items = $config::Get('Items');
$warehouse = $sql->Fetch("SELECT [Items] FROM [warehouse] WHERE [AccountID]=?", [\Core\Tools\Cookie::Get('username')]);

$slots = $getSlots->getSlots(bin2hex($warehouse->Items));

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {

    /*
     * Warehouse Rendering
     */

    print "<div style='overflow: hidden;'>";

    print "<input id='warehouseSlots' type='hidden' value='{$slots}' />";

    print "<div class='storage-frame' style='float: left;'>";
    print "<div class='storage-title'>{$lang->Phrase('account-storage', 'warehouse')}</div>";
    print "<div target='storage' id='left-storage' class='storage-items'>";

    print "<div class='storage-items-squares'>";

    for ($i = 1; $i < 121; $i++) {
        print "<div id='{$i}' class='droppable-item' style='width: 26px; height: 26px; float: left;'></div>";
    }

    print "</div>";

    print $storage->Render($items['warehouse']);
    print "</div>";
    print "</div>";

    /*
     * Web Storage Rendering
     */

    print "<div class='storage-frame' style='float: right;'>";
    print "<div class='storage-title'>{$lang->Phrase('account-storage', 'storage')}</div>";
    print "<div target='storage' id='right-storage' class='storage-items' style='width: " . ($items['storage']['x'] * 26) . "px; height: " . ($items['storage']['y'] * 26) . "px;'>" . $storage->Render($items['storage']) . "</div>";
    print "</div>";

    print "</div>";

//    print "<div class='messageBlock' style='margin-top: 5px;'>To move an item from the \"In-Game Warehouse\" to the \"Web Storage\" or the other way around";
//
//    print "<ul style='list-style-type: square;'>";
//    print "<li>you can simply left click on it...</li>";
//    print "<li>...or right click on it...</li>";
//    print "<li>...or drag it... :)</li>";
//    print "</ul>";

    print "</div>";
} else {
    print Notice::Request($lang->Phrase('request', 'not-logged'));
}

print "</div>";
