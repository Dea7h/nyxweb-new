<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\Tools\Cookie,
    Core\Main\Item\Render;

$user = new User;
$render = new Render;
$account = Cookie::Get('username');

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {

    print "<table class='table-classic'>";
    print "<tr>
            <th>#</th>
            <th>date</th>
            <th>message</th>
            <th>ip</th>
        </tr>";

    $fetch = $sql->FetchAll("SELECT TOP 100 * FROM [NYX_LOGS] WHERE [account]=? ORDER BY [time] DESC", [$account]);

    foreach ($fetch as $id => $log) {
        $log->ip = strrpos($log->ip, '.') !== false ? substr_replace($log->ip, '.**', strrpos($log->ip, '.'), 4) : $log->ip;

        if (!empty($log->hidden_info) && strlen($log->hidden_info) == 20) {
            $itemRender = $render->Item($log->hidden_info);
            if ($itemRender) {
                $log->message = preg_replace("/{%}/", "<font title=\"{$itemRender->options}\">" . $itemRender->name . "</font>", $log->message);
            }
        }

        print "<tr><td>" . ($id + 1) . "</td><td style='text-align: center;' title='" . date('Y/m/d H:i:s', $log->time) . "'><time class='timeago' datetime='" . date('c', $log->time) . "'></time></td><td>{$log->message}</td><td style='text-align: center;'>{$log->ip}</td></tr>";
    }
} else {
    print Notice::Request($lang->Phrase('request', 'not-logged'));
}

print "</div>";
