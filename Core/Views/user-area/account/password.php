<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\Tools\Cookie;

$user = new User;

if ($user->Access()) {
    print "<form action='password'>";
    print "<table class='table-form'>";
    
    print "<tr><td>Account</td><td><input type='text' value='" . Cookie::Get('username') . "' disabled /></td></tr>";
    print "<tr><td>Password</td><td><input type='password' name='password' minlength='4' maxlength='10' required /></td></tr>";
    print "<tr><td>E-Mail Address</td><td><input type='email' name='email' minlength='5' maxlength='35' autocomplete='off' required /></td></tr>";
    print "<tr><td colspan='2' style='height: 5px;'></td></tr>";
    print "<tr><td>New Password</td><td><input type='text' name='newpassword' minlength='4' maxlength='10' autocomplete='off' required /></td></tr>";
    print "<tr><td colspan='2' style='text-align: center;'><input type='submit' value='Change Password' /></td></tr>";
    
    print "</table>";
    print "</form>";
} else {
    print "<div class='content-sub-boxonly'>" . Notice::Request($lang->Phrase('request', 'not-logged')) . "</div>";
}

