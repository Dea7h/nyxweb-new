<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\Tools\Cookie,
    Core\Main\Item\Count;

$user = new User;
$count = new Count;
$config = $config::Get('Resources');
$account = Cookie::Get('username');


if ($user->Access()) {

    $ware = $sql->Fetch("SELECT [Items] FROM [warehouse] WHERE [AccountID]=?", [$account]);

    print "<div class='content-sub-boxonly' style='width: 273px; float: left; margin-right: 0;'>";
    print "<div class='resource-title'>{$lang->Phrase('account-resources', 'warehouse')}</div>";

    print "<div class='resource-block'>";
    print "<select id='deposit-character' style='width: 100px;'>";
    $chars = $sql->FetchAll("SELECT [id],[Name],[Money] FROM [Character] WHERE [AccountID]=? ORDER BY [Money] DESC", [$account]);
    print "<option value=''>-</option>";
    foreach ($chars as $char) {
        print "<option value='{$char->id}'>{$char->Name} (" . number_format($char->Money) . ")</option>";
    }
    print "</select>";
    print "<input type='button' value='{$lang->Phrase('account-resources', 'deposit')}' style='width: 55px;' onclick=\"request('resources', {depositZen: true, character: $('#deposit-character').val(), zen: $('#deposit-zen').val()})\" /><input type='number' id='deposit-zen' placeholder='{$lang->Phrase('account-resources', 'amount')}' min='1' max='2000000000' style='width: 90px; margin-right: 5px;' /></div>";

    foreach ($config as $column => $res) {
        if ($column != 'zen' && $column != 'credits') {
            print "<div class='resource-block'><span title='{$res['name']}'><img src='/assets/images/itemImage/{$res['image']}' style='height: 25px;' /> {$lang->Phrase('account-resources', 'found')} <span class='highlighter'>" . $count->Item(bin2hex($ware->Items), $res['props'])['count'] . "</span></span><input type='button' value='{$lang->Phrase('account-resources', 'deposit')}' style='width: 55px;' onclick=\"request('resources', {deposit: '{$column}'})\" /></div>";
        }
    }

    print "</div>";

    print "<div class='content-sub-boxonly' style='width: 273px; float: right; margin-left: 0;'>";
    print "<div class='resource-title'>{$lang->Phrase('account-resources', 'bank')}</div>";

    print "<div class='resource-block'>";
    print "<select id='withdraw-character' style='width: 94px;'>";
    print "<option value=''>-</option>";
    foreach ($chars as $char) {
        print "<option value='{$char->id}'>{$char->Name} (" . number_format($char->Money) . ")</option>";
    }
    print "</select>";
    print "<input type='button' value='{$lang->Phrase('account-resources', 'withdraw')}' style='width: 61px;' onclick=\"request('resources', {withdrawZen: true, character: $('#withdraw-character').val(), zen: $('#withdraw-zen').val()})\" /><input type='number' id='withdraw-zen' placeholder='{$lang->Phrase('account-resources', 'amount')}' min='1' max='2000000000' style='width: 90px; margin-right: 5px;' /></div>";

    $fetch = $sql->Fetch("SELECT * FROM [NYX_RESOURCES] WHERE [account]=?", [$account]);
    foreach ($config as $column => $res) {
        if ($column != 'zen' && $column != 'credits') {
            print "<div class='resource-block'><span title='{$res['name']}'><img src='/assets/images/itemImage/{$res['image']}' style='height: 25px;' /> {$lang->Phrase('account-resources', 'available')} <span class='highlighter'>{$fetch->$column}</span></span><input type='button' value='{$lang->Phrase('account-resources', 'withdraw')}' style='width: 61px;' onclick=\"request('resources', {withdraw: '{$column}', amount: $('#{$column}-amount').val()})\" /><input type='number' placeholder='{$lang->Phrase('account-resources', 'amount')}' id='{$column}-amount' min='1' max='50' maxlength='2' style='width: 60px; margin-right: 5px;' /></div>";
        }
    }

    print "</div>";
} else {
    print "<div class='content-sub-boxonly'>" . Notice::Request($lang->Phrase('request', 'not-logged')) . "</div>";
}

