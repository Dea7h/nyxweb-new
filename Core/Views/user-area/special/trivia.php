<?php

if (!defined('access')) {
    exit;
}

use Core\Tools\Cookie,
    Core\Tools\Purify,
    Core\Tools\Notice,
    Core\Main\User,
    Core\User\Storage;

$user = new User;
$storage = new Storage;
$items = $config::Get('Items');

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {
    print "TRIVIA";
}

print "</div>";
