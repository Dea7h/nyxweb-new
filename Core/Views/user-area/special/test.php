<?php

if (!defined('access')) {
    exit;
}

use Core\Tools\Cookie,
    Core\Tools\Purify,
    Core\Tools\Notice,
    Core\Main\User,
    Core\User\Storage;

$user = new User;
$storage = new Storage;
$items = $config::Get('Items');

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {

    /*
     * Warehouse Rendering
     */
    
    print "<div class='storage-frame' style='width: " . ($items['test']['x'] * 26 + 32) . "px; height: " . ($items['test']['y'] * 26 + 32) . "; float: right;'>";
    print "<div target='storage' id='{$items['storage']['side']}-storage' class='storage-items' style='width: " . ($items['test']['x'] * 26) . "px; height: " . ($items['test']['y'] * 26) . ";'>" . $storage->Render($items['test']) . "</div>";
    print "</div>";
}

print "</div>";
