<?php
if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\User\Storage,
    Core\Main\Item\Generate,
    Core\Main\Item\Render;

$user = new User;
$storage = new Storage;
$crafts = $config::Get('CraftItemsList');
$generate = new Generate;
$render = new Render;
$request = $GLOBALS['request'];

if ($user->Access()) {
    ?>
    <style>
        .craft-body {
            margin: 5px auto;
            overflow: hidden;
            width: 422px;
            height: 422px;
            background-image: url('/assets/images/items/frame.png');
        }

        .inner-body {
            margin: 15px 15px 15px 15px;
        }

        .inner-body .top {
            height: 220px;
            background-color: #171b27;
            border: 1px solid #282722;
            border-bottom: 0;
            overflow-y: auto;
        }

        .inner-body .top-cont {
            padding: 10px;
        }

        .inner-body .bottom {
            position: relative;
            height: 170px;
            background-color: #11141d;
            border: 1px solid #282722;
        }

        .inner-body .bottom .required {
            padding: 5px;
            font-size: 15px;
            text-align: center;
            border-bottom: 1px solid #282722;
        }

        .inner-body .bottom .required-list {
            padding: 10px 0 10px 0;
            text-align: center;
            border-bottom: 1px solid #282722;
        }

        .inner-body .bottom .required-list .req-item {
            display: inline-block;
            border: 1px solid #282722;
            margin-right: 5px;
        }

        .required-list .req-item .item {
            width: 32px;
            height: 32px;
            border-bottom: 1px solid #282722;
        }

        .inner-body .bottom .required-list .req-item:last-child {
            margin: 0;
        }

        .bottom .price {
            overflow: hidden;
            border-bottom: 1px solid #282722;
            padding: 2px;
        }

        .bottom .price .credits {
            color: #7f842a;
            padding-left: 10px;
            float: left;
        }

        .bottom .price .zen {
            color: #165d13;
            padding-right: 10px;
            float: right;
        }

        .top-cont .items-category {
            display: inline-block;
            font-size: 15px;
        }

        .top-cont .item {
            padding-left: 10px;
        }
        
        .craftButton {
            position: absolute;
            bottom: 8px;
            left: 0;
            right: 0;
            margin: auto;
            background-image: url('/assets/images/others/mix_button1.jpg');
            width: 44px;
            height: 32px;
        }
        
        .craftButton:active {
            background-image: url('/assets/images/others/mix_button2.jpg');
        }
    </style>

    <div class="craft-body">
        <div class="inner-body">
            <div class="top">
                <div class="top-cont">
                    <?php
                    foreach ($crafts as $category => $craft) {
                        print "<div class='items-category'>{$category}</div>";

                        foreach ($craft as $id => $crafto) {
                            $crafto['serial'] = 0;
                            $itemHex = $generate->Hex($crafto);
                            $itemRender = $render->Item($itemHex);

                            print "<div><font class='item' ajax='/user-area/special/craft/{$category}/{$id}' title=\"{$itemRender->options}\">{$itemRender->name}</font></div>";
                        }
                    }
                    ?>

                </div>
            </div>
            <div class="bottom">
                <?php
                if (isset($request[3]) && isset($request[4]) && isset($crafts[ucfirst($request[3])][$request[4]])) {
                    $craft = $crafts[ucfirst($request[3])][$request[4]];
                    $itemHex = $generate->Hex($craft);
                    $itemRender = $render->Item($itemHex);

                    print "<div class='required' title=\"{$itemRender->options}\">Crafting {$itemRender->name}</div>";
                    print "<div class='required-list'>";

                    if ($craft['required'] !== false) {
                        foreach ($craft['required'] as $required) {
                            $required['serial'] = 0;
                            $itemHex = $generate->Hex($required);
                            $itemRender = $render->Item($itemHex);

                            print "<div class='req-item'>";
                            print "<div class='item' title=\"{$itemRender->options}\" style=\"background: url('{$itemRender->image}') center center/contain;\"></div>";
                            print "<div class='count'>{$required['count']}</div>";
                            print "</div>";
                        }
                    } else {
                        print "No items required";
                    }

                    print "</div>";

                    print "<div class='price'><div class='credits'>Credits " . number_format($craft['credits']) . "</div><div class='zen'>Zen " . number_format($craft['zen']) . "</div></div>";

                    print "<div class='craftButton'></div>";
                }
                ?>

            </div>
        </div>
    </div>


    <?php
}
    