<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\Tools\Cookie;

$user = new User;
$account = Cookie::Get('username');

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {
    print "<form action='characterStats'>";
    print "<table class='table-form'>";

    print "<tr><td style='text-align: center;'>";

    $fetch = $sql->FetchAll("SELECT * FROM [Character] WHERE [AccountID]=? ORDER BY [LevelUpPoint] DESC", [$account]);

    print "<select name='character'><option>-</option>";

    foreach ($fetch as $char) {
        print "<option>{$char->Name} Free points {$char->LevelUpPoint}</option>";
    }

    print "</select>";

    print "</td></tr>";
    print "<tr><td colspan='2' style='text-align: center;'><input type='submit' value='{$lang->Phrase('character-main', 'button')}' /></td></tr>";

    print "</table>";
    print "</form>";
} else {
    print Notice::Request($lang->Phrase('request', 'not-logged'));
}

print "</div>";
