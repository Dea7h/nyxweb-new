<?php

if (!defined('access')) {
    exit;
}

use Core\Main\User,
    Core\Tools\Cookie;

$user = new User;
$account = Cookie::Get('username');

print "<div class='content-sub-boxonly'>";

if ($user->Access()) {
    print "<form action='characterMain'>";
    print "<table class='table-form'>";

    $info = $sql->Fetch("SELECT [mainCharacter] FROM [MEMB_INFO] WHERE [memb___id]=?", [$account]);

    print "<tr><td colspan='2' style='text-align: center; padding-bottom: 5px;'>{$lang->Phrase('character-main', 'current')} <b>" . (empty($info->mainCharacter) ? '-' : $info->mainCharacter) . "</b></td></tr>";

    print "<tr><td>{$lang->Phrase('character-main', 'character')}</td><td style='text-align: left;'>";

    $fetch = $sql->FetchAll("SELECT [Name] FROM [Character] WHERE [AccountID]=?", [$account]);

    print "<select name='character'><option>-</option>";

    foreach ($fetch as $char) {
        print "<option " . ($char->Name == $info->mainCharacter ? 'selected' : '') . ">{$char->Name}</option>";
    }

    print "</select>";

    print "</td></tr>";
    print "<tr><td colspan='2' style='text-align: center;'><input type='submit' value='{$lang->Phrase('character-main', 'button')}' /></td></tr>";

    print "</table>";
    print "</form>";
} else {
    print Notice::Request($lang->Phrase('request', 'not-logged'));
}

print "</div>";
