<?php
if (!defined('access')) {
    exit;
}
?>
<script type="text/javascript">
    var events = [
        ['Blood Castle', '00:00', '02:00', '04:00', '06:00', '08:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', '22:00'],
        ['Devil Square', '01:00', '03:00', '05:00', '07:00', '09:00', '11:00', '13:00', '15:00', '17:00', '19:00', '21:00', '23:00'],
        ['Chaos Castle', '00:30', '04:30', '08:30', '12:30', '16:30', '20:30'],
        ['White Wizard', '00:10', '04:10', '08:10', '12:10', '16:10', '20:10', '22:10'],
        ['Golden Invasion', '10:00', '14:00', '18:00', '22:00'],
        ['Skeleton King', '10:25', '14:25', '18:25', '22:25']
    ];

    function eventTimers() {
        var eventsList = '', time = getServerTime(), secs = (time.hours * 60 + time.minutes) * 60 + time.seconds;
        for (i = 0; i < events.length; i++) {
            var event = events[i];
            for (t = 1; t < event.length; t++) {
                var hour = event[t].split(':'), dSecs = (parseInt(hour[0]) * 60 + parseInt(hour[1])) * 60;
                if (dSecs > secs) {
                    var nextSeconds = dSecs, id = t;
                    break;
                }

                if (t === (event.length - 1)) {
                    var hour = event[1].split(':'), nextSeconds = ((parseInt(hour[0]) * 60 + parseInt(hour[1])) * 60 + 86400), id = 1;
                }
            }

            var nextSeconds = nextSeconds - secs, hours = addZero(Math.floor(nextSeconds / 60 / 60)), minutes = addZero(Math.floor((nextSeconds - hours * 60 * 60) / 60)), seconds = addZero(Math.floor(nextSeconds - minutes * 60 - hours * 60 * 60));

            eventsList = eventsList + '<div class="eventBlock"><div class="title">' + event[0] + '<span style="float: right;">' + event[id] + '</span></div><div class="bottom"><?= $lang->Phrase('events', 'starting') ?><span style="float: right;">' + (nextSeconds <= 300 ? "<font style='color: #8e3535 !important;'>" + hours + '<?= $lang->Phrase('events', 'time-hours') ?>' + minutes + '<?= $lang->Phrase('events', 'time-minutes') ?>' + seconds + "<?= $lang->Phrase('events', 'time-seconds') ?></font>" : hours + '<?= $lang->Phrase('events', 'time-hours') ?>' + minutes + '<?= $lang->Phrase('events', 'time-minutes') ?>' + seconds + '<?= $lang->Phrase('events', 'time-seconds') ?>') + '</span></div></div>';
        }
        $('.eventsList').html(eventsList);
        $('#serverTime').html('<?= $lang->Phrase('events', 'server-time') ?> ( ' + addZero(time.hours) + ':' + addZero(time.minutes) + ':' + addZero(time.seconds) + ' )');
    }

    eventTimers();
    setInterval(function () {
        eventTimers();
    }, 500);
</script>
<div class="content-main-title" id="serverTime">Loading...</div>
<div class="content-main-box">
    <div class="content-sub-boxonly" style="padding: 0;">
        <div class="eventsList"></div>
    </div>
</div>