<?php
if (!defined('access')) {
    exit;
}

$server = $config->Get('Server');
$nyx = $config->Get('NYX');

$fp = @fsockopen($server->server, $server->GameServerPort, $errno, $errstr, $server->timeOut);
if (!$fp) {
    $status = false;
} else {
    $status = true;
    fclose($fp);
    
    $online = $sql->Count("SELECT COUNT(*) FROM [MEMB_STAT] WHERE [ConnectStat]='1'");
}

$accounts = $sql->Count("SELECT COUNT(*) FROM [MEMB_INFO]");
$characters = $sql->Count("SELECT COUNT(*) FROM [Character]");
?>
<div class="content-sub-boxonly information">
    <div class="line"><?= $nyx['server-name'] ?><div class="right"><span class="<?= $status ? 'online' : 'offline' ?>"><?= $status ? ($lang->Phrase('information', 'online') . ' ' . $online . '/' . $server->maxOnline) : $lang->Phrase('information', 'offline') ?></span></div></div>
    <div class="line"><?= $lang->Phrase('information', 'version') ?><div class="right"><span><?= $nyx['server-version'] ?></span></div></div>
    <div class="line"><?= $lang->Phrase('information', 'expNdrop') ?><div class="right"><span><?= $nyx['server-exp'] ?> & <?= $nyx['server-drop'] ?></span></div></div>
    <div class="line"><?= $lang->Phrase('information', 'accounts') ?><div class="right"><span><?= $accounts ?></span></div></div>
    <div class="line"><?= $lang->Phrase('information', 'characters') ?><div class="right"><span><?= $characters ?></span></div></div>
</div>