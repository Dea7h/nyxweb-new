<?php
if (!defined('access')) {
    exit;
}
?>
<div class="live-feed">
    <?php
    $fetch = $sql->FetchAll("SELECT TOP 5 * FROM [NYX_LIVEFEED] ORDER BY [time] DESC");
    foreach ($fetch as $obj) {
        print "<div><font style='color: #317fb6;'>{$obj->name}:</font> {$obj->message} <time class='timeago' style='color: #727272;' datetime='" . (date('c', $obj->time)) . "' title='" . (date('d/m/Y H:i:s', $obj->time)) . "'></time></div>";
    }
    ?>
</div>