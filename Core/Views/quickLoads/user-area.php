<?php
if (!defined('access')) {
    exit;
}

use Core\Tools\Cookie;
?>
<?php
if ($user->Access()) {
    $fetch = $sql->Fetch("SELECT * FROM [NYX_RESOURCES] WHERE [account]=?", [Cookie::Get('username')]);
    ?>
    <div class="content-main-title dissapearOnMobile">
        <?= $lang->Phrase('user', 'resources') ?>
    </div>
    <div class="content-main-box dissapearOnMobile">
        <div class="user-area-resources" ajax="/user-area/account/resources">
            <?php
            $config = $config::Get('Resources');

            foreach ($config as $column => $res) {
                if ($res['slots'] == 1) {
                    print "<div class='block' style=\"background: url('/assets/images/itemImage/{$res['image']}') no-repeat center/80%;\" title='{$res['name']}'><span>{$fetch->$column}</span></div>";
                } else {
                    print "<div class='block' style='width: " . ($res['slots'] * 27.5 + ($res['slots'] - 1) * 3) . "px; text-align: center; line-height: 27.5px;'>{$res['name']} " . number_format($fetch->$column) . "</div>";
                }
            }
            ?>
        </div>
    </div>

    <div class="content-main-title">
        <?= $lang->Phrase('user', 'user-area') ?>
    </div>
    <div class="content-main-box">
        <div class="user-area-logout"><?= $lang->Phrase('user', 'greetings') ?> <b><?= Cookie::Get('username') ?></b><input type="button" value="<?= $lang->Phrase('user', 'logout') ?>" style="float: right;" onClick="setCookie('username'); setCookie('password'); quickLoad('user-area');" /></div>
        <div class="content-sub-boxonly user-area-menu">
            <div class="main" onclick="showHide('#menu1', '.user-submenu', 'fast');"><?= $lang->Phrase('user', 'menu-account') ?> <div class="notice-right"><span class="notice-circle">1</span></div></div>
            <div class="user-submenu" id="menu1" style="display: block;">
                <div class="line" ajax="/user-area/account/password">&#10003; <?= $lang->Phrase('user', 'account-password') ?></div>
                <div class="line" ajax="/user-area/account/storage">&#10003; <?= $lang->Phrase('user', 'account-storage') ?></div>
                <div class="line" ajax="/user-area/account/resources">&#10003; <?= $lang->Phrase('user', 'account-resources') ?></div>
                <div class="line" ajax="/user-area/account/logs">&#10003; <?= $lang->Phrase('user', 'account-logs') ?></div>
            </div>
            <div class="main" onclick="showHide('#menu2', '.user-submenu', 'fast');"><?= $lang->Phrase('user', 'menu-character') ?></div>
            <div class="user-submenu" id="menu2">
                <div class="line" ajax="/user-area/character/main">&#10003; <?= $lang->Phrase('user', 'character-main') ?></div>
                <!--<div class="line" ajax="/user-area/character/reset"><?= $lang->Phrase('user', 'character-reset') ?></div>-->
                <div class="line" ajax="/user-area/character/stats"><?= $lang->Phrase('user', 'character-stats') ?></div>
                <div class="line" ajax="/user-area/character/profile"><?= $lang->Phrase('user', 'character-profile') ?></div>
            </div>
            <div class="main" onclick="showHide('#menu3', '.user-submenu', 'fast');"><?= $lang->Phrase('user', 'menu-special') ?></div>
            <div class="user-submenu" id="menu3">
                <div class="line" ajax="/user-area/special/guild"><?= $lang->Phrase('user', 'special-guild') ?></div>
                <div class="line" ajax="/user-area/special/quest"><?= $lang->Phrase('user', 'special-quest') ?></div>
                <div class="line" ajax="/user-area/special/trivia">TRIVIA</div>
                <div class="line" ajax="/user-area/special/craft"><?= $lang->Phrase('user', 'special-craft') ?></div>
                <div class="line" ajax="/user-area/special/chest"><?= $lang->Phrase('user', 'special-chest') ?></div>
                <div class="line" ajax="/user-area/special/recycle"><?= $lang->Phrase('user', 'special-recycle') ?></div>
                <div class="line" ajax="/user-area/special/market"><?= $lang->Phrase('user', 'special-market') ?></div>
            </div>
            <div class="main" onclick="showHide('#menu4', '.user-submenu', 'fast');" style="border-bottom: 0;"><?= $lang->Phrase('user', 'menu-premium') ?></div>
            <div class="user-submenu" id="menu4">
                <div class="line" ajax="/user-area/premium/credits"><?= $lang->Phrase('user', 'premium-credits') ?></div>
                <div class="line" ajax="/user-area/premium/name"><?= $lang->Phrase('user', 'premium-name') ?></div>
                <div class="line" ajax="/user-area/premium/class"><?= $lang->Phrase('user', 'premium-class') ?></div>
                <div class="line" ajax="/user-area/premium/vip"><?= $lang->Phrase('user', 'premium-vip') ?></div>
            </div>
        </div>
    </div>
    <?php
} else {
    ?>
    <div class="content-main-title">
        <?= $lang->Phrase('user', 'user-area') ?>
    </div>
    <div class="content-main-box">
        <div class="content-sub-boxonly user-area">
            <div class="loginShowHide" id="user-area-login">
                <form action="login">
                    <div class="left">
                        <input type="text" name="account" placeholder="<?= $lang->Phrase('user', 'account') ?>" minlength="4" maxlength="10" autocomplete="off" required />
                        <input type="password" name="password" placeholder="<?= $lang->Phrase('user', 'password') ?>"  minlength="4" maxlength="10" autocomplete="off" required />
                    </div>
                    <div class="right">
                        <input type="submit" value="<?= $lang->Phrase('user', 'login') ?>" />
                    </div>
                </form>
                <input type="button" value="<?= $lang->Phrase('user', 'new-account') ?> &#8594;" onclick="showHide('#user-area-register', '.loginShowHide');" />
                <input type="button" value="<?= $lang->Phrase('user', 'forgotten-password') ?> &#8594;" onclick="showHide('#user-area-forgotten', '.loginShowHide');" />
            </div>
            <div class="loginShowHide loginFields" id="user-area-register">
                <form action="register">
                    <input type="text" name="account" placeholder="<?= $lang->Phrase('user', 'account') ?>" minlength="4" maxlength="10" autocomplete="off" required />
                    <input type="password" name="password" placeholder="<?= $lang->Phrase('user', 'password') ?>" minlength="4" maxlength="10" autocomplete="off" required />
                    <input type="password" name="re-password" placeholder="<?= $lang->Phrase('user', 're-password') ?>" minlength="4" maxlength="10" autocomplete="off" required />
                    <input type="email" name="email" placeholder="<?= $lang->Phrase('user', 'email') ?>" minlength="5" maxlength="35" autocomplete="off" required />
                    <input type="submit" value="<?= $lang->Phrase('user', 'register') ?>" />
                    <input type="button" value="&#8592; <?= $lang->Phrase('user', 'login') ?>" onclick="showHide('#user-area-login', '.loginShowHide');" />
                </form>
            </div>
            <div class="loginShowHide loginFields" id="user-area-forgotten">
                <form action="forgotten">
                    <input type="text" name="account" placeholder="<?= $lang->Phrase('user', 'account') ?>" minlength="4" maxlength="10" autocomplete="off" required />
                    <input type="email" name="email" placeholder="<?= $lang->Phrase('user', 'email') ?>" minlength="5" maxlength="35" autocomplete="off" required />
                    <input type="submit" value="<?= $lang->Phrase('user', 'send-email') ?>" />
                    <input type="button" value="&#8592; <?= $lang->Phrase('user', 'login') ?>" onclick="showHide('#user-area-login', '.loginShowHide');" />
                </form>
            </div>
        </div>
    </div>
    <?php
}
