<?php

if (!defined('access')) {
    exit;
}

use Core\Tools\Cookie,
    Core\Tools\Purify,
    Core\Tools\Config,
    Core\Main\User;

$request = $GLOBALS['request'];
$account = Cookie::Get('username');
$config = Config::Get('News');

function putEmojis($text, $emojis) {
    foreach ($emojis as $key => $emoji) {
        $text = str_replace($key, $emoji, $text);
    }
    return $text;
}

if (isset($request[1]) && Purify::_num($request[1])) {
    $fetch = $sql->FetchAll("SELECT * FROM [NYX_NEWS] WHERE [id]=?", [$request[1]]);
} else if (isset($request[1])) {
    $fetch = $sql->FetchAll("SELECT * FROM [NYX_NEWS] WHERE [type]=? ORDER BY [time] DESC", [$request[1]]);
} else {
    $fetch = $sql->FetchAll("SELECT * FROM [NYX_NEWS] ORDER BY [time] DESC");
}

if (!empty($fetch)) {
    $language = Cookie::Get('lang');

    foreach ($fetch as $obj) {
        if ($language !== false && $language !== 'English' && isset($obj->{'title_' . $language})) {
            $obj->title = $obj->{'title_' . $language};
            $obj->message = $obj->{'message_' . $language};
        }

        $comments = $obj->comments == 1 ? "<font ajax='/news/{$obj->id}'>{$lang->Phrase('news', 'comments')} ( <b style='color: #317fb6;'>" . $sql->Count("SELECT COUNT(*) FROM [NYX_NEWS_COMMENTS] WHERE [article_id]=?", [$obj->id]) . "</b> )</font>" : $lang->Phrase('news', 'comments-disabled');
        $output = "<div class='content-sub-title news-title'><span class='tag-block tag-{$obj->type}' ajax='/news/{$obj->type}'>[{$obj->type}]</span> <font ajax='/news/{$obj->id}'>{$obj->title}</font></div>";
        $output .= "<div class='content-sub-box news-content'>" . putEmojis($obj->message, $config['emojis']) . "</div>";
        $output .= "<div class='news-info'>";
        $output .= "<div class='comments'>{$comments}</div>";
        $output .= "<div class='posted'>{$lang->Phrase('news', 'posted-by')} <font style='color: #317fb6;' ajax='/profile/{$obj->author}'>{$obj->author}</font> <time class='timeago' datetime='" . (date('c', $obj->time)) . "' title='" . (date('d/m/Y H:i:s', $obj->time)) . "'></time></div>";
        $output .= "</div>";

        print $output;
    }

    if (isset($request[1]) && Purify::_num($request[1])) {
        if ($obj->comments == 1) {
            $user = new User;
            if ($user->Access()) {
                print "<div class='news-reply-block'>";

                print "<div style='overflow: hidden; background-color: #232220; margin-bottom: 5px;'><div class='reply' onClick=\"$('#reply').css('background-color', 'transparent').text('-'); $('input[name=reply]').val('');\">{$lang->Phrase('news', 'post-reply')} <span id='reply' style='padding: 0 5px 0 5px;'>-</span></div><div class='emojis'>";

                foreach ($config['emojis'] as $name => $code) {
                    print "<font title='{$name}' onclick=\"$('#putEmojisHere').val($('#putEmojisHere').val() + ' {$name}').focus();\" style='cursor: pointer;'>" . $code . "</font>";
                }

                print "</div></div>";

                print "<div style='overflow: hidden;'><form action='news'>";
                print "<textarea placeholder='{$lang->Phrase('news', 'post-comment')}' name='message' id='putEmojisHere'></textarea>";
                print "<input type='submit' value='{$lang->Phrase('news', 'post')}' />";
                print "<input type='hidden' name='reply' />";
                print "<input type='hidden' name='news' value='{$obj->id}' />";

                print "</form></div>";
                print "</div>";
            }

            $fetch = $sql->FetchAll("SELECT * FROM [NYX_NEWS_COMMENTS] WHERE [article_id]=? AND [reply]='0' ORDER BY [rate] DESC, [time] DESC", [$obj->id]);
            if (!empty($fetch)) {
                foreach ($fetch as $comm) {
                    $check = $sql->Fetch("SELECT [vote] FROM [NYX_NEWS_COMMENTS_VOTES] WHERE [id]=? AND [account]=?", [$comm->id, $account]);
                    $voteUP = false;
                    $voteDOWN = false;
                    if (!empty($check)) {
                        if ($check->vote == 'up') {
                            $voteUP = true;
                        } else {
                            $voteDOWN = true;
                        }
                    }

                    print "<div class='news-comment'>";
                    print "<div class='posted-by'><div class='arrow up' " . ($voteUP ? "style='border-bottom: 13px solid #477110;'" : '') . "  title='{$lang->Phrase('news', 'vote-up')}' onClick=\"request('news', {comment: '{$comm->id}', vote: 'up'});\"></div> <span class='rate-circle'>" . ($comm->rate > 0 ? '+' . $comm->rate : $comm->rate) . "</span> <div style='float: left; padding-left: 5px;'>{$lang->Phrase('news', 'by')} <font style='color: #317fb6;' ajax='/profile/{$comm->author}'>{$comm->author}</font> <time class='timeago' datetime='" . (date('c', $comm->time)) . "' title='" . (date('d/m/Y H:i:s', $comm->time)) . "'></time></div><div style='float: right; cursor: pointer;' onClick=\"$('#reply').css('background-color', '#62921e').text('#' + {$comm->id}); $('input[name=reply]').val({$comm->id});\">{$lang->Phrase('news', 'reply')} <img src='/assets/images/icons/reply.png' style='width: 10px;' /></div></div>";
                    print "<div class='message'><div class='arrow down' " . ($voteDOWN ? "style='border-top: 13px solid #984848;'" : '') . " title='{$lang->Phrase('news', 'vote-down')}' onClick=\"request('news', {comment: '{$comm->id}', vote: 'down'});\"></div> <div style='padding: 5px;'>" . putEmojis($comm->message, $config['emojis']) . "</div></div>";
                    print "</div>";

                    $replys = $sql->FetchAll("SELECT * FROM [NYX_NEWS_COMMENTS] WHERE [reply]=? ORDER BY [rate] DESC, [time] DESC", [$comm->id]);
                    if (!empty($replys)) {
                        print "<div class='reply-block'>";
                        print "<div class='reply-line'><div class='line'></div></div>";

                        foreach ($replys as $reply) {
                            $check = $sql->Fetch("SELECT [vote] FROM [NYX_NEWS_COMMENTS_VOTES] WHERE [id]=? AND [account]=?", [$reply->id, $account]);
                            $voteUP = false;
                            $voteDOWN = false;
                            if (!empty($check)) {
                                if ($check->vote == 'up') {
                                    $voteUP = true;
                                } else {
                                    $voteDOWN = true;
                                }
                            }
                            print "<div class='news-comment reply'>";
                            print "<div class='circle'></div>";
                            print "<div class='posted-by'><div class='arrow up' " . ($voteUP ? "style='border-bottom: 13px solid #477110;'" : '') . " title='{$lang->Phrase('news', 'vote-up')}' onClick=\"request('news', {comment: '{$reply->id}', vote: 'up'});\"></div> <span class='rate-circle'>" . ($reply->rate > 0 ? '+' . $reply->rate : $reply->rate) . "</span> <div style='float: left; padding-left: 5px;'>{$lang->Phrase('news', 'by')} <font style='color: #317fb6;' ajax='/profile/{$reply->author}'>{$reply->author}</font> <time class='timeago' datetime='" . (date('c', $reply->time)) . "' title='" . (date('d/m/Y H:i:s', $reply->time)) . "'></time></div></div>";
                            print "<div class='message'><div class='arrow down' " . ($voteDOWN ? "style='border-top: 13px solid #984848;'" : '') . " title='{$lang->Phrase('news', 'vote-down')}' onClick=\"request('news', {comment: '{$reply->id}', vote: 'down'});\"></div> <div style='padding: 5px;'>" . putEmojis($reply->message, $config['emojis']) . "</div></div>";
                            print "</div>";
                        }
                        print "</div>";
                    }
                }
            } else {
                print "<div class='content-sub-boxonly'>{$lang->Phrase('news', 'no-comments')}</div>";
            }
        } else {
            print "<div class='content-sub-boxonly'>{$lang->Phrase('news', 'comments-disabled')}</div>";
        }
    }
} else {
    print "<div class='content-sub-boxonly'>{$lang->Phrase('news', 'no-results')}</div>";
}
