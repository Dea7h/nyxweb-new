<?php
if (!defined('access')) {
    exit;
}

use Core\Tools\Purify;
use Core\Main\Guild;
use Core\Main\Character;
use Core\Main\Item\Render;

$quests = $config->Get('Quests');
$guild = new Guild;
$char = new Character;
$render = new Render;

$charID = Purify::_post($GLOBALS['request'][1], 10);

$fetch = $sql->Fetch("SELECT * FROM [Character] WHERE [Name]=?", [$charID]);

if (!empty($fetch)) {

    $class = $char->_Class($fetch->Class);

    //Experience bar
    if ($fetch->cLevel > 255) {
        $exp = (($fetch->cLevel - 255) + 9) * ($fetch->cLevel - 255) * ($fetch->cLevel - 255) * 1000;
    } else {
        $exp = ($fetch->cLevel + 9) * $fetch->cLevel * $fetch->cLevel * 10;
    }

    $expPercent = $fetch->Experience > $exp ? 100 : ($fetch->Experience < 0 ? 0 : floor(($fetch->Experience / $exp) * 100));

    //Location
    if ($char->Status($fetch->Name)) {
        $location = $char->Map($fetch->MapNumber) . " ( {$fetch->MapPosX}/{$fetch->MapPosY} )";
    } else {
        $stat = $sql->Fetch("SELECT [DisConnectTM] FROM [MEMB_STAT] WHERE [memb___id]=?", [$fetch->AccountID]);
        $location = !empty($stat) ? "<time class='timeago' datetime='" . date('c', strtotime($stat->DisConnectTM)) . "'></time>" : 'never';
    }

    //Guild
    $guild = $guild->Info($fetch->Name);
    if ($guild->name) {
        $guild->name = "<font style='color: #ba8615;'>[</font><font style='color: #727171;'>{$guild->name}</font><font style='color: #ba8615;'>]</font>";
    }

    //NYX_RESOURCES
    $nyx = $sql->Fetch("SELECT * FROM [NYX_RESOURCES] WHERE [account]=?", [$fetch->AccountID]);

    //Bonus Points
    $bonus = $quests['bonusPoints'] * isset($nyx->QuestNumber) ? $nyx->QuestNumber : 1;

    //Total Points
    $totalPoints = number_format($fetch->Strength + $fetch->Dexterity + $fetch->Vitality + $fetch->Energy + $fetch->Leadership + $fetch->LevelUpPoint + $bonus);

    //Profile Message
    $message = empty($fetch->profileMessage) ? 'No meesage' : $fetch->profileMessage;

    if (!empty($fetch->profileImage)) {
        print "<div style='text-align: center; background: #131313; border-bottom: 2px solid #477110;'><img src='{$fetch->profileImage}' onclick=\"window.open($(this).attr('src'));\" style='max-width: 100%; max-height: 250px;' /></div>";
    }

    print "<div class='content-sub-boxonly'>";

    print "<div style=\"position: relative; width: 100%; height: 330px; background: url('/assets/images/others/characters/profiles/{$class['image']}.jpg') no-repeat center/100% 100%;\">";

    print "<div style='position: absolute; top: 10px; left: 10px; padding: 0 4px 0 4px; background-color: rgba(0,0,0,0.3); border-radius: 5px; border: 1px solid #0f0f0f;' title='Profile views'>{$fetch->profileViews}</div>";

    print "<div style='position: absolute; overflow: hidden; right: 10px; top: 10px; width: 50%; background-color: rgba(0,0,0,0.3); border-radius: 5px; border: 1px solid #0f0f0f;'>";

    print "<div style='padding: 5px; font-size: 20px; text-align: center;'>{$guild->name} {$fetch->Name}</div>";

    print "<div style='padding: 5px; font-size: 15px; border-top: 1px solid #0f0f0f;'>Level<div style='float: right;'>{$fetch->cLevel} ( {$expPercent}% )</div></div>";
    print "<div style='padding: 5px; font-size: 15px; border-top: 1px solid #0f0f0f;'>Total Points<div style='float: right;'>{$totalPoints} ( <b title='Quests Bonus Points'>{$bonus}</b> )</div></div>";
    print "<div style='padding: 5px; font-size: 15px; border-top: 1px solid #0f0f0f;'>Last seen<div style='float: right;'>{$location}</div></div>";
    print "<div style='padding: 5px; font-size: 15px; border-top: 1px solid #0f0f0f;'>Player kills<div style='float: right;'>{$fetch->PkCount}</div></div>";

    print "</div>";

    print "<div style='position: absolute; overflow: hidden; left: 0; bottom: 0; width: 100%;'>";

    print "<div style='padding: 5px;'><div style='background-color: rgba(0,0,0,0.3); border-radius: 5px; border: 1px solid #0f0f0f; padding: 5px;'>{$message}</div></div>";

    print "</div>";

    print "</div>";

    print "</div>";

    print "<div class='content-sub-boxonly'>";

    print "<div style=\"position: relative; margin: auto; width: 420px; height: 380px; background: url('/assets/images/others/characters/profiles/inv_{$class['image']}.png') no-repeat center/100% 100%; border-radius: 5px; border: 1px solid #0f0f0f;\">";

    //Inventory rendering
    $inventory = $char->Inventory($fetch->Inventory);

    $inv = [];
    foreach ($inventory as $part => $hex) {
        if (!$inv[$part] = $render->Item($hex)) {
            $inv[$part] = [
                'image' => '',
                'options' => ''
            ];
        }
    }

    print "<div style=\"position: absolute; top: 24px; left: 3px; width: 70px; height: 70px; background: url('{$inv['pet']->image}') no-repeat center center;\" title=\"{$inv['pet']->options}\"></div>";
    print "<div style=\"position: absolute; top: 24px; left: 80px; width: 70px; height: 70px; background: url('{$inv['helm']->image}') no-repeat center center;\" title=\"{$inv['helm']->options}\"></div>";
    print "<div style=\"position: absolute; top: 11px; left: 245px; width: 164px; height: 100px; background: url('{$inv['wings']->image}') no-repeat center center;\" title=\"{$inv['wings']->options}\"></div>";
    print "<div style=\"position: absolute; top: 114px; left: 45px; width: 70px; height: 102px; background: url('{$inv['armor']->image}') no-repeat center center;\" title=\"{$inv['armor']->options}\"></div>";
    print "<div style=\"position: absolute; top: 126px; left: 284px; width: 38px; height: 38px; background: url('{$inv['pendant']->image}') no-repeat center center;\" title=\"{$inv['pendant']->options}\"></div>";
    print "<div style=\"position: absolute; top: 146px; left: 340px; width: 70px; height: 70px; background: url('{$inv['gloves']->image}') no-repeat center center;\" title=\"{$inv['gloves']->options}\"></div>";
    print "<div style=\"position: absolute; top: 236px; left: 10px; width: 70px; height: 134px; background: url('{$inv['leftWeapon']->image}') no-repeat center center;\" title=\"{$inv['leftWeapon']->options}\"></div>";
    print "<div style=\"position: absolute; top: 242px; left: 100px; width: 38px; height: 38px; background: url('{$inv['leftRing']->image}') no-repeat center center;\" title=\"{$inv['leftRing']->options}\"></div>";
    print "<div style=\"position: absolute; top: 242px; left: 282px; width: 38px; height: 38px; background: url('{$inv['rightRing']->image}') no-repeat center center;\" title=\"{$inv['rightRing']->options}\"></div>";
    print "<div style=\"position: absolute; top: 300px; left: 88px; width: 70px; height: 70px; background: url('{$inv['pants']->image}') no-repeat center center;\" title=\"{$inv['pants']->options}\"></div>";
    print "<div style=\"position: absolute; top: 300px; left: 263px; width: 70px; height: 70px; background: url('{$inv['boots']->image}') no-repeat center center;\" title=\"{$inv['boots']->options}\"></div>";
    print "<div style=\"position: absolute; top: 236px; left: 340px; width: 70px; height: 134px; background: url('{$inv['rightWeapon']->image}') no-repeat center center;\" title=\"{$inv['rightWeapon']->options}\"></div>";

    print "</div>";

    print "</div>";
} else {
    print "<div style='border: 1px solid #1b1b1b; margin: 5px;'><img src='/assets/images/others/404.jpg' style='width: 100%;' /></div>";
}

exit;
//Country Flag
$user = $sql->Fetch("SELECT [sno__numb] FROM [MEMB_INFO] WHERE [memb___id]=?", [$obj->AccountID]);
$flag = "<img src='/assets/images/icons/flags/" . (!empty($user->sno__numb) ? $user->sno__numb : 'unknown') . ".png' />";