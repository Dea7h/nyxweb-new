<!DOCTYPE html>
<html>
    <head>
        <title>Nyx MuOnline</title>
        <meta charset="UTF-8">
        <meta name="description" content="MuOnline Season 1 99.62 hardcore old school server">
        <meta name="keywords" content="MuOnline, Season 1, 97d, 99.62, Old school, Hardcore, Fun, New features">
        <meta name="author" content="Dea7h">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="/assets/style/tooltipster.bundle.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/style/jquery-ui.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/style/main.css?r=<?= time() ?>">
        <link rel="icon" type="image/png" href="/assets/images/icons/favicon.png">
    </head>
    <body>
        <div class="main-header dissapearOnMobile">
            <div class="main-container relative"></div>
        </div>
        <div class="top-bar">
            <div class="main-container">
                <div class="menu">
                    <div ajax="/news"><?= $lang->Phrase('menu', 'news') ?></div>
                    <div ajax="/game-files"><?= $lang->Phrase('menu', 'game-files') ?></div>
                    <div ajax="/rankings"><?= $lang->Phrase('menu', 'rankings') ?></div>
                    <div ajax="/statistics"><?= $lang->Phrase('menu', 'statistics') ?></div>
                    <div ajax="/about-us"><?= $lang->Phrase('menu', 'about-us') ?></div>
                    <div onclick="window.open('http://darksteam.net/', '_blank');"><?= $lang->Phrase('menu', 'forums') ?></div>
                </div>
                <div class="language-field dissapearOnMobile">
                    <img src="/assets/images/icons/flags/en.png" onclick="setCookie('lang', 'English'); location.reload();" />
                    <img src="/assets/images/icons/flags/bg.png" onclick="setCookie('lang', 'Bulgarian'); location.reload();" />
                </div>
            </div>
        </div>
        <div class="main-container">
            <div class="main-middle">
                <div class="content-left">
                    <div class="content-main-title"><?= $lang->Phrase('information', 'information') ?></div>
                    <div class="content-main-box" quickLoad="information" autoRefresh="60">Loading...</div>
                    <div quickLoad="events"></div>
                </div>
                <div class="content-middle">
                    <div class="content-middle-title">
                        <?php
                        $menus = $config->Get('subMenus');
                        foreach ($menus as $key => $menu) {
                            print "<div class='submenuBlock' id='submenu-{$key}'>";
                            foreach ($menu as $name => $link) {
                                print "<span class='submenu' ajax='{$link}'>{$name}</span>";
                            }
                            print "</div>";
                        }
                        ?>
                    </div>
                    <div class="content-main-box">
                        <div id="ajaxContent"></div>
                    </div>
                </div>
                <div class="content-right">
                    <div quickLoad="user-area">Loading...</div>
                </div>
            </div>
            <div class="ajaxLoader"></div>
        </div>
        <div class="bottom-bar">
            <div class="footer">
                <div class="title">NyxMu Online Gaming <?= date('Y') ?></div>
                <div class="desc">NyxWeb created by Dea7h | Server files by <a href="http://darksteam.net/" target="_blank">www.DarksTeam.net</a></div>
            </div>
        </div>
    </body>
    <div id="ajaxRequests" onclick="$(this).fadeOut();"></div>
    <input type="hidden" id="momentTimezone" value="<?= $config->Get('NYX')['timezone'] ?>" />
    <script type="text/javascript" src="/assets/javascript/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="/assets/javascript/moment.min.js"></script>
    <script type="text/javascript" src="/assets/javascript/moment-timezone-with-data-2012-2022.js"></script>
    <script type="text/javascript" src="/assets/javascript/tooltipster.bundle.min.js"></script>
    <script type="text/javascript" src="/assets/javascript/main.js?r=<?= time() ?>"></script>
</html>