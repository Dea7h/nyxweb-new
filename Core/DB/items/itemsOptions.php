<?php

return [
    'exo' => [
        'swords' => [
            1 => 'Increase Mana per kill +8',
            2 => 'Increase hit points per kill +8',
            3 => 'Increase attacking(wizardly)speed+7',
            4 => 'Increase Damage +2%',
            5 => 'Increase Damage +level/20',
            6 => 'Excellent Damage Rate +10%',
        ],
        'staffs' => [
            1 => 'Increase Mana per kill +8',
            2 => 'Increase hit points per kill +8',
            3 => 'Increase attacking(wizardly)speed+7',
            4 => 'Increase Wizardy Damage +2%',
            5 => 'Increase Wizardy Damage +level/20',
            6 => 'Excellent Damage Rate +10%',
        ],
        'armors' => [
            1 => 'Increase Zen After Hunt +40%',
            2 => 'Defense success rate +10%',
            3 => 'Reflect damage +5%',
            4 => 'Damage Decrease +4%',
            5 => 'Increase MaxMana +4%',
            6 => 'Increase MaxHP +4%',
        ],
        'wings' => [
            1 => 'Life +{wingsLife} Increased',
            2 => 'Mana +{wingsMana} Increased',
            3 => 'Ignore enemy\'s defenses by 3%',
            4 => '+50 MAX STAMINA',
            5 => 'Increase Attacking(wizardy)speed +7',
            6 => '',
        ],
        'cape' => [
            1 => 'Life +{wingsLife} increased',
            2 => 'Mana +{wingsMana} increased',
            3 => 'Ignore enemy\'s defenses by 3%',
            4 => 'Increase command +20',
            5 => '',
            6 => '',
        ]
    ],
    'add' => [
        'dmg' => 'Additional Damage',
        'deff' => 'Additional Defense',
        'wiz' => 'Additional Wizardy Damage',
        'rec' => 'Automatic HP Recovery',
        'dmgwiz' => 'Additional (Wizardy/Attack) Damage',
    ],
    'skill' => [
        'triple' => 'Triple Shot skill (Mana: 5)',
        'cyclon' => 'Cyclone Cutting skill (Mana:9)',
        'falling' => 'Falling Slash skill (Mana: 9)',
        'upper' => 'Uppercut skill (Mana: 10)',
        'lunge' => 'Lunge skill (Mana: 9)',
        'slashing' => 'Slashing skill (Mana: 10)',
        'power' => 'Power Slash skill (Mana: 15)',
        'force' => 'Force Wave skill (Mana: 10)',
        'defense' => 'Defense skill (Mana: 30)',
    ]
];