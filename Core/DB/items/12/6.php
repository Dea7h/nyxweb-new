<?php

return [
    'name' => 'Wings of Darkness',
    'x' => 4,
    'y' => 2,
    'class' => [48],
    'luck' => true,
    'exo' => 'wings'
];