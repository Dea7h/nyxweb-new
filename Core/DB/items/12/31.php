<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Jewel of Soul x10',
            'dur' => 'Jewel of Soul Bundle of 10'
        ],
        1 => [
            'name' => 'Jewel of Soul x20',
            'dur' => 'Jewel of Soul Bundle of 20'
        ],
        2 => [
            'name' => 'Jewel of Soul x30',
            'dur' => 'Jewel of Soul Bundle of 30'
        ]
    ]
];