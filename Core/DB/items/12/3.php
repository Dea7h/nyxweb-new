<?php

return [
    'name' => 'Butterfly Wings',
    'x' => 5,
    'y' => 3,
    'class' => [33],
    'luck' => true,
    'exo' => 'wings'
];