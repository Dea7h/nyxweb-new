<?php

return [
    'name' => 'Fairy Wings',
    'x' => 3,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'add' => 'rec'
];