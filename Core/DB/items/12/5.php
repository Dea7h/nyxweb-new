<?php

return [
    'name' => 'Dragon Wings',
    'x' => 3,
    'y' => 3,
    'class' => [17],
    'luck' => true,
    'exo' => 'wings'
];