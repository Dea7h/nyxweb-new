<?php

return [
    'name' => 'Archangel Wings',
    'x' => 5,
    'y' => 3,
    'class' => [1],
    'luck' => true,
    'exo' => 'wings'
];