<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Jewel of Bless x10',
            'dur' => 'Jewel of Bless Bundle of 10'
        ],
        1 => [
            'name' => 'Jewel of Bless x20',
            'dur' => 'Jewel of Bless Bundle of 20'
        ],
        2 => [
            'name' => 'Jewel of Bless x30',
            'dur' => 'Jewel of Bless Bundle of 30'
        ]
    ]
];