<?php

return [
    'name' => 'Jewel of Chaos',
    'x' => 1,
    'y' => 1,
    'dur' => 'It is used to combine Chaos items'
];