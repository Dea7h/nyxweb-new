<?php

return [
    'name' => 'Orb of Twisting Slash',
    'x' => 1,
    'y' => 1,
    'class' => [16, 48]
];