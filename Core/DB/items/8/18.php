<?php

return [
    'name' => 'Grand Soul Armor',
    'x' => 2,
    'y' => 3,
    'class' => [1],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];