<?php

return [
    'name' => 'Great Dragon Armor',
    'x' => 2,
    'y' => 3,
    'class' => [17],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];