<?php

return [
    'name' => 'Dark Master Armor',
    'x' => 2,
    'y' => 3,
    'class' => [64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];