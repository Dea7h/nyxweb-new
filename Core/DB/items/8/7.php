<?php

return [
    'name' => 'Sphinx Helm',
    'x' => 2,
    'y' => 3,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hera'
];