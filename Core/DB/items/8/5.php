<?php

return [
    'name' => 'Leather Armor',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Warrior'
];