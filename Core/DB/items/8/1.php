<?php

return [
    'name' => 'Dragon Armor',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hyon'
];