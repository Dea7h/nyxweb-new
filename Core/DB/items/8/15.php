<?php

return [
    'name' => 'Unicorn Armor',
    'x' => 2,
    'y' => 3,
    'class' => [48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Gaion'
];