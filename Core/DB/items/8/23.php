<?php

return [
    'name' => 'Hurricane Armor',
    'x' => 2,
    'y' => 3,
    'class' => [48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];