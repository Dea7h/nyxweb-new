<?php

return [
    'name' => 'Dark Phoenix Armor',
    'x' => 2,
    'y' => 3,
    'class' => [17],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];