<?php

return [
    'name' => 'Blood Fang',
    'x' => 1,
    'y' => 2,
    'dur' => 'Used for creating Blood Castle tickets'
];