<?php

return [
    'x' => 1,
    'y' => 2,
    'level' => [
        0 => [
            'name' => 'Blue Feather'
        ],
        1 => [
            'name' => 'Crest of Monarch'
        ]
    ]
];