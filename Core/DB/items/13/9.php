<?php

return [
    'name' => 'Ring of Poison',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'armors',
    'ancient' => 'Kantata'
];