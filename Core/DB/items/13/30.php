<?php

return [
    'name' => 'Cape of Lord',
    'x' => 2,
    'y' => 3,
    'class' => [64],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'cape'
];