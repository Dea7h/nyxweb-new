<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Energy Fruit',
        ],
        1 => [
            'name' => 'Stamina Fruit',
        ],
        2 => [
            'name' => 'Agility Fruit',
        ],
        3 => [
            'name' => 'Strength Fruit',
        ],
        4 => [
            'name' => 'Command Fruit',
        ]
    ]
];