<?php

return [
    'name' => 'Invisibility Cloak',
    'x' => 2,
    'y' => 2,
    'dur' => 'Ticket for Blood Castle entrance'
];