<?php

return [
    'name' => 'Scroll of Archangel',
    'x' => 1,
    'y' => 2,
    'dur' => 'Used for creating Blood Castle tickets'
];