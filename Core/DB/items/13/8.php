<?php

return [
    'name' => 'Ring of Ice',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'armors',
    'ancient' => 'Warrior'
];