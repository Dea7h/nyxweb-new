<?php

return [
    'name' => 'Guardian Angel',
    'x' => 1,
    'y' => 1,
    'dur' => "Life: [{durability}/255]<br />Reduced damage from monsters +30%<br />Increases max HP +50"
];