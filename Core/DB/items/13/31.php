<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Dark Horse Spirit',
            'image' => '31.gif',
        ],
        1 => [
            'name' => 'Dark Raven Spirit',
            'image' => 'raven.gif',
        ]
    ]
];