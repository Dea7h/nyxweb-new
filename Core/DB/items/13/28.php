<?php

return [
    'name' => 'Pendant of Ability',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'swords',
    'ancient' => 'Gywen'
];