<?php

return [
    'name' => 'Pendant of Ice',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'staffs',
    'ancient' => 'Apollo'
];