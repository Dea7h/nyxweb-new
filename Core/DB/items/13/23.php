<?php

return [
    'name' => 'Ring of Wind',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'armors',
    'ancient' => 'Kantata'
];