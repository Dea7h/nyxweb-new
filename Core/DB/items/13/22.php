<?php

return [
    'name' => 'Ring of Earth',
    'x' => 1,
    'y' => 1,
    'add' => 'rec',
    'exo' => 'armors',
    'ancient' => 'Ceto'
];