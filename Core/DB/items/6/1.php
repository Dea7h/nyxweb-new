<?php

return [
    'name' => 'Horn Shield',
    'x' => 2,
    'y' => 2,
    'class' => [0, 16, 32, 48, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];