<?php

return [
    'name' => 'Bronze Shield',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'defense',
    'add' => 'deff',
    'exo' => 'armors'
];