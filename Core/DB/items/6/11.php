<?php

return [
    'name' => 'Serpent Shield',
    'x' => 2,
    'y' => 2,
    'class' => [16, 32, 48, 64],
    'luck' => true,
    'skill' => 'defense',
    'add' => 'deff',
    'exo' => 'armors'
];