<?php

return [
    'name' => 'Grand Soul Shield',
    'x' => 2,
    'y' => 4,
    'class' => [1],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];