<?php

return [
    'name' => 'Fairy Shield',
    'x' => 2,
    'y' => 2,
    'class' => [32, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];