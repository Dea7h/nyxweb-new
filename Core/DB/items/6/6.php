<?php

return [
    'name' => 'Skull Shield',
    'x' => 2,
    'y' => 2,
    'class' => [0, 16, 32, 48, 64],
    'luck' => true,
    'skill' => 'defense',
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hera'
];