<?php

return [
    'name' => 'Big Round Shield',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'defense',
    'add' => 'deff',
    'exo' => 'armors'
];