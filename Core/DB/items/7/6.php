<?php

return [
    'name' => 'Scale Helm',
    'x' => 2,
    'y' => 2,
    'class' => [16, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Eplate'
];