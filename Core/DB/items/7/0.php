<?php

return [
    'name' => 'Bronze Helm',
    'x' => 2,
    'y' => 2,
    'class' => [16, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];