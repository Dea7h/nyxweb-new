<?php

return [
    'name' => 'Dragon Helm',
    'x' => 2,
    'y' => 2,
    'class' => [16],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hyon'
];