<?php

return [
    'name' => 'Guardian Helm',
    'x' => 2,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];