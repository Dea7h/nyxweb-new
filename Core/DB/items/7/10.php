<?php

return [
    'name' => 'Vine Helm',
    'x' => 2,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Ceto'
];