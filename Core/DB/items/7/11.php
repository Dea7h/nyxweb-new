<?php

return [
    'name' => 'Silk Helm',
    'x' => 2,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Gaia'
];