<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        1 => [
            'name' => 'Devil Square Invite +1',
            'image' => 'ds1.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
        2 => [
            'name' => 'Devil Square Invite +2',
            'image' => 'ds2.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
        3 => [
            'name' => 'Devil Square Invite +3',
            'image' => 'ds3.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
        4 => [
            'name' => 'Devil Square Invite +4',
            'image' => 'ds4.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
        5 => [
            'name' => 'Devil Square Invite +5',
            'image' => 'ds5.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
        6 => [
            'name' => 'Devil Square Invite +6',
            'image' => 'ds6.gif',
            'dur' => 'Ticket for Devil Square entrance'
        ],
    ]
];