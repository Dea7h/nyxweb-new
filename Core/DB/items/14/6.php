<?php

return [
    'name' => 'Large Mana Potion',
    'x' => 1,
    'y' => 1,
    'dur' => 'Amount: [{durability}/255]'
];