<?php

return [
    'name' => 'Medium Mana Potion',
    'x' => 1,
    'y' => 1,
    'dur' => 'Amount: [{durability}/255]'
];