<?php

return [
    'name' => 'Large Heal Potion',
    'x' => 1,
    'y' => 1,
    'dur' => 'Amount: [{durability}/255]'
];