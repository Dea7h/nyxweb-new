<?php

return [
    'name' => 'Devil Key',
    'x' => 1,
    'y' => 1,
    'dur' => 'Used for creating Devil Square tickets'
];