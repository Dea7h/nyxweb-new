<?php

return [
    'name' => 'Devil Eye',
    'x' => 1,
    'y' => 1,
    'dur' => 'Used for creating Devil Square tickets'
];