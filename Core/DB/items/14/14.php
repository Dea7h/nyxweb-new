<?php

return [
    'name' => 'Jewel of Soul',
    'x' => 1,
    'y' => 1,
    'dur' => 'Used to level up items to +9'
];