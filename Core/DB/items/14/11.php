<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Box of Luck',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        1 => [
            'name' => 'Star of Sacred Birth',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        2 => [
            'name' => 'Sack of Magic',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        3 => [
            'name' => 'Heart of Love',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        4 => [
            'name' => 'Broken Box',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        5 => [
            'name' => 'Silver Medal',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        6 => [
            'name' => 'Gold Medal',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        7 => [
            'name' => 'Box of Heaven',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        8 => [
            'name' => 'Box of Kundun +1',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        9 => [
            'name' => 'Box of Kundun +2',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        10 => [
            'name' => 'Box of Kundun +3',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        11 => [
            'name' => 'Box of Kundun +4',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        12 => [
            'name' => 'Box of Kundun +5',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ],
        13 => [
            'name' => 'Heart of Dark Lord',
            'dur' => 'Throw it and you may receive some Zen or Items'
        ]
    ]
];