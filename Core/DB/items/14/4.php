<?php

return [
    'name' => 'Small Mana Potion',
    'x' => 1,
    'y' => 1,
    'dur' => 'Amount: [{durability}/255]'
];