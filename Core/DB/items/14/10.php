<?php

return [
    'name' => 'Town Portal',
    'x' => 1,
    'y' => 2,
    'dur' => 'Teleports you in the town'
];