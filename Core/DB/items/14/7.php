<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Bless Siege Potion',
            'image' => '7.gif',
            'dur' => 'Amount: [{durability}/255]'
        ],
        1 => [
            'name' => 'Soul Siege Potion',
            'image' => '7.gif',
            'dur' => 'Amount: [{durability}/255]'
        ]
    ]
];