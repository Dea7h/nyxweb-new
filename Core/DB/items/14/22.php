<?php

return [
    'name' => 'Jewel of Creation',
    'x' => 1,
    'y' => 1,
    'dur' => 'This item is used for creating fruits'
];