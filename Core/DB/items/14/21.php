<?php

return [
    'x' => 1,
    'y' => 1,
    'level' => [
        0 => [
            'name' => 'Rena',
            'image' => '21.gif',
            'dur' => 'This item is used for events'
        ],
        1 => [
            'name' => 'Stone',
            'image' => 'stone.gif',
            'dur' => 'This item is used for events'
        ],
        2 => [
            'name' => 'Sign of Lord',
            'image' => 'sign.gif',
            'dur' => 'Amount: [{durability}/255]'
        ],
        3 => [
            'name' => 'Sign of Lord',
            'image' => 'sign.gif',
            'dur' => 'Amount: [{durability}/255]'
        ]
    ]
];