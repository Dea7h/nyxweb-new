<?php

return [
    'name' => 'Great Reign Crossbow',
    'x' => 2,
    'y' => 3,
    'class' => [33],
    'luck' => true,
    'skill' => 'triple',
    'add' => 'dmg',
    'exo' => 'swords'
];