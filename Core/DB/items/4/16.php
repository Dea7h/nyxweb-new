<?php

return [
    'name' => 'Saint Crossbow',
    'x' => 2,
    'y' => 4,
    'class' => [32],
    'luck' => true,
    'skill' => 'triple',
    'add' => 'dmg',
    'exo' => 'swords'
];