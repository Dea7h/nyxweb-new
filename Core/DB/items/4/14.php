<?php

return [
    'name' => 'Aqua Gold Crossbow',
    'x' => 2,
    'y' => 3,
    'class' => [32],
    'luck' => true,
    'skill' => 'triple',
    'add' => 'dmg',
    'exo' => 'swords'
];