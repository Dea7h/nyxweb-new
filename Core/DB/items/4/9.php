<?php

return [
    'name' => 'Golden Crossbow',
    'x' => 2,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'skill' => 'triple',
    'add' => 'dmg',
    'exo' => 'swords',
    'ancient' => 'Gaia'
];