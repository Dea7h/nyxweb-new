<?php

return [
    'name' => 'Arrow Viper Bow',
    'x' => 2,
    'y' => 4,
    'class' => [33],
    'luck' => true,
    'skill' => 'triple',
    'add' => 'dmg',
    'exo' => 'swords'
];