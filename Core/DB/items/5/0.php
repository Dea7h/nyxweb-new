<?php

return [
    'name' => 'Skull Staff',
    'x' => 1,
    'y' => 3,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'wiz',
    'exo' => 'staffs',
    'ancient' => 'Apollo'
];