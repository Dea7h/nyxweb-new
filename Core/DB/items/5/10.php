<?php

return [
    'name' => 'Archangel Staff',
    'x' => 1,
    'y' => 4,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'wiz',
    'exo' => 'staffs'
];