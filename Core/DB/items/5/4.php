<?php

return [
    'name' => 'Gorgon Staff',
    'x' => 2,
    'y' => 4,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'wiz',
    'exo' => 'staffs'
];