<?php

return [
    'name' => 'Serpent Staff',
    'x' => 2,
    'y' => 3,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'wiz',
    'exo' => 'staffs'
];