<?php

return [
    'Warrior' => [
        'options' => 'Increase Strength +10<br />Increase attack rate +10 <br />Increase maximum AG +20<br />AG increase Rate + 5<br />Increase Defense + 20<br />Increase agility +10<br />Critical damage rate + 5%<br />Excellent damage rate + 5%<br />Increase Strength + 25',
        'parts' => ['Morning Star', 'Leather Helm', 'Leader Armor', 'Leather Pants', 'Leader Gloves', 'Leather Boots', 'Ring of Ice']
    ],
    'Hyperion' => [
        'options' => 'Increase Energy + 15<br />Increase Agility + 15<br />Increase skill attacking rate +20<br />Increase Mana + 30',
        'parts' => ['Bronze Armor', 'Bronze Pants', 'Bronze Boots']
    ],
    'Eplete' => [
        'options' => 'Increase atack skill + 15<br />Increase atack + 50<br />Increase Wizardry dmg + 5%<br />Maximum HP +50<br />Increase maximum AG +30<br />Increase Critical damage rate +10%<br />Excellent damage rate +10',
        'parts' => ['Scale Helm', 'Scale Armor', 'Scale Pants', 'Plate Shield', 'Pendant of Lightning']
    ],
    'Garuda' => [
        'options' => 'Increase max stamine + 30<br />Double Damage rate + 5%<br />Increase Energy + 15<br />Maximum HP +50<br />Increase skill attack rate + 25<br />Increase Wizardry Damage + 15%',
        'parts' => ['Brass Armor', 'Brass Pants', 'Brass Gloves', 'Brass Boots', 'Pendant of Fire']
    ],
    'Kantata' => [
        'options' => 'Increase Energy + 15<br />Increase Vitality + 30<br />Increase Wizardry Damage + 10%<br />Increase Strength +15<br />Increase skill damage + 25<br />Excellent damage rate + 10%<br />Increase excellent damage + 20',
        'parts' => ['Plate Armor', 'Plate Pants', 'Plate Gloves', 'Ring of Wind', 'Ring of Poison']
    ],
    'Apollo' => [
        'options' => 'Increase Energy + 10<br />Increase wizardry + 5%<br />Increase attack. skill + 10<br />Maximum mana + 30<br />Maximum life + 30<br />Increase max. AG + 20<br />Increase critical damage + 10<br />Increase excellent damage + 10<br />Increase Energy + 30',
        'parts' => ['Pad Helm', 'Pad Armor', 'Pad Pants', 'Pad Gloves', 'Skull Staff', 'Pendant of Ice', 'Ring of Magic']
    ],
    'Evis' => [
        'options' => 'Increase attacking skill + 15<br />Increase stamina + 20<br />Increase wizardry damage + 10<br />Double damage rate 5%<br />Increase damage success rate +50<br />Increase AG regen rate +5',
        'parts' => ['Bone Armor', 'Bone Pants', 'Bone Boots', 'Pendant of Wind']
    ],
    'Hera' => [
        'options' => 'Increase Strength + 15<br />Increase wizardry dmg + 10%<br />Increase defensive skill when equipped with shield + 5%<br />Increase Energy + 15<br />Increase attack success rate + 50<br />Critical damage rate + 10%<br />Excellent damage rate + 10%<br />Increase maximum life + 50<br />Increase maximum mana + 50',
        'parts' => ['Sphinx Mask', 'Sphinx Armor', 'Sphinx Pants', 'Sphinx Gloves', 'Sphinx Boots', 'Skull Shield']
    ],
    'Anubis' => [
        'options' => 'Double Dmg rate + 10%<br />Increase Max mana + 50 <br />Increase. Wizardry dmg + 10%<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20',
        'parts' => ['Legendary Helm', 'Legendary Armor', 'Legendary Gloves', 'Ring of Fire']
    ],
    'Ceto' => [
        'options' => 'Increase Agility + 10<br />Increase Max HP + 50<br />Increase Def skill + 20<br />Increase defensive skill while using shields + 5%<br />Increase Energy + 10<br />Increases Max HP + 50<br />Increase Strength + 20',
        'parts' => ['Vine Helm', 'Vine Pants', 'Vine Gloves', 'Vine Boots', 'Rapier', 'Ring of Earth']
    ],
    'Gaia' => [
        'options' => 'Increase attacking skill + 10<br />Increase max mana + 25<br />Power + 10<br />Double dmg rate + 5%<br />Increase Agility + 30<br />Excellent damage rate + 10%<br />Increase excellent damage + 10',
        'parts' => ['Silk Helm', 'Silk Armor', 'Silk Pants', 'Silk Gloves', 'Golden Crossbow']
    ],
    'Odin' => [
        'options' => 'Increase Energy + 15<br />Increase max life + 50<br />Increase attack success rate + 50<br />Increase Agility + 30<br />Increase maximum mana + 50<br />Ignore enemy\'s defensive skill + 5%<br />Increase maximum AG + 50',
        'parts' => ['Wind Helm', 'Wind Armor', 'Wind Pants', 'Wind Gloves', 'Wind Boots']
    ],
    'Argo' => [
        'options' => 'Increase Agility + 30<br />Power + 30<br />Increase attacking skill + 25<br />Double damage rate + 5%',
        'parts' => ['Spirit Armor', 'Spirit Pants', 'Spirit Gloves']
    ],
    'Gywen' => [
        'options' => 'Double Damage rate + 10%<br />Increase Agility + 30<br />Incr min attacking skill + 20<br />Incr max attacking skill + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase critical damage + 20<br />Increase excellent damage + 20',
        'parts' => ['Guardian Armor', 'Guardian Gloves', 'Guardian Boots', 'Pendant of Ability', 'Silver Bow']
    ],
    'Gaion' => [
        'options' => 'Ignore enemy\'s defensive skill + 5%<br />Double Damage rate + 15%<br />Inc. attacking skill + 15<br />Excellent damage rate + 15%<br />Increase excellent damage + 30<br />Increase wizardry + 20%<br />Increase Strength + 30',
        'parts' => ['Unicorn Armor', 'Unicorn Pants', 'Unicorn Boots', 'Pendant of Water']
    ],
    'Hyon' => [
        'options' => 'Increase defense + 25<br />Double Damage rate + 10%<br />Increase skill damage + 20<br />Critical damage rate + 15%<br />Excellent damage rate + 15%<br />Increase Critical Damage + 20<br />Increase Excellent Damage + 20',
        'parts' => ['Dragon Helm', 'Dragon Gloves', 'Dragon Boots', 'Lightning Sword']
    ]
];
