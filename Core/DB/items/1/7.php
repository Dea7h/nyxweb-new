<?php

return [
    'name' => 'Larkan Axe',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'falling',
    'add' => 'dmg',
    'exo' => 'swords'
];