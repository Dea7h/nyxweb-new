<?php

return [
    'name' => 'Fairy Axe',
    'x' => 1,
    'y' => 3,
    'class' => [0, 32, 48],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];