<?php

return [
    'name' => 'Tomahawk',
    'x' => 1,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'falling',
    'add' => 'dmg',
    'exo' => 'swords'
];