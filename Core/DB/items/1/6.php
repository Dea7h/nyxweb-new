<?php

return [
    'name' => 'Nikkea Axe',
    'x' => 2,
    'y' => 3,
    'class' => [16, 32, 48],
    'luck' => true,
    'skill' => 'falling',
    'add' => 'dmg',
    'exo' => 'swords'
];