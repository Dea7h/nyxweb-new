<?php

return [
    'name' => 'Crescent Axe',
    'x' => 2,
    'y' => 3,
    'class' => [0, 16, 48],
    'luck' => true,
    'skill' => 'falling',
    'add' => 'dmg',
    'exo' => 'swords'
];