<?php

return [
    'name' => 'Halberd',
    'x' => 2,
    'y' => 4,
    'class' => [16, 32, 48],
    'luck' => true,
    'skill' => 'lunge',
    'add' => 'dmg',
    'exo' => 'swords'
];