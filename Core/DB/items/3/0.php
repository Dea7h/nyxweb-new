<?php

return [
    'name' => 'Light Spear',
    'x' => 2,
    'y' => 4,
    'class' => [16, 32, 48],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];