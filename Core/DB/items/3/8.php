<?php

return [
    'name' => 'Great Scythe',
    'x' => 2,
    'y' => 4,
    'class' => [16, 32, 48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];