<?php

return [
    'name' => 'Thunder Pants',
    'x' => 2,
    'y' => 2,
    'class' => [48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];