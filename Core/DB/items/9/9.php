<?php

return [
    'name' => 'Plate Pants',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];