<?php

return [
    'name' => 'Bronze Pants',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hyperio'
];