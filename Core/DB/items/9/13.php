<?php

return [
    'name' => 'Spirit Pants',
    'x' => 2,
    'y' => 2,
    'class' => [32],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Argon'
];