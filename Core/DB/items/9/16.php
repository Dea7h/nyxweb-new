<?php

return [
    'name' => 'Black Dragon Pants',
    'x' => 2,
    'y' => 2,
    'class' => [17],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];