<?php

return [
    'name' => 'Dragon Gloves',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hyon'
];