<?php

return [
    'name' => 'Leather Gloves',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Warrior'
];