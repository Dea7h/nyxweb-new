<?php

return [
    'name' => 'Sphinx Gloves',
    'x' => 2,
    'y' => 2,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Hera'
];