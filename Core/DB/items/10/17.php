<?php

return [
    'name' => 'Dark Phoenix Gloves',
    'x' => 2,
    'y' => 2,
    'class' => [17],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];