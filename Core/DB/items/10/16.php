<?php

return [
    'name' => 'Black Dragon Gloves',
    'x' => 2,
    'y' => 2,
    'class' => [17],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];