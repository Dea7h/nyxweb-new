<?php

return [
    'name' => 'Light Plate Boots',
    'x' => 2,
    'y' => 2,
    'class' => [64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];