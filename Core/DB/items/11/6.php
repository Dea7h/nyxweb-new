<?php

return [
    'name' => 'Scale Boots',
    'x' => 2,
    'y' => 2,
    'class' => [16, 48, 64],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];