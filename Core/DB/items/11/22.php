<?php

return [
    'name' => 'Dark Soul Boots',
    'x' => 2,
    'y' => 2,
    'class' => [1],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];