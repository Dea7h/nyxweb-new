<?php

return [
    'name' => 'Bone Boots',
    'x' => 2,
    'y' => 2,
    'class' => [0, 48],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors',
    'ancient' => 'Evis'
];