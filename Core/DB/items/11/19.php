<?php

return [
    'name' => 'Divine Boots',
    'x' => 2,
    'y' => 2,
    'class' => [33],
    'luck' => true,
    'add' => 'deff',
    'exo' => 'armors'
];