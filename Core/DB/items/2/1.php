<?php

return [
    'name' => 'Morning Star',
    'x' => 1,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'upper',
    'add' => 'dmg',
    'exo' => 'swords',
    'ancient' => 'Warrior'
];