<?php

return [
    'name' => 'Crystal Mace',
    'x' => 2,
    'y' => 3,
    'class' => [0, 16, 32, 48],
    'luck' => true,
    'skill' => 'lunge',
    'add' => 'dmg',
    'exo' => 'swords'
];