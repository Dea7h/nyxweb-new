<?php

return [
    'name' => 'Crystal Sword',
    'x' => 2,
    'y' => 4,
    'class' => [0, 16, 32, 48],
    'luck' => true,
    'skill' => 'lunge',
    'add' => 'dmg',
    'exo' => 'swords'
];