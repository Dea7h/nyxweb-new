<?php

return [
    'name' => 'Chaos Sword',
    'x' => 2,
    'y' => 4,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'lunge',
    'add' => 'dmg',
    'exo' => 'swords'
];