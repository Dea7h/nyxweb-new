<?php

return [
    'name' => 'Mace of the King',
    'x' => 1,
    'y' => 3,
    'class' => [17, 48],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];