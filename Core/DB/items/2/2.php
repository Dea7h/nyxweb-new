<?php

return [
    'name' => 'Iron Hammer',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'upper',
    'add' => 'dmg',
    'exo' => 'swords'
];