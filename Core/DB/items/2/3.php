<?php

return [
    'name' => 'Great Hammer',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'upper',
    'add' => 'dmg',
    'exo' => 'swords'
];