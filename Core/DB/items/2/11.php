<?php

return [
    'name' => 'Lord Scepter',
    'x' => 1,
    'y' => 3,
    'class' => [64],
    'luck' => true,
    'skill' => 'force',
    'add' => 'dmg',
    'exo' => 'swords'
];