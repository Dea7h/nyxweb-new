<?php

return [
    'name' => 'Mace',
    'x' => 1,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];