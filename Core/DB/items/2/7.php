<?php

return [
    'name' => 'Elemental Mace',
    'x' => 1,
    'y' => 3,
    'class' => [33],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];