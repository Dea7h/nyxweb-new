<?php

return [
    'name' => 'Great Lord Scepter',
    'x' => 1,
    'y' => 4,
    'class' => [64],
    'luck' => true,
    'skill' => 'force',
    'add' => 'dmg',
    'exo' => 'swords'
];