<?php

return [
    'name' => 'Kriss',
    'x' => 1,
    'y' => 2,
    'class' => [0, 16, 32, 48, 64],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];