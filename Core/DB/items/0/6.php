<?php

return [
    'name' => 'Gladius',
    'x' => 1,
    'y' => 3,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];