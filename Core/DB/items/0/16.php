<?php

return [
    'name' => 'Destruction Sword',
    'x' => 1,
    'y' => 4,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];