<?php

return [
    'name' => 'Salamander',
    'x' => 2,
    'y' => 3,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];