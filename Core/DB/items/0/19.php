<?php

return [
    'name' => 'Archangel Sword',
    'x' => 1,
    'y' => 4,
    'class' => [16, 48, 64],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];