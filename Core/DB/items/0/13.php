<?php

return [
    'name' => 'Double Blade',
    'x' => 1,
    'y' => 3,
    'class' => [16, 32, 48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];