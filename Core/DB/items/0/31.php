<?php

return [
    'name' => 'Rune Blade',
    'x' => 2,
    'y' => 4,
    'class' => [48],
    'luck' => true,
    'skill' => 'power',
    'add' => 'dmgwiz',
    'exo' => 'swords'
];