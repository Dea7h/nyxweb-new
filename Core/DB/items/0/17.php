<?php

return [
    'name' => 'Spirit Sword',
    'x' => 2,
    'y' => 4,
    'class' => [17],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];