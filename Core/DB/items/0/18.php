<?php

return [
    'name' => 'Thunder Sword',
    'x' => 2,
    'y' => 3,
    'class' => [48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];