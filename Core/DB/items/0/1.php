<?php

return [
    'name' => 'Short Sword',
    'x' => 1,
    'y' => 3,
    'class' => [0, 16, 32, 48, 64],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords'
];