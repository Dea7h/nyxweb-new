<?php

return [
    'name' => 'Rapier',
    'x' => 1,
    'y' => 3,
    'class' => [16, 32, 48, 64],
    'luck' => true,
    'add' => 'dmg',
    'exo' => 'swords',
    'ancient' => 'Ceto'
];