<?php

return [
    'name' => 'Blade',
    'x' => 1,
    'y' => 3,
    'class' => [0, 16, 32, 48, 64],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];