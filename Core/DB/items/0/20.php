<?php

return [
    'name' => 'Knight Blade',
    'x' => 1,
    'y' => 4,
    'class' => [17],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];