<?php

return [
    'name' => 'Legendary Sword',
    'x' => 2,
    'y' => 4,
    'class' => [16, 48],
    'luck' => true,
    'skill' => 'cyclon',
    'add' => 'dmg',
    'exo' => 'swords'
];