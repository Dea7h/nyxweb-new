<?php

return [
    'Swords' => [
        [
            'group' => 0,
            'id' => 11,
            'zen' => 0,
            'credits' => 0,
            'required' => false
        ],
        [
            'group' => 0,
            'id' => 14,
            'zen' => 99999999,
            'credits' => 323,
            'required' => [
                [
                    'group' => 14,
                    'id' => 13,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 14,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 22,
                    'count' => 5
                ]
            ]
        ],
        [
            'group' => 0,
            'id' => 19,
            'zen' => 4534534,
            'credits' => 2211,
            'required' => [
                [
                    'group' => 14,
                    'id' => 13,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 14,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 22,
                    'count' => 5
                ]
            ]
        ]
    ],
    'Staffs' => [
        [
            'zen' => 0,
            'credits' => 211,
            'group' => 5,
            'id' => 8,
            'required' => false
        ],
        [
            'group' => 5,
            'id' => 10,
            'exo_type' => true,
            'exo' => 1,
            'zen' => 0,
            'credits' => 999,
            'required' => [
                [
                    'group' => 14,
                    'id' => 13,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 14,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 22,
                    'count' => 5
                ]
            ]
        ],
        [
            'group' => 5,
            'id' => 11,
            'luck' => true,
            'options' => 4,
            'exo' => 32,
            'zen' => 2000000000,
            'credits' => 550,
            'required' => [
                [
                    'group' => 14,
                    'id' => 13,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 14,
                    'count' => 10
                ],
                [
                    'group' => 14,
                    'id' => 22,
                    'count' => 5
                ]
            ]
        ]
    ]
];
