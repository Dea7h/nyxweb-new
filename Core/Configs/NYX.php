<?php

return [
    //Server Info
    'server-name' => 'NyxMu',
    'server-version' => 'Season 1 ( 99.62 )',
    'server-exp' => '10x',
    'server-drop' => '25%',
    //Settings
    'timezone' => 'Europe/Stockholm',
    'regLimit' => 10, //Registrations limit from the same IP
    'requestTime' => 300, //Minimum time between every request [in milliseconds, 1000 = 1s]
    'cryptoKEY' => 'L/jMzlQVgOAAkmb3d3ubJpT7f1TPXTzSRn27mPMCxh0=', //generated with base64_encode(openssl_random_pseudo_bytes(32));
    'cryptoSEP' => '5=1xjY',
    
    //Characters
    'character' => [
        'quest' => 'test'
    ]
];
