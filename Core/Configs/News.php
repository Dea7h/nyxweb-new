<?php

/*
 * Settings
 * false = unlimited
 * 
 */

return [
    'commentsPerUser' => 20, //maximum number of comments allowed per article /per user/
    'allComments' => 50, //maximum number of comments allowed per article
    'emojis' => [
        ';(' => '&#x1F622',
        ':(' => '&#x1F614',
        ':|' => '&#x1F611',
        ':)' => '&#x1F60A',
        ';)' => '&#x1F609',
        ':P' => '&#x1F61B',
        ':8' => '&#x1F60E',
        ':D' => '&#x1F603',
        ':LOL:' => '&#x1F602',
        'xD' => '&#x1F606',
        ':O' => '&#x1F632',
        ':love:' => '&#x1F60D',
        ':*' => '&#x1F618',
        '<3' => '&#x1F493',
        '<|3' => '&#x1F494',
        ':@' => '&#x1F621',
        ':eggplant:' => '&#x1F346',
        ':hi:' => '&#x1F44B',
        ':yes:' => '&#x1F44D',
        ':no:' => '&#x1F44E'
    ]
];
