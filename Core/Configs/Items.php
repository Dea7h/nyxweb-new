<?php

/*
 * length = items hex length ( 20 = oldschool season 1 | 32/64 = newer versions /not supported yet/ )
 * 
 * x = warehouse/storage squares horisontally
 * y = warehouse/storage squares vertically
 * side = left/right/false
 * real_size = true/false | true if the items shall be full size (like in-game warehouse) | false = 1 square per item
 * image = true/false | true if there should be an image in the item info, false otherwise
 * 
 */

return [
    'hexLength' => 20,
    'possibleHexLengths' => [20, 32, 64],
    //'length' => 20,
    'warehouse' => [
        'x' => 8,
        'y' => 15,
        'side' => 'left',
        'table' => 'warehouse',
        'user_column' => 'AccountID',
        'items_column' => 'Items',
        'real_size' => true,
        'image' => false
    ],
    'storage' => [
        'x' => 10,
        'y' => 15,
        'side' => 'right',
        'table' => 'NYX_RESOURCES',
        'user_column' => 'account',
        'items_column' => 'items',
        'real_size' => false,
        'image' => true
    ],
];
