<?php

use Core\Tools\Lang;
$lang = new Lang();

return [
    'default' => [
        'NyxMu' => '/news'
    ],
    'news' => [
        $lang->Phrase('menu', 'sub-news') => '/news/news',
        $lang->Phrase('menu', 'sub-updates') => '/news/update',
        $lang->Phrase('menu', 'sub-info') => '/news/info'
    ],
    'rankings' => [
        $lang->Phrase('menu', 'sub-rankings') => '/rankings',
        $lang->Phrase('menu', 'sub-resets') => '/rankings/resets',
        $lang->Phrase('menu', 'sub-levels') => '/rankings/levels'
    ]
];
