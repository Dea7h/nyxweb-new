<?php

return [
    //Bonus points per completed quest
    'bonusPoints' => 10,
    'list' => [
        'Find 10 Jewel of Guardian' => [
            'required' => [
                'count' => 10,
                'type' => 1,
                'id' => 1
            ],
            'reward' => [
                'count' => 1,
                'type' => 1,
                'id' => 1
            ]
        ]
    ]
];