<?php

return [
    'chaos' => [
        'name' => 'Jewel of Chaos',
        'image' => '12/15.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 12,
                'id' => 15,
            ]
        ]
    ],
    'bless' => [
        'name' => 'Jewel of Bless',
        'image' => '14/13.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 13,
                'durability' => 1,
                'count' => 1
            ],
            [
                'group' => 12,
                'id' => 30,
                'level' => 0,
                'count' => 10
            ],
            [
                'group' => 12,
                'id' => 30,
                'level' => 1,
                'count' => 20
            ],
            [
                'group' => 12,
                'id' => 30,
                'level' => 2,
                'count' => 30
            ]
        ]
    ],
    'soul' => [
        'name' => 'Jewel of Soul',
        'image' => '14/14.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 14,
                'durability' => 1,
                'count' => 1
            ],
            [
                'group' => 12,
                'id' => 31,
                'level' => 0,
                'count' => 10
            ],
            [
                'group' => 12,
                'id' => 31,
                'level' => 1,
                'count' => 20
            ],
            [
                'group' => 12,
                'id' => 31,
                'level' => 2,
                'count' => 30
            ]
        ]
    ],
    'life' => [
        'name' => 'Jewel of Life',
        'image' => '14/16.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 16,
                'durability' => 1,
            ]
        ]
    ],
    'creation' => [
        'name' => 'Jewel of Creation',
        'image' => '14/22.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 22,
                'durability' => 1,
            ]
        ]
    ],
    'stone' => [
        'name' => 'Stone',
        'image' => '14/21-1.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 21,
                'level' => 1
            ]
        ]
    ],
    'rena' => [
        'name' => 'Rena',
        'image' => '14/21-0.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 21,
                'level' => 0
            ]
        ]
    ],
    'box1' => [
        'name' => 'Box of Kundun +1',
        'image' => '14/11-8.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 8
            ]
        ]
    ],
    'box2' => [
        'name' => 'Box of Kundun +2',
        'image' => '14/11-9.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 9
            ]
        ]
    ],
    'box3' => [
        'name' => 'Box of Kundun +3',
        'image' => '14/11-10.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 10
            ]
        ]
    ],
    'box4' => [
        'name' => 'Box of Kundun +4',
        'image' => '14/11-11.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 11
            ]
        ]
    ],
    'box5' => [
        'name' => 'Box of Kundun +5',
        'image' => '14/11-12.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 12
            ]
        ]
    ],
    'boh' => [
        'name' => 'Box of Heaven',
        'image' => '14/11-7.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 7
            ]
        ]
    ],
    'credits' => [
        'name' => 'Credits',
        'slots' => 5
    ],
    'hol' => [
        'name' => 'Heart of Love',
        'image' => '14/11-3.gif',
        'slots' => 1,
        'props' => [
            [
                'group' => 14,
                'id' => 11,
                'level' => 3
            ]
        ]
    ],
    'zen' => [
        'name' => 'Zen',
        'slots' => 5
    ],
];
