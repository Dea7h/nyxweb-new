<?php
session_start();
error_reporting(E_ALL);
ini_set('display_errors', 1);

define('access', true);

require 'vendor/autoload.php';

use Core\Tools\RequestHandler;
use Core\Tools\Post;
use Core\Tools\Notice;
use Core\Tools\Config;
use Core\Tools\SQL;
use Core\Tools\Lang;
use Core\Main\User;

$user = new User;
$lang = new Lang;
$sql = new SQL;
$config = new Config;
$request = new RequestHandler();

if (Post::Exists('page')) {
    $request->PageCheck();

    $GLOBALS['request'] = explode('/', preg_replace('/\//', '', Post::Get('page'), 1));
    if (empty($GLOBALS['request'][0])) {
        include 'Core/Views/news.php';
    } else if (is_file('Core/Views/' . $GLOBALS['request'][0] . '.php')) {
        include 'Core/Views/' . $GLOBALS['request'][0] . '.php';
    } else {
        print "<div style='border: 1px solid #1b1b1b; margin: 5px;'><img src='/assets/images/others/404.jpg' style='width: 100%;' /></div>";
    }
} else if (Post::Exists('request')) {
    $request->RequestStart();

    if (is_file('Core/Requests/' . Post::Get('request') . '.php')) {
        include 'Core/Requests/' . Post::Get('request') . '.php';
    } else {
        print Notice::Request($lang->Phrase('request', 'denied'));
    }

    $request->RequestEnd();
} elseif (Post::Exists('quickload')) {
    if (is_file('Core/Views/quickLoads/' . Post::Get('quickload') . '.php')) {
        include 'Core/Views/quickLoads/' . Post::Get('quickload') . '.php';
    } else {
        print 'quickLoad not found';
    }
} else {
    include 'Core/index.php';
}